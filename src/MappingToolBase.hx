

	import com.cloudmade.mappingtool.commands.ICommand;
	import com.cloudmade.mappingtool.editing.ICommandList;
	import com.cloudmade.mappingtool.editing.errors.CommitDataValidator;
	import com.cloudmade.mappingtool.editing.errors.ErrorAnnouncer;
	import com.cloudmade.mappingtool.editing.errors.ErrorProcessor;
	import com.cloudmade.mappingtool.editing.errors.InternalErrorProcessor;
	import com.cloudmade.mappingtool.editing.errors.InternalErrorReporter;
	import com.cloudmade.mappingtool.editing.errors.NullInternalErrorProcessor;
	import com.cloudmade.mappingtool.editing.errors.externalization.CommandStackExternalizer;
	import com.cloudmade.mappingtool.editing.errors.externalization.MapDataOutput;
	import com.cloudmade.mappingtool.editing.errors.externalization.MapExtractor;
	import com.cloudmade.mappingtool.events.OSMProxySaveEvent;
	import com.cloudmade.mappingtool.managers.WindowManager;
	import com.cloudmade.mappingtool.map.geo.LatLong;
	import com.cloudmade.mappingtool.model.ApplicationData;
	import com.cloudmade.mappingtool.model.OSMProxy;
	import com.cloudmade.mappingtool.model.settings.ApplicationSettingsFactory;
	import com.cloudmade.mappingtool.services.MapEditService;
	import com.cloudmade.mappingtool.services.MapService;
	import com.cloudmade.mappingtool.ui.editingHistory.EditingHistoryPanel;
	import com.cloudmade.mappingtool.utils.Firefox3MouseWheelHandler;
	import com.pixelbreaker.ui.osx.MacMouseWheel;
	
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.external.ExternalInterface;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import flash.system.fscommand;
	import flash.utils.ByteArray;
	import flash.utils.IExternalizable;
	
	import mx.core.Application;
	import mx.events.FlexEvent;

	class MappingToolBase extends Application {
		public function new() { }
		
		/*[Embed(source="/config/dump.txt", mimeType="application/octet-stream")]*/
		CONFIG::restore
		static var COMMAND_STACK_DUMP:Class<Dynamic>;
		/*[Embed(source="/config/commit.xml", mimeType="application/octet-stream")]*/
		CONFIG::restore
		static var COMMIT_DATA:Class<Dynamic>;
		
		/*[Bindable]*/
		public var historyPanel:EditingHistoryPanel;
		
		/*[Bindable]*/
		var appData:ApplicationData;
		/*[Bindable]*/
		var exitUrl:String;
		/*[Bindable]*/
		var editService:MapEditService;
		
		var settingsInitializeCommand:ICommand;
		var errorProcessor:ErrorProcessor;
		
		static function main() {
			super();
			try {
				fscommand("showmenu", "false");
			} catch (error:Error) {
			}
			CONFIG::sandbox {
				viewSourceURL = "srcview/index.html";
			}
			addEventListener(FlexEvent.INITIALIZE, initializeHandler);
			addEventListener(FlexEvent.PREINITIALIZE, preinitializeHandler);
			addEventListener(Event.ADDED_TO_STAGE, addedToStageHandler);			
		}
		
		function exit():Void {
			navigateToURL(new URLRequest(exitUrl), "_self");
		}
		
		function addedToStageHandler(event:Event):Void {
			stage.addEventListener(KeyboardEvent.KEY_UP, editing_keyUpHandler);
		}

		function editing_keyUpHandler(event:KeyboardEvent):Void {
			// Do not allow editing changes during saving:
			if (WindowManager.getInstance().numWindows > 0) return;
			var char:String = String.fromCharCode(event.charCode);
			if (event.ctrlKey && (char == "z")) {
				editService.undo();
				historyPanel.visible = true;
			} else if (event.ctrlKey && (char == "y")) {
				editService.redo();
				historyPanel.visible = true;
			}
		}
		
		function preinitializeHandler(event:FlexEvent):Void {
			appData = ApplicationData.getInstance();
			appData.map.center = new LatLong(51.515, -0.1);
			appData.map.zoomLevel = 17;
			settingsInitializeCommand = new ApplicationSettingsFactory().createInitializeSettingsCommand(appData.settings, appData.map, parameters["userId"]);
			settingsInitializeCommand.execute();
			if (!isNaN(parameters["lat"]) && !isNaN(parameters["lng"])) {
				appData.map.center = new LatLong(parameters["lat"], parameters["lng"]);
			}
			if (!isNaN(parameters["zoom"])) {
				appData.map.zoomLevel = parameters["zoom"];
			}
			if (ExternalInterface.available) {
				if (parameters["saveCallback"] !== undefined) {
					OSMProxy.getInstance().addEventListener(OSMProxySaveEvent.SAVE_COMPLETE, osmProxy_saveCompleteHandler);
				}
			
				if (parameters["hasUnsavedChangesCallback"] !== undefined) {
					ExternalInterface.addCallback(parameters["hasUnsavedChangesCallback"], function():Bool {
						return editService.commandList.hasUndoCommands;
					});
				}
			}
			exitUrl = parameters["exitUrl"];
			
			CONFIG::restore {
				var dump:ByteArray = new COMMAND_STACK_DUMP();
				var externalizer:CommandStackExternalizer = new CommandStackExternalizer();
				var commandList:ICommandList = externalizer.deserialize(dump.toString());
				var firstCommand:IExternalizable = cast( commandList.source.getItemAt(0), IExternalizable);
				if (firstCommand) {
					appData.map = new MapExtractor(new MapDataOutput()).extractMap(firstCommand);
				}
				MapEditService.getInstance().setCommandList(commandList);
				
				var commitData:ByteArray = new COMMIT_DATA();
				var commitDataXml:XML = new XML(commitData.toString());
				new CommitDataValidator().validate(commitDataXml);
			}
		}
		
		function initializeHandler(event:FlexEvent):Void {
			editService = MapEditService.getInstance();
			Firefox3MouseWheelHandler.initialize(systemManager.stage);
			MacMouseWheel.setup(systemManager.stage);
			// Don't initialize MapService in restore mode because we don't have the original map, only modified.
			if (!CONFIG::restore) {
				MapService.getInstance().initialize();
			}
			
			var canReportErrors:Boolean =
				ExternalInterface.available && 
				(parameters["saveErrorCallback"] !== undefined);
			errorProcessor = new ErrorProcessor(
				OSMProxy.getInstance(),
				editService,
				editService.commandList,
				new ErrorAnnouncer(resourceManager),
				canReportErrors ?
					new InternalErrorProcessor(
						new InternalErrorReporter(parameters["saveErrorCallback"]),
						new CommandStackExternalizer()
					) :
					new NullInternalErrorProcessor()
			);
			errorProcessor.initialize();
		}
		
		function osmProxy_saveCompleteHandler(event:OSMProxySaveEvent):Void {
			ExternalInterface.call(parameters["saveCallback"], event.changesetId);
		}
	}
