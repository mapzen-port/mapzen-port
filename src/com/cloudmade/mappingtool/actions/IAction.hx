package com.cloudmade.mappingtool.actions;

	import flash.events.Event;
	
	interface IAction
	{
		function run(event:Event):Void;
	}
