package com.cloudmade.mappingtool.actions.triggers;

	import flash.events.IEventDispatcher;
	
	class PriorityTrigger implements ITrigger {
		
		var priority:Int;
		var eventType:String;
		var dispatcher:IEventDispatcher;
		
		public function new(dispatcher:IEventDispatcher, eventType:String, ?priority:Int = 0) {
			this.dispatcher = dispatcher;
			this.eventType = eventType;
			this.priority = priority;
		}
		
		public function addListener(listener:Dynamic):Void {
			dispatcher.addEventListener(eventType, listener, false, priority, true);
		}
		
		public function removeListener(listener:Dynamic):Void {
			dispatcher.removeEventListener(eventType, listener);
		}
	}
