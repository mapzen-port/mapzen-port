package com.cloudmade.mappingtool.actions.triggers;

	import flash.events.IEventDispatcher;
	
	class Trigger implements ITrigger {
		
		var eventType:String;
		var dispatcher:IEventDispatcher;
		
		public function new(dispatcher:IEventDispatcher, eventType:String) {
			this.dispatcher = dispatcher;
			this.eventType = eventType;
		}
		
		public function addListener(listener:Dynamic):Void {
			dispatcher.addEventListener(eventType, listener, false, 0, true);
		}
		
		public function removeListener(listener:Dynamic):Void {
			dispatcher.removeEventListener(eventType, listener);
		}
	}
