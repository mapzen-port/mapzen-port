package com.cloudmade.mappingtool.actions.triggers;

	import flash.events.Event;
	import flash.events.IEventDispatcher;
	
	class ConditionalTrigger implements ITrigger {
		
		var eventType:String;
		var dispatcher:IEventDispatcher;
		var listeners:Array<Dynamic> ;
		
		public function new(dispatcher:IEventDispatcher, eventType:String) {
			
			listeners = new Array();
			this.dispatcher = dispatcher;
			this.eventType = eventType;
			dispatcher.addEventListener(eventType, triggerListener, false, 0, true);
		}
		
		public function addListener(listener:Dynamic):Void {
			var index:Int = listeners.indexOf(listener);
			if (index == -1) {
				listeners.push(listener);
			}
		}
		
		public function removeListener(listener:Dynamic):Void {
			var index:Int = listeners.indexOf(listener);
			if (index != -1) {
				listeners.splice(index, 1);
			}
		}
		
		function triggerFilter(event:Event):Bool {
			return true;
		}
		
		function triggerListener(event:Event):Void {
			var triggering:Bool = triggerFilter(event);
			if (triggering) {
				for (listener in listeners) {
					listener(event);
				}
			}
		}
	}
