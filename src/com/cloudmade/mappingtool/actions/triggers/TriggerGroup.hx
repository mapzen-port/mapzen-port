package com.cloudmade.mappingtool.actions.triggers;

	class TriggerGroup implements ITrigger {
		
		var triggers:Array<Dynamic>;
		
		public function new(triggers:Array<Dynamic>) {
			this.triggers = triggers;
		}

		public function addListener(listener:Dynamic):Void {
			for (trigger in triggers) {
				trigger.addListener(listener);
			}
		}
		
		public function removeListener(listener:Dynamic):Void {
			for (trigger in triggers) {
				trigger.removeListener(listener);
			}
		}
	}
