package com.cloudmade.mappingtool.actions.triggers;

	interface ITrigger
	{
		function addListener(listener:Dynamic):Void;
		
		function removeListener(listener:Dynamic):Void;
	}
