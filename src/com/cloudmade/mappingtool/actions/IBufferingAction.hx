package com.cloudmade.mappingtool.actions;

	import flash.events.Event;
	
	interface IBufferingAction implements IAction{
		function begin(event:Event):Void;
		
		function end(event:Event):Void;
	}
