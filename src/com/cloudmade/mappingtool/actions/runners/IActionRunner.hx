package com.cloudmade.mappingtool.actions.runners;

	import com.cloudmade.mappingtool.actions.ITriggerHandler;
	
	import flash.events.IEventDispatcher;
	
	/*[Event(name="run", type="com.cloudmade.mappingtool.ui.actions.ActionRunnerEvent")]*/
	
	interface IActionRunner implements ITriggerHandler, implements IEventDispatcher{
		
	}
