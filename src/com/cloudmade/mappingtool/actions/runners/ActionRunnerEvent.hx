package com.cloudmade.mappingtool.actions.runners;

	import flash.events.Event;

	class ActionRunnerEvent extends Event {
		
		public static var BEGIN:String = "begin";
		public static var RUN:String = "run";
		public static var END:String = "end";
		
		public function new(type:String) {
			super(type);
		}
		
		public override function clone():Event {
			return new ActionRunnerEvent(type);
		}
	}
