package com.cloudmade.mappingtool.actions.runners;

	import com.cloudmade.mappingtool.actions.IBufferingAction;
	import com.cloudmade.mappingtool.actions.triggers.ITrigger;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	/*[Event(name="begin", type="com.cloudmade.mappingtool.ui.actions.ActionRunnerEvent")]*/
	/*[Event(name="run", type="com.cloudmade.mappingtool.ui.actions.ActionRunnerEvent")]*/
	/*[Event(name="end", type="com.cloudmade.mappingtool.ui.actions.ActionRunnerEvent")]*/
	
	class BufferingActionRunner extends EventDispatcher, implements IBufferingActionRunner {
		
		var action:IBufferingAction;
		var beginTrigger:ITrigger;
		var runTrigger:ITrigger;
		var endTrigger:ITrigger;
		
		public function new(action:IBufferingAction, beginTrigger:ITrigger, runTrigger:ITrigger, endTrigger:ITrigger) {
			super();
			this.action = action;
			this.beginTrigger = beginTrigger;
			this.runTrigger = runTrigger;
			this.endTrigger = endTrigger;
		}
		
		public function activate():Void {
			beginTrigger.addListener(beginListener);
		}
		
		public function deactivate():Void {
			beginTrigger.removeListener(beginListener);
			runTrigger.removeListener(runListener);
			endTrigger.removeListener(endListener);
		}
		
		function beginListener(event:Event):Void {
			beginTrigger.removeListener(beginListener);
			runTrigger.addListener(runListener);
			endTrigger.addListener(endListener);
			action.begin(event);
			dispatchEvent(new ActionRunnerEvent(ActionRunnerEvent.BEGIN));
		}
		
		function runListener(event:Event):Void {
			action.run(event);
			dispatchEvent(new ActionRunnerEvent(ActionRunnerEvent.RUN));
		}
		
		function endListener(event:Event):Void {
			beginTrigger.addListener(beginListener);
			runTrigger.removeListener(runListener);
			endTrigger.removeListener(endListener);
			action.end(event);
			dispatchEvent(new ActionRunnerEvent(ActionRunnerEvent.END));
		}
	}
