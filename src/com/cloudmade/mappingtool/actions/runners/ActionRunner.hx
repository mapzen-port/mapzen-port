package com.cloudmade.mappingtool.actions.runners;

	import com.cloudmade.mappingtool.actions.IAction;
	import com.cloudmade.mappingtool.actions.triggers.ITrigger;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	/*[Event(name="run", type="com.cloudmade.mappingtool.ui.actions.ActionRunnerEvent")]*/
	
	class ActionRunner extends EventDispatcher, implements IActionRunner {
		
		var action:IAction;
		var runTrigger:ITrigger;
		
		public function new(action:IAction, runTrigger:ITrigger) {
			super();
			this.action = action;
			this.runTrigger = runTrigger;
		}
		
		public function activate():Void {
			runTrigger.addListener(runListener);
		}
		
		public function deactivate():Void {
			runTrigger.removeListener(runListener);
		}
		
		function runListener(event:Event):Void {
			action.run(event);
			dispatchEvent(new ActionRunnerEvent(ActionRunnerEvent.RUN));
		}
	}
