package com.cloudmade.mappingtool.actions;

	class TriggerHandlerGroup implements ITriggerHandler {
		
		var handlers:Array<Dynamic>;
		
		public function new(handlers:Array<Dynamic>) {
			this.handlers = handlers;
		}
		
		public function activate():Void {
			for (handler in handlers) {
				handler.activate();
			}
		}
		
		public function deactivate():Void {
			for (handler in handlers) {
				handler.deactivate();
			}
		}
	}
