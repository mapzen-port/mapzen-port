package com.cloudmade.mappingtool.actions;

	import com.cloudmade.mappingtool.actions.triggers.ITrigger;
	
	import flash.events.Event;
	
	class TriggerHandlerActivator implements ITriggerHandler {
		
		var handler:ITriggerHandler;
		var activateTrigger:ITrigger;
		var deactivateTrigger:ITrigger;
		
		public function new(handler:ITriggerHandler, activateTrigger:ITrigger, deactivateTrigger:ITrigger) {
			this.handler = handler;
			this.activateTrigger = activateTrigger;
			this.deactivateTrigger = deactivateTrigger;
		}
		
		public function activate():Void {
			activateTrigger.addListener(activateListener);
			deactivateTrigger.addListener(deactivateListener);
		}
		
		public function deactivate():Void {
			activateTrigger.removeListener(activateListener);
			deactivateTrigger.removeListener(deactivateListener);
			handler.deactivate();
		}
		
		function activateListener(event:Event):Void {
			handler.activate();
		}
		
		function deactivateListener(event:Event):Void {
			handler.deactivate();
		}
	}
