package com.cloudmade.mappingtool.actions;

	interface ITriggerHandler
	{
		function activate():Void;
		
		function deactivate():Void;
	}
