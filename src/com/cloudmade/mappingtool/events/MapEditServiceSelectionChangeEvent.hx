package com.cloudmade.mappingtool.events;

	import com.cloudmade.mappingtool.map.model.Element;
	
	import flash.events.Event;

	class MapEditServiceSelectionChangeEvent extends Event {
		
		public static var SELECTION_CHANGE:String = "selectionChange";
		
		public var oldSelection:Element;
		
		public function new(type:String, oldSelection:Element) {
			super(type);
			this.oldSelection = oldSelection;
		}
		
		public override function clone():Event {
			return new MapEditServiceSelectionChangeEvent(type, oldSelection);
		}
	}
