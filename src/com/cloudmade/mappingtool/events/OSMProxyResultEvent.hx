package com.cloudmade.mappingtool.events;

	import flash.events.Event;

	class OSMProxyResultEvent extends Event {
		
		public static var SAVE:String = "save";
		public static var RESULT:String = "result";
		
		public var nodes:Array<Dynamic>;
		public var ways:Array<Dynamic>;
		public var relations:Array<Dynamic>;
		
		public function new(type:String, nodes:Array<Dynamic>, ways:Array<Dynamic>, relations:Array<Dynamic>) {
			super(type);
			this.nodes = nodes;
			this.ways = ways;
			this.relations = relations;
		}
		
		public override function clone():Event {
			return new OSMProxyResultEvent(type, nodes, ways, relations);
		}
	}
