package com.cloudmade.mappingtool.events;

	import flash.events.Event;
	
	class OSMProxyCommitErrorEvent extends Event {
		
		public static var COMMIT_ERROR:String = "commitError";
		
		public var commitData:String;
		
		public function new(type:String, commitData:String) {
			super(type);
			this.commitData = commitData;
		}
		
		public override function clone():Event {
			return new OSMProxyCommitErrorEvent(type, commitData);
		}
	}
