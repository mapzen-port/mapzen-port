package com.cloudmade.mappingtool.events;

	import com.cloudmade.mappingtool.map.model.Tag;
	
	import flash.events.Event;
	
	class MetadataEvent extends Event {
		
		public static var TAG_ADD:String = "tagAdd";
		public static var TAG_REMOVE:String = "tagRemove";
		public static var TAG_KEY_CHANGE:String = "tagKeyChange";
		public static var TAG_VALUE_CHANGE:String = "tagValueChange";
		
		public var tag:Tag;
		public var data:String;
		
		public function new(type:String, tag:Tag, ?data:String = null) {
			super(type);
			this.tag = tag;
			this.data = data;
		}
		
		public override function clone():Event {
			return new MetadataEvent(type, tag, data);
		}
	}
