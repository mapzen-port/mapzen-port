package com.cloudmade.mappingtool.events;

	import flash.events.Event;

	class MapEditServiceEvent extends Event {
		
		public static var ADDING_WAY_CHANGE:String = "addingWayChange";
		
		public function new(type:String) {
			super(type);
		}
		
		public override function clone():Event {
			return new MapEditServiceEvent(type);
		}
	}
