package com.cloudmade.mappingtool.events;

	import flash.events.Event;
	
	class ApplicationDataEvent extends Event {
		
		public static var CONFIGURATION_CHANGE:String = "configurationChange";
		
		public function new(type:String) {
			super(type);
		}
		
		public override function clone():Event {
			return new ApplicationDataEvent(type);
		}
	}
