package com.cloudmade.mappingtool.events;

	import flash.events.Event;

	class OSMProxyLoadErrorEvent extends Event {
		
		public static var LOAD_ERROR:String = "loadError";
		
		public function new(type:String) {
			super(type);
		}
		
		public override function clone():Event {
			return new OSMProxyLoadErrorEvent(type);
		}
	}
