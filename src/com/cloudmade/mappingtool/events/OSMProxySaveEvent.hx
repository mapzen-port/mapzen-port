package com.cloudmade.mappingtool.events;

	import flash.events.Event;

	class OSMProxySaveEvent extends Event {
		
		public static var SAVE_FAILED:String = "saveFailed";
		public static var SAVE_COMPLETE:String = "saveComplete";
		
		public var changesetId:String;
		
		public function new(type:String, ?changesetId:String = null) {
			super(type);
			this.changesetId = changesetId;
		}
		
		public override function clone():Event {
			return new OSMProxySaveEvent(type, changesetId);
		}
	}
