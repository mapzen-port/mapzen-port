package com.cloudmade.mappingtool.model;

	import com.cloudmade.mappingtool.events.ApplicationDataEvent;
	import com.cloudmade.mappingtool.map.events.ZoomEvent;
	import com.cloudmade.mappingtool.map.model.Map;
	import com.cloudmade.mappingtool.model.settings.UserSettings;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	/*[Event(name="configurationChange", type="com.cloudmade.mappingtool.events.ApplicationDataEvent")]*/
	
	public final class ApplicationData extends EventDispatcher {
		
		public var areaTagGroups(getAreaTagGroups, null) : Array<Dynamic> ;
		public var editingEnabled(getEditingEnabled, setEditingEnabled) : Bool;
		public var map(getMap, setMap) : Map;
		public var nodeTagGroups(getNodeTagGroups, null) : Array<Dynamic> ;
		public var selectedTagGroups(getSelectedTagGroups, setSelectedTagGroups) : Array<Dynamic>;
		public var settings(getSettings, null) : UserSettings ;
		public var wayTagGroups(getWayTagGroups, null) : Array<Dynamic> ;
		public static var MIN_EDITING_ZOOM_LEVEL:Int = 15;
		
		static var instance:ApplicationData = null;
		
		public static function getInstance():ApplicationData {
			if (instance == null) {
				instance = new ApplicationData(new SingletonEnforcer());
			}
			return instance;
		}
		
		public function new(enforcer:SingletonEnforcer) {
			super();
			map.addEventListener(ZoomEvent.ZOOM, map_zoomHandler);
		}
		
		var _map:Map ;
		/*[Bindable(event="mapChanged")]*/
		public function getMap():Map{
			return _map;
		}
		CONFIG::public restore
		function setMap(value:Map):Map{
			_map = value;
			dispatchEvent(new Event("mapChanged"));
			return value;
		}
		
		var _settings:UserSettings ;
		public function getSettings():UserSettings {
			return _settings;
		}
		
		/*[Bindable(event="configurationChange")]*/
		public function getNodeTagGroups():Array<Dynamic> {
			return new Array();
		}
		
		/*[Bindable(event="configurationChange")]*/
		public function getWayTagGroups():Array<Dynamic> {
			return new Array();
		}
		
		/*[Bindable(event="configurationChange")]*/
		public function getAreaTagGroups():Array<Dynamic> {
			return new Array();
		}
		
		var _selectedTagGroups:Array<Dynamic>;
		/*[Bindable(event="selectedTagGroupsChanged")]*/
		public function getSelectedTagGroups():Array<Dynamic>{
			return _selectedTagGroups;
		}
		public function setSelectedTagGroups(value:Array<Dynamic>):Array<Dynamic>{
			if (value == _selectedTagGroups) return;
			_selectedTagGroups = value;
			dispatchEvent(new Event("selectedTagGroupsChanged"));
			return value;
		}
		
		var _editingEnabled:Bool ;
		/*[Bindable(event="editingEnabledChanged")]*/
		public function getEditingEnabled():Bool{
			return _editingEnabled;
		}
		public function setEditingEnabled(value:Bool):Bool{
			if (value == _editingEnabled) return;
			_editingEnabled = value;
			dispatchEvent(new Event("editingEnabledChanged"));
			return value;
		}
		
		function map_zoomHandler(event:ZoomEvent):Void {
			editingEnabled = map.zoomLevel > MIN_EDITING_ZOOM_LEVEL;
		}
	}
