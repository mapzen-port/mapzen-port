package com.cloudmade.mappingtool.model.config;

	/*[Bindable]*/
	
	class TagGroup
	 {
		
		public var key:String;
		public var values:Array<Dynamic>;
		
		public function new(key:String, values:Array<Dynamic>) {
			this.key = key;
			this.values = values;
		}
	}
