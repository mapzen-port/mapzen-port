package com.cloudmade.mappingtool.model.settings;

	interface IRecord
	{
		function data():Dynamic;
		
		function data(value:Dynamic):Void;
	}
