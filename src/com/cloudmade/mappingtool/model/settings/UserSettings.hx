package com.cloudmade.mappingtool.model.settings;

	import com.cloudmade.mappingtool.ui.MapNavigator;
	
	import flash.events.EventDispatcher;
	import flash.utils.IDataInput;
	import flash.utils.IDataOutput;
	import flash.utils.IExternalizable;
	
	/*[RemoteClass(alias="com.cloudmade.mappingtool.model.UserSettings")]*/
	
	/*[Event(name="showElementsChange", type="com.cloudmade.mappingtool.model.settings.UserSettingsEvent")]*/
	/*[Event(name="elementsStyleChange", type="com.cloudmade.mappingtool.model.settings.UserSettingsEvent")]*/
	/*[Event(name="fadeBackgroundChange", type="com.cloudmade.mappingtool.model.settings.UserSettingsEvent")]*/
	/*[Event(name="backgroundTypeChange", type="com.cloudmade.mappingtool.model.settings.UserSettingsEvent")]*/
	/*[Event(name="showAreaFillsChange", type="com.cloudmade.mappingtool.model.settings.UserSettingsEvent")]*/
	/*[Event(name="mouseWheelEnabledChange", type="com.cloudmade.mappingtool.model.settings.UserSettingsEvent")]*/
	/*[Event(name="mouseWheelProblemNotifiedChange", type="com.cloudmade.mappingtool.model.settings.UserSettingsEvent")]*/
	
	class UserSettings extends EventDispatcher, implements IExternalizable {
		
		public var backgroundType(getBackgroundType, setBackgroundType) : UInt;
		public var elementsStyle(getElementsStyle, setElementsStyle) : UInt;
		public var fadeBackground(getFadeBackground, setFadeBackground) : Bool;
		public var mouseWheelEnabled(getMouseWheelEnabled, setMouseWheelEnabled) : Bool;
		public var mouseWheelProblemNotified(getMouseWheelProblemNotified, setMouseWheelProblemNotified) : Bool;
		public var showAreaFills(getShowAreaFills, setShowAreaFills) : Bool;
		public var showElements(getShowElements, setShowElements) : Bool;
		public function new() {
			super();
		}
		
		var _showElements:Bool ;
		/*[Bindable(event="showElementsChange")]*/
		public function getShowElements():Bool{
			return _showElements;
		}
		public function setShowElements(value:Bool):Bool{
			if (value == _showElements) return;
			_showElements = value;
			dispatchEvent(new UserSettingsEvent(UserSettingsEvent.SHOW_ELEMENTS_CHANGE));
			return value;
		}
		
		var _elementsStyle:UInt ;
		/*[Bindable(event="elementsStyleChange")]*/
		public function getElementsStyle():UInt{
			return _elementsStyle;
		}
		public function setElementsStyle(value:UInt):UInt{
			if (value == _elementsStyle) return;
			_elementsStyle = value;
			dispatchEvent(new UserSettingsEvent(UserSettingsEvent.ELEMENTS_STYLE_CHANGE));
			return value;
		}
		
		var _fadeBackground:Bool ;
		/*[Bindable(event="fadeBackgroundChange")]*/
		public function getFadeBackground():Bool{
			return _fadeBackground;
		}
		public function setFadeBackground(value:Bool):Bool{
			if (value == _fadeBackground) return;
			_fadeBackground = value;
			dispatchEvent(new UserSettingsEvent(UserSettingsEvent.FADE_BACKGROUND_CHANGE));
			return value;
		}
		
		// TODO: Remove dependency from MapNavigator
		var _backgroundType:UInt ;
		/*[Bindable(event="backgroundTypeChange")]*/
		public function getBackgroundType():UInt{
			return _backgroundType;
		}
		public function setBackgroundType(value:UInt):UInt{
			if (value == _backgroundType) return;
			_backgroundType = value;
			dispatchEvent(new UserSettingsEvent(UserSettingsEvent.BACKGROUND_TYPE_CHANGE));
			return value;
		}
		
		var _showAreaFills:Bool ;
		/*[Bindable(event="showAreaFillsChange")]*/
		public function getShowAreaFills():Bool{
			return _showAreaFills;
		}
		public function setShowAreaFills(value:Bool):Bool{
			if (value == _showAreaFills) return;
			_showAreaFills = value;
			dispatchEvent(new UserSettingsEvent(UserSettingsEvent.SHOW_AREA_FILLS_CHANGE));
			return value;
		}
		
		var _mouseWheelEnabled:Bool ;
		/*[Bindable(event="mouseWheelEnabledChange")]*/
		public function getMouseWheelEnabled():Bool{
			return _mouseWheelEnabled;
		}
		public function setMouseWheelEnabled(value:Bool):Bool{
			if (value == _mouseWheelEnabled) return;
			_mouseWheelEnabled = value;
			dispatchEvent(new UserSettingsEvent(UserSettingsEvent.MOUSE_WHEEL_ENABLED_CHANGE));
			return value;
		}
		
		var _mouseWheelProblemNotified:Bool ;
		/*[Bindable(event="mouseWheelProblemNotifiedChanged")]*/
		public function getMouseWheelProblemNotified():Bool{
			return _mouseWheelProblemNotified;
		}
		public function setMouseWheelProblemNotified(value:Bool):Bool{
			if (value == _mouseWheelProblemNotified) return;
			_mouseWheelProblemNotified = value;
			dispatchEvent(new UserSettingsEvent(UserSettingsEvent.MOUSE_WHEEL_PROBLEM_NOTIFIED_CHANGE));
			return value;
		}
		
		public function readExternal(input:IDataInput):Void {
			showElements = input.readBoolean();
			elementsStyle = input.readUnsignedInt();
			fadeBackground = input.readBoolean();
			backgroundType = input.readUnsignedInt();
			showAreaFills = input.readBoolean();
			mouseWheelEnabled = input.readBoolean();
			mouseWheelProblemNotified = input.readBoolean();
		}
		
		public function writeExternal(output:IDataOutput):Void {
			output.writeBoolean(showElements);
			output.writeUnsignedInt(elementsStyle);
			output.writeBoolean(fadeBackground);
			output.writeUnsignedInt(backgroundType);
			output.writeBoolean(showAreaFills);
			output.writeBoolean(mouseWheelEnabled);
			output.writeBoolean(mouseWheelProblemNotified);
		}
	}
