package com.cloudmade.mappingtool.model.settings;

	import com.cloudmade.mappingtool.actions.IAction;
	import com.cloudmade.mappingtool.actions.ITriggerHandler;
	import com.cloudmade.mappingtool.actions.runners.ActionRunner;
	import com.cloudmade.mappingtool.actions.runners.IActionRunner;
	import com.cloudmade.mappingtool.actions.triggers.ITrigger;
	import com.cloudmade.mappingtool.actions.triggers.Trigger;
	import com.cloudmade.mappingtool.actions.triggers.TriggerGroup;
	import com.cloudmade.mappingtool.commands.ICommand;
	import com.cloudmade.mappingtool.map.events.BoundsChangeEvent;
	import com.cloudmade.mappingtool.map.model.Map;
	
	import flash.net.SharedObject;
	
	class ApplicationSettingsStorageFactory
	 {
		
		static var USER_SETTINGS_KEY:String = "userSettings";
		static var AUTO_SETTINGS_KEY:String = "autoSettings";
		
		var userSettings:UserSettings;
		var autoSettings:AutoSettings;
		var map:Map;
		
		public function new(userSettings:UserSettings, autoSettings:AutoSettings, map:Map) {
			this.userSettings = userSettings;
			this.autoSettings = autoSettings;
			this.map = map;
		}
		
		public function createReadApplicationSettingsCommand(sharedObject:SharedObject):ICommand {
			var userSettingsRecord:IRecord = new SharedObjectRecord(sharedObject, USER_SETTINGS_KEY);
			var autoSettingsRecord:IRecord = new SharedObjectRecord(sharedObject, AUTO_SETTINGS_KEY);
			var appSettingsExternalizer:IExternalizer = createApplicationSettingsExternalizer(userSettingsRecord, autoSettingsRecord);
			var readCommand:ICommand = new ExternalizerReadCommand(appSettingsExternalizer);
			return readCommand;
		}
		
		public function createSaveApplicationSettingsAction(sharedObject:SharedObject):ITriggerHandler {
			var userSettingsRecord:IRecord = new AutoFlushSharedObjectRecord(sharedObject, USER_SETTINGS_KEY);
			var autoSettingsRecord:IRecord = new AutoFlushSharedObjectRecord(sharedObject, AUTO_SETTINGS_KEY);
			var appSettingsExternalizer:IExternalizer = createApplicationSettingsExternalizer(userSettingsRecord, autoSettingsRecord);
			var saveAction:IAction = new ExternalizerWriteAction(appSettingsExternalizer);
			var saveTrigger:ITrigger = createApplicationSettingsChangeTrigger();
			var saveRunner:IActionRunner = new ActionRunner(saveAction, saveTrigger);
			return saveRunner;
		}
		
		function createApplicationSettingsExternalizer(userSettingsRecord:IRecord, autoSettingsRecord:IRecord):IExternalizer {
			var userSettingsExternalizer:IExternalizer = new RecordDataExternalizer(userSettings, userSettingsRecord);
			var autoSettingsExternalizer:IExternalizer = new RecordDataExternalizer(autoSettings, autoSettingsRecord);
			var appSettingsExternalizer:IExternalizer = new ExternalizerGroup([userSettingsExternalizer, autoSettingsExternalizer]);
			return appSettingsExternalizer;
		}
		
		function createApplicationSettingsChangeTrigger():ITrigger {
			var userSettingsTrigger:ITrigger = createUserSettingsChangeTrigger();
			var autoSettingsTrigger:ITrigger = createAutoSettingsChangeTrigger();
			var appSettingsTrigger:ITrigger = new TriggerGroup([userSettingsTrigger, autoSettingsTrigger]);
			return appSettingsTrigger;
		}
		
		function createUserSettingsChangeTrigger():ITrigger {
			var trigger:ITrigger = new TriggerGroup([
				new Trigger(userSettings, UserSettingsEvent.SHOW_ELEMENTS_CHANGE),
				new Trigger(userSettings, UserSettingsEvent.ELEMENTS_STYLE_CHANGE),
				new Trigger(userSettings, UserSettingsEvent.FADE_BACKGROUND_CHANGE),
				new Trigger(userSettings, UserSettingsEvent.BACKGROUND_TYPE_CHANGE),
				new Trigger(userSettings, UserSettingsEvent.SHOW_AREA_FILLS_CHANGE),
				new Trigger(userSettings, UserSettingsEvent.MOUSE_WHEEL_ENABLED_CHANGE),
				new Trigger(userSettings, UserSettingsEvent.MOUSE_WHEEL_PROBLEM_NOTIFIED_CHANGE)
			]);
			return trigger;
		}
		
		function createAutoSettingsChangeTrigger():ITrigger {
			/*var centerTrigger:ITrigger = new Trigger(map, LatLongChangeEvent.CENTER_CHANGE);
			var zoomLevelTrigger:ITrigger = new Trigger(map, ZoomEvent.ZOOM);
			var autoSettingsTrigger:ITrigger = new TriggerGroup([centerTrigger, zoomLevelTrigger]);*/
			// Leveraging enable / disable of bounds update:
			var autoSettingsTrigger:ITrigger = new Trigger(map, BoundsChangeEvent.BOUNDS_CHANGE);
			return autoSettingsTrigger;
		}
	}
