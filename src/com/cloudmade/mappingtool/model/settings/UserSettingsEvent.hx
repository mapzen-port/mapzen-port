package com.cloudmade.mappingtool.model.settings;

	import flash.events.Event;
	
	class UserSettingsEvent extends Event {
		
		public static var SHOW_ELEMENTS_CHANGE:String = "showElementsChange";
		public static var ELEMENTS_STYLE_CHANGE:String = "elementsStyleChange";
		public static var FADE_BACKGROUND_CHANGE:String = "fadeBackgroundChange";
		public static var BACKGROUND_TYPE_CHANGE:String = "backgroundTypeChange";
		public static var SHOW_AREA_FILLS_CHANGE:String = "showAreaFillsChange";
		public static var MOUSE_WHEEL_ENABLED_CHANGE:String = "mouseWheelEnabledChange";
		public static var MOUSE_WHEEL_PROBLEM_NOTIFIED_CHANGE:String = "mouseWheelProblemNotifiedChange";
		
		public function new(type:String) {
			super(type);
		}
		
		public override function clone():Event {
			return new UserSettingsEvent(type);
		}
	}
