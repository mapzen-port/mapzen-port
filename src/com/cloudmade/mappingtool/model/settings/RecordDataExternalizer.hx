package com.cloudmade.mappingtool.model.settings;

	import flash.utils.ByteArray;
	import flash.utils.IExternalizable;
	
	class RecordDataExternalizer implements IExternalizer {
		
		var data:IExternalizable;
		var record:IRecord;
		
		public function new(data:IExternalizable, record:IRecord) {
			this.data = data;
			this.record = record;
		}
		
		public function read():Void {
			var input:ByteArray = cast( record.data, ByteArray);
			if (input) {
				// Supressing backward compatibility exceptions:
				try {
					data.readExternal(input);
				}
				catch (error:Error) {
				}
			}
		}
		
		public function write():Void {
			var output:ByteArray = new ByteArray();
			data.writeExternal(output);
			record.data = output;
		}
	}
