package com.cloudmade.mappingtool.model.settings;

	import flash.net.SharedObject;

	class AutoFlushSharedObjectRecord extends SharedObjectRecord {
		
		public var data(null, setData) : Dynamic;
		var minDiskSpace:Int;
		
		public function new(sharedObject:SharedObject, key:String, ?minDiskSpace:Int = 0) {
			super(sharedObject, key);
			this.minDiskSpace = minDiskSpace;
		}
		
		public override function setData(value:Dynamic):Dynamic{
			super.data = value;
			try {
				sharedObject.flush(minDiskSpace);
			} catch (error:Error) {
			}
			return value;
		}
	}
