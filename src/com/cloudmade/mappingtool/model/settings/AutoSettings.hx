package com.cloudmade.mappingtool.model.settings;

	import com.cloudmade.mappingtool.map.geo.LatLong;
	import com.cloudmade.mappingtool.map.model.Map;
	
	import flash.utils.IDataInput;
	import flash.utils.IDataOutput;
	import flash.utils.IExternalizable;
	
	/*[RemoteClass(alias="com.cloudmade.mappingtool.model.AutoSettings")]*/
	
	class AutoSettings implements IExternalizable {
		
		var map:Map;
		
		public function new(map:Map) {
			this.map = map;
		}
		
		// Do not change serialization because of backward compatibility with Mapzen Alpha Patch.
		
		public function readExternal(input:IDataInput):Void {
			var mapCenterLat:Int = input.readFloat();
			var mapCenterLong:Int = input.readFloat();
			map.center = new LatLong(mapCenterLat, mapCenterLong);
			map.zoomLevel = input.readInt();
		}
		
		public function writeExternal(output:IDataOutput):Void {
			output.writeFloat(map.center.lat);
			output.writeFloat(map.center.long);
			output.writeInt(map.zoomLevel);
		}
	}
