package com.cloudmade.mappingtool.model.settings;

	import com.cloudmade.mappingtool.commands.ICommand;
	import com.cloudmade.mappingtool.map.model.Map;
	
	class ApplicationSettingsFactory
	 {
		
		public function new() {
		}
		
		public function createInitializeSettingsCommand(userSettings:UserSettings, map:Map, userId:String):ICommand {
			var autoSettings:AutoSettings = new AutoSettings(map);
			var storageFactory:ApplicationSettingsStorageFactory = new ApplicationSettingsStorageFactory(userSettings, autoSettings, map);
			var command:ICommand = new InitializeApplicationSettingsCommand(storageFactory, userId);
			return command;
		}
	}
