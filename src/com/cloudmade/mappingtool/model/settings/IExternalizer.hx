package com.cloudmade.mappingtool.model.settings;

	interface IExternalizer
	{
		function read():Void;
		
		function write():Void;		
	}
