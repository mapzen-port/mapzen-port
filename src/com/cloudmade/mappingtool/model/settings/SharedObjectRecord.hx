package com.cloudmade.mappingtool.model.settings;

	import flash.net.SharedObject;
	
	class SharedObjectRecord implements IRecord {
		
		public var data(getData, setData) : Dynamic;
		var sharedObject:SharedObject;
		
		var key:String;
		
		public function new(sharedObject:SharedObject, key:String) {
			this.sharedObject = sharedObject;
			this.key = key;
		}
		
		public function getData():Dynamic{
			return sharedObject.data[key];
		}
		
		public function setData(value:Dynamic):Dynamic{
			sharedObject.data[key] = value;
			return value;
		}
	}
