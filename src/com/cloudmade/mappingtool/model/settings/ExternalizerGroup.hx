package com.cloudmade.mappingtool.model.settings;

	class ExternalizerGroup implements IExternalizer {
		
		var externalizers:Array<Dynamic>;
		
		public function new(externalizers:Array<Dynamic>) {
			this.externalizers = externalizers;
		}
		
		public function read():Void {
			for (externalizer in externalizers) {
				externalizer.read();
			}
		}
		
		public function write():Void {
			for (externalizer in externalizers) {
				externalizer.write();
			}
		}
	}
