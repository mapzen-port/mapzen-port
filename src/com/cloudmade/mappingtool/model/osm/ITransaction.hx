package com.cloudmade.mappingtool.model.osm;

	import flash.events.Event;
	import flash.events.IEventDispatcher;
	import flash.events.IOErrorEvent;
	
	/*[Event(name="complete", type="flash.events.Event")]*/
	/*[Event(name="ioError", type="flash.events.IOErrorEvent")]*/
	
	// TODO: Extend from IDelegate.
	
	interface ITransaction implements IEventDispatcher{
		function send():Void;
		
		function setCredentials(username:String, password:String):Void;
	}
