package com.cloudmade.mappingtool.model.osm;

	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.HTTPStatusEvent;
	import flash.events.IOErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestHeader;
	import flash.net.URLRequestMethod;
	
	/*[Event(name="complete", type="flash.events.Event")]*/
	/*[Event(name="ioError", type="flash.events.IOErrorEvent")]*/
	/*[Event(name="httpStatus", type="flash.events.HTTPStatusEvent")]*/

	class Transaction extends EventDispatcher, implements ITransaction {
		
		public var data(getData, null) : Dynamic ;
		public var requestData(getRequestData, setRequestData) : String;
		public var rootUrl(getRootUrl, setRootUrl) : String;
		public var url(getUrl, setUrl) : String;
		public var usePutMethod(getUsePutMethod, setUsePutMethod) : Bool;
		var username:String;
		var password:String;
		var authorizator:IAuthorizator;
		var loader:URLLoader ;
		
		public function new(?authorizator:IAuthorizator = null, ?usePutMethod:Bool = false, ?rootUrl:String = "", ?url:String = "") {
			
			loader = new URLLoader();
			super();
			_url = url;
			_rootUrl = rootUrl;
			_usePutMethod = usePutMethod;
			this.authorizator = authorizator;
			loader.addEventListener(Event.COMPLETE, loader_completeHandler);
			loader.addEventListener(IOErrorEvent.IO_ERROR, loader_ioErrorHandler);
			loader.addEventListener(HTTPStatusEvent.HTTP_STATUS, loader_httpStatusHandler);
		}
		
		public function getData():Dynamic {
			return loader.data;
		}
		
		var _url:String;
		public function getUrl():String{
			return _url;
		}
		public function setUrl(value:String):String{
			_url = value;
			return value;
		}
		
		var _rootUrl:String;
		public function getRootUrl():String{
			return _rootUrl;
		}
		public function setRootUrl(value:String):String{
			_rootUrl = value;
			return value;
		}
		
		var _usePutMethod:Bool;
		public function getUsePutMethod():Bool{
			return _usePutMethod;
		}
		public function setUsePutMethod(value:Bool):Bool{
			_usePutMethod = value;
			return value;
		}
		
		var _requestData:String;
		public function getRequestData():String{
			return _requestData;
		}
		public function setRequestData(value:String):String{
			_requestData = value;
			return value;
		}
		
		public function setCredentials(username:String, password:String):Void {
			this.username = username;
			this.password = password;
		}
		
		public function send():Void {
			//var param:String = usePutMethod ? "?_method=put" : "";
			var request:URLRequest = new URLRequest(rootUrl + url);
			request.method = URLRequestMethod.POST;
			request.contentType = "text/xml";
			if (requestData != null) {
				request.data = requestData;
			} else {
				// Workaround for 405 HTTP status.
				request.data = "<xml></xml>";
			}
			if (usePutMethod) {
				var putHeader:URLRequestHeader = new URLRequestHeader("X_HTTP_METHOD_OVERRIDE", "PUT");
				request.requestHeaders.push(putHeader);
			}
			/*var encoder:Base64Encoder = new Base64Encoder();
			encoder.encode(username + ":" + password);
			var authHeader:URLRequestHeader = new URLRequestHeader("AUTHORIZATION", "BASIC " + encoder.toString());
			request.requestHeaders.push(authHeader);*/
			if (authorizator) {
				var method:String = usePutMethod ? "PUT" : "POST";
				var authHeader:URLRequestHeader = authorizator.getAuthorizationHeader(method, request.url);
				request.requestHeaders.push(authHeader);
			}
			loader.load(request);
		}
		
		function loader_completeHandler(event:Event):Void {
			dispatchEvent(event);
		}
		
		function loader_ioErrorHandler(event:IOErrorEvent):Void {
			dispatchEvent(event);
		}
		
		function loader_httpStatusHandler(event:HTTPStatusEvent):Void {
			dispatchEvent(event);
		}
	}
