package com.cloudmade.mappingtool.model.osm;

	import flash.net.URLRequestHeader;
	
	interface IAuthorizator
	{
		function getAuthorizationHeader(method:String, url:String):URLRequestHeader;
	}
