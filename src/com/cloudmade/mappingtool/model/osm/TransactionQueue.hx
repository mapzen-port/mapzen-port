package com.cloudmade.mappingtool.model.osm;

	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.HTTPStatusEvent;
	import flash.events.IOErrorEvent;
	
	/*[Event(name="complete", type="flash.events.Event")]*/
	/*[Event(name="ioError", type="flash.events.IOErrorEvent")]*/
	/*[Event(name="httpStatus", type="flash.events.HTTPStatusEvent")]*/
	
	// TODO: Implement IDelegateGroup or replace with SequenceDelegateGroup.
	
	class TransactionQueue extends EventDispatcher, implements ITransaction {
		
		var index:Int ;
		var queue:Array<Dynamic> ;
		
		public function new() {
			
			index = 0;
			queue = new Array();
			super();
		}
		
		public function addTransaction(transaction:ITransaction):Void {
			queue.push(transaction);
			// TODO: Remove this quick fix.
			// Redispatches transaction events as queue events "after" they were sent on source transaction.
			transaction.addEventListener(Event.COMPLETE, transaction_completeHandler, false, -1);
			transaction.addEventListener(IOErrorEvent.IO_ERROR, transaction_ioErrorHandler, false, -1);
			transaction.addEventListener(HTTPStatusEvent.HTTP_STATUS, transaction_httpStatusHandler, false, -1);
		}
		
		public function setCredentials(username:String, password:String):Void {
			for (transaction in queue) {
				transaction.setCredentials(username, password);
			}
		}
		
		public function send():Void {
			if (index == 0) {
				sendTransaction();
			}
		}
		
		function sendTransaction():Void {
			ITransaction(queue[index]).send();
			index++;
		}
		
		function transaction_completeHandler(event:Event):Void {
			if (index == queue.length) {
				index = 0;
				dispatchEvent(event);
			} else {
				sendTransaction();
			}
		}
		
		function transaction_ioErrorHandler(event:IOErrorEvent):Void {
			index = 0;
			dispatchEvent(event);
		}
		
		function transaction_httpStatusHandler(event:HTTPStatusEvent):Void {
			dispatchEvent(event);
		}
	}
