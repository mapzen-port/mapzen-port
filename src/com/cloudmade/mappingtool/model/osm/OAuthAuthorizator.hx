package com.cloudmade.mappingtool.model.osm;

	import flash.external.ExternalInterface;
	import flash.net.URLRequestHeader;
	
	class OAuthAuthorizator implements IAuthorizator {
		
		public function new() {
		}

		public function getAuthorizationHeader(method:String, url:String):URLRequestHeader {
			var header:URLRequestHeader = new URLRequestHeader("Authorization");
			header.value = ExternalInterface.call("getOAuthHeader", method, url);
			return header;
		}
	}
