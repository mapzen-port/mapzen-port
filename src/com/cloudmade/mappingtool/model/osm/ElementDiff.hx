package com.cloudmade.mappingtool.model.osm;

	class ElementDiff
	 {
		
		public var oldId:String;
		public var newId:String;
		public var newVersion:String;
		
		public function new(oldId:String, newId:String, newVersion:String) {
			this.oldId = oldId;
			this.newId = newId;
			this.newVersion = newVersion;
		}
	}
