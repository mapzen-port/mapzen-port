package com.cloudmade.mappingtool.utils;

	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	class Bounds
	 {
		
		var rect:Rectangle;
		
		public function new(rect:Rectangle) {
			this.rect = rect;
		}
		
		public function limitPoint(point:Point):Point {
			var x:Int = minmax(point.x, rect.left, rect.right);
			var y:Int = minmax(point.y, rect.top, rect.bottom);
			var newPoint:Point = new Point(x, y);
			return newPoint;
		}
	}
