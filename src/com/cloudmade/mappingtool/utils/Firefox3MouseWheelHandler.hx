package com.cloudmade.mappingtool.utils;

	import flash.display.Stage;
	import flash.events.MouseEvent;
	import flash.external.ExternalInterface;
	import flash.utils.setTimeout;
	
	class Firefox3MouseWheelHandler
	 {
		public function new() { }
		
		static var LISTENER_PRIORITY:Int = 1000;
		
		static var denyMouseWheel:Bool = false;
		
		public static function initialize(stage:Stage):Void {
			var isFirefox3:Bool = false;
			if (ExternalInterface.available) {
				isFirefox3 = ExternalInterface.call("function(){return navigator.oscpu && navigator.userAgent.match('rv:1\.9')}");
			}
			if (isFirefox3) {
				stage.addEventListener(MouseEvent.MOUSE_WHEEL, stage_mouseWheelHandler, true, LISTENER_PRIORITY);
			}
		}
		
		static function allowMouseWheel():Void {
			denyMouseWheel = false;
		}
		
		static function stage_mouseWheelHandler(event:MouseEvent):Void {
			if (denyMouseWheel) {
				denyMouseWheel = false;
				event.stopImmediatePropagation();
			} else {
				denyMouseWheel = true;
				setTimeout(allowMouseWheel, 1);
			}
		}
	}
