package com.cloudmade.mappingtool.utils;

	public function roundTo(value:Number, precision:int):Number {
		var factor:Number = Math.pow(10, precision);
		return Math.round(value * factor) / factor;
	