package com.cloudmade.mappingtool.utils;

	import flash.events.EventDispatcher;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	import mx.events.FlexEvent;
	
	/*[Event(name="dataChange", type="mx.events.FlexEvent")]*/
	
	class DataDelayer extends EventDispatcher {
		
		public var blocked(getBlocked, setBlocked) : Bool;
		public var data(getData, setData) : Dynamic;
		public var delay(getDelay, setDelay) : Int;
		var newData:Dynamic;
		var hasDelay:Bool ;
		var dataChanged:Bool ;
		var timer:Timer ;
		
		public function new() {
			
			hasDelay = false;
			dataChanged = false;
			timer = new Timer(0, 1);
			super();
			timer.addEventListener(TimerEvent.TIMER_COMPLETE, timer_timerCompleteHandler);
		}
		
		var _data:Dynamic;
		/*[Bindable(event="dataChange")]*/
		public function getData():Dynamic{
			return _data;
		}
		public function setData(value:Dynamic):Dynamic{
			if (value == newData) return;
			dataChanged = true;
			newData = value;
			updateData();
			return value;
		}
		
		public function getDelay():Int{
			return hasDelay ? timer.delay : 0;
		}
		public function setDelay(value:Int):Int{
			timer.reset();
			hasDelay = value > 0;
			if (hasDelay) {
				timer.delay = value;
			}
			updateData();
			return value;
		}
		
		var _blocked:Bool ;
		public function getBlocked():Bool{
			return _blocked;
		}
		public function setBlocked(value:Bool):Bool{
			if (value == _blocked) return;
			_blocked = value;
			if (value) {
				timer.reset();
			} else {
				updateData();
			}
			return value;
		}
		
		function updateData():Void {
			if (blocked || !dataChanged) return;
			if (hasDelay) {
				timer.start();
			} else {
				changeData();
			}
		}
		
		function changeData():Void {
			_data = newData;
			dataChanged = false;
			dispatchEvent(new FlexEvent(FlexEvent.DATA_CHANGE));
		}
		
		function timer_timerCompleteHandler(event:TimerEvent):Void {
			changeData();
		}
	}
