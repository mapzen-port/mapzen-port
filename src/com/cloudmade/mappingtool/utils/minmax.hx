package com.cloudmade.mappingtool.utils;

	public function minmax(value:Number, min:Number, max:Number):Number {
		return Math.min(Math.max(value, min), max);
	