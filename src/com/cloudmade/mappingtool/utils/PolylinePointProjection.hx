package com.cloudmade.mappingtool.utils;

	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	class PolylinePointProjection
	 {
		
		public var projectionIndex(getProjectionIndex, null) : Int ;
		public var projectionPoint(getProjectionPoint, null) : Point ;
		var polylinePoints:Array<Dynamic>;
		
		public function new(polylinePoints:Array<Dynamic>) {
			this.polylinePoints = polylinePoints;
		}
		
		var _projectionIndex:Int;
		public function getProjectionIndex():Int {
			return _projectionIndex;
		}
		
		var _projectionPoint:Point;
		public function getProjectionPoint():Point {
			return _projectionPoint;
		}
		
		public function projectPoint(point:Point):Void {
			var snappedPoint:Point;
			var minDistance:Int = Number.MAX_VALUE;
			var endPoint:Point = polylinePoints[0];
			var index:Int;
			for (i in 1...polylinePoints.length) {
				var startPoint:Point = endPoint;
				endPoint = polylinePoints[i];
				var localPoint:Point = point.subtract(startPoint);
				var localAngle:Int = Math.atan2(localPoint.y, localPoint.x);
				var segmentAngle:Int = Math.atan2(endPoint.y - startPoint.y, endPoint.x - startPoint.x);
				var normLocalPoint:Point = Point.polar(localPoint.length, segmentAngle - localAngle);
				var segmentWidth:Int = endPoint.subtract(startPoint).length;
				var segmentRectangle:Rectangle = new Rectangle(0, -minDistance / 2, segmentWidth , minDistance);
				if (segmentRectangle.containsPoint(normLocalPoint)) {
					var interpolationFactor:Int = 1 - normLocalPoint.x / segmentWidth;
					snappedPoint = Point.interpolate(startPoint, endPoint, interpolationFactor);
					minDistance = Math.abs(normLocalPoint.y);
					index = i;
				}
			}
			_projectionIndex = index;
			_projectionPoint = snappedPoint;
		}
	}
