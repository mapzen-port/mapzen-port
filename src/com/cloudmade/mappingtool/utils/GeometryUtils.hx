package com.cloudmade.mappingtool.utils;

	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	class GeometryUtils
	 {
		public function new() { }
		
		public static var INLYING:UInt = 0;
		public static var OUTLYING:UInt = 1;
		public static var CONTACTING:UInt = 2;
		public static var INTERSECTING:UInt = 3;
		
		static var TOP_EDGE:UInt = 0;
		static var RIGHT_EDGE:UInt = 1;
		static var BOTTOM_EDGE:UInt = 2;
		static var LEFT_EDGE:UInt = 3;
		
		static var clipMin:Int;
		static var clipMax:Int;
		
		// TODO: Replace with standard algorithm: http://en.wikipedia.org/wiki/Line-line_intersection.
		public static function calculateIntersection(startPoint1:Point, endPoint1:Point, startPoint2:Point, endPoint2:Point):Point {
			var d:Int = (endPoint1.x - startPoint1.x) * (startPoint2.y - endPoint2.y) - (startPoint2.x - endPoint2.x) * (endPoint1.y - startPoint1.y);
			if (d == 0) return null;
			var d1:Int = (startPoint2.x - startPoint1.x) * (startPoint2.y - endPoint2.y) - (startPoint2.x - endPoint2.x) * (startPoint2.y - startPoint1.y);
			//var d2:Number = (endPoint1.x - startPoint1.x) * (startPoint2.y - startPoint1.y) - (startPoint2.x - startPoint1.x) * (endPoint1.y - startPoint1.y);
			var t1:Int = d1 / d;
			//var t2:Number = d2 / d;
		    var x:Int = t1 * endPoint1.x + (1 - t1) * startPoint1.x;
   			var y:Int = t1 * endPoint1.y + (1 - t1) * startPoint1.y;
   			return new Point(x, y);
		}
		
		public static function clipLineSegment(point1:Point, point2:Point, bounds:Rectangle):UInt {
			var delta:Point = point2.subtract(point1);
			clipMin = 0;
			clipMax = 1;
			if (clip(-delta.x, point1.x - bounds.x) &&
				clip(delta.x, bounds.width + bounds.x - point1.x) &&
				clip(-delta.y, point1.y - bounds.y) &&
				clip(delta.y, bounds.height + bounds.y - point1.y)) {
				if (clipMax < 1) {
					point2.x = point1.x + clipMax * delta.x;
					point2.y = point1.y + clipMax * delta.y;
				}
				if (clipMin > 0) {
					point1.offset(clipMin * delta.x, clipMin * delta.y);
				}
				if (clipMax == clipMin) {
					return CONTACTING;
				} else if ((clipMax == 1) && (clipMin == 0)) {
					return INLYING;
				}
				return INTERSECTING;
			}
			return OUTLYING;
		}
		
		public static function clipPolygon(points:Array<Dynamic>, bounds:Rectangle):UInt {
			var source:Array<Dynamic> = points;
			var status:UInt = INLYING;
			for (edgeId in 0...4) {
				var clippedPoints:Array<Dynamic> = new Array();
				var n:Int = points.length;
				for (i in 0...n) {
					if (isInsideClipWindow(points[j], bounds, edgeId)) {
						if (isInsideClipWindow(points[i], bounds, edgeId)) {
							clippedPoints.push(points[i]);
						} else {
							clippedPoints.push(calculateSideIntersection(points[j], points[i], bounds, edgeId));
							status = INTERSECTING;
						}
					} else if (isInsideClipWindow(points[i], bounds, edgeId)) {
						clippedPoints.push(calculateSideIntersection(points[j], points[i], bounds, edgeId));
						clippedPoints.push(points[i]);
						status = INTERSECTING;
					}
				}
				points = clippedPoints;
			}
			source.splice.apply(source, [0, source.length].concat(points));
			if (points.length == 0) {
				status = OUTLYING;
			}
			return status;
		} 
		
		static function clip(p:Int, q:Int):Bool {
			if (p == 0) return q >= 0;
			var r:Int = q / p;
			if (p < 0) {
				if (r > clipMax) return false;
				if (r > clipMin) clipMin = r;
			} else {
				if (r < clipMin) return false;
				if (r < clipMax) clipMax = r;
			}
			return true;
		}
		
		static function calculateSideIntersection(point1:Point, point2:Point, bounds:Rectangle, edgeId:UInt):Point {
			switch (edgeId) {
				case TOP_EDGE:		return new Point(point1.x + (bounds.bottom - point1.y) * (point2.x - point1.x) / (point2.y - point1.y), bounds.bottom);
				case BOTTOM_EDGE:	return new Point(point1.x + (bounds.top - point1.y) * (point2.x - point1.x) / (point2.y - point1.y), bounds.top);
				case RIGHT_EDGE:	return new Point(bounds.right, point1.y + (bounds.right - point1.x) * (point2.y - point1.y) / (point2.x - point1.x));
				case LEFT_EDGE:		return new Point(bounds.left, point1.y + (bounds.left - point1.x) * (point2.y - point1.y) / (point2.x - point1.x));
			}
			return null;
		} 
		
		static function isInsideClipWindow(point:Point, bounds:Rectangle, edgeId:UInt):Bool {
	        switch (edgeId) {
	            case TOP_EDGE:		return point.y < bounds.bottom;
	            case BOTTOM_EDGE:	return point.y > bounds.top;
	        	case LEFT_EDGE:		return point.x > bounds.left;
	        	case RIGHT_EDGE:	return point.x < bounds.right;
	        }
	        return false;
		}
	}
