package com.cloudmade.mappingtool.utils;

	import flash.geom.Point;
	
	class PolylinePointNormalization
	 {
		
		var polylinePoints:Array<Dynamic>;
		
		public function new(polylinePoints:Array<Dynamic>) {
			this.polylinePoints = polylinePoints;
		}
		
		public function normalizePoint(point:Point):Int {
			var index:Int = -1;
			var distance:Int = Number.MAX_VALUE;
			
			for (i in 0...polylinePoints.length) {
				var polylinePoint:Point = polylinePoints[i];
				var deltaPoint:Point = polylinePoint.subtract(point);
				var newDistance:Int = deltaPoint.length;
				
				if (newDistance < distance) {
					distance = newDistance;
					index = i;
				}
			}
			
			return index;
		}
	}
