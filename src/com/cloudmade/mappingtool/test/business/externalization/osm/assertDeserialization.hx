package com.cloudmade.mappingtool.test.business.externalization.osm; 
	import com.cloudmade.mappingtool.business.externalization.IXMLDeserializer;
	import com.cloudmade.mappingtool.business.externalization.IXMLSerializer;
	
	import flexunit.framework.Assert;
	
	import mx.utils.ObjectUtil;
	
	internal function assertDeserialization(deserializer:IXMLDeserializer, xml:XML, object:Object):void {
		Assert.assertEquals(0, ObjectUtil.compare(deserializer.deserialize(xml), object));
	