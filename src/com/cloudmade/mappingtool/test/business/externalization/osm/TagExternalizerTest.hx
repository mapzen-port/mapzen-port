package com.cloudmade.mappingtool.test.business.externalization.osm;

	import com.cloudmade.mappingtool.business.externalization.IXMLDeserializer;
	import com.cloudmade.mappingtool.business.externalization.osm.TagExternalizer;
	import com.cloudmade.mappingtool.map.model.Tag;
	
	import flexunit.framework.TestCase;

	class TagExternalizerTest extends TestCase {
		
		public function new(?methodName:String=null)
		{
			super(methodName);
		}
		
		var TAG_EXAMPLE:Tag ;
		var XML_EXAMPLE:XML ;
				
		public function testTagSerialize():Void {
			assertSerialization(new TagExternalizer(), TAG_EXAMPLE, XML_EXAMPLE);
		}
		
		public function testTagDeserialize():Void {
			assertDeserialization(new TagExternalizer(), XML_EXAMPLE, TAG_EXAMPLE);
		}
		
	}
