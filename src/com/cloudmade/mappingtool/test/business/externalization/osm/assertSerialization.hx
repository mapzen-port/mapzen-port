package com.cloudmade.mappingtool.test.business.externalization.osm; 
	import com.cloudmade.mappingtool.business.externalization.IXMLSerializer;
	
	import flexunit.framework.Assert;
	
	internal function assertSerialization(serializer:IXMLSerializer, object:Object, xml:XML):void {
		var serializedXML:XML = serializer.serialize(object);
		Assert.assertEquals(xml, serializedXML);
	