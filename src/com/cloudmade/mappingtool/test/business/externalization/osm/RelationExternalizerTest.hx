package com.cloudmade.mappingtool.test.business.externalization.osm;

	import com.cloudmade.mappingtool.business.externalization.IXMLDeserializer;
	import com.cloudmade.mappingtool.business.externalization.IXMLExternalizer;
	import com.cloudmade.mappingtool.business.externalization.IXMLListDeserializer;
	import com.cloudmade.mappingtool.business.externalization.IXMLListSerializer;
	import com.cloudmade.mappingtool.business.externalization.IXMLSerializer;
	import com.cloudmade.mappingtool.business.externalization.osm.ElementSerializer;
	import com.cloudmade.mappingtool.business.externalization.osm.ListExternalizer;
	import com.cloudmade.mappingtool.business.externalization.osm.MemberExternalizer;
	import com.cloudmade.mappingtool.business.externalization.osm.RelationDeserializer;
	import com.cloudmade.mappingtool.business.externalization.osm.RelationSerializer;
	import com.cloudmade.mappingtool.business.externalization.osm.TagExternalizer;
	import com.cloudmade.mappingtool.map.model.Member;
	import com.cloudmade.mappingtool.map.model.Relation;
	import com.cloudmade.mappingtool.map.model.Tag;
	
	import flexunit.framework.TestCase;

	class RelationExternalizerTest extends TestCase {
		
		public function new(?methodName:String=null)
		{
			super(methodName);
		}
		
		public function testSerializeRelation():Void {
			assertSerialization(createRelationSerializer(), createExampleRelation(), XML_EXAMPLE);
		}
		
		public function testDeserializeRelation():Void {
			assertDeserialization(createRelationDeserializer(), XML_EXAMPLE, createExampleRelation());
		}

		private var XML_EXAMPLE:XML = <relation id="79316" version="4">
			<member type="node" ref="28419328" role="via" /> 
			<member type="way" ref="4452496" role="from" /> 
			<member type="way" ref="24698540" role="to" /> 
			<tag k="except" v="taxi; bicycle" /> 
			<tag k="restriction" v="no_left_turn" /> 
			<tag k="type" v="restriction" /> 
		</relation>;

		function createRelationSerializer():IXMLSerializer {
			var tagExternalizer:IXMLExternalizer = new TagExternalizer();
			var tagsExternalizer:IXMLListSerializer = new ListExternalizer(tagExternalizer, tagExternalizer);
			var elementSerializer:IXMLSerializer = new ElementSerializer(tagsExternalizer);
			
			var memberExternalizer:IXMLExternalizer = new MemberExternalizer();
			var membersExternalizer:IXMLListSerializer = new ListExternalizer(memberExternalizer, memberExternalizer);
			
			return new RelationSerializer(elementSerializer, membersExternalizer);
		}
	  	
		function createRelationDeserializer():IXMLDeserializer {
			var tagExternalizer:IXMLExternalizer = new TagExternalizer();
			var tagsExternalizer:IXMLListDeserializer = new ListExternalizer(tagExternalizer, tagExternalizer);
			
			var memberExternalizer:IXMLExternalizer = new MemberExternalizer();
			var membersExternalizer:IXMLListDeserializer = new ListExternalizer(memberExternalizer, memberExternalizer);
			
			return new RelationDeserializer(tagsExternalizer, membersExternalizer);
		}

	  	function createExampleRelation():Relation {
	  		var members:Array<Dynamic> = new Array();
	  		members.push(new Member("28419328", "node", "via"));
	  		members.push(new Member("4452496", "way", "from"));
	  		members.push(new Member("24698540", "way", "to"));
	  		
	  		var tags:Array<Dynamic> = new Array();
	  		tags.push(new Tag("except", "taxi; bicycle"));
	  		tags.push(new Tag("restriction", "no_left_turn"));
	  		tags.push(new Tag("type", "restriction"));
	  		
	  		var relation:Relation = new Relation("79316", tags, members);
	  		relation.version = "4";
	  		
	  		return relation;
	  	}
	}
