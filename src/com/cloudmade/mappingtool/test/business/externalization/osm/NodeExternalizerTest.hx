package com.cloudmade.mappingtool.test.business.externalization.osm;

	import com.cloudmade.mappingtool.business.externalization.IXMLDeserializer;
	import com.cloudmade.mappingtool.business.externalization.IXMLExternalizer;
	import com.cloudmade.mappingtool.business.externalization.IXMLListExternalizer;
	import com.cloudmade.mappingtool.business.externalization.IXMLSerializer;
	import com.cloudmade.mappingtool.business.externalization.osm.ElementSerializer;
	import com.cloudmade.mappingtool.business.externalization.osm.ListExternalizer;
	import com.cloudmade.mappingtool.business.externalization.osm.NodeDeserializer;
	import com.cloudmade.mappingtool.business.externalization.osm.NodeSerializer;
	import com.cloudmade.mappingtool.business.externalization.osm.TagExternalizer;
	import com.cloudmade.mappingtool.map.geo.LatLong;
	import com.cloudmade.mappingtool.map.model.Node;
	import com.cloudmade.mappingtool.map.model.Tag;
	
	import flexunit.framework.TestCase;

	class NodeExternalizerTest extends TestCase {
		
		public function new(?methodName:String=null)
		{
			super(methodName);
		}
		
		private var XML_EXAMPLE:XML = <node id="26546014" lat="51.4945099" lon="-0.1019644" version="3">
		  <tag k="name" v="London College of Printing" /> 
		  <tag k="created_by" v="Potlatch 0.9a" /> 
		  <tag k="amenity" v="university" /> 
	  	</node>;
	  	
	  	public function testSerializeNode():Void {
			assertSerialization(createNodeSerializer(), createExampleNode(), XML_EXAMPLE);
		}
		
		public function testDeserializeNode():Void {
			assertDeserialization(createNodeDeserializer(), XML_EXAMPLE, createExampleNode());
		}
		
		function createNodeSerializer():IXMLSerializer {
	  		var tagExternalizer:IXMLExternalizer = new TagExternalizer();
	  		var tagsExternalizer:IXMLListExternalizer = new ListExternalizer(tagExternalizer, tagExternalizer);
	  		var elementSerializer:IXMLSerializer = new ElementSerializer(tagsExternalizer);
	  		return new NodeSerializer(elementSerializer);
		}
		
		function createNodeDeserializer():IXMLDeserializer {
	  		var tagExternalizer:IXMLExternalizer = new TagExternalizer();
	  		var tagsExternalizer:IXMLListExternalizer = new ListExternalizer(tagExternalizer, tagExternalizer);
	  		return new NodeDeserializer(tagsExternalizer);
		}

		function createExampleNode():Node {
			var tags:Array<Dynamic> = new Array();
			tags.push(new Tag("name", "London College of Printing"));
			tags.push(new Tag("created_by", "Potlatch 0.9a"));
			tags.push(new Tag("amenity", "university"));
			
			var position:LatLong = new LatLong(51.4945099, -0.1019644);
			var node:Node = new Node("26546014", tags, position);
			node.version = "3";
			
			return node;
		}
	}
