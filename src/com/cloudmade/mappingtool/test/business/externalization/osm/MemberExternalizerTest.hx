package com.cloudmade.mappingtool.test.business.externalization.osm;

	import com.cloudmade.mappingtool.business.externalization.IXMLDeserializer;
	import com.cloudmade.mappingtool.business.externalization.IXMLSerializer;
	import com.cloudmade.mappingtool.business.externalization.osm.MemberExternalizer;
	import com.cloudmade.mappingtool.map.model.Member;
	
	import flexunit.framework.TestCase;
	
	import mx.utils.ObjectUtil;

	class MemberExternalizerTest extends TestCase {
		
		public function new(?methodName:String=null)
		{
			super(methodName);
		}
		
		var XML_EXAMPLE:XML ;
		var MEMBER_EXAMPLE:Member ;
		
		public function testMemberSerialize():Void {
			assertSerialization(new MemberExternalizer(), MEMBER_EXAMPLE, XML_EXAMPLE);
		}
		
		public function testMemberDeserialize():Void {
			assertDeserialization(new MemberExternalizer(), XML_EXAMPLE, MEMBER_EXAMPLE);
		}
	}
