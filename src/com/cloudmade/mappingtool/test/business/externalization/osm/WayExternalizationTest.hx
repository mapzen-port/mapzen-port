package com.cloudmade.mappingtool.test.business.externalization.osm;

	import com.cloudmade.mappingtool.business.externalization.IXMLDeserializer;
	import com.cloudmade.mappingtool.business.externalization.IXMLExternalizer;
	import com.cloudmade.mappingtool.business.externalization.IXMLListDeserializer;
	import com.cloudmade.mappingtool.business.externalization.IXMLListSerializer;
	import com.cloudmade.mappingtool.business.externalization.IXMLSerializer;
	import com.cloudmade.mappingtool.business.externalization.osm.ElementSerializer;
	import com.cloudmade.mappingtool.business.externalization.osm.ListExternalizer;
	import com.cloudmade.mappingtool.business.externalization.osm.NodeReferenceDeserializer;
	import com.cloudmade.mappingtool.business.externalization.osm.NodeReferenceSerializer;
	import com.cloudmade.mappingtool.business.externalization.osm.TagExternalizer;
	import com.cloudmade.mappingtool.business.externalization.osm.WayDeserializer;
	import com.cloudmade.mappingtool.business.externalization.osm.WaySerializer;
	import com.cloudmade.mappingtool.config.IReadOnlyHashMap;
	import com.cloudmade.mappingtool.config.ReadOnlyHashMap;
	import com.cloudmade.mappingtool.map.model.Node;
	import com.cloudmade.mappingtool.map.model.Tag;
	import com.cloudmade.mappingtool.map.model.Way;
	
	import flexunit.framework.TestCase;

	class WayExternalizationTest extends TestCase {
		
		public function new(?methodName:String=null)
		{
			super(methodName);
		}
		
		private static const XML_EXAMPLE:XML = <way id="0" version="3">
			<nd ref="1" /> 
			<nd ref="2" /> 
			<nd ref="3" /> 
			<tag k="addr:housenumber" v="63" /> 
			<tag k="addr:street" v="Saint Mary Axe" /> 
			<tag k="building" v="yes" /> 
		</way>;
		
		public function testSerializeWay():Void {
			assertSerialization(createWaySerializer(), createExampleWay(), XML_EXAMPLE);
		}
		
		public function testDeserializeWay():Void {
			assertDeserialization(createWayDeserializer(), XML_EXAMPLE, createExampleWay());
		}
		
		function createWaySerializer():IXMLSerializer {
			var tagExternalizer:IXMLExternalizer = new TagExternalizer();
			var tagsExternalizer:IXMLListSerializer = new ListExternalizer(tagExternalizer, tagExternalizer);
			var elementSerializer:IXMLSerializer = new ElementSerializer(tagsExternalizer);
			
			var nodeReferenceSerializer:IXMLSerializer = new NodeReferenceSerializer();
			var nodeReferencesSerializer:IXMLListSerializer = new ListExternalizer(nodeReferenceSerializer, null);
			
			return new WaySerializer(elementSerializer, nodeReferencesSerializer);
		}
		
		function createWayDeserializer():IXMLDeserializer {
			var tagDeserializer:IXMLDeserializer = new TagExternalizer();
			var tagsDeserializer:IXMLListDeserializer = new ListExternalizer(null, tagDeserializer);
			
			var nodeReferenceDeserializer:IXMLDeserializer = new NodeReferenceDeserializer(createNodeMap());
			var nodeReferencesDeserializer:IXMLListDeserializer = new ListExternalizer(null, nodeReferenceDeserializer);
			
			return new WayDeserializer(tagsDeserializer, nodeReferencesDeserializer);
		}
		
		function createNodeMap():IReadOnlyHashMap {
			var map:Dynamic = new Object();
			
			var nodes:Array<Dynamic> = createNodes();
			
			for (node in nodes) {
				map[node.id] = node;
			}
			
			return new ReadOnlyHashMap(map);
		}
		
		function createNodes():Array<Dynamic> {
			var nodes:Array<Dynamic> = new Array();
	  		nodes.push(new Node("1"));
	  		nodes.push(new Node("2"));
	  		nodes.push(new Node("3"));
 			return nodes;
		}
		
		function createExampleWay():Way {
			var tags:Array<Dynamic> = new Array();
	  		tags.push(new Tag("addr:housenumber", "63"));
	  		tags.push(new Tag("addr:street", "Saint Mary Axe"));
	  		tags.push(new Tag("building", "yes"));
	  		
	  		var nodes:Array<Dynamic> = createNodes();
			
			var way:Way = new Way("0", tags, nodes);
			way.version = "3";
			
			return way;
		}
	}
