package com.cloudmade.mappingtool.test.business.externalization.osm;

	import com.cloudmade.mappingtool.business.externalization.IXMLListSerializer;
	import com.cloudmade.mappingtool.business.externalization.IXMLSerializer;
	import com.cloudmade.mappingtool.business.externalization.osm.ListExternalizer;
	import com.cloudmade.mappingtool.business.externalization.osm.TagExternalizer;
	import com.cloudmade.mappingtool.business.externalization.osm.ValidTagsSerializer;
	import com.cloudmade.mappingtool.map.model.Tag;
	
	import flexunit.framework.TestCase;

	class ValidTagsSerializerTest extends TestCase {
		
		public function new(?methodName:String=null)
		{
			super(methodName);
		}
		
		public function testSerializeValidTags():Void {
			var tagSerializer:IXMLSerializer = new TagExternalizer();
			var tagsSerializer:IXMLListSerializer = new ListExternalizer(tagSerializer, null);
			var validTagsSerializer:IXMLListSerializer = new ValidTagsSerializer(tagsSerializer);
			
			var tags:Array<Dynamic> = new Array();
			tags.push(new Tag("building", "yes"));
			tags.push(new Tag("vasya", "pupkin"));
			tags.push(new Tag("", ""));
			tags.push(new Tag("building", "opana"));
			
			var xml:XMLList = validTagsSerializer.serialize(tags);
			
			var expectedXml:XML = Xml.parse("<tags>
				<tag k="building" v="yes"/>
				<tag k="vasya" v="pupkin"/>
			</tags>");
			
			assertEquals(expectedXml.children(), xml);
		}
	}
