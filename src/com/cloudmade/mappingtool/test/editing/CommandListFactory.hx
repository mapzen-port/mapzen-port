package com.cloudmade.mappingtool.test.editing;

	import com.cloudmade.mappingtool.editing.CommandList;
	import com.cloudmade.mappingtool.editing.ICommandList;
	
	import mx.collections.ArrayList;
	import mx.collections.IList;
	
	class CommandListFactory
	 {
		
		public function new() {
		}
		
		public function create(commandsNum:Int, currentIndex:Int):ICommandList {
			var commands:IList = new ArrayList();
			for (i in 0...commandsNum) {
				commands.addItem(new ElementCommand());
			}
			return new CommandList(commands, currentIndex);
		}
	}
