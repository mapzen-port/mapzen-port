package com.cloudmade.mappingtool.test.editing.errors.externalization;

	import com.cloudmade.mappingtool.commands.editing.IElementCommand;
	import com.cloudmade.mappingtool.commands.editing.events.AddNodeToWayEvent;
	import com.cloudmade.mappingtool.commands.editing.events.EditingEvent;
	import com.cloudmade.mappingtool.commands.editing.events.MoveNodeEvent;
	import com.cloudmade.mappingtool.commands.editing.events.MoveWayEvent;
	import com.cloudmade.mappingtool.commands.editing.events.NodeEditingEvent;
	import com.cloudmade.mappingtool.commands.editing.events.WayEditingEvent;
	import com.cloudmade.mappingtool.commands.editing.factories.EditingCommandFactoryProvider;
	import com.cloudmade.mappingtool.commands.editing.factories.IEditingCommandFactory;
	import com.cloudmade.mappingtool.editing.CommandList;
	import com.cloudmade.mappingtool.editing.ICommandList;
	import com.cloudmade.mappingtool.editing.errors.externalization.CommandStackExternalizer;
	import com.cloudmade.mappingtool.editing.errors.externalization.MapDataOutput;
	import com.cloudmade.mappingtool.editing.errors.externalization.MapExtractor;
	import com.cloudmade.mappingtool.map.geo.LatLong;
	import com.cloudmade.mappingtool.map.model.Map;
	import com.cloudmade.mappingtool.map.model.Node;
	import com.cloudmade.mappingtool.map.model.Way;
	import com.cloudmade.mappingtool.test.assertRecursivelyEquals;
	
	import flash.utils.IExternalizable;
	
	import flexunit.framework.TestCase;
	
	import mx.collections.ArrayList;
	import mx.resources.ResourceManager;

	class CommandStackExternalizerTest extends TestCase {
		
		public function new(?methodName:String = null) {
			super(methodName);
		}
		
		public function testExternalization():Void {
			var map:Map = new DummyMap();
			var factory:IEditingCommandFactory = createFactory(map);
			var editingEvents:Array<Dynamic> = createEditingEvents();
			var commands:Array<Dynamic> = createCommands(factory, editingEvents);
			var commandList:ICommandList = new CommandList(new ArrayList(commands), commands.length);
			var extCommandList:ICommandList = externalizeCommandList(commandList);
			var firstCommand:IExternalizable = cast( commandList.source.getItemAt(0), IExternalizable);
			var extMap:Map = new MapExtractor(new MapDataOutput()).extractMap(firstCommand);
			assertExternalizationEditing(map, commandList, extMap, extCommandList);
		}
		
		function createEditingEvents():Array<Dynamic> {
			var node:Node = new Node("-1", null, new LatLong(10, 10));
			var addNodeEvent:EditingEvent = new NodeEditingEvent(NodeEditingEvent.ADD_NODE, node);
			var moveNodeEvent:MoveNodeEvent = new MoveNodeEvent(MoveNodeEvent.MOVE_NODE, node, new LatLong(20, 20));
			var way:Way = new Way("-2", null, [node]);
			var addWayEvent:WayEditingEvent = new WayEditingEvent(WayEditingEvent.ADD_WAY, way);
			var anotherNode:Node = new Node("-3", null, new LatLong(0, 0));
			var appendNodeEvent:AddNodeToWayEvent = new AddNodeToWayEvent(AddNodeToWayEvent.APPEND_WAY_NODE, anotherNode, way);
			var moveWayEvent:MoveWayEvent = new MoveWayEvent(MoveWayEvent.MOVE_WAY, way, new LatLong(-10, -10));
			var removeWayEvent:WayEditingEvent = new WayEditingEvent(WayEditingEvent.REMOVE_WAY, way);
			return [addNodeEvent, moveNodeEvent, addWayEvent, appendNodeEvent, moveWayEvent, removeWayEvent];
		}
		
		function createFactory(map:Map):IEditingCommandFactory {
			return new EditingCommandFactoryProvider(
				map, new DummyNewElementIdGenerator(),
				ResourceManager.getInstance(), new DummyElementNamer()
			).create();
		}
		
		function createCommands(factory:IEditingCommandFactory, events:Array<Dynamic>):Array<Dynamic> {
			var commands:Array<Dynamic> = new Array();
			for (event in events) {
				commands.push(factory.create(event));
			}
			return commands;
		}
		
		function externalizeCommandList(commandList:ICommandList):ICommandList {
			var externalizer:CommandStackExternalizer = new CommandStackExternalizer();
			var data:String = externalizer.serialize(commandList);
			var extCommandList:ICommandList = externalizer.deserialize(data);
			return extCommandList;
		}
		
		function assertExternalizationEditing(map:Map, commandList:ICommandList, extMap:Map, extCommandList:ICommandList):Void {
			var commands:Array<Dynamic> = commandList.source.toArray();
			var extCommands:Array<Dynamic> = commandList.source.toArray();
			assertRecursivelyEquals(map, extMap);
			for (i in 0...commands.length) {
				IElementCommand(commands[i]).execute();
				IElementCommand(extCommands[i]).execute();
				assertRecursivelyEquals(map, extMap);
			}
		}
	}
