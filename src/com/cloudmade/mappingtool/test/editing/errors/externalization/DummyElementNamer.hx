package com.cloudmade.mappingtool.test.editing.errors.externalization;

	import com.cloudmade.mappingtool.commands.editing.factories.IElementNamer;
	import com.cloudmade.mappingtool.map.model.Element;

	class DummyElementNamer implements IElementNamer {
		
		public function new() {
		}
		
		public function getName(element:Element):String {
			return element.toString();
		}
	}
