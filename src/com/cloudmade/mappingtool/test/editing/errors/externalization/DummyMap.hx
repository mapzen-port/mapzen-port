package com.cloudmade.mappingtool.test.editing.errors.externalization;

	import com.cloudmade.mappingtool.map.model.Map;
	
	import flash.utils.IDataInput;
	import flash.utils.IDataOutput;
	
	/*[RemoteClass(alias="com.cloudmade.mappingtool.test.utils.DummyMap")]*/
	
	class DummyMap extends Map {
		
		public function new() {
			super();
		}
		
		public override function readExternal(input:IDataInput):Void {
		}
		
		public override function writeExternal(output:IDataOutput):Void {
		}
	}
