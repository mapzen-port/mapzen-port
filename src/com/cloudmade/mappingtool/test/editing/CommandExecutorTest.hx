package com.cloudmade.mappingtool.test.editing;

	import com.cloudmade.mappingtool.commands.editing.IElementCommand;
	import com.cloudmade.mappingtool.editing.CommandExecutor;
	import com.cloudmade.mappingtool.editing.ICommandExecutor;
	import com.cloudmade.mappingtool.editing.ICommandList;
	import com.cloudmade.mappingtool.editing.ListTruncator;
	
	import flexunit.framework.TestCase;

	class CommandExecutorTest extends TestCase {
		
		public function new(?methodName:String = null) {
			super(methodName);
		}
		
		public function testCommandPuttingToCommandList():Void {
			var commandList:ICommandList = new CommandListFactory().create(4, 2);
			var listTruncator:ListTruncator = new ListTruncator(commandList.source);
			var commandExecutor:ICommandExecutor = new CommandExecutor(commandList, listTruncator);
			var newCommand:IElementCommand = new ElementCommand();
			commandExecutor.execute(newCommand);
			assertEquals(2, commandList.source.getItemIndex(newCommand));
			assertEquals(3, commandList.currentIndex);
		}
	}
