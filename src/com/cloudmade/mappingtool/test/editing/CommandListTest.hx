package com.cloudmade.mappingtool.test.editing;

	import com.cloudmade.mappingtool.editing.ICommandList;
	
	import flexunit.framework.TestCase;

	class CommandListTest extends TestCase {
		
		public function new(?methodName:String = null) {
			super(methodName);
		}
		
		public function testCurrentIndex():Void {
			var commandList:ICommandList = new CommandListFactory().create(5, 1);
			assertEquals(1, commandList.undoCommands.length);
			assertEquals(4, commandList.redoCommands.length);
			
			commandList.currentIndex += 2;
			assertEquals(3, commandList.undoCommands.length);
			assertEquals(2, commandList.redoCommands.length);
		}
		
		public function testClear():Void {
			var commandList:ICommandList = new CommandListFactory().create(4, 2);
			commandList.clear();
			assertEquals(0, commandList.currentIndex);
			assertEquals(0, commandList.source.length);
		}
	}
