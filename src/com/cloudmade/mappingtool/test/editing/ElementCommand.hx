package com.cloudmade.mappingtool.test.editing;

	import com.cloudmade.mappingtool.commands.editing.IElementCommand;
	import com.cloudmade.mappingtool.map.model.Element;
	
	import flash.utils.IDataInput;
	import flash.utils.IDataOutput;

	class ElementCommand implements IElementCommand {
		
		public var category(getCategory, null) : String ;
		public var description(getDescription, null) : String ;
		public var target(getTarget, null) : Element ;
		public var undone:Bool ;
		public var redone:Bool ;
		
		public function new() {
		
		undone = false;
		redone = false;
		}
		
		public function getCategory():String {
			return null;
		}
		
		public function getTarget():Element {
			return null;
		}
		
		public function getDescription():String {
			return null;
		}
		public function setDescription(value:String):Void {
		}
		
		public function execute():Void {
		}
		
		public function undo():Void {
			undone = true;
		}
		
		public function redo():Void {
			redone = true;
		}
		
		public function readExternal(input:IDataInput):Void {
		}
		
		public function writeExternal(output:IDataOutput):Void {
		}
	}
