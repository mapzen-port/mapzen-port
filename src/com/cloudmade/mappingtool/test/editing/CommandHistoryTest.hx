package com.cloudmade.mappingtool.test.editing;

	import com.cloudmade.mappingtool.editing.CommandHistory;
	import com.cloudmade.mappingtool.editing.ICommandList;
	
	import flexunit.framework.TestCase;

	class CommandHistoryTest extends TestCase {
		
		public function new(?methodName:String = null) {
			super(methodName);
		}
		
		public function testEmptyUndo():Void {
			var commandList:ICommandList = createCommandList(4, 3);
			new CommandHistory(commandList).undo(0);
			assertCommands(commandList.source.toArray(), "undone", false);
			assertEquals(3, commandList.currentIndex);
		}
		
		public function testUndo():Void {
			var commandList:ICommandList = createCommandList(4, 3);
			new CommandHistory(commandList).undo(2);
			assertCommands(commandList.undoCommands, "undone", false);
			assertCommands(commandList.redoCommands.slice(0, 2), "undone", true);
			assertCommands(commandList.redoCommands.slice(2), "undone", false);
			assertEquals(1, commandList.currentIndex);
		}
		
		public function testUndoNormalization():Void {
			var commandList:ICommandList = createCommandList(4, 2);
			new CommandHistory(commandList).undo(3);
			assertEquals(0, commandList.currentIndex);
		}
		
		public function testEmptyRedo():Void {
			var commandList:ICommandList = createCommandList(4, 3);
			new CommandHistory(commandList).redo(0);
			assertCommands(commandList.source.toArray(), "redone", false);
			assertEquals(3, commandList.currentIndex);
		}
		
		public function testRedo():Void {
			var commandList:ICommandList = createCommandList(4, 2);
			new CommandHistory(commandList).redo(2);
			assertCommands(commandList.undoCommands.slice(0, 2), "redone", false);
			assertCommands(commandList.undoCommands.slice(2), "redone", true);
			assertEquals(4, commandList.currentIndex);
		}
		
		public function testRedoNormalization():Void {
			var commandList:ICommandList = createCommandList(4, 2);
			new CommandHistory(commandList).redo(3);
			assertEquals(4, commandList.currentIndex);
		}
		
		function assertCommands(commands:Array<Dynamic>, fieldName:String, fieldValue:Bool):Void {
			for (command in commands) {
				assertEquals(fieldValue, command[fieldName]);
			}
		}
		
		function createCommandList(commandsNum:Int, currentIndex:Int):ICommandList {
			return new CommandListFactory().create(commandsNum, currentIndex);
		}
	}
