package com.cloudmade.mappingtool.test.data;

	import com.cloudmade.mappingtool.data.ArrayIterator;
	import com.cloudmade.mappingtool.data.IIterator;
	
	import flexunit.framework.TestCase;

	class ArrayIteratorTest extends TestCase {
		
		public function new(?methodName:String = null) {
			super(methodName);
		}
		
		public function testGetNext():Void {
			assertEquals(1, createIterator(0).getNext());
		}
		
		public function testTrueHasNext():Void {
			assertTrue(createIterator(1).hasNext());
		}
		
		public function testFalseHasNext():Void {
			assertFalse(createIterator(3).hasNext());
		}
		
		public function testMoveForward():Void {
			var iterator:IIterator = createIterator(1);
			iterator.moveForward();
			assertEquals(3, iterator.getNext());
		}
		
		function createIterator(index:Int):IIterator {
			return new ArrayIterator([1, 2, 3], index);
		}
	}
