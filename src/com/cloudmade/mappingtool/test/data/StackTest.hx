package com.cloudmade.mappingtool.test.data;

	import com.cloudmade.mappingtool.data.IIterator;
	import com.cloudmade.mappingtool.data.IStack;
	import com.cloudmade.mappingtool.data.Stack;
	
	import flexunit.framework.TestCase;

	class StackTest extends TestCase {
		
		public function new(?methodName:String = null) {
			super(methodName);
		}
		
		public function testInsertAfter():Void {
			var stack:IStack = createStack(1);
			stack.insertAfter(0);
			assertEquals(1, stack.getPrevious());
			assertEquals(0, stack.getNext());
		}
		
		public function testInsertBefore():Void {
			var stack:IStack = createStack(1);
			stack.insertBefore(0);
			assertEquals(0, stack.getPrevious());
			assertEquals(2, stack.getNext());
		}
		
		public function testRemoveNext():Void {
			var stack:IStack = createStack(1);
			stack.removeNext();
			assertEquals(1, stack.getPrevious());
			assertEquals(3, stack.getNext());
		}
		
		public function testRemovePrevious():Void {
			var stack:IStack = createStack(1);
			stack.removePrevious();
			assertFalse(stack.hasPrevious());
			assertEquals(2, stack.getNext());
		}
		
		public function testClear():Void {
			var stack:IStack = createStack(1);
			stack.clear();
			assertFalse(stack.hasNext());
			assertFalse(stack.hasPrevious());
		}
		
		public function testGetNextIterator():Void {
			var stack:IStack = createStack(2);
			var iterator:IIterator = stack.getNextIterator();
			assertEquals(3, iterator.getNext());
		}
		
		public function testGetPreviousIterator():Void {
			var stack:IStack = createStack(1);
			var iterator:IIterator = stack.getPreviousIterator();
			assertEquals(1, iterator.getNext());
		}
		
		function createStack(index:Int):IStack {
			return new Stack([1, 2, 3], index);
		}
	}
