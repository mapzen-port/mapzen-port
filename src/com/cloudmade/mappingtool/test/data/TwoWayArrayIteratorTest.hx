package com.cloudmade.mappingtool.test.data;

	import com.cloudmade.mappingtool.data.ITwoWayIterator;
	import com.cloudmade.mappingtool.data.TwoWayArrayIterator;
	
	import flexunit.framework.TestCase;

	class TwoWayArrayIteratorTest extends TestCase {
		
		public function new(?methodName:String = null) {
			super(methodName);
		}
		
		public function testGetPrevious():Void {
			assertEquals(3, createIterator(3).getPrevious());
		}
		
		public function testTrueHasPrevious():Void {
			assertTrue(createIterator(1).hasPrevious());
		}
		
		public function testFalseHasPrevious():Void {
			assertFalse(createIterator(0).hasPrevious());
		}
		
		public function testMoveBackward():Void {
			var iterator:ITwoWayIterator = createIterator(2);
			iterator.moveBackward();
			assertEquals(1, iterator.getPrevious());
		}
		
		function createIterator(index:Int):ITwoWayIterator {
			return new TwoWayArrayIterator([1, 2, 3], index);
		}
	}
