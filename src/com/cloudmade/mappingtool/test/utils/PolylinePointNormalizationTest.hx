package com.cloudmade.mappingtool.test.utils;

	import com.cloudmade.mappingtool.utils.PolylinePointNormalization;
	
	import flash.geom.Point;
	
	import flexunit.framework.TestCase;

	class PolylinePointNormalizationTest extends TestCase {
		
		public function new(?methodName:String = null) {
			super(methodName);
		}
		
		public function testNormalization():Void {
			testPoint(10, -10, 1);
			testPoint(30, 50, 3);
			testPoint(0, 10, 0);
		}
		
		function testPoint(pointX:Int, pointY:Int, index:Int):Void {
			var polylinePoints:Array<Dynamic> = [
				new Point(0, 0),
				new Point(10, 0),
				new Point(20, 10),
				new Point(20, 20)
			];
			var point:Point = new Point(pointX, pointY);
			
			var pointNormalization:PolylinePointNormalization = new PolylinePointNormalization(polylinePoints);
			var pointIndex:Int = pointNormalization.normalizePoint(point);
			assertEquals(index, pointIndex);
		}
	}
