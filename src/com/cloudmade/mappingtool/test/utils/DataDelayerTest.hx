package com.cloudmade.mappingtool.test.utils;

	import com.cloudmade.mappingtool.utils.DataDelayer;
	
	import flash.events.Event;
	import flash.utils.setTimeout;
	
	import flexunit.framework.EventfulTestCase;
	
	import mx.events.FlexEvent;
	
	class DataDelayerTest extends EventfulTestCase {
		
		static var DUMMY_EVENT:Event = new Event("dummy");
		
		var data:Dynamic;
		var delayer:DataDelayer;
		
		public function new(?methodName:String = null) {
			super(methodName);
		}
		
		public function testData1():Void {
			var data:Dynamic = new Object();
			var delayer:DataDelayer = new DataDelayer();
			assertEquals(null, delayer.data);
			delayer.data = data;
			assertEquals(data, delayer.data);
		}
		public function testData2():Void {
			var data:Dynamic = new Object();
			var delayer:DataDelayer = new DataDelayer();
			delayer.blocked = true;
			delayer.data = data;
			assertEquals(null, delayer.data);
			delayer.blocked = false;
			assertEquals(data, delayer.data);
		}
		
		public function testDelay():Void {
			var delayer:DataDelayer = new DataDelayer();
			assertEquals(0, delayer.delay);
			delayer.delay = 0;
			assertEquals(0, delayer.delay);
			delayer.delay = 100;
			assertEquals(100, delayer.delay);
		}
		
		public function testBlocked():Void {
			var delayer:DataDelayer = new DataDelayer();
			assertEquals(false, delayer.blocked);
			delayer.blocked = true;
			assertEquals(true, delayer.blocked);
		}
		
		public function testDataChangeEvent1():Void {
			var data:Dynamic = new Object();
			var delayer:DataDelayer = new DataDelayer();
			listenForEvent(delayer, FlexEvent.DATA_CHANGE, EVENT_EXPECTED);
			delayer.data = data;
			assertEvents();
		}
		public function testDataChangeEvent2():Void {
			var data:Dynamic = new Object();
			var delayer:DataDelayer = new DataDelayer();
			delayer.data = data;
			listenForEvent(delayer, FlexEvent.DATA_CHANGE, EVENT_UNEXPECTED);
			delayer.data = data;
			assertEvents();
		}
		public function testDataChangeEvent3():Void {
			var data:Dynamic = new Object();
			var delayer:DataDelayer = new DataDelayer();
			delayer.blocked = true;
			listenForEvent(delayer, FlexEvent.DATA_CHANGE, EVENT_UNEXPECTED);
			delayer.data = data;
			assertEvents();
		}
		public function testDataChangeEvent4():Void {
			var data:Dynamic = new Object();
			var delayer:DataDelayer = new DataDelayer();
			delayer.blocked = true;
			delayer.data = data;
			listenForEvent(delayer, FlexEvent.DATA_CHANGE, EVENT_EXPECTED);
			delayer.blocked = false;
			assertEvents();
		}
		public function testDataChangeEvent5():Void {
			var data:Dynamic = new Object();
			var delayer:DataDelayer = new DataDelayer();
			delayer.blocked = true;
			delayer.data = data;
			delayer.data = null;
			delayer.data = data;
			listenForEvent(delayer, FlexEvent.DATA_CHANGE, EVENT_EXPECTED);
			delayer.blocked = false;
			assertEvents();
		}
		public function testDataChangeEvent6():Void {
			var data:Dynamic = new Object();
			var delayer:DataDelayer = new DataDelayer();
			delayer.blocked = true;
			listenForEvent(delayer, FlexEvent.DATA_CHANGE, EVENT_UNEXPECTED);
			delayer.blocked = false;
			assertEvents();
		}
		public function testDataChangeEvent7():Void {
			data = null;
			delayer = new DataDelayer();
			delayer.delay = 1000;
			listenForEvent(delayer, FlexEvent.DATA_CHANGE, EVENT_UNEXPECTED);
			setTimeout(addAsync(assertDelayerData, 1000), 900, DUMMY_EVENT);
			delayer.data = new Object();
		}
		public function testDataChangeEvent8():Void {
			data = new Object();
			delayer = new DataDelayer();
			delayer.delay = 1000;
			listenForEvent(delayer, FlexEvent.DATA_CHANGE, EVENT_EXPECTED);
			setTimeout(addAsync(assertDelayerData, 1200), 1100, DUMMY_EVENT);
			delayer.data = data;
		}
		public function testDataChangeEvent9():Void {
			data = null;
			delayer = new DataDelayer();
			delayer.delay = 1000;
			listenForEvent(delayer, FlexEvent.DATA_CHANGE, EVENT_UNEXPECTED);
			setTimeout(setDelayerBlocked, 500, true);
			setTimeout(setDelayerBlocked, 1000, false);
			setTimeout(addAsync(assertDelayerData, 1700), 1600, DUMMY_EVENT);
			delayer.data = new Object();
		}
		public function testDataChangeEvent10():Void {
			data = new Object();
			delayer = new DataDelayer();
			delayer.delay = 1000;
			listenForEvent(delayer, FlexEvent.DATA_CHANGE, EVENT_EXPECTED);
			setTimeout(setDelayerBlocked, 500, true);
			setTimeout(setDelayerBlocked, 1000, false);
			setTimeout(addAsync(assertDelayerData, 2200), 2100, DUMMY_EVENT);
			delayer.data = data;
		}
		
		function setDelayerBlocked(blocked:Bool):Void {
			delayer.blocked = blocked;
		}
		
		function assertDelayerData(event:Event):Void {
			assertEvents();
			assertEquals(data, delayer.data);
		}
	}
