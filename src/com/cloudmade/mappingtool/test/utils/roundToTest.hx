package com.cloudmade.mappingtool.test.utils;

	import com.cloudmade.mappingtool.utils.roundTo;
	
	import flexunit.framework.TestCase;

	class roundToTest extends TestCase {
		
		public function new(?methodName:String = null) {
			super(methodName);
		}
		
		public function testRoundTo():Void {
			assertEquals(0.123, roundTo(0.123456789, 3));
			assertEquals(123457000, roundTo(123456789, -3));
		}
	}
