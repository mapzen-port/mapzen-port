package com.cloudmade.mappingtool.test.utils;

	import com.cloudmade.mappingtool.utils.GeometryUtils;
	
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	import flexunit.framework.TestCase;

	class GeometryUtilsTest extends TestCase {
		
		public function new(?methodName:String = null) {
			super(methodName);
		}
		
		public function testCalculateIntersection1():Void {
			var startPoint1:Point = new Point(1, 1);
			var endPoint1:Point = new Point(5, 5);
			var startPoint2:Point = new Point(3, 4);
			var endPoint2:Point = new Point(8, 4);
			var intersectionPoint:Point = GeometryUtils.calculateIntersection(startPoint1, endPoint1, startPoint2, endPoint2);
			assertEquals(4, intersectionPoint.x);
			assertEquals(4, intersectionPoint.y);
		}
		public function testCalculateIntersection2():Void {
			var startPoint1:Point = new Point(1, 1);
			var endPoint1:Point = new Point(5, 1);
			var startPoint2:Point = new Point(6, 2);
			var endPoint2:Point = new Point(6, 4);
			var intersectionPoint:Point = GeometryUtils.calculateIntersection(startPoint1, endPoint1, startPoint2, endPoint2);
			assertEquals(6, intersectionPoint.x);
			assertEquals(1, intersectionPoint.y);
		}
		public function testCalculateIntersection3():Void {
			var startPoint1:Point = new Point(1, 1);
			var endPoint1:Point = new Point(5, 1);
			var startPoint2:Point = new Point(3, 4);
			var endPoint2:Point = new Point(1, 4);
			var intersectionPoint:Point = GeometryUtils.calculateIntersection(startPoint1, endPoint1, startPoint2, endPoint2);
			assertEquals(intersectionPoint, null);
		}
		public function testCalculateIntersection4():Void {			
			var startPoint1:Point = new Point(2, 2);
			var endPoint1:Point = new Point(2, 2);
			var startPoint2:Point = new Point(2, 4);
			var endPoint2:Point = new Point(2, 6);
			var intersectionPoint:Point = GeometryUtils.calculateIntersection(startPoint1, endPoint1, startPoint2, endPoint2);
			assertEquals(intersectionPoint, null);
		}
		
		public function testClipLineSegment1():Void {
			var bounds:Rectangle = new Rectangle(20, 20, 60, 40);
			var point1:Point = new Point(0, 40);
			var point2:Point = new Point(40, 40);
			var result:UInt = GeometryUtils.clipLineSegment(point1, point2, bounds);
			assertEquals(GeometryUtils.INTERSECTING, result);
			assertEquals(20, point1.x);
			assertEquals(40, point1.y);
			assertEquals(40, point2.x);
			assertEquals(40, point2.y);
		}
		public function testClipLineSegment2():Void {
			var bounds:Rectangle = new Rectangle(20, 20, 60, 40);
			var point1:Point = new Point(0, 40);
			var point2:Point = new Point(60, 10);
			var result:UInt = GeometryUtils.clipLineSegment(point1, point2, bounds);
			assertEquals(GeometryUtils.INTERSECTING, result);
			assertEquals(20, point1.x);
			assertEquals(30, point1.y);
			assertEquals(40, point2.x);
			assertEquals(20, point2.y);
		}
		public function testClipLineSegment3():Void {
			var bounds:Rectangle = new Rectangle(20, 20, 60, 40);
			var point1:Point = new Point(50, 40);
			var point2:Point = new Point(70, 50);
			var result:UInt = GeometryUtils.clipLineSegment(point1, point2, bounds);
			assertEquals(GeometryUtils.INLYING, result);
			assertEquals(50, point1.x);
			assertEquals(40, point1.y);
			assertEquals(70, point2.x);
			assertEquals(50, point2.y);
		}
		public function testClipLineSegment4():Void {
			var bounds:Rectangle = new Rectangle(20, 20, 60, 40);
			var point1:Point = new Point(40, 60);
			var point2:Point = new Point(40, 80);
			var result:UInt = GeometryUtils.clipLineSegment(point1, point2, bounds);
			assertEquals(GeometryUtils.CONTACTING, result);
			assertEquals(40, point1.x);
			assertEquals(60, point1.y);
			assertEquals(40, point2.x);
			assertEquals(60, point2.y);
		}
		public function testClipLineSegment5():Void {
			var bounds:Rectangle = new Rectangle(20, 20, 60, 40);
			var point1:Point = new Point(60, 70);
			var point2:Point = new Point(80, 70);
			var result:UInt = GeometryUtils.clipLineSegment(point1, point2, bounds);
			assertEquals(GeometryUtils.OUTLYING, result);
			assertEquals(60, point1.x);
			assertEquals(70, point1.y);
			assertEquals(80, point2.x);
			assertEquals(70, point2.y);
		}
		
		public function testClipPolygonIntersecting1():Void {
			var bounds:Rectangle = new Rectangle(100, 100, 400, 400);
			var points:Array<Dynamic> = [
				new Point(0, 300),
				new Point(300, 0),
				new Point(600, 0),
				new Point(500, 300),
				new Point(0, 300)
			];
			var result:UInt = GeometryUtils.clipPolygon(points, bounds);
			
			assertEquals(GeometryUtils.INTERSECTING, result);
			
			assertEquals(5, points.length);
			
			assertEquals(100, points[0].x);
			assertEquals(200, points[0].y);
			
			assertEquals(200, points[1].x);
			assertEquals(100, points[1].y);
			
			assertEquals(500, points[2].x);
			assertEquals(100, points[2].y);
			
			assertEquals(500, points[3].x);
			assertEquals(300, points[3].y);
			
			assertEquals(100, points[4].x);
			assertEquals(300, points[4].y);
		}
		public function testClipPolygonOutlying1():Void {
			var bounds:Rectangle = new Rectangle(100, 100, 400, 400);
			
			var points:Array<Dynamic> = [
				new Point(500, 300),
				new Point(600, 300),
				new Point(500, 400)
			];
			var result:UInt = GeometryUtils.clipPolygon(points, bounds);
			
			assertEquals(GeometryUtils.OUTLYING, result);
		}
		public function testClipPolygonInlying1():Void {
			var bounds:Rectangle = new Rectangle(100, 100, 400, 400);
			
			var points:Array<Dynamic> = [
				new Point(200, 300),
				new Point(300, 300),
				new Point(200, 400)
			];
			var result:UInt = GeometryUtils.clipPolygon(points, bounds);
			
			assertEquals(GeometryUtils.INLYING, result);
			
			assertEquals(3, points.length);
			
			assertEquals(200, points[0].x);
			assertEquals(300, points[0].y);
			
			assertEquals(300, points[1].x);
			assertEquals(300, points[1].y);
			
			assertEquals(200, points[2].x);
			assertEquals(400, points[2].y);
		}
	}
