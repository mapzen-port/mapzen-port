package com.cloudmade.mappingtool.test.utils;

	import com.cloudmade.mappingtool.utils.PolylinePointProjection;
	
	import flash.geom.Point;
	
	import flexunit.framework.TestCase;

	class PolylinePointProjectionTest extends TestCase {
		
		public function new(?methodName:String = null) {
			super(methodName);
		}
		
		public function testProjection():Void {
			var projection:PolylinePointProjection = createPolylinePointProjection(new Point(20, 20));
			assertEquals(2, projection.projectionIndex);
			assertTrue(projection.projectionPoint.equals(new Point(30, 10)));
		}
		
		public function testInvariableSourcePoint():Void {
			var point:Point = new Point(20, 20);
			var projection:PolylinePointProjection = createPolylinePointProjection(point);
			assertTrue(point.equals(new Point(20, 20)));
		}
		
		public function testHighPointProjection():Void {
			var projection:PolylinePointProjection = createPolylinePointProjection(new Point(50, 1000));
			assertEquals(3, projection.projectionIndex);
			// Eliminate rounding error:
			projection.projectionPoint.x = Math.round(projection.projectionPoint.x);
			assertTrue(projection.projectionPoint.equals(new Point(50, 20)));
		}
		
		/*public function testStartEdgeProjection():void {
			var projection:PolylinePointProjection = createPolylinePointProjection(new Point(-10, 10));
			assertStrictlyEquals(0, projection.projectionIndex);
			assertTrue(projection.projectionPoint.equals(new Point(-10, 0)));
		}
		
		public function testEndEdgeProjection():void {
			var projection:PolylinePointProjection = createPolylinePointProjection(new Point(80, 0));
			assertEquals(4, projection.projectionIndex);
			assertTrue(projection.projectionPoint.equals(new Point(80, 20)));
		}*/
		
		function createPolylinePointProjection(point:Point):PolylinePointProjection {
			var polylinePoints:Array<Dynamic> = [new Point(0, 0), new Point(20, 0), new Point(40, 20), new Point(60, 20)];
			var projection:PolylinePointProjection = new PolylinePointProjection(polylinePoints);
			projection.projectPoint(point);
			return projection;
		}
	}
