package com.cloudmade.mappingtool.test.config.conditions;

	import com.cloudmade.mappingtool.config.conditions.ICondition;

	class DummyCondition implements ICondition {
		
		public var type:String;
		public var parameters:Dynamic;
		
		public function new(type:String, parameters:Dynamic) {
			this.type;
			this.parameters = parameters;
		}
	}
