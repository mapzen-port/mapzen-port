package com.cloudmade.mappingtool.test.config.conditions.filters;

	import com.cloudmade.mappingtool.config.conditions.ICondition;
	import com.cloudmade.mappingtool.config.conditions.IConditionConstructor;
	import com.cloudmade.mappingtool.test.config.conditions.DummyCondition;

	class DummyConditionConstructor implements IConditionConstructor {
		
		public function new() {
		}

		public function create(type:String, parameters:Dynamic):ICondition
		{
			return new DummyCondition(type, parameters);
		}
		
	}
