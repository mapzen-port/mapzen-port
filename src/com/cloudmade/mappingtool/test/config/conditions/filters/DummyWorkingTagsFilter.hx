package com.cloudmade.mappingtool.test.config.conditions.filters;

	import com.cloudmade.mappingtool.config.conditions.filters.ITagsFilter;
	import com.cloudmade.mappingtool.map.model.ITag;
	import com.cloudmade.mappingtool.map.model.Tag;

	class DummyWorkingTagsFilter implements ITagsFilter {
		
		public function new()
		{
		}

		public function filterTags(tags:Array<Dynamic>):Array<Dynamic>
		{
			var dummyTag:ITag = new Tag("dummy", "dummy");
			return [dummyTag];
		}
		
	}
