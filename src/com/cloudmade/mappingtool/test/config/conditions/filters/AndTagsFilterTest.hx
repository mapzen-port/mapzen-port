package com.cloudmade.mappingtool.test.config.conditions.filters;

	import com.cloudmade.mappingtool.config.conditions.filters.AndTagsFilter;
	import com.cloudmade.mappingtool.config.conditions.filters.ITagsFilter;
	
	import flexunit.framework.TestCase;

	class AndTagsFilterTest extends TestCase {
		
		public function new(?methodName:String=null)
		{
			super(methodName);
		}
		
		public function testSuccessfulFiltration():Void {
			var workingFilter:ITagsFilter = new DummyWorkingTagsFilter();
			var andFilter:ITagsFilter = new AndTagsFilter([workingFilter, workingFilter, workingFilter]);
			var filteredTags:Array<Dynamic> = andFilter.filterTags(new Array());
			
			assertNotNull(filteredTags);
			assertEquals(3, filteredTags.length);
		}
		
		public function testFailingFiltration():Void {
			var failingFilter:ITagsFilter = new DummyFailingTagsFilter();
			var andFilter:ITagsFilter = new AndTagsFilter([failingFilter, failingFilter, failingFilter]);
			var filteredTags:Array<Dynamic> = andFilter.filterTags(new Array());
			
			assertNull(filteredTags);
		}
	}
