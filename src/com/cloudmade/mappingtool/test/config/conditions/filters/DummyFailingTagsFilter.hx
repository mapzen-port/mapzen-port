package com.cloudmade.mappingtool.test.config.conditions.filters;

	import com.cloudmade.mappingtool.config.conditions.filters.ITagsFilter;

	class DummyFailingTagsFilter implements ITagsFilter {
		
		public function new()
		{
		}

		public function filterTags(tags:Array<Dynamic>):Array<Dynamic>
		{
			return null;
		}
	}
