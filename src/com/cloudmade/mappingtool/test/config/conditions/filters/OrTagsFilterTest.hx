package com.cloudmade.mappingtool.test.config.conditions.filters;

	import com.cloudmade.mappingtool.config.conditions.filters.ITagsFilter;
	import com.cloudmade.mappingtool.config.conditions.filters.OrTagsFilter;
	
	import flexunit.framework.TestCase;

	class OrTagsFilterTest extends TestCase {
		
		public function new(?methodName:String=null)
		{
			super(methodName);
		}
		
		var workingFilter:ITagsFilter ;
		var failingFilter:ITagsFilter ;
		
		public function testSuccessfulFiltration():Void {
			var filter:ITagsFilter = new OrTagsFilter([failingFilter, workingFilter, failingFilter]);
			var filteredTags:Array<Dynamic> = filter.filterTags(new Array());
			
			assertNotNull(filteredTags);
			assertEquals(1, filteredTags.length);
		}
		
		public function testFailingFiltration():Void {
			var filter:ITagsFilter = new OrTagsFilter([failingFilter, failingFilter, failingFilter]);
			var filteredTags:Array<Dynamic> = filter.filterTags(new Array());
			
			assertNull(filteredTags);
		}
	}
