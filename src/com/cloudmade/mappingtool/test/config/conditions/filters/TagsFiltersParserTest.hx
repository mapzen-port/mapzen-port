package com.cloudmade.mappingtool.test.config.conditions.filters;

	import com.cloudmade.mappingtool.config.parsers.IXMLListParser;
	import com.cloudmade.mappingtool.config.parsers.IXMLParser;
	import com.cloudmade.mappingtool.config.parsers.TypeTagParser;
	import com.cloudmade.mappingtool.config.conditions.ConditionsParser;
	import com.cloudmade.mappingtool.config.conditions.IConditionConstructor;
	import com.cloudmade.mappingtool.config.conditions.filters.AndTagsFilter;
	import com.cloudmade.mappingtool.config.conditions.filters.ITagsFilter;
	import com.cloudmade.mappingtool.config.conditions.filters.TagsFilterConstructor;
	import com.cloudmade.mappingtool.map.model.Tag;
	
	import flexunit.framework.TestCase;

	class TagsFiltersParserTest extends TestCase {
		
		public function new(?methodName:String=null)
		{
			super(methodName);
		}
		
		public function testParseTagsFilters():Void {
			var xml:XML = Xml.parse("<type><or>
					<and>
						<tag key="building" value="*" />
						<or>
							<tag key="amenity" value="restaurant" />
							<tag key="amenity" value="restaurant;pub" />
							<tag key="amenity" value="pub;restaurant" />
						</or>
					</and>
					<tag key="restaurant" value="*" />
				</or></type>");
			
			var tagParser:IXMLParser = new TypeTagParser();
			var tagsFilterConstructor:IConditionConstructor = new TagsFilterConstructor();
			var parser:IXMLListParser = new ConditionsParser(tagParser, tagsFilterConstructor);
			
			var filter:ITagsFilter = new AndTagsFilter(parser.parse(xml.children()));
			
			var tags:Array<Dynamic> = new Array();
			tags.push(new Tag("building", "true"));
			tags.push(new Tag("amenity", "restaurant"));
			tags.push(new Tag("amenity", "restaurant;pub"));
			tags.push(new Tag("vasya", "pupkin"));
			tags.push(new Tag("opa", "opa"));
			tags.push(new Tag("restaurant", "krutoy"));
			
			var filteredTags:Array<Dynamic> = filter.filterTags(tags);
			
			assertEquals(4, filteredTags.length);
		}
		/*
		private function createTagsFilter():ITagsFilter {
			var filter1:ITagsFilter = new IsTagsFilter(new Tag("amenity", "restaurant"));
			var filter2:ITagsFilter = new IsTagsFilter(new Tag("amenity", "restaurant;pub"));
			var filter3:ITagsFilter = new IsTagsFilter(new Tag("amenity", "pub;restaurant"));
			var filter4:ITagsFilter = new OrTagsFilter([filter1, filter2, filter3]);
			var filter5:ITagsFilter = new IsTagsFilter(new AnyValueTag("building"));
			var filter6:ITagsFilter = new AndTagsFilter([filter4, filter5]);
			var filter7:ITagsFilter = new IsTagsFilter(new AnyValueTag("restaurant"));
			return new OrTagsFilter([filter6, filter7]);
		}
		*/
	}
