package com.cloudmade.mappingtool.test.config.conditions.filters;

	import com.cloudmade.mappingtool.config.AnyValueTag;
	import com.cloudmade.mappingtool.config.conditions.filters.ITagsFilter;
	import com.cloudmade.mappingtool.config.conditions.filters.IsTagsFilter;
	import com.cloudmade.mappingtool.map.model.Tag;
	
	import flexunit.framework.TestCase;

	class IsTagsFilterTest extends TestCase {
		
		public function new(?methodName:String=null)
		{
			super(methodName);
		}

		public function testSuccessfulFiltration():Void {
			var filter:ITagsFilter = new IsTagsFilter(new AnyValueTag("building"));
			
			var tags:Array<Dynamic> = new Array();
			tags.push(new Tag("building", "yes"));
			tags.push(new Tag("building", "true"));
			tags.push(new Tag("bla", "bla"));
			
			var filteredTags:Array<Dynamic> = filter.filterTags(tags);
			
			assertNotNull(filteredTags);
			assertEquals(2, filteredTags.length);
		}
		
		public function testFailingFiltration():Void {
			var filter:ITagsFilter = new IsTagsFilter(new Tag("ble", "ble"));
			
			var tags:Array<Dynamic> = new Array();
			tags.push(new Tag("bla", "bla"));
			tags.push(new Tag("blo", "bla"));
			tags.push(new Tag("bla", "ble"));
			
			var filteredTags:Array<Dynamic> = filter.filterTags(tags);
			
			assertNull(filteredTags);
		}		
	}
