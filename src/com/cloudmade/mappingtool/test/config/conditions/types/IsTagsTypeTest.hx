package com.cloudmade.mappingtool.test.config.conditions.types;

	import com.cloudmade.mappingtool.config.AnyValueTag;
	import com.cloudmade.mappingtool.config.conditions.types.ITagsType;
	import com.cloudmade.mappingtool.config.conditions.types.IsTagsType;
	import com.cloudmade.mappingtool.map.model.Tag;
	
	import flexunit.framework.TestCase;

	class IsTagsTypeTest extends TestCase {
		
		public function new(?methodName:String=null)
		{
			super(methodName);
		}
		
		var anyTagsType:ITagsType ;
		var nullTagsType:ITagsType ;
		
		public function testCorrespondsTo():Void {
			var tagsType:ITagsType = new IsTagsType(new AnyValueTag("building"));
			
			var tags:Array<Dynamic> = new Array();
			tags.push(new Tag("foo", "bar"));
			tags.push(new Tag("building", "yes"));
			
			assertTrue(tagsType.correspondsTo(tags));
		}
		
		public function testNotCorrespondsTo():Void {
			var tagsType:ITagsType = new IsTagsType(new AnyValueTag("building"));
			
			var tags:Array<Dynamic> = new Array();
			tags.push(new Tag("foo", "bar"));
			tags.push(new Tag("bla", "bla"));
			
			assertTrue(!tagsType.correspondsTo(tags));
		}
		
	}
