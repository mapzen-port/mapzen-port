package com.cloudmade.mappingtool.test.config.conditions.types;

	import com.cloudmade.mappingtool.config.conditions.types.AndTagsType;
	import com.cloudmade.mappingtool.config.conditions.types.ITagsType;
	
	import flexunit.framework.TestCase;

	class AndTagsTypeTest extends TestCase {
		
		public function new(?methodName:String=null)
		{
			super(methodName);
		}
		
		var anyTagsType:ITagsType ;
		var nullTagsType:ITagsType ;
				
		public function testCorrespondsTo():Void
		{
			var tagsType:ITagsType = new AndTagsType([anyTagsType, anyTagsType, anyTagsType]);
			
			assertTrue(tagsType.correspondsTo(new Array()));
		}
		
		public function testNotCorrespondsTo():Void
		{
			var tagsType:ITagsType = new AndTagsType([anyTagsType, nullTagsType, anyTagsType]);
			
			assertTrue(!tagsType.correspondsTo(new Array()));
		}
		
	}
