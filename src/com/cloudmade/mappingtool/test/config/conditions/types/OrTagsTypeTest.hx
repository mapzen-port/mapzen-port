package com.cloudmade.mappingtool.test.config.conditions.types;

	import com.cloudmade.mappingtool.config.conditions.types.ITagsType;
	import com.cloudmade.mappingtool.config.conditions.types.OrTagsType;
	
	import flexunit.framework.TestCase;

	class OrTagsTypeTest extends TestCase {
		
		public function new(?methodName:String=null)
		{
			super(methodName);
		}
		
		var anyTagsType:ITagsType ;
		var nullTagsType:ITagsType ;
				
		public function testCorrespondsTo():Void
		{
			var tagsType:ITagsType = new OrTagsType([nullTagsType, anyTagsType, nullTagsType]);
			
			assertTrue(tagsType.correspondsTo(new Array()));
		}
		
		public function testNotCorrespondsTo():Void
		{
			var tagsType:ITagsType = new OrTagsType([nullTagsType, nullTagsType, nullTagsType]);
			
			assertTrue(!tagsType.correspondsTo(new Array()));
		}
	}
