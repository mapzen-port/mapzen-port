package com.cloudmade.mappingtool.test.config.conditions.types;

	import com.cloudmade.mappingtool.config.conditions.ConditionsParser;
	import com.cloudmade.mappingtool.config.conditions.IConditionConstructor;
	import com.cloudmade.mappingtool.config.conditions.types.AndTagsType;
	import com.cloudmade.mappingtool.config.conditions.types.ITagsType;
	import com.cloudmade.mappingtool.config.conditions.types.TagsTypeConstructor;
	import com.cloudmade.mappingtool.config.parsers.IXMLListParser;
	import com.cloudmade.mappingtool.config.parsers.IXMLParser;
	import com.cloudmade.mappingtool.config.parsers.TypeTagParser;
	import com.cloudmade.mappingtool.map.model.Tag;
	
	import flexunit.framework.TestCase;

	class TagsTypeParserTest extends TestCase {
		
		public function new(?methodName:String=null)
		{
			super(methodName);
		}
	
		public function testParseTypeTags():Void {
			var xml:XML = Xml.parse("<type><or>
					<and>
						<tag key="building" value="*" />
						<or>
							<tag key="amenity" value="restaurant" />
							<tag key="amenity" value="restaurant;pub" />
							<tag key="amenity" value="pub;restaurant" />
						</or>
					</and>
					<tag key="restaurant" value="*" />
				</or></type>");
			
			var tagParser:IXMLParser = new TypeTagParser();
			var tagsTypeConstructor:IConditionConstructor = new TagsTypeConstructor();
			var parser:IXMLListParser = new ConditionsParser(tagParser, tagsTypeConstructor);
			
			var tagsType:ITagsType = new AndTagsType(parser.parse(xml.children()));
			
			var tags:Array<Dynamic> = new Array();
			tags.push(new Tag("building", "true"));
			tags.push(new Tag("vasya", "pupkin"));
			tags.push(new Tag("amenity", "restaurant;pub"));
			
			assertTrue(tagsType.correspondsTo(tags));
			
			tags.pop();
			
			assertTrue(!tagsType.correspondsTo(tags));
		}	
	}
