package com.cloudmade.mappingtool.test.config.conditions.types;

	import com.cloudmade.mappingtool.config.conditions.types.ITagsType;
	import com.cloudmade.mappingtool.config.conditions.types.NotTagsType;
	
	import flexunit.framework.TestCase;

	class NotTagsTypeTest extends TestCase {
		
		public function new(?methodName:String=null)
		{
			super(methodName);
		}
		
		var anyTagsType:ITagsType ;
		var nullTagsType:ITagsType ;
				
		public function testCorrespondsTo():Void
		{
			var tagsType:ITagsType = new NotTagsType(nullTagsType);
			
			assertTrue(tagsType.correspondsTo(new Array()));
		}
		
		public function testNotCorrespondsTo():Void
		{
			var tagsType:ITagsType = new NotTagsType(anyTagsType);
			
			assertTrue(!tagsType.correspondsTo(new Array()));
		}
	}
