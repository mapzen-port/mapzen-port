package com.cloudmade.mappingtool.test.config.conditions.types;

	import com.cloudmade.mappingtool.config.conditions.types.ITagsType;
	
	class AnyTagsType implements ITagsType {
		
		public function new()
		{
		}

		public function correspondsTo(tags:Array<Dynamic>):Bool
		{
			return true;
		}
		
	}
