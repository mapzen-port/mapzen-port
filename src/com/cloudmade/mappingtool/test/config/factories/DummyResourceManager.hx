package com.cloudmade.mappingtool.test.config.factories;

	import flash.events.Event;
	import flash.events.IEventDispatcher;
	import flash.system.ApplicationDomain;
	import flash.system.SecurityDomain;
	
	import mx.resources.IResourceBundle;
	import mx.resources.IResourceManager;

	class DummyResourceManager implements IResourceManager {
		
		public var localeChain(getLocaleChain, setLocaleChain) : Array<Dynamic>;
		public function new() {
		}
		
		public function addEventListener(type:String, listener:Dynamic, ?useCapture:Bool = false, ?priority:Int = 0, ?useWeakReference:Bool = false):Void {
		}
		
		public function getLocaleChain():Array<Dynamic>{
			return null;
		}
		
		public function setLocaleChain(value:Array<Dynamic>):Array<Dynamic>{
			return value;
	}
		
		public function removeEventListener(type:String, listener:Dynamic, ?useCapture:Bool = false):Void {
		}
		
		public function dispatchEvent(event:Event):Bool {
			return false;
		}
		
		public function hasEventListener(type:String):Bool {
			return false;
		}
		
		public function loadResourceModule(url:String, ?update:Bool = true, ?applicationDomain:ApplicationDomain = null, ?securityDomain:SecurityDomain = null):IEventDispatcher {
			return null;
		}
		
		public function unloadResourceModule(url:String, ?update:Bool = true):Void {
		}
		
		public function willTrigger(type:String):Bool {
			return false;
		}
		
		public function addResourceBundle(resourceBundle:IResourceBundle):Void {
		}
		
		public function removeResourceBundle(locale:String, bundleName:String):Void {
		}
		
		public function removeResourceBundlesForLocale(locale:String):Void {
		}
		
		public function update():Void {
		}
		
		public function getLocales():Array<Dynamic> {
			return null;
		}
		
		public function getPreferredLocaleChain():Array<Dynamic> {
			return null;
		}
		
		public function getBundleNamesForLocale(locale:String):Array<Dynamic> {
			return null;
		}
		
		public function getResourceBundle(locale:String, bundleName:String):IResourceBundle {
			return null;
		}
		
		public function findResourceBundleWithResource(bundleName:String, resourceName:String):IResourceBundle {
			return null;
		}
		
		public function getObject(bundleName:String, resourceName:String, ?locale:String = null):Dynamic {
		
		}
		
		public function getString(bundleName:String, resourceName:String, ?parameters:Array<Dynamic> = null, ?locale:String = null):String {
			return "Resource";
		}
		
		public function getStringArray(bundleName:String, resourceName:String, ?locale:String = null):Array<Dynamic> {
			return null;
		}
		
		public function getNumber(bundleName:String, resourceName:String, ?locale:String = null):Int {
			return 0;
		}
		
		public function getInt(bundleName:String, resourceName:String, ?locale:String = null):Int {
			return 0;
		}
		
		public function getUint(bundleName:String, resourceName:String, ?locale:String = null):UInt {
			return 0;
		}
		
		public function getBoolean(bundleName:String, resourceName:String, ?locale:String = null):Bool {
			return false;
		}
		
		public function getClass(bundleName:String, resourceName:String, ?locale:String = null):Class<Dynamic> {
			return null;
		}
		
		public function installCompiledResourceBundles(applicationDomain:ApplicationDomain, locales:Array<Dynamic>, bundleNames:Array<Dynamic>):Void {
		}
		
		public function initializeLocaleChain(compiledLocales:Array<Dynamic>):Void {
		}
	}
