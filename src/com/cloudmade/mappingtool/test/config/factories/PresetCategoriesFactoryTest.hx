package com.cloudmade.mappingtool.test.config.factories;

	import com.cloudmade.mappingtool.config.factories.IPresetsFactory;
	import com.cloudmade.mappingtool.config.factories.PresetCategoriesFactory;
	import com.cloudmade.mappingtool.config.factories.resources.IPresetsResources;
	
	import flexunit.framework.TestCase;
	
	import mx.resources.IResourceManager;

	class PresetCategoriesFactoryTest extends TestCase {
		
		public function new(?methodName:String = null) {
			super(methodName);
		}
		
		public function testParsing():Void {
			var resourceManager:IResourceManager = new DummyResourceManager();
			var presetCategoriesFactory:IPresetsFactory = new PresetCategoriesFactory(resourceManager);
			var presetsResources:IPresetsResources = new DummyPresetsResources();
			var presetCategories:Array<Dynamic> = presetCategoriesFactory.createPresets(presetsResources);
			assertEquals(1, presetCategories.length);
			assertEquals("Resource", presetCategories[0].name);
		}
	}
