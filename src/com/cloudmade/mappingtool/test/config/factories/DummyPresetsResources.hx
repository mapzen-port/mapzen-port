package com.cloudmade.mappingtool.test.config.factories;

	import com.cloudmade.mappingtool.config.factories.resources.IPresetsResources;

	class DummyPresetsResources implements IPresetsResources {
		
		public var categoryBundleName(getCategoryBundleName, null) : String ;
		public var presetBundleName(getPresetBundleName, null) : String ;
		public var presetIconBundleName(getPresetIconBundleName, null) : String ;
		public var presetsXml(getPresetsXml, null) : XMLList ;
		public var typesXml(getTypesXml, null) : XMLList ;
		public function new() {
		}
		
		public function getTypesXml():XMLList {
			var xml:XML = Xml.parse("<types><type name="type"><tag key="building" value="yes" /></type></types>")
			return xml.type;
		}
		
		public function getPresetsXml():XMLList {
			var xml:XML = Xml.parse("<categories><category name="category"/></categories>")
			return xml.category;
		}
		
		public function getPresetBundleName():String {
			return null;
		}
		
		public function getPresetIconBundleName():String {
			return null;
		}
		
		public function getCategoryBundleName():String {
			return null;
		}
	}
