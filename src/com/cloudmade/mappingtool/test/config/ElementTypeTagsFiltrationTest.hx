package com.cloudmade.mappingtool.test.config;

	import com.cloudmade.mappingtool.config.AnyValueTag;
	import com.cloudmade.mappingtool.config.conditions.filters.AnyTagsFilter;
	import com.cloudmade.mappingtool.config.conditions.filters.TagsFilter;
	import com.cloudmade.mappingtool.config.conditions.filters.ITagsFilter;
	import com.cloudmade.mappingtool.map.model.ITag;
	import com.cloudmade.mappingtool.map.model.Tag;
	import com.cloudmade.mappingtool.test.assertRecursivelyEquals;
	
	import flexunit.framework.TestCase;

	class ElementTypeTagsFiltrationTest extends TestCase {
		
		public function new(?methodName:String = null) {
			super(methodName);
		}
		
		public function testSuccessfulFiltration():Void {
			var firstTypeTag:ITag = new Tag("key", "first");
			var secondTypeTag:ITag = new Tag("key", "second");
			var typeTags:Array<Dynamic> = [firstTypeTag, secondTypeTag];
			var filter:ITagsFilter = new TagsFilter(typeTags);
			var firstTag:ITag = new Tag("key", "first");
			var secondTag:ITag = new Tag("key", "second");
			var thirdTag:ITag = new Tag("key", "third");
			var tags:Array<Dynamic> = [firstTag, secondTag, thirdTag];
			var filteredTags:Array<Dynamic> = filter.filterTags(tags);
			assertEquals(2, filteredTags.length);
			assertEquals(firstTag, filteredTags[0]);
			assertEquals(secondTag, filteredTags[1]);
		}
		
		public function testUnsuccessfulFiltration():Void {
			var firstTypeTag:ITag = new Tag("key", "first");
			var secondTypeTag:ITag = new Tag("key", "second");
			var typeTags:Array<Dynamic> = [firstTypeTag, secondTypeTag];
			var filter:ITagsFilter = new TagsFilter(typeTags);
			var firstTag:ITag = new Tag("key", "1");
			var secondTag:ITag = new Tag("key", "2");
			var thirdTag:ITag = new Tag("key", "3");
			var tags:Array<Dynamic> = [firstTag, secondTag, thirdTag];
			var filteredTags:Array<Dynamic> = filter.filterTags(tags);
			assertEquals(0, filteredTags.length);
		}
		
		public function testAnyTagFiltration():Void {
			var filter:ITagsFilter = new AnyTagsFilter();
			var firstTag:ITag = new Tag("key", "first");
			var secondTag:ITag = new Tag("key", "second");
			var thirdTag:ITag = new Tag("key", "third");
			var tags:Array<Dynamic> = [firstTag, secondTag, thirdTag];
			var filteredTags:Array<Dynamic> = filter.filterTags(tags);
			assertTrue(filteredTags != tags);
			assertRecursivelyEquals(filteredTags, tags);
		}
		
		public function testAnyValueFiltration():Void {
			var firstTypeTag:ITag = new AnyValueTag("key");
			var secondTypeTag:ITag = new Tag("k", "2");
			var typeTags:Array<Dynamic> = [firstTypeTag, secondTypeTag];
			var filter:ITagsFilter = new TagsFilter(typeTags);
			var firstTag:ITag = new Tag("key", "1");
			var secondTag:ITag = new Tag("k", "2");
			var thirdTag:ITag = new Tag("k", "3");
			var fourthTag:ITag = new Tag("key", "4");
			var tags:Array<Dynamic> = [firstTag, secondTag, thirdTag, fourthTag];
			var filteredTags:Array<Dynamic> = filter.filterTags(tags);
			assertEquals(3, filteredTags.length);
			assertEquals(firstTag, filteredTags[0]);
			assertEquals(fourthTag, filteredTags[1]);
			assertEquals(secondTag, filteredTags[2]);
		}
	}
