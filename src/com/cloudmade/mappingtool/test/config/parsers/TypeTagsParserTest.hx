package com.cloudmade.mappingtool.test.config.parsers;

	import com.cloudmade.mappingtool.config.parsers.DefaultTypeTagsParser;
	import com.cloudmade.mappingtool.config.parsers.IXMLListParser;
	import com.cloudmade.mappingtool.config.parsers.IXMLParser;
	import com.cloudmade.mappingtool.config.parsers.TagParser;
	import com.cloudmade.mappingtool.config.parsers.XMLListParser;
	import com.cloudmade.mappingtool.map.model.Tag;
	
	import flexunit.framework.TestCase;

	class TypeTagsParserTest extends TestCase {
		
		public function new(?methodName:String=null)
		{
			super(methodName);
		}
		
		public function testParseTypeTags():Void {
			var tagParser:IXMLParser = new TagParser();
			var tagsParser:IXMLListParser = new XMLListParser(tagParser);
			
			var xml:XML = new XML('<type name="building"><tag key="building" value="*"/><default><tag key="building" value="yes"/></default></type>');
			
			var buildingTags:Array<Dynamic> = tagsParser.parse(xml.tag);
			
			assertEquals(1, buildingTags.length);
			assertTrue(buildingTags[0].equals(new Tag("building", "*")));
		}
		
		public function testParseDefaultTypeTags():Void {
			var tagParser:IXMLParser = new TagParser();
			var tagsParser:IXMLListParser = new XMLListParser(tagParser);
			
			var defaultTagsParser:IXMLParser = new DefaultTypeTagsParser(tagsParser);
			
			var xml:XML = new XML('<type name="building"><tag key="building" value="*"/><default><tag key="building" value="yes"/></default></type>');
			
			var buildingTags:Array<Dynamic> = cast( defaultTagsParser.parse(xml), Array);
			
			assertEquals(1, buildingTags.length);
			assertTrue(buildingTags[0].equals(new Tag("building", "yes")));
		}
	}
