package com.cloudmade.mappingtool.test.config.parsers;

	import com.cloudmade.mappingtool.config.ElementType;
	import com.cloudmade.mappingtool.config.IElementImage;
	import com.cloudmade.mappingtool.config.IElementType;
	import com.cloudmade.mappingtool.config.IReadOnlyHashMap;
	import com.cloudmade.mappingtool.config.ReadOnlyHashMap;
	import com.cloudmade.mappingtool.config.parsers.ElementImageParser;
	import com.cloudmade.mappingtool.config.parsers.IStylizedFactoryProvider;
	import com.cloudmade.mappingtool.config.parsers.IXMLParser;
	import com.cloudmade.mappingtool.config.conditions.types.ITagsType;
	import com.cloudmade.mappingtool.config.conditions.types.IsTagsType;
	import com.cloudmade.mappingtool.map.model.Tag;
	
	import flexunit.framework.TestCase;

	class ElementImageParserTest extends TestCase {
		
		public function new(?methodName:String=null)
		{
			super(methodName);
		}
		
		public function testParseElementImage():Void {
			var xml:XML = Xml.parse("<node layer="testLayer" type="testType" renderer="testRenderer" styleName="testStyleName"/>");
			
			var tagsType:ITagsType = new IsTagsType(new Tag("building", "yes"));
			var elementType:IElementType = new ElementType(tagsType);
			var rawTypeMap:Dynamic = {testType: elementType};
			var typeMap:IReadOnlyHashMap = new ReadOnlyHashMap(rawTypeMap);
			
			var layerMap:IReadOnlyHashMap = new ReadOnlyHashMap({testLayer: 2});
			
			var factoryProvider:IStylizedFactoryProvider = new DummyFactoryProvider();
			
			var parser:IXMLParser = new ElementImageParser(typeMap, layerMap, factoryProvider);
			
			var elementImage:IElementImage = cast( parser.parse(xml), IElementImage);
			
			assertTrue(Std.is( elementImage.newInstance(), DummyRendererClass));
		}
	}
