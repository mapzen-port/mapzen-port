package com.cloudmade.mappingtool.test.config.parsers;

	import com.cloudmade.mappingtool.config.IElementPreset;
	import com.cloudmade.mappingtool.config.IReadOnlyHashMap;
	import com.cloudmade.mappingtool.config.ReadOnlyHashMap;
	import com.cloudmade.mappingtool.config.parsers.ClassFactoryProvider;
	import com.cloudmade.mappingtool.config.parsers.ElementPresetParser;
	import com.cloudmade.mappingtool.config.parsers.IClassLocalizer;
	import com.cloudmade.mappingtool.config.parsers.IFactoryProvider;
	import com.cloudmade.mappingtool.config.parsers.IXMLParser;
	import com.cloudmade.mappingtool.map.model.ITag;
	import com.cloudmade.mappingtool.map.model.Tag;
	
	import flexunit.framework.TestCase;
	
	import mx.core.BitmapAsset;

	class ElementPresetParserTest extends TestCase {
		
		public function new(?methodName:String = null) {
			super(methodName);
		}
		
		public function testParsing():Void {
			var xml:XML = Xml.parse("<preset name="Bank Preset" type="bank" icon="bank"/>");
			var tagMapSource:Dynamic = new Object();
			var defaultTag:ITag = new Tag("key", "value");
			tagMapSource["bank"] = [defaultTag];
			var defaultTagMap:IReadOnlyHashMap = new ReadOnlyHashMap(tagMapSource);
			var localizer:IClassLocalizer = new BankLocalizer();
			var iconProvider:IFactoryProvider = new ClassFactoryProvider(localizer);
			var presetParser:IXMLParser = new ElementPresetParser(defaultTagMap, iconProvider);
			var preset:IElementPreset = cast( presetParser.parse(xml), IElementPreset);
			assertEquals("Bank Preset", preset.name);
			assertEquals(1, preset.defaultTags.length);
			assertEquals(defaultTag, preset.defaultTags[0]);
			assertTrue(Std.is( preset.icon.newInstance(), BitmapAsset));
		}
	}
