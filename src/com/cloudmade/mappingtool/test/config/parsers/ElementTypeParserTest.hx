package com.cloudmade.mappingtool.test.config.parsers;

	import com.cloudmade.mappingtool.config.IElementType;
	import com.cloudmade.mappingtool.config.parsers.ElementTypeParser;
	import com.cloudmade.mappingtool.config.parsers.IXMLParser;
	
	import flexunit.framework.TestCase;

	class ElementTypeParserTest extends TestCase {
		
		public function new(?methodName:String=null)
		{
			super(methodName);
		}
		
		public function testParseElementType():Void {
			var xml:XML = <type name="building" priority="2">
				<tag key="building" value="*" />
			</type>
			
			var elementTypeParser:IXMLParser = new ElementTypeParser(new DummyParser());
			
			var elementType:IElementType = cast( elementTypeParser.parse(xml), IElementType);
			
			assertEquals(elementType.priority, "2");
		}
	}
