package com.cloudmade.mappingtool.test.config.parsers;

	import com.cloudmade.mappingtool.config.parsers.IXMLParser;
	import com.cloudmade.mappingtool.config.parsers.LayerParser;
	
	import flexunit.framework.TestCase;

	class LayerParserTest extends TestCase {
		
		public function new(?methodName:String=null)
		{
			super(methodName);
		}
		
		public function testParseLayers():Void {
			var xml:XML = Xml.parse("<layers>
				<layer name="underworld"/>
				<layer name="world"/>
				<layer name="coast_poly"/>
			</layers>");
			
			var parser:IXMLParser = new LayerParser();
			
			var layerIndex:Int = cast( parser.parse(xml.layer[1]), int);
			
			assertEquals(1, layerIndex);
		}
	}
