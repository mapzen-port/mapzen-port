package com.cloudmade.mappingtool.test.config.parsers;

	import com.cloudmade.mappingtool.config.IPresetCategory;
	import com.cloudmade.mappingtool.config.IStringLocalizer;
	import com.cloudmade.mappingtool.config.parsers.IXMLListParser;
	import com.cloudmade.mappingtool.config.parsers.IXMLParser;
	import com.cloudmade.mappingtool.config.parsers.LocalizedPresetCategoryParser;
	import com.cloudmade.mappingtool.config.parsers.PresetCategoryParser;
	
	import flexunit.framework.TestCase;

	class PresetCategoryParserTest extends TestCase {
		
		public function new(?methodName:String = null) {
			super(methodName);
		}
		
		public function testParsing():Void {
			var xml:XML = <category name="Category">Xml.parse("<preset/>")<preset/><~/category>;
			var presetsParser:IXMLListParser = new DummyPresetParser();
			var categoryParser:IXMLParser = new PresetCategoryParser(presetsParser);
			var category:IPresetCategory = categoryParser.parse(xml) as IPresetCategory;
			assertEquals("Category", category.name);
			assertEquals(0, category.presets.length);
		}
		
		public function testLocalizedParsing():void {
			var xml:XML = <category name="Category"></category>;
			var presetsParser:IXMLListParser = new DummyPresetParser();
			var categoryParser:IXMLParser = new PresetCategoryParser(presetsParser);
			var localizer:IStringLocalizer = new DummyPresetCategoryLocalizer();
			var localizedCategoryParser:IXMLParser = new LocalizedPresetCategoryParser(categoryParser, localizer);
			var localizedCategory:IPresetCategory = cast( localizedCategoryParser.parse(xml), IPresetCategory);
			assertEquals("Localized Category", localizedCategory.name);
		}
	}
