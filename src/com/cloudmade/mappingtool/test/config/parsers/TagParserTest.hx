package com.cloudmade.mappingtool.test.config.parsers;

	import com.cloudmade.mappingtool.config.parsers.IXMLListParser;
	import com.cloudmade.mappingtool.config.parsers.IXMLParser;
	import com.cloudmade.mappingtool.config.parsers.TagParser;
	import com.cloudmade.mappingtool.config.parsers.XMLListParser;
	import com.cloudmade.mappingtool.map.model.Tag;
	
	import flexunit.framework.TestCase;
	
	class TagParserTest extends TestCase {
		
		public function new(?methodName:String = null) {
			super(methodName);
		}
		
		public function testParseTag():Void {
			var parser:IXMLParser = new TagParser();
			var xml:XML = Xml.parse("<tag key="highway" value="primary"/>");
			var tag:Tag = cast( parser.parse(xml), Tag);
			
			assertTrue(tag.equals(new Tag("highway", "primary")));
		}
		
		public function testParseTagList():Void {
			var tagParser:IXMLParser = new TagParser();
			var parser:IXMLListParser = new XMLListParser(tagParser);
			var xml:XML = Xml.parse("<tags><tag key="highway" value="primary"/><tag key="highway" value="service"/></tags>");
			var tags:Array<Dynamic> = parser.parse(xml.tag);
			
			assertEquals(2, tags.length);
			assertTrue(tags[0].equals(new Tag("highway", "primary")));
			assertTrue(tags[1].equals(new Tag("highway", "service")));
		}
		
	}
