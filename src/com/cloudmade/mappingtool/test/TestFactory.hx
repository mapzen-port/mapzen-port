package com.cloudmade.mappingtool.test;

	import com.cloudmade.mappingtool.test.business.externalization.osm.MemberExternalizerTest;
	import com.cloudmade.mappingtool.test.business.externalization.osm.NodeExternalizerTest;
	import com.cloudmade.mappingtool.test.business.externalization.osm.RelationExternalizerTest;
	import com.cloudmade.mappingtool.test.business.externalization.osm.TagExternalizerTest;
	import com.cloudmade.mappingtool.test.business.externalization.osm.ValidTagsSerializerTest;
	import com.cloudmade.mappingtool.test.business.externalization.osm.WayExternalizationTest;
	import com.cloudmade.mappingtool.test.config.conditions.filters.AndTagsFilterTest;
	import com.cloudmade.mappingtool.test.config.conditions.filters.IsTagsFilterTest;
	import com.cloudmade.mappingtool.test.config.conditions.filters.NotTagsFilterTest;
	import com.cloudmade.mappingtool.test.config.conditions.filters.OrTagsFilterTest;
	import com.cloudmade.mappingtool.test.config.conditions.filters.TagsFiltersParserTest;
	import com.cloudmade.mappingtool.test.config.conditions.types.AndTagsTypeTest;
	import com.cloudmade.mappingtool.test.config.conditions.types.IsTagsTypeTest;
	import com.cloudmade.mappingtool.test.config.conditions.types.NotTagsTypeTest;
	import com.cloudmade.mappingtool.test.config.conditions.types.OrTagsTypeTest;
	import com.cloudmade.mappingtool.test.config.conditions.types.TagsTypeParserTest;
	import com.cloudmade.mappingtool.test.config.factories.PresetCategoriesFactoryTest;
	import com.cloudmade.mappingtool.test.config.parsers.ElementImageParserTest;
	import com.cloudmade.mappingtool.test.config.parsers.ElementPresetParserTest;
	import com.cloudmade.mappingtool.test.config.parsers.ElementTypeParserTest;
	import com.cloudmade.mappingtool.test.config.parsers.LayerParserTest;
	import com.cloudmade.mappingtool.test.config.parsers.PresetCategoryParserTest;
	import com.cloudmade.mappingtool.test.config.parsers.TagParserTest;
	import com.cloudmade.mappingtool.test.config.parsers.TypeTagsParserTest;
	import com.cloudmade.mappingtool.test.data.ArrayIteratorTest;
	import com.cloudmade.mappingtool.test.data.StackTest;
	import com.cloudmade.mappingtool.test.data.TwoWayArrayIteratorTest;
	import com.cloudmade.mappingtool.test.editing.CommandExecutorTest;
	import com.cloudmade.mappingtool.test.editing.CommandHistoryTest;
	import com.cloudmade.mappingtool.test.editing.CommandListTest;
	import com.cloudmade.mappingtool.test.editing.errors.externalization.CommandStackExternalizerTest;
	import com.cloudmade.mappingtool.test.map.ExternalizationTest;
	import com.cloudmade.mappingtool.test.map.geo.LatLongBoundsTest;
	import com.cloudmade.mappingtool.test.map.geo.LatLongTest;
	import com.cloudmade.mappingtool.test.map.geo.MercatorTest;
	import com.cloudmade.mappingtool.test.map.model.ElementTest;
	import com.cloudmade.mappingtool.test.map.model.MapTest;
	import com.cloudmade.mappingtool.test.map.model.NodeTest;
	import com.cloudmade.mappingtool.test.map.model.TagTest;
	import com.cloudmade.mappingtool.test.map.model.WayTest;
	import com.cloudmade.mappingtool.test.ui.actions.ConditionalActionTriggerTest;
	import com.cloudmade.mappingtool.test.ui.renderers.TagRendererBaseTest;
	import com.cloudmade.mappingtool.test.utils.GeometryUtilsTest;
	import com.cloudmade.mappingtool.test.utils.PolylinePointNormalizationTest;
	import com.cloudmade.mappingtool.test.utils.PolylinePointProjectionTest;
	import com.cloudmade.mappingtool.test.utils.roundToTest;
	
	import flexunit.framework.Test;
	import flexunit.framework.TestSuite;
	
	class TestFactory
	 {
		
		public function new() {
		}
		
		public function create():Test {
			var test:TestSuite = new TestSuite();
			test.addTest(createDataTest());
			test.addTest(createCommandEditingTest());
			test.addTest(createTagFiltrationTest());
			test.addTest(createParserTest());
			test.addTest(createOSMExternalizationTest());
			test.addTest(createMapTest());
			test.addTest(createGeoTest());
			test.addTest(createGeometryTest());
			test.addTest(createActionTest());
			test.addTest(createComponentTest());
			test.addTest(createMathTest());
			test.addTest(createExternalizationTest());
			test.addTest(createConstructionTest());
			return test;
		}
		
		function createDataTest():Test {
			return createTestSuite(ArrayIteratorTest, TwoWayArrayIteratorTest, StackTest);
		}
		
		function createCommandEditingTest():Test {
			return createTestSuite(CommandListTest, CommandExecutorTest, CommandHistoryTest);
		}
		
		function createTagFiltrationTest():Test {
			return createTestSuite(AndTagsFilterTest, OrTagsFilterTest, IsTagsFilterTest, NotTagsFilterTest, TagsFiltersParserTest);
		}
		
		function createTypeTagsTest():Test {
			return createTestSuite(AndTagsTypeTest, OrTagsTypeTest, IsTagsTypeTest, NotTagsTypeTest, TagsTypeParserTest);
		}
		
		function createOSMExternalizationTest():Test {
			return createTestSuite(TagExternalizerTest, MemberExternalizerTest, NodeExternalizerTest, RelationExternalizerTest, WayExternalizationTest, ValidTagsSerializerTest);
		}
		
		function createExternalizationTest():Test {
			return createTestSuite(ExternalizationTest, CommandStackExternalizerTest);
		}
		
		function createParserTest():Test {
			return createTestSuite(TagParserTest, TypeTagsParserTest, LayerParserTest, ElementPresetParserTest, PresetCategoryParserTest, ElementTypeParserTest, ElementImageParserTest);
		}
		
		function createMapTest():Test {
			return createTestSuite(MapTest, TagTest, ElementTest, NodeTest, WayTest);
		}
		
		function createGeoTest():Test  {
			return createTestSuite(MercatorTest, LatLongTest, LatLongBoundsTest);
		}
		
		function createGeometryTest():Test {
			return createTestSuite(GeometryUtilsTest, PolylinePointProjectionTest, PolylinePointNormalizationTest);
		}
		
		function createActionTest():Test {
			return createTestSuite(ConditionalActionTriggerTest);
		}
		
		function createComponentTest():Test {
			return createTestSuite(TagRendererBaseTest/*, DataDelayerTest*/);
		}
		
		function createMathTest():Test {
			return createTestSuite(roundToTest);
		}
		
		function createConstructionTest():Test {
			return createTestSuite(PresetCategoriesFactoryTest);
		}
		
		function createTestSuite(tests:Array<Dynamic>):TestSuite {
			var testSuite:TestSuite = new TestSuite();
			for (test in tests) {
				testSuite.addTestSuite(test);
			}
			return testSuite;
		}
	}
