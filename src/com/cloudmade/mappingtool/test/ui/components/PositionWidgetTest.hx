package com.cloudmade.mappingtool.test.ui.components;

	import com.cloudmade.mappingtool.map.geo.LatLong;
	import com.cloudmade.mappingtool.model.ApplicationData;
	import com.gorillalogic.flexmonkey.commands.CommandRunner;
	import com.gorillalogic.flexmonkey.commands.FlexCommand;
	import com.gorillalogic.flexmonkey.core.MonkeyEvent;
	import com.gorillalogic.flexmonkey.core.MonkeyUtils;
	
	import flexunit.framework.TestCase;
	
	import mx.controls.TextInput;

	class PositionWidgetTest extends TestCase {
		public function new() { }
		
		public function testPosition():Void {
			var cmdRunner:CommandRunner = new CommandRunner();
			cmdRunner.addEventListener(MonkeyEvent.READY_FOR_VALIDATION, addAsync(validationHandler, 5000));
			cmdRunner.runCommands([
				new FlexCommand("longInput", "SelectText", ["0", "100"], "automationName"),
				new FlexCommand("longInput", "Input", ["10"], "automationName"),
				new FlexCommand("latInput", "SelectText", ["0", "100"], "automationName"),
				new FlexCommand("latInput", "Input", ["5"], "automationName"),
				new FlexCommand("Go", "Click", ["10"], "automationName")
			]);
		}
		
		public function testInputs():Void {
			var appData:ApplicationData = ApplicationData.getInstance();
			var longInput:TextInput = cast( MonkeyUtils.findComponentWith("longInput", "id"), TextInput);
			var latInput:TextInput = cast( MonkeyUtils.findComponentWith("latInput", "id"), TextInput);
			var oldCenter:LatLong = appData.map.center;
			appData.map.center = new LatLong(10, 10);
			assertEquals(10, longInput.text);
            assertEquals(10, latInput.text);
            appData.map.center = oldCenter;
	   }
		
		function validationHandler(event:MonkeyEvent):Void {
			var center:LatLong = ApplicationData.getInstance().map.center;
			var longInput:TextInput = cast( MonkeyUtils.findComponentWith("longInput", "id"), TextInput);
			var latInput:TextInput = cast( MonkeyUtils.findComponentWith("latInput", "id"), TextInput);
			assertEquals(10, center.long);
            assertEquals(5, center.lat);
		}
	}
