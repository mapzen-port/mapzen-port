package com.cloudmade.mappingtool.test.ui.actions;

	import com.cloudmade.mappingtool.actions.triggers.ConditionalTrigger;
	import com.cloudmade.mappingtool.actions.triggers.ITrigger;
	
	import flash.display.Sprite;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	import flash.events.MouseEvent;
	
	import flexunit.framework.TestCase;

	class ConditionalActionTriggerTest extends TestCase {
		
		public function new(?methodName:String = null) {
			super(methodName);
		}
		
		public function testExpectedOnceTrigger():Void {
			assertEquals(1, getFiredNumber(true));
		}
		
		public function testUnexpectedTrigger():Void {
			assertEquals(0, getFiredNumber(false));
		}
		
		public function testBubblingEventTargets():Void {
			var parent:Sprite = new Sprite();
			var child:Sprite = new Sprite();
			parent.addChild(child);
			var trigger:ITrigger = new ConditionalTrigger(parent, MouseEvent.CLICK);
			var listener:Dynamic = function(event:MouseEvent):Void {
				assertEquals(child, event.target);
				assertEquals(parent, event.currentTarget);
			};
			trigger.addListener(addAsync(listener, 1));
			child.dispatchEvent(new MouseEvent(MouseEvent.CLICK));
		}
		
		function getFiredNumber(flag:Bool):Int {
			var firedNumber:Int = 0;
			var listener:Dynamic = function(event:TestEvent):Void {
				firedNumber++;
			};
			var triggerWrapper:TriggerTestWrapper = new TriggerTestWrapper(listener);
			triggerWrapper.fire(flag);
			return firedNumber;
		}
	}
