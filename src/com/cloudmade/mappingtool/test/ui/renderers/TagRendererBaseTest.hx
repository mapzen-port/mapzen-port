package com.cloudmade.mappingtool.test.ui.renderers;

	import flexunit.framework.TestCase;

	class TagRendererBaseTest extends TestCase {
		
		public function new(?methodName:String = null) {
			super(methodName);
		}
		
		public function testRemoveControlCharacters():Void {
			var input:String = "some\x00 \x05string\x10\x15";
			var tagRenderer:DummyTagRenderer = new DummyTagRenderer();
			var output:String = tagRenderer.publicRemoveControlCharacters(input);
			assertEquals("some string", output);
		}
	}
