package com.cloudmade.mappingtool.test.map;

	import com.cloudmade.mappingtool.business.externalization.IXMLDeserializer;
	import com.cloudmade.mappingtool.business.externalization.osm.MapElementsDeserializerFactory;
	import com.cloudmade.mappingtool.map.geo.LatLong;
	import com.cloudmade.mappingtool.map.model.Element;
	import com.cloudmade.mappingtool.map.model.IMapElements;
	import com.cloudmade.mappingtool.map.model.Map;
	import com.cloudmade.mappingtool.map.model.Member;
	import com.cloudmade.mappingtool.map.model.Node;
	import com.cloudmade.mappingtool.map.model.Relation;
	import com.cloudmade.mappingtool.map.model.Tag;
	import com.cloudmade.mappingtool.map.model.Way;
	
	import flash.utils.ByteArray;
	
	import flexunit.framework.TestCase;
	
	import mx.utils.ObjectUtil;

	class ExternalizationTest extends TestCase {
		
		/*[Embed(source="../../../../../config/map.xml", mimeType="application/octet-stream")]*/
		static var MAP_XML:Class<Dynamic>;
		
		public function new(?methodName:String = null) {
			super(methodName);
		}
		
		public function testTag():Void {
			var tag:Tag = new Tag("key", "value");
			assertExternalizable(tag);
		}
		
		public function testElement():Void {
			var element:Element = new Element("1", [new Tag("key1", "value1"), new Tag("key2", "value2")]);
			assertExternalizable(element);
		}
		
		public function testLatLong():Void {
			var latLong:LatLong = new LatLong(10, 20);
			assertExternalizable(latLong);
		}
		
		public function testNode():Void {
			var node:Node = new Node("2", [new Tag("key", "value")], new LatLong(20, 10));
			assertExternalizable(node);
		}
		
		public function testWay():Void {
			var node1:Node = new Node("1", [new Tag("key1", "value1")], new LatLong(0, 0));
			var node2:Node = new Node("2", [new Tag("key2", "value2")], new LatLong(10, 10));
			var way:Way = new Way("3", [new Tag("key3", "value3")], [node1, node2]);
			assertExternalizable(way);
		}
		
		public function testMember():Void {
			var member:Member = new Member("1", "type", "role");
			assertExternalizable(member);
		}
		
		public function testRelation():Void {
			var relation:Relation = new Relation("1", [new Tag("key", "value")], [new Member("2", "type", "role")]);
			assertExternalizable(relation);
		}
		
		public function testMap():Void {
			var node1:Node = new Node("0", null, new LatLong(10, 10));
			var node2:Node = new Node("1", null, new LatLong(20, 20))
			var way1:Way = new Way("2", null, null);
			var way2:Way = new Way("3", [new Tag("key", "value")], [node1, node2])
			var map:Map = new Map(64, 2, 10);
			map.center = new LatLong(-10, -30);
			map.zoomLevel = 8;
			map.setSize(600, 400);
			map.setElements([node1, node2], [way1, way2], null);
			assertExternalizable(map);
		}
		
		public function testMapNodeReferences():Void {
			var mapBytes:ByteArray = new MAP_XML();
			var mapXml:XML = new XML(mapBytes.toString());
			var deserializerFactory:MapElementsDeserializerFactory = new MapElementsDeserializerFactory();
			var deserializer:IXMLDeserializer = deserializerFactory.create();
			var mapElements:IMapElements = cast( deserializer.deserialize(mapXml), IMapElements);
			var map:Map = new Map();
			map.setElements(mapElements.nodes, mapElements.ways, mapElements.relations);
			var newMap:Map = cast( externalizeObject(map), Map);
			var ways:Array<Dynamic> = newMap.ways.source;
			for (way in ways) {
				var nodes:Array<Dynamic> = way.nodes.source;
				for (node in nodes) {
					assertEquals(node, newMap.getNode(node.id));
				}
			}
		}
		
		function assertExternalizable(object:Dynamic):Void {
			var newObject:Dynamic = externalizeObject(object);
			var compareResult:Int = ObjectUtil.compare(object, newObject);
			assertEquals(0, compareResult);
		}
		
		function externalizeObject(object:Dynamic):Dynamic {
			var bytes:ByteArray = new ByteArray();
			bytes.writeObject(object);
			bytes.position = 0;
			var newObject:Dynamic = bytes.readObject();
			return newObject;
		}
	}
