package com.cloudmade.mappingtool.test.map.geo;

	import com.cloudmade.mappingtool.map.geo.IProjection;
	import com.cloudmade.mappingtool.map.geo.ITransformation;
	import com.cloudmade.mappingtool.map.geo.LatLong;
	import com.cloudmade.mappingtool.map.geo.MercatorProjection;
	import com.cloudmade.mappingtool.map.geo.MercatorTransformation;
	
	import flash.geom.Point;
	
	import flexunit.framework.TestCase;

	class MercatorTest extends TestCase {
		
		public function new(?methodName:String = null) {
			super(methodName);
		}
		
		public function testProject1():Void {
			var projection:IProjection = new MercatorProjection();
			var transformation:ITransformation = new MercatorTransformation(256, 0);
			var point:Point = transformation.transform(projection.project(new LatLong(75, 45)));
			assertEquals(160, Math.round(point.x));
			assertEquals(45, Math.round(point.y));
		}
		public function testProject2():Void {
			var projection:IProjection = new MercatorProjection();
			var transformation:ITransformation = new MercatorTransformation(256, 10);
			var point:Point = transformation.transform(projection.project(new LatLong(-30, -150)));
			assertEquals(21845, Math.round(point.x));
			assertEquals(153990, Math.round(point.y));
		}
		
		public function testUnproject1():Void {
			var projection:IProjection = new MercatorProjection();
			var transformation:ITransformation = new MercatorTransformation(256, 0);
			var latLong:LatLong = projection.unproject(transformation.untransform(new Point(0, 0)));
			assertEquals(85, Math.round(latLong.lat));
			assertEquals(-180, Math.round(latLong.long));
		}
		public function testUnproject2():Void {
			var projection:IProjection = new MercatorProjection();
			var transformation:ITransformation = new MercatorTransformation(256, 10);
			var latLong:LatLong = projection.unproject(transformation.untransform(new Point(140000, 140000)));
			assertEquals(-12, Math.round(latLong.lat));
			assertEquals(12, Math.round(latLong.long));
		}
	}
