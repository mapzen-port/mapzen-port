package com.cloudmade.mappingtool.test.map.geo;

	import com.cloudmade.mappingtool.map.geo.LatLongBounds;
	
	import flexunit.framework.TestCase;
	
	class LatLongBoundsTest extends TestCase {
		
		public function new(?methodName:String = null) {
			super(methodName);
		}
		
		public function testConstructor():Void {
			var bounds:LatLongBounds = new LatLongBounds(0, 0, 10, 10);
			assertEquals(0, bounds.westLong);
			assertEquals(0, bounds.southLat);
			assertEquals(10, bounds.eastLong);
			assertEquals(10, bounds.northLat);
		}
		
		public function testEquals():Void {
			var bounds1:LatLongBounds = new LatLongBounds(0, 0, 10, 10);
			var bounds2:LatLongBounds = new LatLongBounds(0, 0, 10, 10);
			assertTrue(bounds1.equals(bounds2));
			var bounds3:LatLongBounds = new LatLongBounds(0, 0, 10, 9);
			assertFalse(bounds1.equals(bounds3));
		}
		
		public function testClone():Void {
			var sourceBounds:LatLongBounds = new LatLongBounds(0, 0, 10, 10);
			var cloneBounds:LatLongBounds = sourceBounds.clone();
			assertTrue(cloneBounds.equals(sourceBounds) && (cloneBounds != sourceBounds));
		}
	}
