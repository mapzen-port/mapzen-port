package com.cloudmade.mappingtool.test.map.geo;

	import com.cloudmade.mappingtool.map.geo.LatLong;
	
	import flexunit.framework.TestCase;

	class LatLongTest extends TestCase {
		
		public function new(?methodName:String = null) {
			super(methodName);
		}
		
		public function testCreateFromRadians():Void {
			var latLong:LatLong = LatLong.createFromRadians(2, 2);
			assertEquals(2, latLong.latRadians);
			assertEquals(2, latLong.longRadians);
		}
		
		public function testConstructor():Void {
			var latLong:LatLong = new LatLong(10, 10);
			assertEquals(10, latLong.lat);
			assertEquals(10, latLong.long);
		}
		
		public function testRadians():Void {
			var latLong:LatLong = new LatLong(10, 10);
			assertEquals(10 * Math.PI / 180, latLong.latRadians);
			assertEquals(10 * Math.PI / 180, latLong.longRadians);
		}
		
		public function testEquals():Void {
			var latLong1:LatLong = new LatLong(10, 10);
			var latLong2:LatLong = new LatLong(10, 10);
			assertTrue(latLong1.equals(latLong2));
			var latLong3:LatLong = new LatLong(10, 9);
			assertFalse(latLong1.equals(latLong3));
		}
		
		public function testClone():Void {
			var sourceLatLong:LatLong = new LatLong(10, 10);
			var cloneLatLong:LatLong = sourceLatLong.clone();
			assertTrue(cloneLatLong.equals(sourceLatLong))
			assertFalse(cloneLatLong == sourceLatLong);
		}
		
		public function testOffset():Void {
			var sourceLatLong:LatLong = new LatLong(10, 10);
			var offsetLatLong:LatLong = sourceLatLong.offset(10, 5);
			assertEquals(20, offsetLatLong.lat);
			assertEquals(15, offsetLatLong.long);
		}
		
		public function testNormalize():Void {
			var sourceLatLong:LatLong = new LatLong(90, 360);
			var normLatLong:LatLong = sourceLatLong.normalize(80);
			assertEquals(80, normLatLong.lat);
			assertEquals(360, normLatLong.long);
		}
	}
