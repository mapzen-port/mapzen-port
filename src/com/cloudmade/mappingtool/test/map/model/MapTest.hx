package com.cloudmade.mappingtool.test.map.model;

	import com.cloudmade.mappingtool.map.events.BoundsChangeEvent;
	import com.cloudmade.mappingtool.map.events.LatLongChangeEvent;
	import com.cloudmade.mappingtool.map.events.MapEvent;
	import com.cloudmade.mappingtool.map.events.ResizeEvent;
	import com.cloudmade.mappingtool.map.events.ZoomEvent;
	import com.cloudmade.mappingtool.map.geo.LatLong;
	import com.cloudmade.mappingtool.map.model.Map;
	import com.cloudmade.mappingtool.map.model.Node;
	import com.cloudmade.mappingtool.map.model.Way;
	
	import flexunit.framework.EventfulTestCase;
	
	class MapTest extends EventfulTestCase {
		
		public function new(?methodName:String = null) {
			super(methodName);
		}
		
		public function testConstructor():Void {
			var map:Map = new Map();
			assertEquals(256, map.tileSize);
			assertEquals(0, map.minZoomLevel);
			assertEquals(18, map.maxZoomLevel);
			map = new Map(64, 2, 10);
			assertEquals(64, map.tileSize);
			assertEquals(2, map.minZoomLevel);
			assertEquals(10, map.maxZoomLevel);
		}
		
		public function testBounds():Void {
			var map:Map = new Map();
			map.zoomLevel = 5;
			map.setSize(500, 500);
			map.center = new LatLong(50, 0);
			assertEquals(-11, Math.round(map.bounds.westLong));
			assertEquals(42, Math.round(map.bounds.southLat));
			assertEquals(11, Math.round(map.bounds.eastLong));
			assertEquals(57, Math.round(map.bounds.northLat));
		}
		
		public function testCenter1():Void {
			var map:Map = new Map();
			var latLong:LatLong = new LatLong(10, 10);
			listenForEvent(map, LatLongChangeEvent.CENTER_CHANGE, EVENT_EXPECTED);
			listenForEvent(map, MapEvent.COORDINATES_CHANGE, EVENT_EXPECTED);
			map.center = latLong;
			assertEvents();
			assertEquals(latLong, map.center);
		}
		public function testCenter2():Void {
			var map:Map = new Map();
			var latLong:LatLong = new LatLong(10, 10);
			map.center = latLong;
			listenForEvent(map, LatLongChangeEvent.CENTER_CHANGE, EVENT_UNEXPECTED);
			listenForEvent(map, MapEvent.COORDINATES_CHANGE, EVENT_UNEXPECTED);
			map.center = latLong.clone();
			assertEvents();
			assertEquals(latLong, map.center);
		}
		
		public function testWidth1():Void {
			var map:Map = new Map();
			listenForEvent(map, ResizeEvent.RESIZE, EVENT_EXPECTED);
			listenForEvent(map, MapEvent.COORDINATES_CHANGE, EVENT_EXPECTED);
			map.width = 100;
			assertEvents();
			assertEquals(100, map.width);
		}
		public function testWidth2():Void {
			var map:Map = new Map();
			map.width = 100;
			listenForEvent(map, ResizeEvent.RESIZE, EVENT_UNEXPECTED);
			listenForEvent(map, MapEvent.COORDINATES_CHANGE, EVENT_UNEXPECTED);
			map.width = 100;
			assertEvents();
		}
		
		public function testHeight1():Void {
			var map:Map = new Map();
			listenForEvent(map, ResizeEvent.RESIZE, EVENT_EXPECTED);
			listenForEvent(map, MapEvent.COORDINATES_CHANGE, EVENT_EXPECTED);
			map.height = 100;
			assertEvents();
			assertEquals(100, map.height);
		}
		public function testHeight2():Void {
			var map:Map = new Map();
			map.height = 100;
			listenForEvent(map, ResizeEvent.RESIZE, EVENT_UNEXPECTED);
			listenForEvent(map, MapEvent.COORDINATES_CHANGE, EVENT_UNEXPECTED);
			map.height = 100;
			assertEvents();
		}
		
		public function testZoomLevel1():Void {
			var map:Map = new Map();
			listenForEvent(map, ZoomEvent.ZOOM, EVENT_EXPECTED);
			listenForEvent(map, MapEvent.COORDINATES_CHANGE, EVENT_EXPECTED);
			map.zoomLevel = 10;
			assertEvents();
			assertEquals(10, map.zoomLevel);
		}
		public function testZoomLevel2():Void {
			var map:Map = new Map();
			map.zoomLevel = 10;
			listenForEvent(map, ZoomEvent.ZOOM, EVENT_UNEXPECTED);
			listenForEvent(map, MapEvent.COORDINATES_CHANGE, EVENT_UNEXPECTED);
			map.zoomLevel = 10;
			assertEvents();
		}
		public function testZoomLevel3():Void {
			var map:Map = new Map();
			map.zoomLevel = 100;
			assertEquals(18, map.zoomLevel);
			map.zoomLevel = -100;
			assertEquals(0, map.zoomLevel);
		}
		
		public function testGetNode():Void {
			var map:Map = new Map();
			var node1:Node = new Node("100", null, new LatLong(10, 10));
			var node2:Node = new Node("101", null, new LatLong(11, 11));
			var node3:Node = new Node("102", null, new LatLong(12, 12));
			map.nodes.source = [node1, node2];
			assertEquals(node1, map.getNode("100"));
			assertEquals(node2, map.getNode("101"));
			assertEquals(null, map.getNode("102"));
			assertEquals(null, map.getNode("103"));
			map.nodes.addItem(node3);
			assertEquals(node3, map.getNode("102"));
			map.nodes.removeItem(node2);
			assertEquals(null, map.getNode("101"));
			map.nodes.setItemAt(node2, 0);
			assertEquals(node2, map.getNode("101"));
			assertEquals(null, map.getNode("100"));
		}
		
		public function testGetWay():Void {
			var map:Map = new Map();
			var way1:Way = new Way("100", null, null);
			var way2:Way = new Way("101", null, null);
			var way3:Way = new Way("102", null, null);
			map.ways.source = [way1, way2];
			assertEquals(way1, map.getWay("100"));
			assertEquals(way2, map.getWay("101"));
			assertEquals(null, map.getWay("102"));
			assertEquals(null, map.getWay("103"));
			map.ways.addItem(way3);
			assertEquals(way3, map.getWay("102"));
			map.ways.removeItem(way2);
			assertEquals(null, map.getWay("101"));
			map.ways.setItemAt(way2, 0);
			assertEquals(way2, map.getWay("101"));
			assertEquals(null, map.getWay("100"));
		}
		
		public function testGetNodeWays1():Void {
			var map:Map = new Map();
			var node:Node = new Node("0", null, new LatLong(0, 0));
			var ways:Array<Dynamic> = map.getNodeWays(node);
			assertEquals(0, ways.length);
		}
		public function testGetNodeWays2():Void {
			var map:Map = new Map();
			var node:Node = new Node("0", null, new LatLong(0, 0));
			var way:Way = new Way("1", null, [node.clone(), node.clone()]);
			map.ways.addItem(way);
			var ways:Array<Dynamic> = map.getNodeWays(node);
			assertEquals(0, ways.length);
		}
		public function testGetNodeWays3():Void {
			var map:Map = new Map();
			var node:Node = new Node("0", null, new LatLong(0, 0));
			var way:Way = new Way("1", null, [node, node.clone()]);
			map.ways.addItem(way);
			var ways:Array<Dynamic> = map.getNodeWays(node);
			assertEquals(1, ways.length);
			assertEquals(way, ways[0]);
		}
		
		public function testSetElements():Void {
			var map:Map = new Map();
			var nodes:Array<Dynamic> = new Array();
			var ways:Array<Dynamic> = new Array();
			listenForEvent(map, MapEvent.ELEMENTS_CHANGE, EVENT_EXPECTED);
			map.setElements(nodes, ways, null);
			assertEvents();
			assertEquals(nodes, map.nodes.source);
			assertEquals(ways, map.ways.source);
		}
				
		public function testSetSize():Void {
			var map:Map = new Map();
			listenForEvent(map, ResizeEvent.RESIZE, EVENT_EXPECTED);
			listenForEvent(map, MapEvent.COORDINATES_CHANGE, EVENT_EXPECTED);
			map.setSize(100, 100);
			assertEvents();
			assertEquals(2, dispatchedExpectedEvents.length);
			assertEquals(100, map.width);
			assertEquals(100, map.height);
		}
		
		public function testResizeEvent():Void {
			var map:Map = new Map();
			listenForEvent(map, ResizeEvent.RESIZE, EVENT_EXPECTED);
			map.setSize(100, 100);
			map.width = 200;
			map.height = 200;
			map.setSize(300, 300);
			assertEvents();
			var resizeEvent0:ResizeEvent = dispatchedExpectedEvents[0];
			var resizeEvent1:ResizeEvent = dispatchedExpectedEvents[1];
			var resizeEvent2:ResizeEvent = dispatchedExpectedEvents[2];
			var resizeEvent3:ResizeEvent = dispatchedExpectedEvents[3];
			assertEquals(100, resizeEvent1.oldWidth);
			assertEquals(100, resizeEvent1.oldHeight);
			assertEquals(200, resizeEvent2.oldWidth);
			assertEquals(100, resizeEvent2.oldHeight);
			assertEquals(200, resizeEvent3.oldWidth);
			assertEquals(200, resizeEvent3.oldHeight);
		}
		
		public function testBoundsChangeEvent1():Void {
			var map:Map = new Map();
			listenForEvent(map, BoundsChangeEvent.BOUNDS_CHANGE, EVENT_EXPECTED);
			map.setSize(100, 100);
			assertEvents();
		}
		public function testBoundsChangeEvent2():Void {
			var map:Map = new Map();
			listenForEvent(map, BoundsChangeEvent.BOUNDS_CHANGE, EVENT_UNEXPECTED);
			map.disableBoundsUpdate();
			map.setSize(100, 100);
			assertEvents();
		}
		public function testBoundsChangeEvent3():Void {
			var map:Map = new Map();
			listenForEvent(map, BoundsChangeEvent.BOUNDS_CHANGE, EVENT_EXPECTED);
			map.disableBoundsUpdate();
			map.setSize(100, 100);
			map.enableBoundsUpdate();
			assertEvents();
		}
	}
