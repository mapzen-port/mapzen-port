package com.cloudmade.mappingtool.test.map.model;

	import com.cloudmade.mappingtool.map.events.LatLongChangeEvent;
	import com.cloudmade.mappingtool.map.geo.LatLong;
	import com.cloudmade.mappingtool.map.model.Node;
	import com.cloudmade.mappingtool.map.model.Tag;
	
	import flexunit.framework.EventfulTestCase;

	class NodeTest extends EventfulTestCase {
		
		public function new(?methodName:String = null) {
			super(methodName);
		}
		
		public function testConstructor():Void {
			var latLong:LatLong = new LatLong(10, 10);
			var tags:Array<Dynamic> = [new Tag("key", "value")];
			var node:Node = new Node("123", tags, latLong);
			assertEquals(123, node.id);
			assertEquals(tags, node.tags.source);
			assertEquals(latLong, node.position);
		}
		
		public function testPosition1():Void {
			var latLong1:LatLong = new LatLong(0, 0);
			var latLong2:LatLong = new LatLong(10, 10);
			var node:Node = new Node("123", null, latLong1);
			listenForEvent(node, LatLongChangeEvent.POSITION_CHANGE, EVENT_EXPECTED);
			node.position = latLong2;
			assertEvents();
			assertEquals(latLong2, node.position);
			assertEquals(latLong1, LatLongChangeEvent(lastDispatchedExpectedEvent).oldValue);
		}
		public function testPosition2():Void {
			var latLong:LatLong = new LatLong(10, 10);
			var node:Node = new Node("123", null, latLong);
			listenForEvent(node, LatLongChangeEvent.POSITION_CHANGE, EVENT_UNEXPECTED);
			node.position = latLong.clone();
			assertEvents();
			assertEquals(latLong, node.position);
		}
	}
