package com.cloudmade.mappingtool.test.map.model;

	import com.cloudmade.mappingtool.map.geo.LatLong;
	import com.cloudmade.mappingtool.map.model.Node;
	import com.cloudmade.mappingtool.map.model.Tag;
	import com.cloudmade.mappingtool.map.model.Way;
	
	import flash.geom.Point;
	
	import flexunit.framework.EventfulTestCase;

	class WayTest extends EventfulTestCase {
		
		public function new(?methodName:String = null) {
			super(methodName);
		}
		
		public function testConstructor():Void {
			var tags:Array<Dynamic> = [new Tag("key", "value")];
			var nodes:Array<Dynamic> = [new Node("100", [new Tag("key1", "value1")], new LatLong(10, 10)),
							   new Node("101", [new Tag("key2", "value2")], new LatLong(20, 20)),
							   new Node("102", [new Tag("key3", "value3")], new LatLong(30, 30))];
			var way:Way = new Way("123", tags, nodes);
			assertEquals(123, way.id);
			assertEquals(tags, way.tags.source);
			assertEquals(nodes, way.nodes.source);
		}
		
		public function testGetNodePoints():Void {
			var node1:Node = new Node("100", [new Tag("key1", "value1")], new LatLong(10, 10));
			var node2:Node = new Node("101", [new Tag("key2", "value2")], new LatLong(20, 20));
			var node3:Node = new Node("102", [new Tag("key3", "value3")], new LatLong(30, 30));
			var way:Way = new Way("123", null, [node1, node2, node3]);
			var point1:Point = new Point(10, 10);
			var point2:Point = new Point(20, 20);
			var point3:Point = new Point(30, 30);
			node1.move(10, 10);
			node2.move(20, 20);
			node3.move(30, 30);
			var points:Array<Dynamic> = way.getNodePoints();
			assertEquals(3, points.length);
			assertTrue(point1.equals(points[0]));
			assertTrue(point2.equals(points[1]));
			assertTrue(point3.equals(points[2]));
		}
	}
