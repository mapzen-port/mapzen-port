package com.cloudmade.mappingtool.test.map.model;

	import com.cloudmade.mappingtool.map.events.TagEvent;
	import com.cloudmade.mappingtool.map.model.Tag;
	
	import flexunit.framework.EventfulTestCase;

	class TagTest extends EventfulTestCase {
		
		public function new(?methodName:String = null) {
			super(methodName);
		}
		
		public function testConstructor():Void {
			var tag:Tag = new Tag("someKey", "someValue");
			assertEquals("someKey", tag.key);
			assertEquals("someValue", tag.value);
		}
		
		public function testKey1():Void {
			var tag:Tag = new Tag(null, null);
			listenForEvent(tag, TagEvent.KEY_CHANGE, EVENT_EXPECTED);
			tag.key = "someKey";
			assertEvents();
			assertEquals("someKey", tag.key);
		}
		public function testKey2():Void {
			var tag:Tag = new Tag("someKey", null);
			listenForEvent(tag, TagEvent.KEY_CHANGE, EVENT_UNEXPECTED);
			tag.key = "someKey";
			assertEvents();
		}
		
		public function testValue1():Void {
			var tag:Tag = new Tag(null, null);
			listenForEvent(tag, TagEvent.VALUE_CHANGE, EVENT_EXPECTED);
			tag.value = "someValue";
			assertEvents();
			assertEquals("someValue", tag.value);
		}
		public function testValue2():Void {
			var tag:Tag = new Tag(null, "someValue");
			listenForEvent(tag, TagEvent.VALUE_CHANGE, EVENT_UNEXPECTED);
			tag.value = "someValue";
			assertEvents();
		}
		
		public function testEquals():Void {
			var tag1:Tag = new Tag("key", "value");
			var tag2:Tag = new Tag("key", "value");
			assertTrue(tag1.equals(tag2));
			var tag3:Tag = new Tag("key", ":value1");
			assertFalse(tag1.equals(tag3));
		}
		
		public function testClone():Void {
			var sourceTag:Tag = new Tag("key", "value");
			var cloneTag:Tag = sourceTag.clone();
			assertTrue(cloneTag.equals(sourceTag));
			assertFalse(cloneTag == sourceTag);
		}
	}
