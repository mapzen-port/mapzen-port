package com.cloudmade.mappingtool.test.map.model;

	import com.cloudmade.mappingtool.map.model.Element;
	import com.cloudmade.mappingtool.map.model.Tag;
	
	import flexunit.framework.EventfulTestCase;

	class ElementTest extends EventfulTestCase {
		
		public function new(?methodName:String = null) {
			super(methodName);
		}
		
		public function testConstructor():Void {
			var tags:Array<Dynamic> = [new Tag("key", "value")];
			var element:Element = new Element("123", tags);
			assertEquals(123, element.id);
			assertEquals(tags, element.tags.source);
		}
		
		public function testX():Void {
			var element:Element = new Element("100", null);
			element.x = 10;
			assertEquals(10, element.x);
		}
		
		public function testY():Void {
			var element:Element = new Element("100", null);
			element.y = 10;
			assertEquals(10, element.y);
		}
		
		public function testGetTag1():Void {
			var element:Element = new Element("100", null);
			assertNull(element.getTag("key", "value"));
		}
		public function testGetTag2():Void {
			var tag:Tag = new Tag("key", "value");
			var element:Element = new Element("100", [tag]);
			assertStrictlyEquals(tag, element.getTag("key", "value"));
		}
		public function testGetTag3():Void {
			var tag:Tag = new Tag("key", "value");
			var element:Element = new Element("100", [tag]);
			assertNull(element.getTag(null, null));
		}
		
		public function testMove():Void {
			var element:Element = new Element("100", null);
			element.move(10, 10);
			assertEquals(10, element.x);
			assertEquals(10, element.y);
		}
	}
