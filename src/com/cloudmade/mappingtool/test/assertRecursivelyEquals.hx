package com.cloudmade.mappingtool.test;

	import flexunit.framework.Assert;
	
	import mx.utils.ObjectUtil;
	
	public function assertRecursivelyEquals(firstObject:Object, secondObject:Object):void {
		Assert.assertEquals(0, ObjectUtil.compare(firstObject, secondObject));
	