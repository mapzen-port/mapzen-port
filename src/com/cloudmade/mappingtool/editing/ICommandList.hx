/**
 * Undo/redo functionality for commands.
 */
package com.cloudmade.mappingtool.editing;

	import flash.events.IEventDispatcher;
	
	import mx.collections.IList;
	
	/*[Event(name="change", type="flash.events.Event")]*/
	/*[Event(name="hasUndoCommandsChange", type="flash.events.Event")]*/
	
	interface ICommandList implements IEventDispatcher{
		function source():IList;
		
		/*[Bindable("change")]*/
		function undoCommands():Array<Dynamic>;
		
		/*[Bindable("change")]*/
		function redoCommands():Array<Dynamic>;
		
		/*[Bindable("hasUndoCommandsChange")]*/
		function hasUndoCommands():Bool;
		
		/*[Bindable("change")]*/
		function currentIndex():Int;
		function currentIndex(value:Int):Void;
		
		function clear():Void;
	}
