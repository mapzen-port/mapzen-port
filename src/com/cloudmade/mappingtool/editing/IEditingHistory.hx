/**
 * Undo/redo functionality.
 */
package com.cloudmade.mappingtool.editing;

	interface IEditingHistory
	{
		function undo(?count:Int = 1):Void;
		function redo(?count:Int = 1):Void;
	}
