/**
 * Undo/redo functionality for commands.
 * 
 * @see com.cloudmade.mappingtool.editing.ICommandList
 * @see com.cloudmade.mappingtool.commands.IRedoableCommand
 * @see com.cloudmade.mappingtool.commands.IUndoableCommand
 */
package com.cloudmade.mappingtool.editing;

	import com.cloudmade.mappingtool.commands.IRedoableCommand;
	import com.cloudmade.mappingtool.commands.IUndoableCommand;
	
	class CommandHistory implements IEditingHistory {
		
		var commandList:ICommandList;
		
		public function new(commandList:ICommandList) {
			this.commandList = commandList;
		}
		
		public function undo(?count:Int = 1):Void {
			count = Math.min(commandList.currentIndex, count);
			var undoCommands:Array<Dynamic> = commandList.undoCommands;
			for (i in 0...count) {
				IUndoableCommand(undoCommands.pop()).undo();
			}
			commandList.currentIndex -= count;
		}
		
		public function redo(?count:Int = 1):Void {
			count = Math.min(commandList.source.length - commandList.currentIndex, count);
			var redoCommands:Array<Dynamic> = commandList.redoCommands.slice(0, count);
			for (command in redoCommands) {
				command.redo();
			}
			commandList.currentIndex += count;
		}
	}
