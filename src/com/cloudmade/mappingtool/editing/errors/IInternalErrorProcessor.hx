package com.cloudmade.mappingtool.editing.errors;

	import com.cloudmade.mappingtool.editing.ICommandList;
	
	interface IInternalErrorProcessor
	{
		function processInternalError(commandList:ICommandList, commitData:String):Void;
	}
