package com.cloudmade.mappingtool.editing.errors;

	import mx.controls.Alert;
	import mx.resources.IResourceManager;
	
	class ErrorAnnouncer implements IErrorAnnouncer {
		
		var resourceManager:IResourceManager;
		
		public function new(resourceManager:IResourceManager) {
			this.resourceManager = resourceManager;
		}
		
		public function announceServiceFailure():Void {
			showAlert("service_failure");
		}
		
		public function announceDataConflict():Void {
			showAlert("data_conflict");
		}
		
		public function announceInternalError():Void {
			showAlert("internal_error");
		}
		
		function showAlert(resourceName:String):Void {
			Alert.show(resourceManager.getString("Global", resourceName));
		}
	}
