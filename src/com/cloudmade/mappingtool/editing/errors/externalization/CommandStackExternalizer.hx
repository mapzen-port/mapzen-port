package com.cloudmade.mappingtool.editing.errors.externalization;

	import com.cloudmade.mappingtool.editing.CommandList;
	import com.cloudmade.mappingtool.editing.ICommandList;
	
	import flash.utils.ByteArray;
	
	import mx.collections.ArrayList;
	import mx.utils.Base64Decoder;
	import mx.utils.Base64Encoder;
	
	class CommandStackExternalizer
	 {
		
		public function new() {
		}
		
		public function serialize(commandList:ICommandList):String {
			var bytes:ByteArray = new ByteArray();
			bytes.writeObject(commandList.source.toArray());
			bytes.writeInt(commandList.currentIndex);
			
			var encoder:Base64Encoder = new Base64Encoder();
			encoder.insertNewLines = false;
			encoder.encodeBytes(bytes);
			return encoder.toString();
		}
		
		public function deserialize(data:String):ICommandList {
			var decoder:Base64Decoder = new Base64Decoder();
			decoder.decode(data);
			var bytes:ByteArray = decoder.toByteArray();
			
			var commands:Array<Dynamic> = bytes.readObject();
			var index:Int = bytes.readInt();
			return new CommandList(new ArrayList(commands), index);
		}
	}
