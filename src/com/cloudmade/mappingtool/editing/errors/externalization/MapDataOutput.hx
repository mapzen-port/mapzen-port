package com.cloudmade.mappingtool.editing.errors.externalization;

	import com.cloudmade.mappingtool.map.model.Map;
	
	import flash.utils.ByteArray;
	import flash.utils.IDataOutput;

	class MapDataOutput implements IDataOutput {
		
		public var endian(getEndian, setEndian) : String;
		public var map(getMap, null) : Map ;
		public var objectEncoding(getObjectEncoding, setObjectEncoding) : UInt;
		public function new() {
		}
		
		var _map:Map;
		public function getMap():Map {
			return _map;
		}
		
		public function writeBytes(bytes:ByteArray, ?offset:UInt = 0, ?length:UInt = 0):Void {
		}
		
		public function writeBoolean(value:Bool):Void {
		}
		
		public function writeByte(value:Int):Void {
		}
		
		public function writeShort(value:Int):Void {
		}
		
		public function writeInt(value:Int):Void {
		}
		
		public function writeUnsignedInt(value:UInt):Void {
		}
		
		public function writeFloat(value:Int):Void {
		}
		
		public function writeDouble(value:Int):Void {
		}
		
		public function writeMultiByte(value:String, charSet:String):Void {
		}
		
		public function writeUTF(value:String):Void {
		}
		
		public function writeUTFBytes(value:String):Void {
		}
		
		public function writeObject(object:Dynamic):Void {
			if (Std.is( object, Map)) {
				_map = object;
			}
		}
		
		public function getObjectEncoding():UInt{
			return 0;
		}
		
		public function setObjectEncoding(version:UInt):UInt{
			return version;
	}
		
		public function getEndian():String{
			return null;
		}
		
		public function setEndian(type:String):String{
			return type;
	}
	}
