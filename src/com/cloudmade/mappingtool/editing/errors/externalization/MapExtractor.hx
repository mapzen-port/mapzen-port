package com.cloudmade.mappingtool.editing.errors.externalization;

	import com.cloudmade.mappingtool.commands.editing.IMacroElementCommand;
	import com.cloudmade.mappingtool.map.model.Map;
	
	import flash.utils.IExternalizable;
	
	class MapExtractor
	 {
		
		var mapDataOutput:MapDataOutput;
		
		public function new(mapDataOutput:MapDataOutput) {
			this.mapDataOutput = mapDataOutput;
		}
		
		public function extractMap(command:IExternalizable):Map {
			while (Std.is( command, IMacroElementCommand)) {
				command = IMacroElementCommand(command).commands[0];
			}
			command.writeExternal(mapDataOutput);
			return mapDataOutput.map;
		}
	}
