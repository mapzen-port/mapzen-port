package com.cloudmade.mappingtool.editing.errors;

	import com.cloudmade.mappingtool.config.IReadOnlyHashMap;
	import com.cloudmade.mappingtool.config.parsers.HashMapParser;
	import com.cloudmade.mappingtool.config.parsers.IXMLListToObjectParser;
	import com.cloudmade.mappingtool.config.validators.BooleanXMLParser;
	import com.cloudmade.mappingtool.config.validators.ReferencesValidator;
	import com.cloudmade.mappingtool.config.validators.UniqueNamesValidator;
	
	import mx.controls.Alert;
	
	class CommitDataValidator
	 {
		
		public function new() {
		}
		
		public function validate(commitData:XML):Void {
			new UniqueNamesValidator(function(id:String):Void {
				showAlert("Duplicate new element definition: " + id);
			}).validate(commitData.children().children(), "id");
			
			var newElementMapParser:IXMLListToObjectParser = new HashMapParser("id", new BooleanXMLParser());
			var newElementMap:IReadOnlyHashMap = cast( newElementMapParser.parse(commitData.create.children()), IReadOnlyHashMap);
			new ReferencesValidator(newElementMap, function(id:String):Void {
				showAlert("Absent new node reference: " + id);
			}).validate(commitData.children().way.nd.(@ref < 0), "ref");
			new ReferencesValidator(newElementMap, function(id:String):Void {
				showAlert("Absent new member reference: " + id);
			}).validate(commitData.children().relation.member.(@ref < 0), "ref");
		}
		
		function showAlert(message:String):Void {
			Alert.show(message, "", Alert.NONMODAL);
		}
	}
