package com.cloudmade.mappingtool.editing.errors;

	import com.cloudmade.mappingtool.editing.ICommandList;
	import com.cloudmade.mappingtool.events.OSMProxyCommitErrorEvent;
	import com.cloudmade.mappingtool.events.OSMProxyLoadErrorEvent;
	import com.cloudmade.mappingtool.events.OSMProxyResultEvent;
	import com.cloudmade.mappingtool.events.OSMProxySaveEvent;
	import com.cloudmade.mappingtool.model.OSMProxy;
	import com.cloudmade.mappingtool.services.MapEditService;
	
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	
	class ErrorProcessor
	 {
		
		var osmProxy:OSMProxy;
		var editService:MapEditService;
		var commandList:ICommandList;
		var errorAnnouncer:IErrorAnnouncer;
		var internalErrorProcessor:IInternalErrorProcessor;
		
		var commitErrorHandled:Bool ;
		
		public function new(osmProxy:OSMProxy, editService:MapEditService, commandList:ICommandList, errorAnnouncer:IErrorAnnouncer, internalErrorProcessor:IInternalErrorProcessor) {
			
			commitErrorHandled = false;
			this.osmProxy = osmProxy;
			this.editService = editService;
			this.commandList = commandList;
			this.errorAnnouncer = errorAnnouncer;
			this.internalErrorProcessor = internalErrorProcessor;
		}
		
		public function initialize():Void {
			osmProxy.addEventListener(OSMProxyLoadErrorEvent.LOAD_ERROR, handleOsmProxyLoadError, false, 0, true);
			editService.addEventListener(IOErrorEvent.IO_ERROR, handleDataConflict, false, 0, true);
			osmProxy.addEventListener(OSMProxySaveEvent.SAVE_FAILED, handleDataConflict, false, 0, true);
			switchToDataConflicts();
		}
		
		function handleOsmProxyLoadError(event:OSMProxyLoadErrorEvent):Void {
			errorAnnouncer.announceServiceFailure();
			switchToDataConflicts();
		}
		
		function switchToDataConflicts():Void {
			osmProxy.removeEventListener(OSMProxyResultEvent.RESULT, handleOsmProxyResult);
			osmProxy.removeEventListener(OSMProxyCommitErrorEvent.COMMIT_ERROR, handleInternalError);
			commandList.removeEventListener(Event.CHANGE, handleCommandListChange);
		}
		
		function switchToInternalErrors():Void {
			osmProxy.removeEventListener(OSMProxyResultEvent.RESULT, handleOsmProxyResult);
			osmProxy.addEventListener(OSMProxyCommitErrorEvent.COMMIT_ERROR, handleInternalError, false, 0, true);
			commandList.addEventListener(Event.CHANGE, handleCommandListChange, false, 0, true);
		}
		
		function handleDataConflict(event:Event):Void {
			if (!commitErrorHandled) {
				errorAnnouncer.announceDataConflict();
				osmProxy.addEventListener(OSMProxyResultEvent.RESULT, handleOsmProxyResult, false, 0, true);
			} else {
				commitErrorHandled = false;
			}
		}
		
		function handleOsmProxyResult(event:OSMProxyResultEvent):Void {
			switchToInternalErrors();
		}
		
		function handleCommandListChange(event:Event):Void {
			switchToDataConflicts();
		}
		
		function handleInternalError(event:OSMProxyCommitErrorEvent):Void {
			commitErrorHandled = true;
			internalErrorProcessor.processInternalError(commandList, event.commitData);
			errorAnnouncer.announceInternalError();
			commandList.removeEventListener(Event.CHANGE, handleCommandListChange);
		}
	}
