package com.cloudmade.mappingtool.editing.errors;

	import com.cloudmade.mappingtool.editing.ICommandList;
	import com.cloudmade.mappingtool.editing.errors.externalization.CommandStackExternalizer;

	class InternalErrorProcessor implements IInternalErrorProcessor {
		
		var errorReporter:IInternalErrorReporter;
		var commandStackExternalizer:CommandStackExternalizer;
		
		public function new(errorReporter:IInternalErrorReporter, commandStackExternalizer:CommandStackExternalizer) {
			this.errorReporter = errorReporter;
			this.commandStackExternalizer = commandStackExternalizer;
		}
		
		public function processInternalError(commandList:ICommandList, commitData:String):Void {
			var commandsData:String = commandStackExternalizer.serialize(commandList);
			errorReporter.reportInternalError(commandsData, commitData);
		}
	}
