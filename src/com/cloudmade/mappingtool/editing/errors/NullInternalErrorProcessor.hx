package com.cloudmade.mappingtool.editing.errors;

	import com.cloudmade.mappingtool.editing.ICommandList;

	class NullInternalErrorProcessor implements IInternalErrorProcessor {
		
		public function new() {
		}
		
		public function processInternalError(commandList:ICommandList, commitData:String):Void {
		}
	}
