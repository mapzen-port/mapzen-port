package com.cloudmade.mappingtool.editing.errors;

	import flash.external.ExternalInterface;
	
	class InternalErrorReporter implements IInternalErrorReporter {
		
		var errorCallbackName:String;
		
		public function new(errorCallbackName:String) {
			this.errorCallbackName = errorCallbackName;
		}
		
		public function reportInternalError(commandsData:String, commitData:String):Void {
			ExternalInterface.call(errorCallbackName, commitData, commandsData);
		}
	}
