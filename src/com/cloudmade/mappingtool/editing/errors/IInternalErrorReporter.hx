package com.cloudmade.mappingtool.editing.errors;

	interface IInternalErrorReporter
	{
		function reportInternalError(commandsData:String, commitData:String):Void;
	}
