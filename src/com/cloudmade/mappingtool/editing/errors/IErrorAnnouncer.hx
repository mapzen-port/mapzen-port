package com.cloudmade.mappingtool.editing.errors;

	interface IErrorAnnouncer
	{
		function announceServiceFailure():Void;
		
		function announceDataConflict():Void;
		
		function announceInternalError():Void;
	}
