/**
 * Executes editing commands.
 * 
 * @see com.cloudmade.mappingtool.editing.ICommandList
 * @see com.cloudmade.mappingtool.commands.editing.IElementCommand
 */
package com.cloudmade.mappingtool.editing;

	import com.cloudmade.mappingtool.commands.editing.IElementCommand;

	class CommandExecutor implements ICommandExecutor {
		
		var commandList:ICommandList;
		var listTruncator:ListTruncator;
		
		public function new(commandList:ICommandList, listTruncator:ListTruncator) {
			this.commandList = commandList;
			this.listTruncator = listTruncator;
		}
		
		public function execute(command:IElementCommand):Void {
			command.execute();
			commandList.source.addItemAt(command, commandList.currentIndex);
			commandList.currentIndex++;
			listTruncator.truncate(commandList.currentIndex);
		}
	}
