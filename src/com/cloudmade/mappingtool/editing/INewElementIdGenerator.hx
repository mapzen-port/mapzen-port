package com.cloudmade.mappingtool.editing;

	interface INewElementIdGenerator
	{
		function getIdForNewElement():String;
	}
