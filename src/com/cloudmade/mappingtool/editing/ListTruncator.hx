package com.cloudmade.mappingtool.editing;

	import mx.collections.IList;
	
	class ListTruncator
	 {
		
		var list:IList;
		
		public function new(list:IList) {
			this.list = list;
		}
		
		public function truncate(fromIndex:Int):Void {
			var removeNum:Int = list.length - fromIndex;
			for (i in 0...removeNum) {
				list.removeItemAt(fromIndex);
			}
		}
	}
