/**
 * A list of commands.
 */
package com.cloudmade.mappingtool.editing;

	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	import mx.collections.IList;
	import mx.events.CollectionEvent;
	
	/*[Event(name="change", type="flash.events.Event")]*/
	
	class CommandList extends EventDispatcher, implements ICommandList {
		
		public var currentIndex(getCurrentIndex, setCurrentIndex) : Int;
		public var hasUndoCommands(getHasUndoCommands, null) : Bool ;
		public var redoCommands(getRedoCommands, null) : Array<Dynamic> ;
		public var source(getSource, null) : IList ;
		public var undoCommands(getUndoCommands, null) : Array<Dynamic> ;
		public function new(source:IList, ?currentIndex:Int = 0) {
			_source = source;
			_currentIndex = currentIndex;
			lastHasUndoCommandsValue = hasUndoCommands;
			source.addEventListener(CollectionEvent.COLLECTION_CHANGE, handleSourceChange, false, 0, true);
			addEventListener(Event.CHANGE, handleChange, false, 0, true);
		}
		
		var _source:IList;
		public function getSource():IList {
			return _source;
		}
		
		/*[Bindable("change")]*/
		public function getUndoCommands():Array<Dynamic> {
			return source.toArray().slice(0, currentIndex);
		}
		
		/*[Bindable("change")]*/
		public function getRedoCommands():Array<Dynamic> {
			return source.toArray().slice(currentIndex);
		}
		
		var lastHasUndoCommandsValue:Bool;

		/*[Bindable("hasUndoCommandsChange")]*/
		public function getHasUndoCommands():Bool {
			return (undoCommands.length > 0);
		}
		
		var _currentIndex:Int ;
		/*[Bindable("change")]*/
		public function getCurrentIndex():Int{
			return _currentIndex;
		}
		public function setCurrentIndex(value:Int):Int{
			if (value == _currentIndex) return;
			_currentIndex = value;
			notifyAboutChange();
			return value;
		}
		
		public function clear():Void {
			_currentIndex = 0;
			source.removeAll();
		}
		
		function notifyAboutChange():Void {
			dispatchEvent(new Event(Event.CHANGE));
		}
		
		function notifyAboutHasUndoCommandsChange():Void {
			dispatchEvent(new Event('hasUndoCommandsChange'));
		}
		
		function handleSourceChange(event:CollectionEvent):Void {
			notifyAboutChange();
		}
		
		function handleChange(event:Event):Void {
			var	newHasUndoCommandsValue:Bool = hasUndoCommands;
			if (newHasUndoCommandsValue != lastHasUndoCommandsValue) {
				notifyAboutHasUndoCommandsChange();
				lastHasUndoCommandsValue = newHasUndoCommandsValue;
			}
		}
	}
