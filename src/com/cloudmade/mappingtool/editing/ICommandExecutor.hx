/**
 * Executes editing commands.
 * 
 * @see com.cloudmade.mappingtool.commands.editing.IElementCommand
 */
package com.cloudmade.mappingtool.editing;

	import com.cloudmade.mappingtool.commands.editing.IElementCommand;
	
	interface ICommandExecutor
	{
		function execute(command:IElementCommand):Void;
	}
