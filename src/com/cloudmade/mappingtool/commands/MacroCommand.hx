package com.cloudmade.mappingtool.commands;

	class MacroCommand implements IRedoableCommand {
		
		var _commands:Array<Dynamic>;
		
		public function new(?commands:Array<Dynamic> = null) {
			_commands = commands;
		}

		public function execute():Void {
			for (command in _commands) {
				command.execute();
			}
		}
		
		public function undo():Void {
			var invertedCommands:Array<Dynamic> = _commands.slice().reverse();
			for (command in invertedCommands) {
				command.undo();
			}
		}
		
		public function redo():Void {
			for (command in _commands) {
				command.redo();
			}
		}
	}
