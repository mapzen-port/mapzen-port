package com.cloudmade.mappingtool.commands;

	interface IUndoableCommand implements ICommand{
		function undo():Void;
	}
