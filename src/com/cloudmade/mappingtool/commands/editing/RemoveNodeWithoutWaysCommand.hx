package com.cloudmade.mappingtool.commands.editing;

	import com.cloudmade.mappingtool.map.model.Element;
	import com.cloudmade.mappingtool.map.model.Map;
	import com.cloudmade.mappingtool.map.model.Node;
	
	/*[RemoteClass(alias="com.cloudmade.mappingtool.commands.editing.RemoveNodeWithoutWaysCommand")]*/
	
	class RemoveNodeWithoutWaysCommand extends BaseNodeCommand, implements IElementCommand {
		
		public var category(getCategory, null) : String ;
		public var target(getTarget, null) : Element ;
		public function new(?map:Map = null, ?node:Node = null) {
			super(map, node);
		}
		
		public function getCategory():String {
			return ElementCommandCategory.DELETE;
		}
		
		public override function getTarget():Element {
			// Parent way was already removed.
			return (map.getNodeWays(node).length > 0) ? null : node;
		}
		
		// Keep in mind that parent way might be closed, so there will be two commands with same node.
		// But all must work fine.
		public override function execute():Void {
			if (updateNode()) {
				if (map.getNodeWays(node).length == 0) {
					map.nodes.removeItem(node);
				}
			}
		}
		
		public override function undo():Void {
			if (!updateNode()) {
				map.nodes.addItem(node);
			}
		}
	}
