package com.cloudmade.mappingtool.commands.editing;

	import com.cloudmade.mappingtool.map.model.Map;
	import com.cloudmade.mappingtool.map.model.Way;
	
	/*[RemoteClass(alias="com.cloudmade.mappingtool.commands.editing.RemoveWayCommand")]*/
	
	class RemoveWayCommand extends BaseWayCommand, implements IElementCommand {
		
		public var category(getCategory, null) : String ;
		public function new(?map:Map = null, ?way:Way = null) {
			super(map, way);
		}
		
		public function getCategory():String {
			return ElementCommandCategory.DELETE;
		}
		
		public override function execute():Void {
			if (updateWay()) {
				map.ways.removeItem(way);
			}
		}
		
		public override function undo():Void {
			if (!updateWay()) {
				map.ways.addItem(way);
			}
		}
	}
