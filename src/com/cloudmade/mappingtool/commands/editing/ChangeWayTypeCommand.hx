package com.cloudmade.mappingtool.commands.editing;

	import com.cloudmade.mappingtool.map.model.Map;
	import com.cloudmade.mappingtool.map.model.Way;
	
	import flash.utils.IDataInput;
	import flash.utils.IDataOutput;
	
	/*[RemoteClass(alias="com.cloudmade.mappingtool.commands.editing.ChangeWayTypeCommand")]*/
	
	class ChangeWayTypeCommand extends BaseWayCommand, implements IElementCommand {
		
		public var category(getCategory, null) : String ;
		var isOneWay:Bool;
		var oldIsOneWay:Bool;
		
		public function new(?map:Map = null, ?way:Way = null, ?isOneWay:Bool = false) {
			super(map, way);
			this.isOneWay = isOneWay;
		}
		
		public function getCategory():String {
			return ElementCommandCategory.MODIFY;
		}
		
		public override function execute():Void {
			if (updateWay()) {
				oldIsOneWay = way.isOneWay;
				way.isOneWay = isOneWay;
				refreshWay(way);
			}
		}
		
		public override function undo():Void {
			if (updateWay()) {
				way.isOneWay = oldIsOneWay;
				refreshWay(way);
			}
		}
		
		public override function readExternal(input:IDataInput):Void {
			super.readExternal(input);
			isOneWay = input.readBoolean();
			oldIsOneWay = input.readBoolean();
		}
		
		public override function writeExternal(output:IDataOutput):Void {
			super.writeExternal(output);
			output.writeBoolean(isOneWay);
			output.writeBoolean(oldIsOneWay);
		}
		
	}
