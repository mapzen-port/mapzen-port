package com.cloudmade.mappingtool.commands.editing;

	import com.cloudmade.mappingtool.map.model.Element;
	import com.cloudmade.mappingtool.map.model.Map;
	import com.cloudmade.mappingtool.map.model.Way;
	
	import flash.utils.IDataInput;
	import flash.utils.IDataOutput;
	
	/*[RemoteClass(alias="com.cloudmade.mappingtool.commands.editing.BaseWayCommand")]*/
	
	class BaseWayCommand extends BaseElementCommand {
		
		public var target(getTarget, null) : Element ;
		var way:Way;
		
		public function new(?map:Map = null, ?way:Way = null) {
			super(map);
			this.way = way;
		}
		
		public function getTarget():Element {
			return way;
		}
		
		public override function readExternal(input:IDataInput):Void {
			super.readExternal(input);
			way = input.readObject();
		}
		
		public override function writeExternal(output:IDataOutput):Void {
			super.writeExternal(output);
			output.writeObject(way);
		}
		
		function updateWay():Bool {
			var newWay:Way = map.getWay(way.id);
			if (newWay) {
				way = newWay;
				return true;
			}
			return false;
		}
	}
