package com.cloudmade.mappingtool.commands.editing;

	import com.cloudmade.mappingtool.map.model.Map;
	import com.cloudmade.mappingtool.map.model.Way;
	
	/*[RemoteClass(alias="com.cloudmade.mappingtool.commands.editing.ReverseWayCommand")]*/
	
	class ReverseWayCommand extends BaseWayCommand, implements IElementCommand {
		
		public var category(getCategory, null) : String ;
		public function new(?map:Map = null, ?way:Way = null) {
			super(map, way);
		}
		
		public function getCategory():String {
			return ElementCommandCategory.MODIFY;
		}
		
		public override function execute():Void {
			if (updateWay()) {
				way.nodes.source.reverse();
				refreshWay(way);
			}
		}
		
		public override function undo():Void {
			execute();
		}
	}
