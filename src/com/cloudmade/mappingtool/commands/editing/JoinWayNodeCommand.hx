package com.cloudmade.mappingtool.commands.editing;

	import com.cloudmade.mappingtool.map.model.Map;
	import com.cloudmade.mappingtool.map.model.Node;
	import com.cloudmade.mappingtool.map.model.Way;
	
	import flash.utils.IDataInput;
	import flash.utils.IDataOutput;
	
	/*[RemoteClass(alias="com.cloudmade.mappingtool.commands.editing.JoinWayNodeCommand")]*/
	
	class JoinWayNodeCommand extends BaseWayCommand, implements IElementCommand {
		
		public var category(getCategory, null) : String ;
		var node:Node;
		var index:Int;
		var currentIndex:Int;
		
		public function new(?map:Map = null, ?way:Way = null, ?node:Node = null, ?index:Int = -1) {
			super(map, way);
			this.node = node;
			this.index = index;
		}
		
		public function getCategory():String {
			return ElementCommandCategory.MODIFY;
		}

		public override function execute():Void {
			if (!updateWay()) return;
			updateNode();
			if (index != -1) {
				way.nodes.addItemAt(node, index);
			} else {
				way.nodes.addItem(node);
			}
			refreshWay(way);
		}
		
		public override function undo():Void {
			if (updateWay() && updateNode(false)) {
				currentIndex = way.nodes.source.lastIndexOf(node);
				way.nodes.removeItemAt(currentIndex);
				refreshWay(way);
			}
		}
		
		public override function redo():Void {
			if (updateWay()) {
				updateNode();
				way.nodes.addItemAt(node, currentIndex);
				refreshWay(way);
			}
		}
		
		public override function readExternal(input:IDataInput):Void {
			super.readExternal(input);
			node = input.readObject();
			index = input.readInt();
			currentIndex = input.readInt();
		}
		
		public override function writeExternal(output:IDataOutput):Void {
			super.writeExternal(output);
			output.writeObject(node);
			output.writeInt(index);
			output.writeInt(currentIndex);
		}
		
		// Normalize - in case when node is out of view port.
		function updateNode(?normalize:Bool = true):Bool {
			var newNode:Node = map.getNode(node.id);
			if (newNode) {
				node = newNode;
				return true;
			} else if (normalize) {
				map.nodes.addItem(node);
			}
			return false;
		}
	}
