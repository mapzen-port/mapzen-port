package com.cloudmade.mappingtool.commands.editing;

	import com.cloudmade.mappingtool.map.model.Map;
	import com.cloudmade.mappingtool.map.model.Way;
	
	import flash.utils.IDataInput;
	import flash.utils.IDataOutput;
	
	/*[RemoteClass(alias="com.cloudmade.mappingtool.commands.editing.AddWayCommand")]*/
	
	class AddWayCommand extends BaseWayCommand, implements IElementCommand {
		
		public var category(getCategory, null) : String ;
		var source:Way;
		
		public function new(?map:Map = null, ?way:Way = null) {
			super(map, way);
		}
		
		public function getCategory():String {
			return ElementCommandCategory.CREATE;
		}
		
		public override function execute():Void {
			if (updateWay()) return;
			// If command is executing first time, save source way, so in
			// future it will be possible to reapply changes in original order.
			// But don't replace way in first execution when way is also stored as a selection.
			if (source) {
				way = source.clone();
			} else {
				source = way.clone();
			}
			map.ways.addItem(way);
		}
		
		public override function undo():Void {
			if (updateWay()) {
				map.ways.removeItem(way);
			}
		}
		
		public override function readExternal(input:IDataInput):Void {
			super.readExternal(input);
			source = input.readObject();
		}
		
		public override function writeExternal(output:IDataOutput):Void {
			super.writeExternal(output);
			output.writeObject(source);
		}
	}
