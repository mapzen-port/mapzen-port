package com.cloudmade.mappingtool.commands.editing;

	import com.cloudmade.mappingtool.map.model.Element;
	import com.cloudmade.mappingtool.map.model.Map;
	import com.cloudmade.mappingtool.map.model.Node;
	
	import flash.utils.IDataInput;
	import flash.utils.IDataOutput;
	
	/*[RemoteClass(alias="com.cloudmade.mappingtool.commands.editing.BaseNodeCommand")]*/
	
	class BaseNodeCommand extends BaseElementCommand {
		
		public var target(getTarget, null) : Element ;
		var node:Node;
		
		public function new(?map:Map = null, ?node:Node = null) {
			super(map);
			this.node = node;
		}
		
		public function getTarget():Element {
			return node;
		}
		
		public override function readExternal(input:IDataInput):Void {
			super.readExternal(input);
			node = input.readObject();
		}
		
		public override function writeExternal(output:IDataOutput):Void {
			super.writeExternal(output);
			output.writeObject(node);
		}
		
		function updateNode():Bool {
			var newNode:Node = map.getNode(node.id);
			if (newNode) {
				node = newNode;
				return true;
			}
			return false;
		}
	}
