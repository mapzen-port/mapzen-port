package com.cloudmade.mappingtool.commands.editing.tags;

	import com.cloudmade.mappingtool.map.model.Element;
	import com.cloudmade.mappingtool.map.model.Map;
	import com.cloudmade.mappingtool.map.model.Tag;
	
	import flash.utils.IDataInput;
	import flash.utils.IDataOutput;
	
	/*[RemoteClass(alias="com.cloudmade.mappingtool.commands.editing.tags.ChangeElementTagValueCommand")]*/
	
	class ChangeElementTagValueCommand extends BaseChangeElementTagCommand {
		
		var value:String;
		var oldValue:String ;
		
		public function new(?map:Map = null, ?element:Element = null, ?tag:Tag = null, ?value:String = null) {
			
			oldValue = "";
			super(map, element, tag);
			this.value = value;
		}
		
		public override function readExternal(input:IDataInput):Void {
			super.readExternal(input);
			value = input.readUTF();
			oldValue = input.readUTF();
		}
		
		public override function writeExternal(output:IDataOutput):Void {
			super.writeExternal(output);
			output.writeUTF(value);
			output.writeUTF(oldValue);
		}
		
		override function concreteExecute():Void {
			if (updateTag()) {
				oldValue = tag.value;
				tag.value = value;
			}
		}
		
		override function concreteUndo():Void {
			if (updateNewTag()) {
				tag.value = oldValue;
			}
		}
	}
