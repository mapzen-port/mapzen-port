package com.cloudmade.mappingtool.commands.editing.tags;

	import com.cloudmade.mappingtool.map.model.Element;
	import com.cloudmade.mappingtool.map.model.Map;
	import com.cloudmade.mappingtool.map.model.Tag;
	
	import flash.utils.IDataInput;
	import flash.utils.IDataOutput;
	
	/*[RemoteClass(alias="com.cloudmade.mappingtool.commands.editing.tags.BaseChangeElementTagCommand")]*/
	
	class BaseChangeElementTagCommand extends BaseElementTagCommand {
		
		var sourceTag:Tag;
		
		public function new(?map:Map = null, ?element:Element = null, ?tag:Tag = null) {
			super(map, element, tag);
		}
		
		public override function execute():Void {
			if (!sourceTag) {
				sourceTag = tag.clone();
			}
			super.execute();
		}
		
		public override function readExternal(input:IDataInput):Void {
			super.readExternal(input);
			sourceTag = input.readObject();
		}
		
		public override function writeExternal(output:IDataOutput):Void {
			super.writeExternal(output);
			output.writeObject(sourceTag);
		}
		
		function updateTag():Bool {
			var newTag:Tag = element.getTag(sourceTag.key, sourceTag.value);
			return changeTag(newTag);
		}
		
		function updateNewTag():Bool {
			var newTag:Tag = element.getTag(tag.key, tag.value);
			return changeTag(newTag);
		}
		
		function changeTag(newTag:Tag):Bool {
			if (newTag) {
				tag = newTag;
				return true;
			}
			return false;
		}
	}
