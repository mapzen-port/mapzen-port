package com.cloudmade.mappingtool.commands.editing.tags;

	import com.cloudmade.mappingtool.map.model.Element;
	import com.cloudmade.mappingtool.map.model.Map;
	import com.cloudmade.mappingtool.map.model.Tag;
	
	import flash.utils.IDataInput;
	import flash.utils.IDataOutput;
	
	/*[RemoteClass(alias="com.cloudmade.mappingtool.commands.editing.tags.RemoveElementTagCommand")]*/
	
	class RemoveElementTagCommand extends BaseChangeElementTagCommand {
		
		var index:Int;
		var tagRemoved:Bool;
		
		public function new(?map:Map = null, ?element:Element = null, ?tag:Tag = null) {
			super(map, element, tag);
		}
		
		override function concreteExecute():Void {
			if (updateTag()) {
				index = element.tags.getItemIndex(tag);
				element.tags.removeItemAt(index);
				tagRemoved = true;
			} else {
				tagRemoved = false;
			}
		}
		
		override function concreteUndo():Void {
			if (tagRemoved) {
				element.tags.addItemAt(tag, index);
				tagRemoved = false;
			}
		}
		
		public override function readExternal(input:IDataInput):Void {
			super.readExternal(input);
			index = input.readInt();
			tagRemoved = input.readBoolean();
		}
		
		public override function writeExternal(output:IDataOutput):Void {
			super.writeExternal(output);
			output.writeInt(index);
			output.writeBoolean(tagRemoved);
		}
	}
