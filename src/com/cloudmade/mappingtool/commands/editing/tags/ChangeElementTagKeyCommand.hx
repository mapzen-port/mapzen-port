package com.cloudmade.mappingtool.commands.editing.tags;

	import com.cloudmade.mappingtool.map.model.Element;
	import com.cloudmade.mappingtool.map.model.Map;
	import com.cloudmade.mappingtool.map.model.Tag;
	
	import flash.utils.IDataInput;
	import flash.utils.IDataOutput;
	
	/*[RemoteClass(alias="com.cloudmade.mappingtool.commands.editing.tags.ChangeElementTagKeyCommand")]*/
	
	class ChangeElementTagKeyCommand extends BaseChangeElementTagCommand {
		
		var key:String;
		var oldKey:String ;
		
		public function new(?map:Map = null, ?element:Element = null, ?tag:Tag = null, ?key:String = null) {
			
			oldKey = "";
			super(map, element, tag);
			this.key = key;
		}
		
		public override function readExternal(input:IDataInput):Void {
			super.readExternal(input);
			key = input.readUTF();
			oldKey = input.readUTF();
		}
		
		public override function writeExternal(output:IDataOutput):Void {
			super.writeExternal(output);
			output.writeUTF(key);
			output.writeUTF(oldKey);
		}
		
		override function concreteExecute():Void {
			if (updateTag()) {
				oldKey = tag.key;
				tag.key = key;
			}
		}
		
		override function concreteUndo():Void {
			if (updateNewTag()) {
				tag.key = oldKey;
			}
		}
	}
