package com.cloudmade.mappingtool.commands.editing.tags;

	import com.cloudmade.mappingtool.map.model.Element;
	import com.cloudmade.mappingtool.map.model.Map;
	import com.cloudmade.mappingtool.map.model.Tag;
	
	import flash.utils.IDataInput;
	import flash.utils.IDataOutput;
	
	/*[RemoteClass(alias="com.cloudmade.mappingtool.commands.editing.tags.AddElementTagCommand")]*/
	
	class AddElementTagCommand extends BaseElementTagCommand {
		
		var index:Int;
		
		public function new(?map:Map = null, ?element:Element = null, ?tag:Tag = null) {
			super(map, element, tag);
		}
		
		override function concreteExecute():Void {
			element.tags.addItem(tag);
		}
		
		override function concreteUndo():Void {
			index = element.tags.getItemIndex(tag);
			element.tags.removeItemAt(index);
		}
		
		override function concreteRedo():Void {
			element.tags.addItemAt(tag, index);
		}
		
		public override function readExternal(input:IDataInput):Void {
			super.readExternal(input);
			index = input.readInt();
		}
		
		public override function writeExternal(output:IDataOutput):Void {
			super.writeExternal(output);
			output.writeInt(index);
		}
	}
