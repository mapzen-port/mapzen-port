package com.cloudmade.mappingtool.commands.editing.tags;

	import com.cloudmade.mappingtool.commands.editing.BaseElementCommand;
	import com.cloudmade.mappingtool.commands.editing.ElementCommandCategory;
	import com.cloudmade.mappingtool.commands.editing.IElementCommand;
	import com.cloudmade.mappingtool.map.model.Element;
	import com.cloudmade.mappingtool.map.model.Map;
	import com.cloudmade.mappingtool.map.model.Node;
	import com.cloudmade.mappingtool.map.model.Tag;
	import com.cloudmade.mappingtool.map.model.Way;
	
	import flash.utils.IDataInput;
	import flash.utils.IDataOutput;
	
	/*[RemoteClass(alias="com.cloudmade.mappingtool.commands.editing.tags.BaseElementTagCommand")]*/
	
	class BaseElementTagCommand extends BaseElementCommand, implements IElementCommand {
		
		public var category(getCategory, null) : String ;
		public var target(getTarget, null) : Element ;
		var element:Element;
		var tag:Tag;
		
		public function new(?map:Map = null, ?element:Element = null, ?tag:Tag = null) {
			super(map);
			this.element = element;
			this.tag = tag;
		}
		
		public function getCategory():String {
			return ElementCommandCategory.MODIFY;
		}
		
		public function getTarget():Element {
			return element;
		}
		
		public override function execute():Void {
			if (updateElement()) {
				concreteExecute();
				refreshElement();
			}
		}
		
		public override function undo():Void {
			if (updateElement()) {
				concreteUndo();
				refreshElement();
			}
		}
		
		public override function redo():Void {
			if (updateElement()) {
				concreteRedo();
				refreshElement();
			}
		}
		
		public override function readExternal(input:IDataInput):Void {
			super.readExternal(input);
			element = input.readObject();
			tag = input.readObject();
		}
		
		public override function writeExternal(output:IDataOutput):Void {
			super.writeExternal(output);
			output.writeObject(element);
			output.writeObject(tag);
		}
		
		function concreteExecute():Void {
		}
		
		function concreteUndo():Void {
		}
		
		function concreteRedo():Void {
			concreteExecute();
		}
		
		function updateElement():Bool {
			var newElement:Element;
			if (Std.is( element, Node)) {
				newElement = map.getNode(element.id);
			} else if (Std.is( element, Way)) {
				newElement = map.getWay(element.id);
			}
			if (newElement) {
				element = newElement;
				return true;
			}
			return false;
		}
		
		function refreshElement():Void {
			if (Std.is( element, Node)) {
				refreshNode(cast( element, Node));
			} else if (Std.is( element, Way)) {
				refreshWay(cast( element, Way));
			}
		}
	}
