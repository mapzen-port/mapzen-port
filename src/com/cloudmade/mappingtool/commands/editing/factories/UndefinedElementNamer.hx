package com.cloudmade.mappingtool.commands.editing.factories;

	import com.cloudmade.mappingtool.config.IStringLocalizer;
	import com.cloudmade.mappingtool.map.model.Element;
	import com.cloudmade.mappingtool.map.model.Node;
	import com.cloudmade.mappingtool.map.model.Way;

	class UndefinedElementNamer implements IElementNamer {
		
		var elementNamer:IElementNamer;
		var localizer:IStringLocalizer;
		
		public function new(elementNamer:IElementNamer, localizer:IStringLocalizer) {
			this.elementNamer = elementNamer;
			this.localizer = localizer;
		}
		
		public function getName(element:Element):String {
			var elementName:String = elementNamer.getName(element);
			if (elementName != null) {
				return elementName;
			}
			if (Std.is( element, Node)) {
				return localizer.getString("undefined_node");
			}
			if (Std.is( element, Way)) {
				return localizer.getString("undefined_way");
			}
			return null;
		}
	}
