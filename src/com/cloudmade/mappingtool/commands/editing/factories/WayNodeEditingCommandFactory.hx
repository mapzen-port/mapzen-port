package com.cloudmade.mappingtool.commands.editing.factories;

	import com.cloudmade.mappingtool.commands.editing.DeferredElementCommand;
	import com.cloudmade.mappingtool.commands.editing.IElementCommand;
	import com.cloudmade.mappingtool.commands.editing.MacroElementCommand;
	import com.cloudmade.mappingtool.commands.editing.events.AddNodeToWayEvent;
	import com.cloudmade.mappingtool.commands.editing.events.EditingEvent;
	import com.cloudmade.mappingtool.commands.editing.events.IDescriptor;
	import com.cloudmade.mappingtool.commands.editing.events.NodeEditingEvent;
	import com.cloudmade.mappingtool.commands.editing.events.WayEditingEvent;
	import com.cloudmade.mappingtool.commands.editing.events.WayNodeEditingEvent;
	import com.cloudmade.mappingtool.editing.INewElementIdGenerator;
	import com.cloudmade.mappingtool.map.geo.LatLong;
	import com.cloudmade.mappingtool.map.model.Map;
	import com.cloudmade.mappingtool.map.model.Node;
	import com.cloudmade.mappingtool.map.model.Way;
	
	import flash.geom.Point;
	
	/*
		This class contains breaking workaround that was created as a quick fix for
		CMD-7705 and CMD-7701. The main problem is that deffered commands are created
		each time when executing while others are created once and rely on assumtion
		that ids they are using won't change. This is not true when using deffered
		commands because new ids were generated each time when they are executing.
		That's why CachingNewElementIdGenerator was created. But this is not the right
		way to go. Deffered commands were itroduced for reusing splitting and remove node
		commands because their creation rely on Map.getNodeWays, so we need appropriate
		editing to be done till that moment. The right solution is creating unit tests
		with CMD-7650, CMD-7652, CMD-7653, CMD-7705 and CMD-7701 cases incuded and then
		fully rewriting breaking algorithm, so getNodeWays will be called only once while
		creating break commands. Take into account indexes shift when playing with nodes.
	*/
	
	class WayNodeEditingCommandFactory extends ElementEditingCommandFactory {
		
		var nodeFactory:IEditingCommandFactory;
		var wayFactory:IEditingCommandFactory;
		var newElementIdGenerator:INewElementIdGenerator;
		
		var breakRadius:Int;
		
		public function new(map:Map, descriptor:IDescriptor, elementNamer:IElementNamer, nodeFactory:IEditingCommandFactory, wayFactory:IEditingCommandFactory, newElementIdGenerator:INewElementIdGenerator, ?breakRadius:Int = 10) {
			super(map, descriptor, elementNamer);
			
			this.nodeFactory = nodeFactory;
			this.wayFactory = wayFactory;
			this.newElementIdGenerator = newElementIdGenerator;
			this.breakRadius = breakRadius;
			
			addCreateCommandMethod(WayNodeEditingEvent.SPLIT_NODE_WAYS, createSplitNodeWaysCommand);
			addCreateCommandMethod(WayNodeEditingEvent.BREAK_NODE_WAYS, createBreakNodeWaysCommand);
		}
		
		function createSplitNodeWaysCommand(event:WayNodeEditingEvent):IElementCommand {
			var node:Node = event.node;
			var ways:Array<Dynamic> = map.getNodeWays(node);
			
			// Breaking workaround start
			// Need to ensure that ways will be processed in the same order while breaking,
			// so splitting will create new splitted ways with same ids using
			// CachingNewElementIdGenerator in the same order. Otherwise future editing
			// might be applied to a wrong way.
			ways.sortOn("id", Array.NUMERIC | Array.DESCENDING);
			// Breaking workaround end
			
			var commands:Array<Dynamic> = new Array();
			var splittedWayNames:Array<Dynamic> = new Array();
			
			for (way in ways) {
				var validNodeIndexes:Array<Dynamic> = getNodeIndexesExcludingEdges(node, way);
				if (validNodeIndexes.length > 0) { 
					var command:IElementCommand = createSplitWayCommand(way, validNodeIndexes);
					commands.push(command);
					
					var wayName:String = elementNamer.getName(way);
					splittedWayNames.push(wayName);
				}
			}
			
			if (commands.length == 0) return null;
			
			var macroCommand:IElementCommand = new MacroElementCommand(commands);
			setupCommandDescription(macroCommand, event.type, node, [splittedWayNames.join(", ")]);
			return macroCommand;
		}
		
		function createSplitWayCommand(way:Way, nodeIndexes:Array<Dynamic>):IElementCommand {
			var nodes:Array<Dynamic> = way.nodes.source;
			
			var commands:Array<Dynamic> = new Array();
			
			var removeWayEvent:WayEditingEvent = new WayEditingEvent(WayEditingEvent.REMOVE_WAY_WITHOUT_NODES, way);
			var removeWayCommand:IElementCommand = wayFactory.create(removeWayEvent);
			commands.push(removeWayCommand);
			
			var prevNodeIndex:Int = 0;
			
			nodeIndexes.push(nodes.length - 1);
			
			for (nodeIndex in nodeIndexes) {
				var nodesPart:Array<Dynamic> = nodes.slice(prevNodeIndex, nodeIndex + 1);
				var wayPart:Way = new Way(newElementIdGenerator.getIdForNewElement(), way.cloneTags(), nodesPart);
				var addWayEvent:EditingEvent = new WayEditingEvent(WayEditingEvent.ADD_WAY_WITHOUT_NODES, wayPart);
				var addWayCommand:IElementCommand = wayFactory.create(addWayEvent);
				commands.push(addWayCommand);
				
				prevNodeIndex = nodeIndex;
			}
			
			return new MacroElementCommand(commands);
		}
		
		function createBreakNodeWaysCommand(event:WayNodeEditingEvent):IElementCommand {
			var commands:Array<Dynamic> = new Array();
			
			var node:Node = event.node;
			var ways:Array<Dynamic> = map.getNodeWays(node);
			
			if (!breakingIsApplicable(node, ways)) return null;
			
			for (way in ways) {
				var indexes:Array<Dynamic> = getNodeIndexes(node, way);
				var addBreakWayNodesCommand:IElementCommand = createAddBreakWayNodesCommand(way, indexes);
				commands.push(addBreakWayNodesCommand);
			}
			
			// Breaking workaround start
			var newGenerator:CachingNewElementIdGenerator = new CachingNewElementIdGenerator(newElementIdGenerator);
			var clonedFactory:IEditingCommandFactory = clone(newGenerator);
			var splitNodeWaysCommand:IElementCommand = new DeferredElementCommand(createSplitWaysByNodeIdCommand, node.id, clonedFactory, newGenerator);
			commands.push(splitNodeWaysCommand);
			
			var removeNodeCommand:IElementCommand = new DeferredElementCommand(createRemoveNodeByIdCommand, node.id);
			commands.push(removeNodeCommand);
			// Breaking workaround end
			
			var macroCommand:IElementCommand = new MacroElementCommand(commands);
			var wayNames:Array<Dynamic> = ways.map(
				function(item:Way, index:Int, array:Array<Dynamic>):String {
					return elementNamer.getName(item);
				}
			);
			setupCommandDescription(macroCommand, event.type, node, [wayNames.join(", ")]);
			return macroCommand;
		}
		
		// Breaking workaround start
		function clone(newGenerator:INewElementIdGenerator):IEditingCommandFactory {
			return new WayNodeEditingCommandFactory(map, descriptor, elementNamer, nodeFactory, wayFactory, newGenerator, breakRadius);
		}
		// Breaking workaround end
		
		function createRemoveNodeByIdCommand(nodeId:String):IElementCommand {
			// Breaking workaround start
			var node:Node = map.getNode(nodeId);
			// Breaking workaround end
			var removeNodeEvent:EditingEvent = new NodeEditingEvent(NodeEditingEvent.REMOVE_NODE, node);
			return nodeFactory.create(removeNodeEvent);
		}
		
		function createSplitWaysByNodeIdCommand(nodeId:String, clonedFactory:IEditingCommandFactory, newGenerator:CachingNewElementIdGenerator):IElementCommand {
			// Breaking workaround start
			newGenerator.reset();
			var node:Node = map.getNode(nodeId);
			// Breaking workaround end
			var splitNodeWaysEvent:EditingEvent = new WayNodeEditingEvent(WayNodeEditingEvent.SPLIT_NODE_WAYS, node);
			return clonedFactory.create(splitNodeWaysEvent);
		} 
		
		function breakingIsApplicable(node:Node, ways:Array<Dynamic>):Bool {
			if (ways.length > 1) return true;
			
			var way:Way = ways[0];
			var indexes:Array<Dynamic> = getNodeIndexes(node, way);
			if (indexes.length > 1) return true; //if way intersects itself in this node
			
			var nodeIndex:Int = indexes[0];
			var nodeIsFirst:Bool = (nodeIndex == 0);
			var nodeIsLast:Bool = (nodeIndex == way.nodes.source.length - 1);
			var nodeIsOnTheEdge:Bool = nodeIsFirst || nodeIsLast;
			return !nodeIsOnTheEdge; 
		}
		
		function createAddBreakWayNodesCommand(way:Way, nodeIndexes:Array<Dynamic>):IElementCommand {
			var commands:Array<Dynamic> = new Array();
			
			var nodes:Array<Dynamic> = way.nodes.source;
			var newNodeCount:Int = 0;
			
			for (index in nodeIndexes) {
				if (index > 0) {
					var startLeftPosition:LatLong = nodes[index].position;
					var endLeftPosition:LatLong = nodes[index - 1].position;
					if (additionalNodeIsNeeded(startLeftPosition, endLeftPosition)) {
						var leftNodeIndex:Int = index + newNodeCount;
						var addLeftNodeCommand:IElementCommand = createAddAdditionalNodeForBreakingCommand(
								startLeftPosition, endLeftPosition, way, leftNodeIndex);
						commands.push(addLeftNodeCommand);
						newNodeCount++;
					}
				}
				
				if (index < nodes.length - 1) {
					var startRightPosition:LatLong = nodes[index].position;
					var endRightPosition:LatLong = nodes[index + 1].position;
					if (additionalNodeIsNeeded(startRightPosition, endRightPosition)) {
						var rightNodeIndex:Int = index + newNodeCount + 1;
						var addRightNodeCommand:IElementCommand = createAddAdditionalNodeForBreakingCommand(
								startRightPosition, endRightPosition, way, rightNodeIndex);
						commands.push(addRightNodeCommand);
						newNodeCount++;
					}
				}
			}
			
			return new MacroElementCommand(commands);
		}
		
		function createAddAdditionalNodeForBreakingCommand(startNodePosition:LatLong, endNodePosition:LatLong, way:Way, index:Int):IElementCommand {
			
			var nodePosition:LatLong = getAdditionalNodePosition(startNodePosition, endNodePosition);
			var node:Node = new Node(newElementIdGenerator.getIdForNewElement(), null, nodePosition);
			
			var appendNodeEvent:EditingEvent = new AddNodeToWayEvent(
					AddNodeToWayEvent.APPEND_WAY_NODE, node, way, index);
			return nodeFactory.create(appendNodeEvent);
		}
		
		function additionalNodeIsNeeded(startPosition:LatLong, endPosition:LatLong):Bool {
			var startPoint:Point = map.project(startPosition);
			var endPoint:Point = map.project(endPosition);
			return (Point.distance(startPoint, endPoint) > breakRadius);
		}
		
		function getAdditionalNodePosition(startNode:LatLong, endNode:LatLong):LatLong {
			var startPoint:Point = map.project(startNode);
			var endPoint:Point = map.project(endNode);
			
			var pointDelta:Point = endPoint.subtract(startPoint);
			var angle:Int = Math.atan2(pointDelta.y, pointDelta.x);
			var additionalPointDelta:Point = Point.polar(breakRadius, angle);
			var additionalPoint:Point = startPoint.add(additionalPointDelta);
			
			return map.unproject(additionalPoint);
		}
		
		function getNodeIndexesExcludingEdges(node:Node, way:Way):Array<Dynamic> {
			var indexes:Array<Dynamic> = getNodeIndexes(node, way);
			if (indexes[0] == 0) {
				indexes.shift();
			}
			if (indexes[indexes.length - 1] == way.nodes.source.length - 1) {
				indexes.pop();
			}
			return indexes;
		}
		
		function getNodeIndexes(node:Node, way:Way):Array<Dynamic> {
			var nodes:Array<Dynamic> = way.nodes.source;
			var indexes:Array<Dynamic> = new Array();
			
			var index:Int = -1;
			while (true) {
				index = nodes.indexOf(node, index + 1);
				if (index == -1) break;
				indexes.push(index);
			}
			
			return indexes;
		}
	}
