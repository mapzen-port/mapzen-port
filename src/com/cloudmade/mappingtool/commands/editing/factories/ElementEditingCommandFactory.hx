package com.cloudmade.mappingtool.commands.editing.factories;

	import com.cloudmade.mappingtool.commands.editing.IElementCommand;
	import com.cloudmade.mappingtool.commands.editing.MacroElementCommand;
	import com.cloudmade.mappingtool.commands.editing.events.ChangeElementTagEvent;
	import com.cloudmade.mappingtool.commands.editing.events.ChangeElementTagsEvent;
	import com.cloudmade.mappingtool.commands.editing.events.EditingEvent;
	import com.cloudmade.mappingtool.commands.editing.events.ElementTagEditingEvent;
	import com.cloudmade.mappingtool.commands.editing.events.IDescriptor;
	import com.cloudmade.mappingtool.commands.editing.events.TagFieldName;
	import com.cloudmade.mappingtool.commands.editing.tags.AddElementTagCommand;
	import com.cloudmade.mappingtool.commands.editing.tags.ChangeElementTagKeyCommand;
	import com.cloudmade.mappingtool.commands.editing.tags.ChangeElementTagValueCommand;
	import com.cloudmade.mappingtool.commands.editing.tags.RemoveElementTagCommand;
	import com.cloudmade.mappingtool.map.model.Element;
	import com.cloudmade.mappingtool.map.model.Map;
	import com.cloudmade.mappingtool.map.model.Tag;

	class ElementEditingCommandFactory implements IEditingCommandFactory {
		
		var map:Map;
		var descriptor:IDescriptor;
		var elementNamer:IElementNamer;
		
		var createMethodMap:Dynamic ;
		
		public function new(map:Map, descriptor:IDescriptor, elementNamer:IElementNamer) {
			
			createMethodMap = new Object();
			this.map = map;
			this.descriptor = descriptor;
			this.elementNamer = elementNamer;
			addCreateCommandMethod(ElementTagEditingEvent.ADD_ELEMENT_TAG, createAddTagCommand);
			addCreateCommandMethod(ElementTagEditingEvent.REMOVE_ELEMENT_TAG, createRemoveTagCommand);
			addCreateCommandMethod(ChangeElementTagEvent.CHANGE_ELEMENT_TAG_KEY, createChangeTagKeyCommand);
			addCreateCommandMethod(ChangeElementTagEvent.CHANGE_ELEMENT_TAG_VALUE, createChangeTagValueCommand);
			addCreateCommandMethod(ChangeElementTagsEvent.CHANGE_ELEMENT_TAGS, createChangeTagsCommand);
		}

		public function create(event:EditingEvent):IElementCommand {
			var createMethod:Dynamic = getCreateCommandMethod(event.type);
			var command:IElementCommand = createMethod(event);
			return command;
		}
		
		function setupCommandDescription(command:IElementCommand, type:String, element:Element, ?parameters:Array<Dynamic> = null):Void {
			var name:String = elementNamer.getName(element);
			var descriptionParameters:Array<Dynamic> = [name].concat(parameters);
			var description:String = descriptor.describe(type, descriptionParameters);
			command.setDescription(description);
		}
		
		function addCreateCommandMethod(type:String, func:Dynamic):Void {
			createMethodMap[type] = func;
		}
		
		function getCreateCommandMethod(type:String):Dynamic {
			return createMethodMap[type];
		}
		
		function createAddTagCommand(event:ElementTagEditingEvent):IElementCommand {
			var command:IElementCommand = new AddElementTagCommand(map, event.element, event.tag);
			if (event.fieldName == TagFieldName.METADATA) {
				setupCommandDescription(command, event.type, event.element);
			}
			else {
				setupTagFieldCommandDescription(command, event.type, event.element, event.fieldName, event.tag.value);
			}
			return command;
		}
		
		function createRemoveTagCommand(event:ElementTagEditingEvent):IElementCommand {
			var command:IElementCommand = new RemoveElementTagCommand(map, event.element, event.tag);
			if (event.fieldName == TagFieldName.METADATA) {
				setupCommandDescription(command, event.type, event.element, [event.tag.toString()]);
			}
			else {
				setupTagFieldCommandDescription(command, event.type, event.element, event.fieldName, event.tag.value);
			}
			return command;
		}
		
		function createChangeTagKeyCommand(event:ChangeElementTagEvent):IElementCommand {
			var command:IElementCommand = new ChangeElementTagKeyCommand(map, event.element, event.tag, event.newValue);
			setupCommandDescription(command, event.type, event.element, [event.tag.key, event.newValue]);
			return command;
		}
		
		function createChangeTagValueCommand(event:ChangeElementTagEvent):IElementCommand {
			var command:IElementCommand = new ChangeElementTagValueCommand(map, event.element, event.tag, event.newValue);
			if (event.fieldName == TagFieldName.METADATA) {
				setupCommandDescription(command, event.type, event.element, [event.tag.key, event.newValue]);
			}
			else {
				setupTagFieldCommandDescription(command, event.type, event.element, event.fieldName, event.newValue);
			}
			return command;
		}
		
		function setupTagFieldCommandDescription(command:IElementCommand, type:String, element:Element, fieldName:TagFieldName, value:String):Void {
			var descFieldName:String = descriptor.describe(fieldName.toString());
			setupCommandDescription(command, type + "_field", element, [descFieldName, value]);
		}
		
		function createChangeTagsCommand(event:ChangeElementTagsEvent):IElementCommand {
			var element:Element = event.element;
			var commands:Array<Dynamic> = new Array();
			for (oldTag in event.oldTags) {
				commands.push(new RemoveElementTagCommand(map, element, oldTag));
			}
			for (newTag in event.newTags) {
				commands.push(new AddElementTagCommand(map, element, newTag));
			}
			var command:IElementCommand = new MacroElementCommand(commands);
			setupCommandDescription(command, event.type, element, [event.oldType, event.newType]);
			return command;
		}
	}
