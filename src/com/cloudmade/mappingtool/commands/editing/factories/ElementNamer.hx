package com.cloudmade.mappingtool.commands.editing.factories;

	import com.cloudmade.mappingtool.config.IElementType;
	import com.cloudmade.mappingtool.config.IElementTypeFinder;
	import com.cloudmade.mappingtool.config.INamed;
	import com.cloudmade.mappingtool.map.model.Element;
	import com.cloudmade.mappingtool.map.model.Node;

	class ElementNamer implements IElementNamer {
		
		var nodeFinder:IElementTypeFinder;
		var wayFinder:IElementTypeFinder;
		var areaFinder:IElementTypeFinder;
		
		public function new(nodeFinder:IElementTypeFinder, wayFinder:IElementTypeFinder, areaFinder:IElementTypeFinder) {
			this.nodeFinder = nodeFinder;
			this.wayFinder = wayFinder;
			this.areaFinder = areaFinder;
		}
		
		public function getName(element:Element):String {
			var elementTags:Array<Dynamic> = element.tags.source;
			var elementType:IElementType;
			if (Std.is( element, Node)) {
				elementType = nodeFinder.findType(elementTags);
			}
			else {
				elementType = areaFinder.findType(elementTags);
				if (elementType == null) {
					elementType = wayFinder.findType(elementTags);
				}
			}
			if (Std.is( elementType, INamed)) {
				return INamed(elementType).name;
			}
			return null;
		}
	}
