package com.cloudmade.mappingtool.commands.editing.factories;

	import com.cloudmade.mappingtool.map.model.Element;
	
	interface IElementNamer
	{
		function getName(element:Element):String;
	}
