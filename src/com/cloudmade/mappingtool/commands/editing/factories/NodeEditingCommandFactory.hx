package com.cloudmade.mappingtool.commands.editing.factories;

	import com.cloudmade.mappingtool.business.GetNodeParentsNumberDelegate;
	import com.cloudmade.mappingtool.business.GetNodeRelationsNumberDelegate;
	import com.cloudmade.mappingtool.business.GetNodeWaysNumberDelegate;
	import com.cloudmade.mappingtool.business.IDelegate;
	import com.cloudmade.mappingtool.business.IDelegateGroup;
	import com.cloudmade.mappingtool.business.ParallelDelegateGroup;
	import com.cloudmade.mappingtool.commands.editing.AddNodeCommand;
	import com.cloudmade.mappingtool.commands.editing.AsyncRemoveNodeWithoutWaysCommand;
	import com.cloudmade.mappingtool.commands.editing.DisplayWayCommand;
	import com.cloudmade.mappingtool.commands.editing.IElementCommand;
	import com.cloudmade.mappingtool.commands.editing.JoinWayNodeCommand;
	import com.cloudmade.mappingtool.commands.editing.MacroElementCommand;
	import com.cloudmade.mappingtool.commands.editing.MoveNodeCommand;
	import com.cloudmade.mappingtool.commands.editing.RemoveNodeCommand;
	import com.cloudmade.mappingtool.commands.editing.RemoveNodeWithoutWaysCommand;
	import com.cloudmade.mappingtool.commands.editing.RemoveWayNodeCommand;
	import com.cloudmade.mappingtool.commands.editing.UpdateWayNodesCommand;
	import com.cloudmade.mappingtool.commands.editing.events.AddNodeToWayEvent;
	import com.cloudmade.mappingtool.commands.editing.events.IDescriptor;
	import com.cloudmade.mappingtool.commands.editing.events.InsertWayNodeEvent;
	import com.cloudmade.mappingtool.commands.editing.events.MoveNodeEvent;
	import com.cloudmade.mappingtool.commands.editing.events.NodeEditingEvent;
	import com.cloudmade.mappingtool.map.model.Map;
	import com.cloudmade.mappingtool.map.model.Node;
	import com.cloudmade.mappingtool.map.model.Way;

	class NodeEditingCommandFactory extends ElementEditingCommandFactory {
		
		var osmUrl:String;
		
		public function new(map:Map, descriptor:IDescriptor, elementNamer:IElementNamer, osmUrl:String) {
			super(map, descriptor, elementNamer);
			this.osmUrl = osmUrl;
			addCreateCommandMethod(MoveNodeEvent.MOVE_NODE, createMoveNodeCommand);
			addCreateCommandMethod(NodeEditingEvent.ADD_NODE, createAddNodeCommand);
			addCreateCommandMethod(NodeEditingEvent.REMOVE_NODE, createRemoveNodeCommand);
			addCreateCommandMethod(NodeEditingEvent.REMOVE_NODE_WITHOUT_WAYS, createRemoveNodeWithoutWaysCommand);
			addCreateCommandMethod(AddNodeToWayEvent.JOIN_WAY_NODE, createJoinWayNodeCommand);
			addCreateCommandMethod(AddNodeToWayEvent.APPEND_WAY_NODE, createAppendWayNodeCommand);
			addCreateCommandMethod(InsertWayNodeEvent.INSERT_WAY_NODE, createInsertWayNodeCommand);
		}
		
		function createMoveNodeCommand(event:MoveNodeEvent):IElementCommand {
			var moveNodeCommand:IElementCommand = new MoveNodeCommand(map, event.node, event.position);
			var commands:Array<Dynamic> = [moveNodeCommand];
			var ways:Array<Dynamic> = map.getNodeWays(event.node);
			for (way in ways) {
				commands.push(new DisplayWayCommand(map, way));
				commands.push(new UpdateWayNodesCommand(map, way));
			}
			var command:IElementCommand = new MacroElementCommand(commands);
			setupCommandDescription(command, event.type, event.node);
			return command;
		}
		
		function createAddNodeCommand(event:NodeEditingEvent):IElementCommand {
			var command:IElementCommand = new AddNodeCommand(map, event.node);
			setupCommandDescription(command, event.type, event.node);
			return command;
		}
		
		function createRemoveNodeCommand(event:NodeEditingEvent):IElementCommand {
			var node:Node = event.node;
			var ways:Array<Dynamic> = map.getNodeWays(node);
			var commands:Array<Dynamic> = new Array();
			for (way in ways) {
				var nodes:Array<Dynamic> = way.nodes.source;
				var nodeCount:Int = 0;
				var nodeIndex:Int = -1;
				while (true) {
					nodeIndex = nodes.indexOf(node, nodeIndex + 1);
					if (nodeIndex == -1) break;
					
					var wayCommand:IElementCommand = new RemoveWayNodeCommand(map, way, nodeIndex - nodeCount);
					commands.push(wayCommand); 
					
					nodeCount++;
				}
			}
			var nodeCommand:IElementCommand = new RemoveNodeCommand(map, node);
			commands.push(nodeCommand);
			var macroCommand:IElementCommand = new MacroElementCommand(commands);
			setupCommandDescription(macroCommand, event.type, node);
			return macroCommand;
		}
		
		function createRemoveNodeWithoutWaysCommand(event:NodeEditingEvent):IElementCommand {
			var node:Node = event.node;
			if (node.isNew) {
				return new RemoveNodeWithoutWaysCommand(map, node);
			}
			return createAsyncRemoveNodeWithoutWaysCommand(node);
		}
		
		function createAsyncRemoveNodeWithoutWaysCommand(node:Node):IElementCommand {
			var waysDelegate:IDelegate = new GetNodeWaysNumberDelegate(osmUrl, node.id);
			var relationsDelegate:IDelegate = new GetNodeRelationsNumberDelegate(osmUrl, node.id);
			var delegateGroup:IDelegateGroup = new ParallelDelegateGroup([waysDelegate, relationsDelegate]);
			var parentsDelegate:IDelegate = new GetNodeParentsNumberDelegate(delegateGroup);
			var command:IElementCommand = new AsyncRemoveNodeWithoutWaysCommand(map, node, parentsDelegate);
			return command;
		}
		
		function createJoinWayNodeCommand(event:AddNodeToWayEvent):IElementCommand {
			var command:IElementCommand = new JoinWayNodeCommand(map, event.way, event.node, event.index);
			setupCommandDescription(command, event.type, event.way);
			return command;
		}
		
		function createAppendWayNodeCommand(event:AddNodeToWayEvent):IElementCommand {
			var addCommand:IElementCommand = createAddNodeCommand(event);
			var joinCommand:IElementCommand = createJoinWayNodeCommand(event);
			var command:IElementCommand = new MacroElementCommand([addCommand, joinCommand]);
			setupCommandDescription(command, event.type, event.way);
			return command;
		}
		
		function createInsertWayNodeCommand(event:InsertWayNodeEvent):IElementCommand {
			var appendCommand:IElementCommand = createAppendWayNodeCommand(event);
			var joinCommand:IElementCommand = new JoinWayNodeCommand(map, event.joinWay, event.node);
			var command:IElementCommand = new MacroElementCommand([appendCommand, joinCommand]);
			var joinWayName:String = elementNamer.getName(event.joinWay);
			setupCommandDescription(command, event.type, event.way, [joinWayName]);
			return command;
		}
	}
