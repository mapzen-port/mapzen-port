package com.cloudmade.mappingtool.commands.editing.factories;

	import com.cloudmade.mappingtool.commands.editing.IElementCommand;
	import com.cloudmade.mappingtool.commands.editing.events.EditingEvent;
	import com.cloudmade.mappingtool.commands.editing.events.ElementEditingEvent;
	import com.cloudmade.mappingtool.commands.editing.events.NodeEditingEvent;
	import com.cloudmade.mappingtool.commands.editing.events.WayEditingEvent;
	import com.cloudmade.mappingtool.commands.editing.events.WayNodeEditingEvent;
	
	class EditingCommandFactory implements IEditingCommandFactory {
		
		var elementFactory:IEditingCommandFactory;
		var nodeFactory:IEditingCommandFactory;
		var wayFactory:IEditingCommandFactory;
		var wayNodeFactory:IEditingCommandFactory;
		
		public function new(elementFactory:IEditingCommandFactory, nodeFactory:IEditingCommandFactory, wayFactory:IEditingCommandFactory, wayNodeFactory:IEditingCommandFactory) {
			this.elementFactory = elementFactory;
			this.nodeFactory = nodeFactory;
			this.wayFactory = wayFactory;
			this.wayNodeFactory = wayNodeFactory;
		}
		
		public function create(event:EditingEvent):IElementCommand {
			var factory:IEditingCommandFactory = getFactory(event);
			return factory.create(event);
		}
		
		function getFactory(event:EditingEvent):IEditingCommandFactory {
			if (Std.is( event, WayNodeEditingEvent)) {
				return wayNodeFactory;
			} else if (Std.is( event, NodeEditingEvent)) {
				return nodeFactory;
			} else if (Std.is( event, WayEditingEvent)) {
				return wayFactory;
			} else if (Std.is( event, ElementEditingEvent)) {
				return elementFactory;
			}
			return null;
		}
	}
