package com.cloudmade.mappingtool.commands.editing.factories;

	import com.cloudmade.mappingtool.commands.editing.AddWayCommand;
	import com.cloudmade.mappingtool.commands.editing.ChangeWayTypeCommand;
	import com.cloudmade.mappingtool.commands.editing.IElementCommand;
	import com.cloudmade.mappingtool.commands.editing.MacroElementCommand;
	import com.cloudmade.mappingtool.commands.editing.RemoveWayCommand;
	import com.cloudmade.mappingtool.commands.editing.ReverseWayCommand;
	import com.cloudmade.mappingtool.commands.editing.UpdateWayNodesCommand;
	import com.cloudmade.mappingtool.commands.editing.events.AddNodeToWayEvent;
	import com.cloudmade.mappingtool.commands.editing.events.AddWayFromNodeEvent;
	import com.cloudmade.mappingtool.commands.editing.events.ChangeWayTypeEvent;
	import com.cloudmade.mappingtool.commands.editing.events.EditingEvent;
	import com.cloudmade.mappingtool.commands.editing.events.IDescriptor;
	import com.cloudmade.mappingtool.commands.editing.events.MoveNodeEvent;
	import com.cloudmade.mappingtool.commands.editing.events.MoveWayEvent;
	import com.cloudmade.mappingtool.commands.editing.events.NodeEditingEvent;
	import com.cloudmade.mappingtool.commands.editing.events.WayEditingEvent;
	import com.cloudmade.mappingtool.map.geo.LatLong;
	import com.cloudmade.mappingtool.map.model.Map;
	import com.cloudmade.mappingtool.map.model.Node;
	import com.cloudmade.mappingtool.map.model.Way;

	class WayEditingCommandFactory extends ElementEditingCommandFactory {
		
		var nodeFactory:IEditingCommandFactory;
		var wayNodeFactory:IEditingCommandFactory;
		
		public function new(map:Map, descriptor:IDescriptor, elementNamer:IElementNamer, nodeFactory:IEditingCommandFactory) {
			super(map, descriptor, elementNamer);
			this.nodeFactory = nodeFactory;
			this.wayNodeFactory = wayNodeFactory;
			addCreateCommandMethod(MoveWayEvent.MOVE_WAY, createMoveWayCommand);
			addCreateCommandMethod(WayEditingEvent.ADD_WAY, createAddWayCommand);
			addCreateCommandMethod(WayEditingEvent.REMOVE_WAY, createRemoveWayCommand);
			addCreateCommandMethod(AddWayFromNodeEvent.ADD_WAY_FROM_NODE, createAddWayFromNodeCommand);
			addCreateCommandMethod(WayEditingEvent.ADD_WAY_WITHOUT_NODES, createAddWayWithoutNodesCommand);
			addCreateCommandMethod(WayEditingEvent.REMOVE_WAY_WITHOUT_NODES, createRemoveWayWithoutNodesCommand);
			addCreateCommandMethod(ChangeWayTypeEvent.CHANGE_WAY_TYPE, createChangeWayTypeCommand);
			addCreateCommandMethod(WayEditingEvent.REVERSE_WAY, createReverseWayCommand);
		}
		
		function createMoveWayCommand(event:MoveWayEvent):IElementCommand {
			var way:Way = event.way;
			var position:LatLong = event.position;
			var firstNode:Node = cast( way.nodes.getItemAt(0), Node);
			var latOffset:Int = position.lat - firstNode.position.lat;
			var longOffset:Int = position.long - firstNode.position.long;
			var command:IElementCommand = createMoveNodesCommand(way.nodes.source, latOffset, longOffset);
			setupCommandDescription(command, event.type, way);
			return command;
		}
		
		function createMoveNodesCommand(nodes:Array<Dynamic>, latOffset:Int, longOffset:Int):IElementCommand {
			var commands:Array<Dynamic> = new Array();
			var workedNodes:Dynamic = new Object();
			for (node in nodes) {
				if (node.id in workedNodes) continue;
				workedNodes[node.id] = null;
				var position:LatLong = node.position.offset(latOffset, longOffset);
				var event:MoveNodeEvent = new MoveNodeEvent(MoveNodeEvent.MOVE_NODE, node, position);
				commands.push(nodeFactory.create(event));
			}
			return new MacroElementCommand(commands);
		}
		
		function createAddWayCommand(event:WayEditingEvent):IElementCommand {
			var way:Way = event.way;
			var commands:Array<Dynamic> = new Array();
			var nodes:Array<Dynamic> = way.nodes.source;
			for (node in nodes) {
				var nodeEvent:EditingEvent = new NodeEditingEvent(NodeEditingEvent.ADD_NODE, node);
				commands.push(nodeFactory.create(nodeEvent));
			}
			commands.push(new AddWayCommand(map, way));
			commands.push(new UpdateWayNodesCommand(map, way));
			var command:IElementCommand = new MacroElementCommand(commands);
			
			var eventType:String = event.type;
			if (way.isArea) {
				eventType = WayEditingEvent.ADD_AREA;
			}
			setupCommandDescription(command, eventType, way);
			return command;
		}
		
		function createAddWayFromNodeCommand(event:AddWayFromNodeEvent):IElementCommand {
			var addCommand:IElementCommand = createAddWayCommand(event);
			var appendEvent:AddNodeToWayEvent = new AddNodeToWayEvent(AddNodeToWayEvent.APPEND_WAY_NODE, event.node, event.way);
			var appendCommand:IElementCommand = nodeFactory.create(appendEvent);
			var command:IElementCommand = new MacroElementCommand([addCommand, appendCommand]);
			setupCommandDescription(command, event.type, event.way);
			return command;
		}
		
		function createAddWayWithoutNodesCommand(event:WayEditingEvent):IElementCommand {
			var addWayCommand:IElementCommand = new AddWayCommand(map, event.way);
			var updateWayNodesCommand:IElementCommand = new UpdateWayNodesCommand(map, event.way);
			return new MacroElementCommand([addWayCommand, updateWayNodesCommand]);
		}
		
		function createRemoveWayCommand(event:WayEditingEvent):IElementCommand {
			var way:Way = event.way;
			var commands:Array<Dynamic> = new Array();
			commands.push(new RemoveWayCommand(map, way));
			var nodes:Array<Dynamic> = way.nodes.source;
			for (node in nodes) {
				var nodeEvent:EditingEvent = new NodeEditingEvent(NodeEditingEvent.REMOVE_NODE_WITHOUT_WAYS, node);
				commands.push(nodeFactory.create(nodeEvent));
			}
			var command:IElementCommand = new MacroElementCommand(commands);
			setupCommandDescription(command, event.type, way);
			return command;
		}
		
		function createRemoveWayWithoutNodesCommand(event:WayEditingEvent):IElementCommand {
			return new RemoveWayCommand(map, event.way);
		}
		
		function createChangeWayTypeCommand(event:ChangeWayTypeEvent):IElementCommand {
			var command:IElementCommand = new ChangeWayTypeCommand(map, event.way, event.isOneWay);
			var type:String = event.type + "_" + event.isOneWay.toString();
			setupCommandDescription(command, type, event.way);
			return command;
		}
		
		function createReverseWayCommand(event:WayEditingEvent):IElementCommand {
			var command:IElementCommand = new ReverseWayCommand(map, event.way);
			setupCommandDescription(command, event.type, event.way);
			return command;
		}
	}
