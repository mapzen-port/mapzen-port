package com.cloudmade.mappingtool.commands.editing.factories;

	import com.cloudmade.mappingtool.commands.editing.events.IDescriptor;
	import com.cloudmade.mappingtool.commands.editing.events.ResourceDescriptor;
	import com.cloudmade.mappingtool.config.IStringLocalizer;
	import com.cloudmade.mappingtool.config.StringLocalizer;
	import com.cloudmade.mappingtool.editing.INewElementIdGenerator;
	import com.cloudmade.mappingtool.map.model.Map;
	
	import mx.resources.IResourceManager;
	
	[ResourceBundle("EditingCommands")]
	
	class EditingCommandFactoryProvider
	 {
		
		var map:Map;
		var elementNamer:IElementNamer;
		var newElementIdGenerator:INewElementIdGenerator;
		var resourceManager:IResourceManager;
		
		public function new(map:Map, newElementIdGenerator:INewElementIdGenerator, resourceManager:IResourceManager, elementNamer:IElementNamer) {
			this.map = map;
			this.newElementIdGenerator = newElementIdGenerator;
			this.resourceManager = resourceManager;
			this.elementNamer = elementNamer;
		}
		
		public function create():IEditingCommandFactory {
			var descriptor:IDescriptor = new ResourceDescriptor(resourceManager, "EditingCommands");
			var localizer:IStringLocalizer = new StringLocalizer(resourceManager, "EditingCommands");
			var undefinedElementNamer:IElementNamer = new UndefinedElementNamer(elementNamer, localizer);
			
			var elementFactory:IEditingCommandFactory = new ElementEditingCommandFactory(map, descriptor, undefinedElementNamer);
			var nodeFactory:IEditingCommandFactory = new NodeEditingCommandFactory(map, descriptor, undefinedElementNamer, CONFIG::osm);
			var wayFactory:IEditingCommandFactory = new WayEditingCommandFactory(map, descriptor, undefinedElementNamer, nodeFactory);
			var wayNodeFactory:IEditingCommandFactory = new WayNodeEditingCommandFactory(map, descriptor, undefinedElementNamer, nodeFactory, wayFactory, newElementIdGenerator);
			
			return new EditingCommandFactory(elementFactory, nodeFactory, wayFactory, wayNodeFactory);
		}
	}
