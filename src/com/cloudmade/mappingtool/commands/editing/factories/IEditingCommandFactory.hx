package com.cloudmade.mappingtool.commands.editing.factories;

	import com.cloudmade.mappingtool.commands.editing.IElementCommand;
	import com.cloudmade.mappingtool.commands.editing.events.EditingEvent;
	
	interface IEditingCommandFactory
	{
		function create(event:EditingEvent):IElementCommand;
	}
