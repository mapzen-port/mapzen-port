package com.cloudmade.mappingtool.commands.editing;

	interface IMacroElementCommand implements IElementCommand{
		function commands():Array<Dynamic>;
	}
