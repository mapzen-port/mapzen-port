package com.cloudmade.mappingtool.commands.editing;

	interface IDescribable
	{
		function description():String;
	}
