package com.cloudmade.mappingtool.commands.editing;

	import com.cloudmade.mappingtool.business.IDelegate;
	import com.cloudmade.mappingtool.map.model.Element;
	import com.cloudmade.mappingtool.map.model.Map;
	import com.cloudmade.mappingtool.map.model.Node;
	
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.utils.IDataInput;
	import flash.utils.IDataOutput;
	
	/*[Event(name="complete", type="flash.events.Event")]*/
	/*[Event(name="ioError", type="flash.events.IOErrorEvent")]*/
	
	/*[RemoteClass(alias="com.cloudmade.mappingtool.commands.editing.AsyncRemoveNodeWithoutWaysCommand")]*/

	class AsyncRemoveNodeWithoutWaysCommand extends AsyncRemoveNodeWithoutWaysDispatcherCommand, implements IAsyncElementCommand {
		
		public var result(getResult, null) : Dynamic ;
		public var target(getTarget, null) : Element ;
		var delegate:IDelegate;
		
		public function new(?map:Map = null, ?node:Node = null, ?delegate:IDelegate = null) {
			super(map, node);
			setDelegate(delegate);
		}
		
		var _result:Dynamic;
		public function getResult():Dynamic {
			return _result;
		}
		
		public override function getTarget():Element {
			// Take into account parent way that is being removed.
			return (delegate.result > 1) ? null : node;
		}
		
		public function send():Void {
			delegate.send();
			_result = null;
		}
		
		function setDelegate(delegate:IDelegate):Void {
			this.delegate = delegate;
			if (delegate != null) {
				delegate.addEventListener(Event.COMPLETE, delegate_completeHandler);
				delegate.addEventListener(IOErrorEvent.IO_ERROR, delegate_ioErrorHandler);
			}
		}
		
		function delegate_completeHandler(event:Event):Void {
			_result = true;
			dispatchEvent(event);
		}
		
		function delegate_ioErrorHandler(event:IOErrorEvent):Void {
			_result = false;
			dispatchEvent(event);
		}
		
		public override function readExternal(input:IDataInput):Void {
			super.readExternal(input);
			setDelegate(cast( input.readObject(), IDelegate));
		}
		
		public override function writeExternal(output:IDataOutput):Void {
			super.writeExternal(output);
			output.writeObject(delegate);
		}
		
	}
