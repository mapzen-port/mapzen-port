package com.cloudmade.mappingtool.commands.editing;

	import com.cloudmade.mappingtool.commands.IRedoableCommand;
	import com.cloudmade.mappingtool.map.model.Map;
	import com.cloudmade.mappingtool.map.model.Node;
	import com.cloudmade.mappingtool.map.model.Way;
	
	import flash.utils.IDataInput;
	import flash.utils.IDataOutput;
	import flash.utils.IExternalizable;
	
	/*[RemoteClass(alias="com.cloudmade.mappingtool.commands.editing.BaseElementCommand")]*/
	
	class BaseElementCommand implements IRedoableCommand, implements IDescribable, implements IExternalizable {
		
		public var description(getDescription, null) : String ;
		var map:Map;
		
		public function new(?map:Map = null) {
			this.map = map;
		}
		
		var _description:String ;
		public function getDescription():String {
			return _description;
		}
		public function setDescription(value:String):Void {
			_description = value;
		}
		
		public function execute():Void {
		}
		
		public function undo():Void {
		}
		
		public function redo():Void {
			execute();
		}
		
		public function readExternal(input:IDataInput):Void {
			map = input.readObject();
			setDescription(input.readUTF());
		}
		
		public function writeExternal(output:IDataOutput):Void {
			output.writeObject(map);
			output.writeUTF(description);
		}
		
		function refreshNode(node:Node):Void {
			map.nodes.setItemAt(node, map.nodes.getItemIndex(node));
		}
		
		function refreshWay(way:Way):Void {
			map.ways.setItemAt(way, map.ways.getItemIndex(way));
		}
	}
