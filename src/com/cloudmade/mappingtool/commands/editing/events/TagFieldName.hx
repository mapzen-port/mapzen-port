package com.cloudmade.mappingtool.commands.editing.events;

	class TagFieldName
	 {
		
		public static var METADATA:TagFieldName = new TagFieldName("metadata");
		public static var NAME:TagFieldName = new TagFieldName("name");
		public static var STREET:TagFieldName = new TagFieldName("street");
		public static var NUMBER:TagFieldName = new TagFieldName("number");
		public static var MAX_SPEED:TagFieldName = new TagFieldName("max_speed");
		public static var REFERENCE:TagFieldName = new TagFieldName("reference");
		public static var LANES:TagFieldName = new TagFieldName("lanes");
		
		var name:String;
		
		public function new(name:String) {
			this.name = name;
		}
		
		public function toString():String {
			return name;
		}
	}
