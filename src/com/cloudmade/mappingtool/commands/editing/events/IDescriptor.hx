package com.cloudmade.mappingtool.commands.editing.events;

	interface IDescriptor
	{
		function describe(type:String, ?parameters:Array<Dynamic> = null):String;
	}
