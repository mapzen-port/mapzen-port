package com.cloudmade.mappingtool.commands.editing.events;

	import com.cloudmade.mappingtool.map.model.Node;
	import com.cloudmade.mappingtool.map.model.Way;
	
	import flash.events.Event;

	class AddNodeToWayEvent extends NodeEditingEvent {
		
		public static var JOIN_WAY_NODE:String = "joinWayNode";
		public static var APPEND_WAY_NODE:String = "appendWayNode";
		
		public var way:Way;
		public var index:Int;
		
		public function new(type:String, node:Node, way:Way, ?index:Int = -1, ?bubbles:Bool = false) {
			super(type, node, bubbles);
			this.way = way;
			this.index = index;
		}
		
		public override function clone():Event {
			return new AddNodeToWayEvent(type, node, way, index, bubbles);
		}
	}
