package com.cloudmade.mappingtool.commands.editing.events;

	import com.cloudmade.mappingtool.map.model.Element;
	import com.cloudmade.mappingtool.map.model.Tag;
	
	import flash.events.Event;

	class ElementTagEditingEvent extends ElementEditingEvent {
		
		public static var ADD_ELEMENT_TAG:String = "addElementTag";
		public static var REMOVE_ELEMENT_TAG:String = "removeElementTag";
		
		public var tag:Tag;
		public var fieldName:TagFieldName;
		
		public function new(type:String, element:Element, tag:Tag, ?bubbles:Bool = false) {
			super(type, element, bubbles);
			this.tag = tag;
			fieldName = TagFieldName.METADATA;
		}
		
		public override function clone():Event {
			return new ElementTagEditingEvent(type, element, tag, bubbles);
		}
	}
