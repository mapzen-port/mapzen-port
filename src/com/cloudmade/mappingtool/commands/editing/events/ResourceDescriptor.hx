package com.cloudmade.mappingtool.commands.editing.events;

	import mx.resources.IResourceManager;
	
	class ResourceDescriptor implements IDescriptor {
		
		var resourceManager:IResourceManager;
		var bundleName:String;
		var undefinedDescription:String ;
		
		public function new(resourceManager:IResourceManager, bundleName:String, ?undefinedDescription:String = "") {
			
			undefinedDescription = "";
			this.resourceManager = resourceManager;
			this.bundleName = bundleName;
		}

		public function describe(type:String, ?parameters:Array<Dynamic> = null):String {
			var description:String = resourceManager.getString(bundleName, type, parameters);
			if (description) {
				return description;
			}
			return undefinedDescription;
		}
	}
