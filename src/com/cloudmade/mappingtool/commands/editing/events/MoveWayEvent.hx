package com.cloudmade.mappingtool.commands.editing.events;

	import com.cloudmade.mappingtool.map.geo.LatLong;
	import com.cloudmade.mappingtool.map.model.Way;
	
	import flash.events.Event;

	class MoveWayEvent extends WayEditingEvent {
		
		public static var MOVE_WAY:String = "moveWay";
		
		public var position:LatLong;
		
		public function new(type:String, way:Way, position:LatLong, ?bubbles:Bool = false) {
			super(type, way, bubbles);
			this.position = position;
		}
		
		public override function clone():Event {
			return new MoveWayEvent(type, way, position, bubbles);
		}
	}
