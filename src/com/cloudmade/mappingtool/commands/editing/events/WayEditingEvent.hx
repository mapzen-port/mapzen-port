package com.cloudmade.mappingtool.commands.editing.events;

	import com.cloudmade.mappingtool.map.model.Way;
	
	import flash.events.Event;
	
	class WayEditingEvent extends EditingEvent {
		
		public static var ADD_WAY:String = "addWay";
		public static var ADD_AREA:String = "addArea";
		public static var ADD_WAY_WITHOUT_NODES:String = "addWayWithoutNodes";
		public static var REMOVE_WAY:String = "removeWay";
		public static var REMOVE_WAY_WITHOUT_NODES:String = "removeWayWithoutNodes";
		public static var REVERSE_WAY:String = "reverseWay";
		
		public var way:Way;
		
		public function new(type:String, way:Way, ?bubbles:Bool = false) {
			super(type, bubbles);
			this.way = way;
		}
		
		public override function clone():Event {
			return new WayEditingEvent(type, way, bubbles);
		}
	}
