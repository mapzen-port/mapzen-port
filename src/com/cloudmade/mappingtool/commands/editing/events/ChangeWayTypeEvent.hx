package com.cloudmade.mappingtool.commands.editing.events;

	import com.cloudmade.mappingtool.map.model.Way;
	
	import flash.events.Event;

	class ChangeWayTypeEvent extends WayEditingEvent {
		
		public static var CHANGE_WAY_TYPE:String = "changeWayType";
		
		public var isOneWay:Bool;
		
		public function new(type:String, way:Way, isOneWay:Bool, ?bubbles:Bool = false) {
			super(type, way, bubbles);
			this.isOneWay = isOneWay;
		}
		
		public override function clone():Event {
			return new ChangeWayTypeEvent(type, way, isOneWay, bubbles);
		}
	}
