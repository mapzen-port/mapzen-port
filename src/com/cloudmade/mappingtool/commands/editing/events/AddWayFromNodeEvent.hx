package com.cloudmade.mappingtool.commands.editing.events;

	import com.cloudmade.mappingtool.map.model.Node;
	import com.cloudmade.mappingtool.map.model.Way;
	
	import flash.events.Event;

	class AddWayFromNodeEvent extends WayEditingEvent {
		
		public static var ADD_WAY_FROM_NODE:String = "addWayFromNode";
		
		public var node:Node;
		
		public function new(type:String, way:Way, node:Node, ?bubbles:Bool = false) {
			super(type, way, bubbles);
			this.node = node;
		}
		
		public override function clone():Event {
			return new AddWayFromNodeEvent(type, way, node, bubbles);
		}
	}
