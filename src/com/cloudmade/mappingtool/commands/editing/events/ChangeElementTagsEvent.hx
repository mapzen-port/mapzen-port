package com.cloudmade.mappingtool.commands.editing.events;

	import com.cloudmade.mappingtool.map.model.Element;
	
	import flash.events.Event;

	class ChangeElementTagsEvent extends ElementEditingEvent {
		
		public static var CHANGE_ELEMENT_TAGS:String = "changeElementTags";
		
		public var oldTags:Array<Dynamic>;
		public var newTags:Array<Dynamic>;
		
		public var oldType:String;
		public var newType:String;
		
		public function new(type:String, element:Element, oldTags:Array<Dynamic>, newTags:Array<Dynamic>, ?bubbles:Bool = false) {
			super(type, element, bubbles);
			this.oldTags = oldTags;
			this.newTags = newTags;
		}
		
		public override function clone():Event {
			return new ChangeElementTagsEvent(type, element, oldTags, newTags, bubbles);
		}
	}
