package com.cloudmade.mappingtool.commands.editing.events;

	import com.cloudmade.mappingtool.map.model.Node;
	
	import flash.events.Event;

	class WayNodeEditingEvent extends NodeEditingEvent {
		
		public static var SPLIT_NODE_WAYS:String = "splitNodeWays";
		public static var BREAK_NODE_WAYS:String = "breakNodeWays";
		
		public function new(type:String, node:Node, ?bubbles:Bool=false)
		{
			super(type, node, bubbles);
		}
		
		public override function clone():Event {
			return new WayNodeEditingEvent(type, node, bubbles);
		}
	}
