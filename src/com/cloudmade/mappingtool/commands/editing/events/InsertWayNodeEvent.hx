package com.cloudmade.mappingtool.commands.editing.events;

	import com.cloudmade.mappingtool.map.model.Node;
	import com.cloudmade.mappingtool.map.model.Way;
	
	import flash.events.Event;

	class InsertWayNodeEvent extends AddNodeToWayEvent {
		
		public static var INSERT_WAY_NODE:String = "insertWayNode";
		
		public var joinWay:Way;
		
		public function new(type:String, node:Node, joinWay:Way, way:Way, ?index:Int = -1, ?bubbles:Bool = false) {
			super(type, node, way, index, bubbles);
			this.joinWay = joinWay;
		}
		
		public override function clone():Event {
			return new InsertWayNodeEvent(type, node, joinWay, way, index, bubbles);
		}
	}
