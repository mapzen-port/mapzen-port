package com.cloudmade.mappingtool.commands.editing.events;

	import com.cloudmade.mappingtool.map.model.Element;
	import com.cloudmade.mappingtool.map.model.Tag;
	
	import flash.events.Event;

	class ChangeElementTagEvent extends ElementTagEditingEvent {
		
		public static var CHANGE_ELEMENT_TAG_KEY:String = "changeElementTagKey";
		public static var CHANGE_ELEMENT_TAG_VALUE:String = "changeElementTagValue";
		
		public var newValue:String;
		
		public function new(type:String, element:Element, tag:Tag, newValue:String, ?bubbles:Bool=false) {
			super(type, element, tag, bubbles);
			this.newValue = newValue;
		}
		
		public override function clone():Event {
			return new ChangeElementTagEvent(type, element, tag, newValue, bubbles);
		}
	}
