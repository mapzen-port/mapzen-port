package com.cloudmade.mappingtool.commands.editing.events;

	import flash.events.Event;
	
	class EditingEvent extends Event {
		
		public function new(type:String, ?bubbles:Bool = false) {
			super(type, bubbles);
		}
		
		public override function clone():Event {
			return new EditingEvent(type, bubbles);
		}
	}
