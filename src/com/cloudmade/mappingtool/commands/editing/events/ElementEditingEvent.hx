package com.cloudmade.mappingtool.commands.editing.events;

	import com.cloudmade.mappingtool.map.model.Element;
	
	import flash.events.Event;
	
	class ElementEditingEvent extends EditingEvent {
		
		public var element:Element;
		
		public function new(type:String, element:Element, ?bubbles:Bool = false) {
			super(type, bubbles);
			this.element = element;
		}
		
		public override function clone():Event {
			return new ElementEditingEvent(type, element, bubbles);
		}
	}
