package com.cloudmade.mappingtool.commands.editing.events;

	import com.cloudmade.mappingtool.map.model.Node;
	
	import flash.events.Event;
	
	class NodeEditingEvent extends EditingEvent {
		
		public static var ADD_NODE:String = "addNode";
		public static var REMOVE_NODE:String = "removeNode";
		public static var REMOVE_NODE_WITHOUT_WAYS:String = "removeNodeWithoutWays";
		
		public var node:Node;
		
		public function new(type:String, node:Node, ?bubbles:Bool = false) {
			super(type, bubbles);
			this.node = node;
		}
		
		public override function clone():Event {
			return new NodeEditingEvent(type, node, bubbles);
		}
	}
