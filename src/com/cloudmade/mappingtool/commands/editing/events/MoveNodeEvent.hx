package com.cloudmade.mappingtool.commands.editing.events;

	import com.cloudmade.mappingtool.map.geo.LatLong;
	import com.cloudmade.mappingtool.map.model.Node;
	import com.cloudmade.mappingtool.utils.roundTo;
	
	import flash.events.Event;

	class MoveNodeEvent extends NodeEditingEvent {
		
		public static var MOVE_NODE:String = "moveNode";
		
		public var position:LatLong;
		
		public function new(type:String, node:Node, position:LatLong, ?bubbles:Bool = false) {
			super(type, node, bubbles);
			this.position = position;
		}
		
		public override function clone():Event {
			return new MoveNodeEvent(type, node, position, bubbles);
		}
	}
