package com.cloudmade.mappingtool.commands.editing;

	import com.cloudmade.mappingtool.map.geo.LatLong;
	import com.cloudmade.mappingtool.map.model.Map;
	import com.cloudmade.mappingtool.map.model.Node;
	import com.cloudmade.mappingtool.map.model.Way;
	
	import flash.utils.IDataInput;
	import flash.utils.IDataOutput;
	
	/*[RemoteClass(alias="com.cloudmade.mappingtool.commands.editing.MoveNodeCommand")]*/
	
	class MoveNodeCommand extends AddNodeCommand {
		
		public var category(getCategory, null) : String ;
		var position:LatLong;
		var oldPosition:LatLong;
		
		public function new(?map:Map = null, ?node:Node = null, ?position:LatLong = null) {
			super(map, node);
			this.position = position;
		}
		
		public override function getCategory():String {
			return ElementCommandCategory.MODIFY;
		}
		
		public override function execute():Void {
			super.execute();
			oldPosition = node.position;
			node.position = position;
			refreshNode(node);
		}
		
		public override function undo():Void {
			super.execute();
			node.position = oldPosition;
			refreshNode(node);
		}
		
		public override function readExternal(input:IDataInput):Void {
			super.readExternal(input);
			position = input.readObject();
			oldPosition = input.readObject();
		}
		
		public override function writeExternal(output:IDataOutput):Void {
			super.writeExternal(output);
			output.writeObject(position);
			output.writeObject(oldPosition);
		}
		
		override function refreshNode(node:Node):Void {
			super.refreshNode(node);
			var ways:Array<Dynamic> = map.getNodeWays(node);
			for (way in ways) {
				refreshWay(way);
			}
		}
	}
