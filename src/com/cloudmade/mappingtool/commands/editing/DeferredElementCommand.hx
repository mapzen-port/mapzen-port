package com.cloudmade.mappingtool.commands.editing;

	import com.cloudmade.mappingtool.map.model.Element;
	
	import flash.utils.IDataInput;
	import flash.utils.IDataOutput;

	/*[RemoteClass(alias="com.cloudmade.mappingtool.commands.editing.DeferredElementCommand")]*/
	
	class DeferredElementCommand implements IMacroElementCommand {
		
		public var category(getCategory, null) : String ;
		public var commands(getCommands, null) : Array<Dynamic> ;
		public var description(getDescription, null) : String ;
		public var target(getTarget, null) : Element ;
		var createMethod:Dynamic;
		var args:Array<Dynamic>;
		var command:IElementCommand;
		
		public function new(?createMethod:Dynamic = null, args:Array<Dynamic>) {
			this.createMethod = createMethod;
			this.args = args;
		}
		
		public function getCategory():String {
			return command.category;
		}
		
		public function getTarget():Element {
			return command.target;
		}
		
		public function getCommands():Array<Dynamic> {
			return Std.is( command, IMacroElementCommand) ? IMacroElementCommand(command).commands : null;
		}
		
		var _description:String ;
		public function getDescription():String {
			return _description;
		}
		public function setDescription(value:String):Void {
			_description = value;
		}

		public function execute():Void {
			if (createMethod != null) {
				command = createMethod.apply(null, args);
			}
			if (command != null) {
				command.execute();
			}
		}
		
		public function undo():Void {
			if (command != null) {
				command.undo();
			}
		}
		
		public function redo():Void {
			execute();
		}
		
		public function readExternal(input:IDataInput):Void {
			command = input.readObject();
			setDescription(input.readUTF());
		}
		
		public function writeExternal(output:IDataOutput):Void {
			output.writeObject(command);
			output.writeUTF(description);
		}
	}
