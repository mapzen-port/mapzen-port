package com.cloudmade.mappingtool.commands.editing;

	import com.cloudmade.mappingtool.map.model.Map;
	import com.cloudmade.mappingtool.map.model.Node;
	
	import flash.utils.IDataInput;
	import flash.utils.IDataOutput;
	
	/*[RemoteClass(alias="com.cloudmade.mappingtool.commands.editing.AddNodeCommand")]*/
	
	class AddNodeCommand extends BaseNodeCommand, implements IElementCommand {
		
		public var category(getCategory, null) : String ;
		var source:Node;
		
		public function new(?map:Map = null, ?node:Node = null) {
			super(map, node);
		}
		
		public function getCategory():String {
			return ElementCommandCategory.CREATE;
		}
		
		public override function execute():Void {
			if (updateNode()) return;
			// If command is executing first time, save source node, so in
			// future it will be possible to reapply changes in original order.
			// But don't replace node in first execution when node is also stored as a selection.
			if (source) {
				node = source.clone();
			} else {
				source = node.clone();
			}
			map.nodes.addItem(node);
		}
		
		public override function undo():Void {
			if (updateNode()) {
				map.nodes.removeItem(node);
			}
		}
		
		public override function readExternal(input:IDataInput):Void {
			super.readExternal(input);
			source = input.readObject();
		}
		
		public override function writeExternal(output:IDataOutput):Void {
			super.writeExternal(output);
			output.writeObject(source);
		}
	}
