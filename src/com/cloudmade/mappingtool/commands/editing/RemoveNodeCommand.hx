package com.cloudmade.mappingtool.commands.editing;

	import com.cloudmade.mappingtool.map.model.Map;
	import com.cloudmade.mappingtool.map.model.Node;
	
	/*[RemoteClass(alias="com.cloudmade.mappingtool.commands.editing.RemoveNodeCommand")]*/
	
	class RemoveNodeCommand extends BaseNodeCommand, implements IElementCommand {
		
		public var category(getCategory, null) : String ;
		public function new(?map:Map = null, ?node:Node = null) {
			super(map, node);
		}
		
		public function getCategory():String {
			return ElementCommandCategory.DELETE;
		}
		
		public override function execute():Void {
			if (updateNode()) {
				map.nodes.removeItem(node);
			}
		}
		
		public override function undo():Void {
			if (!updateNode()) {
				map.nodes.addItem(node);
			}
		}
	}
