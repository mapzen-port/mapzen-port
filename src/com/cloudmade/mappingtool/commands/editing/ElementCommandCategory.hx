package com.cloudmade.mappingtool.commands.editing;

	class ElementCommandCategory
	 {
		public function new() { }
		
		public static var CREATE:String = "create";
		public static var MODIFY:String = "modify";
		public static var DELETE:String = "delete";
	}
