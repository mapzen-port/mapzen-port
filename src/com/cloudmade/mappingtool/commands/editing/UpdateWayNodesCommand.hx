package com.cloudmade.mappingtool.commands.editing;

	import com.cloudmade.mappingtool.map.model.Element;
	import com.cloudmade.mappingtool.map.model.Map;
	import com.cloudmade.mappingtool.map.model.Node;
	import com.cloudmade.mappingtool.map.model.Way;
	
	/*[RemoteClass(alias="com.cloudmade.mappingtool.commands.editing.UpdateWayCommand")]*/

	class UpdateWayNodesCommand extends BaseWayCommand, implements IElementCommand {
		
		public var category(getCategory, null) : String ;
		public var target(getTarget, null) : Element ;
		public function new(?map:Map = null, ?way:Way = null) {
			super(map, way);
		}
		
		public function getCategory():String {
			return null;
		}
		
		public override function getTarget():Element {
			return null;
		}
		
		public override function execute():Void {
			if (!updateWay()) return;
			var newNodes:Array<Dynamic> = new Array();
			var nodes:Array<Dynamic> = way.nodes.source;
			for (node in nodes) {
				var newNode:Node = map.getNode(node.id);
				if (newNode) {
					newNodes.push(newNode);
				} else {
					newNodes.push(node);
					map.nodes.addItem(node);
				}
			}
			way.nodes.source = newNodes;
		}
		
		public override function undo():Void {
			execute();
		}
	}
