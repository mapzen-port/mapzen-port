package com.cloudmade.mappingtool.commands.editing;

	import com.cloudmade.mappingtool.map.model.Map;
	import com.cloudmade.mappingtool.map.model.Node;
	import com.cloudmade.mappingtool.map.model.Way;
	
	import flash.utils.IDataInput;
	import flash.utils.IDataOutput;
	
	/*[RemoteClass(alias="com.cloudmade.mappingtool.commands.editing.RemoveWayNodeCommand")]*/
	
	class RemoveWayNodeCommand extends BaseWayCommand, implements IElementCommand {
		
		public var category(getCategory, null) : String ;
		var nodeIndex:Int;
		var removedNode:Node;
		var wayRemoved:Bool ;
		
		public function new(?map:Map = null, ?way:Way = null, ?nodeIndex:Int = 0) {
			
			wayRemoved = false;
			super(map, way);
			this.nodeIndex = nodeIndex;
		}
		
		public function getCategory():String {
			return wayRemoved ? ElementCommandCategory.DELETE : ElementCommandCategory.MODIFY;
		}
		
		public override function execute():Void {
			if (!updateWay()) return;
			removedNode = cast( way.nodes.removeItemAt(nodeIndex), Node);
			if (way.nodes.length > 1) {
				refreshWay(way);
			} else {
				map.ways.removeItem(way);
				wayRemoved = true;
			}
		}
		
		public override function undo():Void {
			if (wayRemoved) {
				map.ways.addItem(way);
				wayRemoved = false;
			} else if (!updateWay()) {
				return;
			}
			way.nodes.addItemAt(removedNode, nodeIndex);
			refreshWay(way);
		}
		
		public override function readExternal(input:IDataInput):Void {
			super.readExternal(input);
			nodeIndex = input.readInt();
			removedNode = input.readObject();
			wayRemoved = input.readBoolean();
		}
		
		public override function writeExternal(output:IDataOutput):Void {
			super.writeExternal(output);
			output.writeInt(nodeIndex);
			output.writeObject(removedNode);
			output.writeBoolean(wayRemoved);
		}
	}
