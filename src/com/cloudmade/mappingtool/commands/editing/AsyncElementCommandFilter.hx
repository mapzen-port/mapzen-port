package com.cloudmade.mappingtool.commands.editing;

	class AsyncElementCommandFilter
	 {
		
		var commands:Array<Dynamic>;
		
		public function new(commands:Array<Dynamic>) {
			this.commands = commands;
		}
		
		public function getAsyncCommands():Array<Dynamic> {
			return findAsyncCommands(commands);
		}
		
		function findAsyncCommands(commands:Array<Dynamic>):Array<Dynamic> {
			var asyncCommands:Array<Dynamic> = new Array();
			for (command in commands) {
				if (Std.is( command, IAsyncElementCommand)) {
					asyncCommands.push(command);
				}
				if (Std.is( command, IMacroElementCommand)) {
					var subCommands:Array<Dynamic> = IMacroElementCommand(command).commands;
					var subAsyncCommands:Array<Dynamic> = findAsyncCommands(subCommands);
					asyncCommands = asyncCommands.concat(subAsyncCommands);
				}
			}
			return asyncCommands;
		}
	}
