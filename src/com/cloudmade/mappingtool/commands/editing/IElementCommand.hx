package com.cloudmade.mappingtool.commands.editing;

	import com.cloudmade.mappingtool.commands.IRedoableCommand;
	import com.cloudmade.mappingtool.map.model.Element;
	
	import flash.utils.IExternalizable;
	
	interface IElementCommand implements IRedoableCommand, implements IDescribable, implements IExternalizable{
		function category():String;
		
		function target():Element;
		
		// WORKAROUND: Using method instead of setter because of weird 1000 compiler error.
		function setDescription(value:String):Void;
	}
