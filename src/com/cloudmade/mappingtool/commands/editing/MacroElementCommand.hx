package com.cloudmade.mappingtool.commands.editing;

	import com.cloudmade.mappingtool.commands.MacroCommand;
	import com.cloudmade.mappingtool.map.model.Element;
	
	import flash.utils.IDataInput;
	import flash.utils.IDataOutput;
	
	/*[RemoteClass(alias="com.cloudmade.mappingtool.commands.editing.MacroElementCommand")]*/

	class MacroElementCommand extends MacroCommand, implements IMacroElementCommand {
		
		public var category(getCategory, null) : String ;
		public var commands(getCommands, null) : Array<Dynamic> ;
		public var description(getDescription, null) : String ;
		public var target(getTarget, null) : Element ;
		var undone:Bool ;
		
		public function new(?commands:Array<Dynamic> = null) {
			
			undone = false;
			super(commands);
		}
		
		public function getCommands():Array<Dynamic> {
			return _commands;
		}
		
		public function getCategory():String {
			return null;
		}
		
		public function getTarget():Element {
			return null;
		}
		
		var _description:String ;
		public function getDescription():String {
			return _description;
		}
		public function setDescription(value:String):Void {
			_description = value;
		}
		
		public override function undo():Void {
			super.undo();
			undone = true;
		}
		
		public override function redo():Void {
			super.redo();
			undone = false;
		}
		
		public function readExternal(input:IDataInput):Void {
			_commands = input.readObject();
			undone = input.readBoolean();
			setDescription(input.readUTF());
		}
		
		public function writeExternal(output:IDataOutput):Void {
			output.writeObject(_commands);
			output.writeBoolean(undone);
			output.writeUTF(description);
		}
	}
