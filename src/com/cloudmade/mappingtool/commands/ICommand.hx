package com.cloudmade.mappingtool.commands;

	interface ICommand
	{
		function execute():Void;
	}
