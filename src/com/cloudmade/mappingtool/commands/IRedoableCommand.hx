package com.cloudmade.mappingtool.commands;

	interface IRedoableCommand implements IUndoableCommand{
		function redo():Void;
	}
