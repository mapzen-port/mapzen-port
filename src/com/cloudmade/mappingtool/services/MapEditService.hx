package com.cloudmade.mappingtool.services;

	import com.cloudmade.mappingtool.business.IDelegateGroup;
	import com.cloudmade.mappingtool.business.SequenceParallelDelegateGroupBuilder;
	import com.cloudmade.mappingtool.commands.MacroCommand;
	import com.cloudmade.mappingtool.commands.editing.AsyncElementCommandFilter;
	import com.cloudmade.mappingtool.commands.editing.ElementCommandCategory;
	import com.cloudmade.mappingtool.commands.editing.IElementCommand;
	import com.cloudmade.mappingtool.commands.editing.IMacroElementCommand;
	import com.cloudmade.mappingtool.commands.editing.events.AddNodeToWayEvent;
	import com.cloudmade.mappingtool.commands.editing.events.ChangeWayTypeEvent;
	import com.cloudmade.mappingtool.commands.editing.events.EditingEvent;
	import com.cloudmade.mappingtool.commands.editing.events.InsertWayNodeEvent;
	import com.cloudmade.mappingtool.commands.editing.events.MoveNodeEvent;
	import com.cloudmade.mappingtool.commands.editing.events.MoveWayEvent;
	import com.cloudmade.mappingtool.commands.editing.events.NodeEditingEvent;
	import com.cloudmade.mappingtool.commands.editing.events.WayEditingEvent;
	import com.cloudmade.mappingtool.commands.editing.factories.IEditingCommandFactory;
	import com.cloudmade.mappingtool.editing.CommandExecutor;
	import com.cloudmade.mappingtool.editing.CommandHistory;
	import com.cloudmade.mappingtool.editing.CommandList;
	import com.cloudmade.mappingtool.editing.ICommandExecutor;
	import com.cloudmade.mappingtool.editing.ICommandList;
	import com.cloudmade.mappingtool.editing.IEditingHistory;
	import com.cloudmade.mappingtool.editing.INewElementIdGenerator;
	import com.cloudmade.mappingtool.editing.ListTruncator;
	import com.cloudmade.mappingtool.events.MapEditServiceEvent;
	import com.cloudmade.mappingtool.events.MapEditServiceSelectionChangeEvent;
	import com.cloudmade.mappingtool.events.OSMProxyResultEvent;
	import com.cloudmade.mappingtool.map.events.MapEvent;
	import com.cloudmade.mappingtool.map.geo.LatLong;
	import com.cloudmade.mappingtool.map.model.Element;
	import com.cloudmade.mappingtool.map.model.Map;
	import com.cloudmade.mappingtool.map.model.Member;
	import com.cloudmade.mappingtool.map.model.Node;
	import com.cloudmade.mappingtool.map.model.Relation;
	import com.cloudmade.mappingtool.map.model.Tag;
	import com.cloudmade.mappingtool.map.model.Way;
	import com.cloudmade.mappingtool.model.ApplicationData;
	import com.cloudmade.mappingtool.model.OSMProxy;
	import com.cloudmade.mappingtool.model.osm.ElementDiff;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.geom.Point;
	
	import mx.collections.ArrayList;
	import mx.collections.IList;
	import mx.events.CollectionEvent;
	import mx.events.CollectionEventKind;
	import mx.resources.IResourceManager;
	import mx.resources.ResourceManager;
	
	[ResourceBundle("Global")]
	
	/*[Event(name="ioError", type="flash.events.IOErrorEvent")]*/
	/*[Event(name="addingWayChange", type="com.cloudmade.mappingtool.events.MapEditServiceEvent")]*/
	/*[Event(name="selectionChange", type="com.cloudmade.mappingtool.events.MapEditServiceSelectionChangeEvent")]*/

	class MapEditService extends EventDispatcher, implements INewElementIdGenerator, implements IEditingHistory {
		
		public var addingWay(getAddingWay, setAddingWay) : Bool;
		public var presetTags(getPresetTags, setPresetTags) : Array<Dynamic>;
		public var selectedCategory(getSelectedCategory, setSelectedCategory) : UInt;
		public var selection(getSelection, setSelection) : Element;
		public static var NODES:UInt = 0;
		public static var WAYS:UInt = 1;
		public static var AREAS:UInt = 2;
		
		static var instance:MapEditService = null;
		
		public static function getInstance():MapEditService {
			if (instance == null) {
				instance = new MapEditService(new SingletonEnforcer());
			}
			return instance;
		}
		
		public var lastId:Int ;
		/*[Bindable]*/
		public var commandList:ICommandList;
		
		var commandHistory:IEditingHistory;
		var commandExecutor:ICommandExecutor;
		var editingCommandFactory:IEditingCommandFactory;
		var commandListTruncator:ListTruncator;
		
		var map:Map;
		var movedSelectedElement:Bool ;
		var movingSelectedElement:Bool ;
		var mapService:MapService ;
		var appData:ApplicationData ;
		var resourceManager:IResourceManager ;
		
		public function new(enforcer:SingletonEnforcer) {
			
			lastId = 0;
			movedSelectedElement = false;
			movingSelectedElement = false;
			mapService = MapService.getInstance();
			appData = ApplicationData.getInstance();
			resourceManager = ResourceManager.getInstance();
			super();
			map = appData.map;
			setCommandList(new CommandList(new ArrayList(), 0));
			map.addEventListener(MapEvent.ELEMENTS_CHANGE, map_elementsChangeHandler);
		}
		
		public function setEditingCommandFactory(factory:IEditingCommandFactory):Void {
			editingCommandFactory = factory;
		}
		
		public function setCommandList(commandList:ICommandList):Void {
			this.commandList = commandList;
			commandHistory = new CommandHistory(commandList);
			commandListTruncator = new ListTruncator(commandList.source);
			commandExecutor = new CommandExecutor(commandList, commandListTruncator);
		}
		
		var addingWayCommandIndex:Int;
		var _addingWay:Bool ;
		public function getAddingWay():Bool{
			return _addingWay;
		}
		public function setAddingWay(value:Bool):Bool{
			if (value == _addingWay) return;
			_addingWay = value;
			if (value) {
				// addingWay is changed after add way command execution:
				addingWayCommandIndex = commandList.currentIndex - 1;
			}
			dispatchEvent(new MapEditServiceEvent(MapEditServiceEvent.ADDING_WAY_CHANGE));
			return value;
		}
		
		var _selectedCategory:UInt;
		public function getSelectedCategory():UInt{
			return _selectedCategory;
		}
		public function setSelectedCategory(value:UInt):UInt{
			if (value == _selectedCategory) return;
			_selectedCategory = value;
			/*if (_selectedCategory != getElementCategory(selection)) {
				selection = null;
			}
			if (_selectedCategory != WAYS) {
				finishAddingWay();
			}*/
			return value;
		}
		
		var _presetTags:Array<Dynamic> ;
		public function getPresetTags():Array<Dynamic>{
			var tags:Array<Dynamic> = new Array();
			for (tag in _presetTags) {
				tags.push(tag.clone());
			}
			return tags;
		}
		public function setPresetTags(tags:Array<Dynamic>):Array<Dynamic>{
			_presetTags = tags;
			return tags;
		}
		
		var _selection:Element;
		/*[Bindable(event="selectionChange")]*/
		public function getSelection():Element{
			return _selection;
		}
		public function setSelection(value:Element):Element{
			if (value == _selection) return;
			var oldSelection:Element = _selection;
			_selection = value;
			dispatchSelectionChangeEvent(oldSelection);
			return value;
		}
		
		public function getIdForNewElement():String {
			return String(--lastId);
		}
		
		public function addNode(position:LatLong):Void {
			var node:Node;
			if (selectedCategory == NODES) {
				node = new Node(String(--lastId), presetTags, position);
				executeEditingCommand(new NodeEditingEvent(NodeEditingEvent.ADD_NODE, node));
				selection = node;
			} else {
				node = new Node(String(--lastId), null, position);
				if (!addingWay) {
					var way:Way = new Way(String(--lastId), presetTags, [node]);
					way.isArea = selectedCategory == AREAS;
					executeEditingCommand(new WayEditingEvent(WayEditingEvent.ADD_WAY, way));
					selection = way;
					addingWay = true;
				} else {
					executeEditingCommand(new AddNodeToWayEvent(AddNodeToWayEvent.APPEND_WAY_NODE, node, cast( selection, Way)));
					refreshSelection();
				}
			}
		}
		
		public function joinWayNode(node:Node):Void {
			if (addingWay) {
				executeEditingCommand(new AddNodeToWayEvent(AddNodeToWayEvent.JOIN_WAY_NODE, node, cast( selection, Way)));
				refreshSelection();
			}
		}
		
		public function insertWayNode(index:Int, position:LatLong):Void {
			var node:Node = new Node(String(--lastId), null, position);
			executeEditingCommand(new AddNodeToWayEvent(AddNodeToWayEvent.APPEND_WAY_NODE, node, cast( selection, Way), index));
			refreshSelection();
		}
		
		public function insertAndAppendWayNode(way:Way, index:Int, position:LatLong):Void {
			var node:Node = new Node(String(--lastId), null, position);
			executeEditingCommand(new InsertWayNodeEvent(InsertWayNodeEvent.INSERT_WAY_NODE, node, cast( selection, Way), way, index));
			refreshSelection();
		}
		
		public function removeSelectedElement():Void {
			if (Std.is( selection, Node)) {
				executeEditingCommand(new NodeEditingEvent(NodeEditingEvent.REMOVE_NODE, cast( selection, Node)));
			} else if (Std.is( selection, Way)) {
				addingWay = false;
				executeEditingCommand(new WayEditingEvent(WayEditingEvent.REMOVE_WAY, cast( selection, Way)));
			}
			selection = null;
		}
		
		public function startMovingSelectedElement():Void {
			if (!movingSelectedElement) {
				movingSelectedElement = true;
				movedSelectedElement = false;
			}
		}
		
		public function moveSelectedElement(xOffset:Int, yOffset:Int):Void {
			var node:Node;
			if (Std.is( selection, Node)) {
				node = cast( selection, Node);
			} else if (Std.is( selection, Way)) {
				node = cast( Way(selection).nodes.getItemAt(0), Node);
			} else {
				return;
			}
			// Calculate new position first.
			var position:LatLong = getNodeOffsetPosition(node, xOffset, yOffset);
			// If we are moving selection using one command, then undo previous move command if it exist.
			if (movingSelectedElement && movedSelectedElement) {
				commandHistory.undo();
			}
			// And execute new one that uses newest position.
			if (Std.is( selection, Node)) {
				executeEditingCommand(new MoveNodeEvent(MoveNodeEvent.MOVE_NODE, cast( selection, Node), position));
			} else if (Std.is( selection, Way)) {
				executeEditingCommand(new MoveWayEvent(MoveWayEvent.MOVE_WAY, cast( selection, Way), position));
			}
			movedSelectedElement = true;
			refreshSelection();
		}
		
		public function stopMovingSelectedElement():Void {
			movingSelectedElement = false;
		}
		
		public function finishAddingWay():Void {
			addingWay = false;
		}
		
		public function revertAddingWay():Void {
			finishAddingWay();
			undo(commandList.currentIndex - addingWayCommandIndex);
			commandListTruncator.truncate(commandList.currentIndex);
		}
		
		public function setOneWay(value:Bool):Void {
			if (Std.is( selection, Way)) {
				executeEditingCommand(new ChangeWayTypeEvent(ChangeWayTypeEvent.CHANGE_WAY_TYPE, cast( selection, Way), value));
			}
		}
		
		public function reverse():Void {
			if (Std.is( selection, Way)) {
				executeEditingCommand(new WayEditingEvent(WayEditingEvent.REVERSE_WAY, cast( selection, Way)));
			}
		}
		
		public function undo(?count:Int = 1):Void {
			selection = null;
			commandHistory.undo(count);
			finishAddingWay();
		}
		
		public function redo(?count:Int = 1):Void {
			selection = null;
			commandHistory.redo(count);
		}
		
		var osmProxy:OSMProxy;
		var saveComment:String;
		var delegateGroup:IDelegateGroup;
		
		public function save(osmProxy:OSMProxy, ?comment:String = ""):Void {
			this.osmProxy = osmProxy;
			saveComment = comment;
			var commands:Array<Dynamic> = commandList.undoCommands;
			var asyncCommandFilter:AsyncElementCommandFilter = new AsyncElementCommandFilter(commands);
			var asyncCommands:Array<Dynamic> = asyncCommandFilter.getAsyncCommands();
			if (asyncCommands.length > 0) {
				var delegateGroupBuilder:SequenceParallelDelegateGroupBuilder = new SequenceParallelDelegateGroupBuilder(asyncCommands);
				delegateGroup = delegateGroupBuilder.build(10);
				delegateGroup.addEventListener(Event.COMPLETE, delegateGroup_completeHandler, false, 0, true);
				delegateGroup.addEventListener(IOErrorEvent.IO_ERROR, delegateGroup_ioErrorHandler, false, 0, true);
				delegateGroup.send();
			} else {
				delegateGroup = null;
				commitChanges();
			}
		}
		
		function commitChanges():Void {
			osmProxy.addEventListener(OSMProxyResultEvent.SAVE, osmProxy_saveHandler);
			osmProxy.openChangeset([new Tag("created_by", "Mapzen Beta"), new Tag("comment", saveComment)]);
			var modifiedRelationsMap:Dynamic = new Object();
			var relations:Array<Dynamic>;
			var relation:Relation;
			var node:Node;
			var nodesChanges:ElementsChanges = getElementsChanges(Node);
			for (node in nodesChanges.createdMap) {
				osmProxy.createNode(node);
			}
			for (node in nodesChanges.modifiedMap) {
				osmProxy.modifyNode(node);
			}
			for (node in nodesChanges.deletedMap) {
				osmProxy.deleteNode(node);
				relations = map.getElementRelations(node);
				for (relation in relations) {
					removeElementFromRelation(node.id, relation);
					modifiedRelationsMap[relation.id] = relation;
				}
			}
			var way:Way;
			var waysChanges:ElementsChanges = getElementsChanges(Way);
			for (way in waysChanges.createdMap) {
				osmProxy.createWay(way);
			}
			for (way in waysChanges.modifiedMap) {
				osmProxy.modifyWay(way);
			}
			for (way in waysChanges.deletedMap) {
				osmProxy.deleteWay(way);
				relations = map.getElementRelations(way);
				for (relation in relations) {
					removeElementFromRelation(way.id, relation);
					modifiedRelationsMap[relation.id] = relation;
				}
			}
			for (relation in modifiedRelationsMap) {
				osmProxy.modifyRelation(relation);
			}
			osmProxy.closeChangeset();
			osmProxy = null;
		}
		
		function removeElementFromRelation(elementId:String, relation:Relation):Void {
			relation.members.source = relation.members.source.filter(
				function(item:Member, index:Int, array:Array<Dynamic>):Bool {
					return item.elementId != elementId;
				}
			);
		}
		
		function delegateGroup_completeHandler(event:Event):Void {
			commitChanges();
		}
		
		function delegateGroup_ioErrorHandler(event:IOErrorEvent):Void {
			dispatchEvent(event);
		}
		
		public function executeEditingCommand(event:EditingEvent):Void {
			var command:IElementCommand = editingCommandFactory.create(event);
			if (command) {
				commandExecutor.execute(command);
			}
		}
		
		function getNodeOffsetPosition(node:Node, xOffset:Int, yOffset:Int):LatLong {
			var point:Point = map.project(node.position);
			return map.unproject(new Point(point.x + xOffset, point.y + yOffset));
		}
		
		function refreshSelection():Void {
			if (selection != null) {
				dispatchSelectionChangeEvent(selection);
			}
		}
		
		function getElementsChanges(targetsClass:Class<Dynamic>):ElementsChanges {
			var elementsChanges:ElementsChanges = new ElementsChanges();
			var commands:Array<Dynamic> = commandList.undoCommands;
			updateElementsChanges(elementsChanges, commands, targetsClass);
			return elementsChanges;
		}
		
		function updateElementsChanges(elementsChanges:ElementsChanges, commands:Array<Dynamic>, targetsClass:Class<Dynamic>):Void {
			for (command in commands) {
				var target:Element = command.target;
				if (Std.is( target, targetsClass)) {
					var commandType:String = command.category;
					if (commandType == ElementCommandCategory.CREATE) {
						elementsChanges.createdMap[target.id] = target;
					} else if (commandType == ElementCommandCategory.MODIFY) {
						if (!target.isNew) {
							elementsChanges.modifiedMap[target.id] = target;
						}
					} else if (commandType == ElementCommandCategory.DELETE) {
						if (!target.isNew) {
							elementsChanges.deletedMap[target.id] = target;
							delete elementsChanges.modifiedMap[target.id];
						} else {
							delete elementsChanges.createdMap[target.id];
						}
					}
				}
				// Maybe it's better to not extend IMacroElementCommand from IElementCommand.
				if (Std.is( command, IMacroElementCommand)) {
					updateElementsChanges(elementsChanges, IMacroElementCommand(command).commands, targetsClass);
				}
			}
		}
		
		function dispatchSelectionChangeEvent(oldSelection:Element):Void {
			dispatchEvent(new MapEditServiceSelectionChangeEvent(MapEditServiceSelectionChangeEvent.SELECTION_CHANGE, oldSelection));
		}
		
		function refreshMap():Void {
			var refreshEvent:CollectionEvent = new CollectionEvent(CollectionEvent.COLLECTION_CHANGE);
			refreshEvent.kind = CollectionEventKind.RESET;
			map.nodes.dispatchEvent(refreshEvent);
			map.ways.dispatchEvent(refreshEvent);
		}
		
		function getElementCategory(element:Element):UInt {
			if (Std.is( element, Node)) return NODES;
			if ((Std.is( element, Way)) && Way(element).isArea) return AREAS;
			return WAYS;
		}
		
		function map_elementsChangeHandler(event:MapEvent):Void {
			if (!mapService.isLoading) {
				new MacroCommand(commandList.undoCommands).execute();
				var newSelection:Element;
				if (Std.is( selection, Node)) {
					newSelection = map.getNode(selection.id);
				} else if (Std.is( selection, Way)) {
					newSelection = map.getWay(selection.id);
				}
				if (newSelection != selection) {
					selection = newSelection;
				} else {
					refreshSelection();
				}
			} else {
				selection = null;
			}
		}
		
		function osmProxy_saveHandler(event:OSMProxyResultEvent):Void {
			var osmProxy:OSMProxy = cast( event.target, OSMProxy);
			osmProxy.removeEventListener(OSMProxyResultEvent.SAVE, osmProxy_saveHandler);
			commandList.clear();
			var diff:ElementDiff;
			for (diff in event.nodes) {
				var node:Node = map.getNode(diff.oldId);
				if (node) {
					node.id = diff.newId;
					node.version = diff.newVersion;
				}
			}
			for (diff in event.ways) {
				var way:Way = map.getWay(diff.oldId);
				if (way) {
					way.id = diff.newId;
					way.version = diff.newVersion;
				}
			}
			for (diff in event.relations) {
				var relation:Relation = map.getRelation(diff.oldId);
				if (relation) {
					relation.id = diff.newId;
					relation.version = diff.newVersion;
				}
			}
			refreshMap();
		}
	}
