package com.cloudmade.mappingtool.services;

	import com.cloudmade.mappingtool.events.OSMProxyResultEvent;
	import com.cloudmade.mappingtool.map.events.BoundsChangeEvent;
	import com.cloudmade.mappingtool.map.events.ZoomEvent;
	import com.cloudmade.mappingtool.map.model.Map;
	import com.cloudmade.mappingtool.model.ApplicationData;
	import com.cloudmade.mappingtool.model.OSMProxy;
	
	import flash.events.EventDispatcher;

	public final class MapService extends EventDispatcher {
		
		public var isLoading(getIsLoading, null) : Bool ;
		static var instance:MapService = null;
		
		public static function getInstance():MapService {
			if (instance == null) {
				instance = new MapService(new SingletonEnforcer());
			}
			return instance;
		}
		
		var map:Map;
		var osmProxy:OSMProxy;
		var appData:ApplicationData;
		
		public function new(enforcer:SingletonEnforcer) {
			super();
		}
		
		var _isLoading:Bool;
		public function getIsLoading():Bool {
			return _isLoading;
		}
		
		public function initialize():Void {
			appData = ApplicationData.getInstance();
			osmProxy = OSMProxy.getInstance();
			map = appData.map;
			osmProxy.addEventListener(OSMProxyResultEvent.RESULT, osmProxy_resultHandler);
			map.addEventListener(BoundsChangeEvent.BOUNDS_CHANGE, map_boundsChangeHandler);
			map.addEventListener(ZoomEvent.ZOOM, map_zoomHandler);
		}
		
		function map_zoomHandler(event:ZoomEvent):Void {
			_isLoading = true;
			map.setElements(null, null, null);
		}
		
		function map_boundsChangeHandler(event:BoundsChangeEvent):Void {
			if (appData.editingEnabled) {
				osmProxy.getMap(map.bounds);
			} else {
				osmProxy.close();
			}
		}
		
		function osmProxy_resultHandler(event:OSMProxyResultEvent):Void {
			_isLoading = false;
			map.setElements(event.nodes, event.ways, event.relations);
		}
	}
