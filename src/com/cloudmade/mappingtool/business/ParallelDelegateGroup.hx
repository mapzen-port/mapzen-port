package com.cloudmade.mappingtool.business;

	/*[RemoteClass(alias="com.cloudmade.mappingtool.business.ParallelDelegateGroup")]*/
	
	class ParallelDelegateGroup extends ExternalizableDelegateGroup {
		
		public function new(?delegates:Array<Dynamic> = null) {
			super(delegates);
		}
		
		public override function send():Void {
			super.send();
			for (delegate in delegates) {
				delegate.send();
			}
		}
	}
