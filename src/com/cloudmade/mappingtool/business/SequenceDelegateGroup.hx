package com.cloudmade.mappingtool.business;

	import flash.events.Event;
	
	/*[RemoteClass(alias="com.cloudmade.mappingtool.business.SequenceDelegateGroup")]*/
	
	class SequenceDelegateGroup extends ExternalizableDelegateGroup {
		
		public function new(?delegates:Array<Dynamic> = null) {
			super(delegates);
		}
		
		public override function send():Void {
			super.send();
			var delegate:IDelegate = delegates[0];
			if (delegate) {
				delegate.send();
			}
		}
		
		override function delegate_completeHandler(event:Event):Void {
			super.delegate_completeHandler(event);
			if (completeNumber < delegates.length) {
				var delegate:IDelegate = delegates[completeNumber];
				delegate.send();
			}
		}
	}
