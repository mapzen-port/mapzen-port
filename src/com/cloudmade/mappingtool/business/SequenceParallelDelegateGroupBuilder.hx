package com.cloudmade.mappingtool.business;

	class SequenceParallelDelegateGroupBuilder
	 {
		
		var delegates:Array<Dynamic>;
		
		public function new(delegates:Array<Dynamic>) {
			this.delegates = delegates;
		}
		
		public function build(unitSize:Int):IDelegateGroup {
			var parallelDelegates:Array<Dynamic> = new Array();
			var i:Int = 0;
			while (i < delegates.length) {
				var delegatesUnit:Array<Dynamic> = delegates.slice(i, i + unitSize);
				var parallelDelegate:IDelegateGroup = new ParallelDelegateGroup(delegatesUnit);
				parallelDelegates.push(parallelDelegate);
				i += unitSize;
			}
			var sequenceDelegate:IDelegateGroup = new SequenceDelegateGroup(parallelDelegates);
			return sequenceDelegate;
		}
	}
