package com.cloudmade.mappingtool.business;

	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	
	/*[Event(name="complete", type="flash.events.Event")]*/
	/*[Event(name="ioError", type="flash.events.IOErrorEvent")]*/
	
	class Delegate extends EventDispatcher, implements IDelegate {
		
		public var result(getResult, null) : Dynamic ;
		var url:String;
		
		var loader:URLLoader;
		
		public function new(url:String) {
			super();
			this.url = url;
			loader = new URLLoader();
			loader.addEventListener(Event.COMPLETE, loader_completeHandler);
			loader.addEventListener(IOErrorEvent.IO_ERROR, loader_ioErrorHandler);
		}
		
		var _result:Dynamic;
		public function getResult():Dynamic {
			return _result;
		}
		
		public function send():Void {
			var request:URLRequest = createRequest();
			loader.load(request);
			_result = null;
		}
		
		function createRequest():URLRequest {
			var request:URLRequest = new URLRequest(url);
			return request;
		}
		
		function createResult(response:String):Dynamic {
			return null;
		}
		
		function loader_completeHandler(event:Event):Void {
			_result = createResult(cast( loader.data, String));
			dispatchEvent(event);
		}
		
		function loader_ioErrorHandler(event:Event):Void {
			dispatchEvent(event);
		}
	}
