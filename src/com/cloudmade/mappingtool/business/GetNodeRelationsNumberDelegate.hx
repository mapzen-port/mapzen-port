package com.cloudmade.mappingtool.business;

	/*[RemoteClass(alias="com.cloudmade.mappingtool.business.GetNodeRelationsNumberDelegate")]*/
	
	class GetNodeRelationsNumberDelegate extends ExternalizableDelegate {
		
		public function new(?url:String = null, ?nodeId:String = null) {
			url += "node/" + nodeId + "/relations";
			super(url);
		}
		
		override function createResult(response:String):Dynamic {
			var xml:XML = new XML(response);
			var result:Int = xml.relation.length();
			return result;
		}
	}
