package com.cloudmade.mappingtool.business.externalization;

	interface IXMLListDeserializer
	{
		function deserialize(xml:XMLList):Array<Dynamic>;
	}
