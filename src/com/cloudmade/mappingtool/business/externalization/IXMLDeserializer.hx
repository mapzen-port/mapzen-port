package com.cloudmade.mappingtool.business.externalization;

	interface IXMLDeserializer
	{
		function deserialize(xml:XML):Dynamic;
	}
