package com.cloudmade.mappingtool.business.externalization;

	interface IXMLListSerializer
	{
		function serialize(objects:Array<Dynamic>):XMLList;
	}
