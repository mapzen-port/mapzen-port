package com.cloudmade.mappingtool.business.externalization.osm;

	import com.cloudmade.mappingtool.business.externalization.IXMLDeserializer;
	import com.cloudmade.mappingtool.config.IReadOnlyHashMap;

	class NodeReferenceDeserializer implements IXMLDeserializer {
		
		var nodeMap:IReadOnlyHashMap;
		
		public function new(nodeMap:IReadOnlyHashMap)
		{
			this.nodeMap = nodeMap;
		}

		public function deserialize(xml:XML):Dynamic
		{
			return nodeMap.getData(xml.@ref);
		}
		
	}
