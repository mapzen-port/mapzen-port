package com.cloudmade.mappingtool.business.externalization.osm;

	import com.cloudmade.mappingtool.business.externalization.IXMLSerializer;

	class NodeReferenceSerializer implements IXMLSerializer {
		
		public function new()
		{
		}

		public function serialize(node:Dynamic):XML
		{
			return Xml.parse("<nd ref={node.id}/>");
		}
		
	}
