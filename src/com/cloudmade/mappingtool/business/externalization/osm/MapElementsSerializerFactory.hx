package com.cloudmade.mappingtool.business.externalization.osm;

	import com.cloudmade.mappingtool.business.externalization.IXMLListSerializer;
	import com.cloudmade.mappingtool.business.externalization.IXMLSerializer;
	
	class MapElementsSerializerFactory
	 {
		
		public function new()
		{
		}

		public function create():IXMLSerializer {
			var elementSerializer:IXMLSerializer = createElementSerializer();
			
			var nodesSerializer:IXMLListSerializer = createNodesSerializer(elementSerializer);
			var waysSerializer:IXMLListSerializer = createWaysSerializer(elementSerializer);
			var relationsSerializer:IXMLListSerializer = createRelationsSerializer(elementSerializer);
			
			return new MapElementsSerializer(nodesSerializer, waysSerializer, relationsSerializer);
		}
		
		function createNodesSerializer(elementSerializer:IXMLSerializer):IXMLListSerializer {
			var nodeSerializer:IXMLSerializer = new NodeSerializer(elementSerializer);
			return createListSerializer(nodeSerializer);
		}
		
		function createWaysSerializer(elementSerializer:IXMLSerializer):IXMLListSerializer {
			var nodeReferenceSerializer:IXMLSerializer = new NodeReferenceSerializer();
			var nodeReferencesSerializer:IXMLListSerializer = createListSerializer(nodeReferenceSerializer);
			var waySerializer:IXMLSerializer = new WaySerializer(elementSerializer, nodeReferencesSerializer);
			return createListSerializer(waySerializer);
		}
		
		function createRelationsSerializer(elementSerializer:IXMLSerializer):IXMLListSerializer {
			var memberSerializer:IXMLSerializer = new MemberExternalizer();
			var membersSerializer:IXMLListSerializer = createListSerializer(memberSerializer);
			var relationSerializer:Dynamic = new RelationSerializer(elementSerializer, membersSerializer);
			return createListSerializer(relationSerializer);
		}

		function createElementSerializer():IXMLSerializer {
			var tagSerializer:IXMLSerializer = new TagExternalizer();
			var tagsSerializer:IXMLListSerializer = createListSerializer(tagSerializer);
			return new ElementSerializer(tagsSerializer);
		}
		
		function createListSerializer(serializer:IXMLSerializer):IXMLListSerializer {
			return new ListExternalizer(serializer, null);
		}
	}
