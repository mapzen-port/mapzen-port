package com.cloudmade.mappingtool.business.externalization.osm;

	import com.cloudmade.mappingtool.business.externalization.IXMLListSerializer;
	import com.cloudmade.mappingtool.business.externalization.IXMLSerializer;
	import com.cloudmade.mappingtool.map.model.ITag;

	class ValidTagsSerializer implements IXMLListSerializer {
		
		var tagsSerializer:IXMLListSerializer;
		
		public function new(tagsSerializer:IXMLListSerializer) {
			this.tagsSerializer = tagsSerializer;
		}

		public function serialize(objects:Array<Dynamic>):XMLList {
			var filteredTags:Array<Dynamic> = new Array();
			var tagKeys:Dynamic = new Object();
			
			for (tag in objects) {
				if (tag.isEmpty) continue;
				if (tagKeys[tag.key]) continue;
				
				filteredTags.push(tag);
				tagKeys[tag.key] = true;
			}
			
			return tagsSerializer.serialize(filteredTags);
		}
		
	}
