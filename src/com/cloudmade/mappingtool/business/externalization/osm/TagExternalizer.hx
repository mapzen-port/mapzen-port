package com.cloudmade.mappingtool.business.externalization.osm;

	import com.cloudmade.mappingtool.business.externalization.IXMLExternalizer;
	import com.cloudmade.mappingtool.map.model.Tag;

	class TagExternalizer implements IXMLExternalizer {
		
		public function new()
		{
		}

		public function serialize(object:Dynamic):XML
		{
			var xml:XML = Xml.parse("<tag/>");
			xml.@k = object.key;
			xml.@v = object.value;
			return xml;
		}
		
		public function deserialize(xml:XML):Dynamic
		{
			return new Tag(xml.@k, xml.@v);
		}
		
	}
