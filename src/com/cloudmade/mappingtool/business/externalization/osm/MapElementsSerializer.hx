package com.cloudmade.mappingtool.business.externalization.osm;

	import com.cloudmade.mappingtool.business.externalization.IXMLListSerializer;
	import com.cloudmade.mappingtool.business.externalization.IXMLSerializer;

	class MapElementsSerializer implements IXMLSerializer {
		
		var nodesSerializer:IXMLListSerializer;
		var waysSerializer:IXMLListSerializer;
		var relationsSerializer:IXMLListSerializer;
		
		public function new(nodesSerializer:IXMLListSerializer, waysSerializer:IXMLListSerializer, relationsSerializer:IXMLListSerializer) {
			this.nodesSerializer = nodesSerializer;
			this.waysSerializer = waysSerializer;
			this.relationsSerializer = relationsSerializer;
		}

		public function serialize(object:Dynamic):XML
		{
			var xml:XML = <osm version="0.6" generator="Mapzen"></osm>;
			
			var nodesXml:XMLList = nodesSerializer.serialize(object.nodes);
			var waysXml:XMLList = waysSerializer.serialize(object.ways);
			var relationsXml:XMLList = relationsSerializer.serialize(object.relations);
			
			xml.appendChild(nodesXml);
			xml.appendChild(waysXml);
			xml.appendChild(relationsXml);
			
			return xml;
		}
		
	}
