package com.cloudmade.mappingtool.business.externalization.osm;

	import com.cloudmade.mappingtool.business.externalization.IXMLDeserializer;
	import com.cloudmade.mappingtool.business.externalization.IXMLListExternalizer;
	import com.cloudmade.mappingtool.business.externalization.IXMLSerializer;
	
	class ListExternalizer implements IXMLListExternalizer {
		
		var objectSerializer:IXMLSerializer;
		var objectDeserializer:IXMLDeserializer;
		
		public function new(objectSerializer:IXMLSerializer, objectDeserializer:IXMLDeserializer) {
			this.objectSerializer = objectSerializer;
			this.objectDeserializer = objectDeserializer;
		}
		
		public function serialize(objects:Array<Dynamic>):XMLList {
			var xml:XML = Xml.parse("<objects></objects>");
			
			for (object in objects) {
				xml.appendChild(objectSerializer.serialize(object));
			}
			
			return xml.children();
		}
		
		public function deserialize(xmlList:XMLList):Array<Dynamic> {
			var list:Array<Dynamic> = new Array();
			
			for (xml in xmlList) {
				list.push(objectDeserializer.deserialize(xml));
			}
			
			return list;
		}
	}
