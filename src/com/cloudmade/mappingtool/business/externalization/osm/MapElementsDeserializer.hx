package com.cloudmade.mappingtool.business.externalization.osm;

	import com.cloudmade.mappingtool.business.externalization.IXMLDeserializer;
	import com.cloudmade.mappingtool.business.externalization.IXMLListDeserializer;
	import com.cloudmade.mappingtool.config.IHashMap;
	import com.cloudmade.mappingtool.map.model.MapElements;
	import com.cloudmade.mappingtool.map.model.Node;

	class MapElementsDeserializer implements IXMLDeserializer {
		
		var nodesDeserializer:IXMLListDeserializer;
		var waysDeserializer:IXMLListDeserializer;
		var relationsDeserializer:IXMLListDeserializer;
		var nodeMap:IHashMap;
		
		public function new(nodesDeserializer:IXMLListDeserializer,  
				waysDeserializer:IXMLListDeserializer, relationsDeserializer:IXMLListDeserializer, nodeMap:IHashMap) {
			this.nodesDeserializer = nodesDeserializer;
			this.waysDeserializer = waysDeserializer;
			this.relationsDeserializer = relationsDeserializer;
			this.nodeMap = nodeMap;
		}

		public function deserialize(xml:XML):Dynamic
		{
			var nodes:Array<Dynamic> = nodesDeserializer.deserialize(xml.node);
			
			// nodeMap needs to be populated before deserializing ways because 
			// it is used in waysDeserializer (through nodeReferenceDeserializer)
			for (node in nodes) {
				nodeMap.setData(node.id, node);
			}
			
			var ways:Array<Dynamic> = waysDeserializer.deserialize(xml.way);
			var relations:Array<Dynamic> = relationsDeserializer.deserialize(xml.relation);
			
			return new MapElements(nodes, ways, relations);
		}
		
	}
