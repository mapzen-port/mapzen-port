package com.cloudmade.mappingtool.business.externalization.osm;

	import com.cloudmade.mappingtool.business.externalization.IXMLDeserializer;
	import com.cloudmade.mappingtool.business.externalization.IXMLListDeserializer;
	import com.cloudmade.mappingtool.map.model.Way;

	class WayDeserializer implements IXMLDeserializer {
		
		var tagsDeserializer:IXMLListDeserializer;
		var nodeReferencesDeserializer:IXMLListDeserializer;
		
		public function new(tagsDeserializer:IXMLListDeserializer, nodeReferencesDeserializer:IXMLListDeserializer) {
			this.tagsDeserializer = tagsDeserializer;
			this.nodeReferencesDeserializer = nodeReferencesDeserializer;
		}

		public function deserialize(xml:XML):Dynamic
		{
			var tags:Array<Dynamic> = tagsDeserializer.deserialize(xml.tag);
			var nodes:Array<Dynamic> = nodeReferencesDeserializer.deserialize(xml.nd);
			
			var way:Way = new Way(xml.@id, tags, nodes);
			way.version = xml.@version;
			
			return way;
		}
		
	}
