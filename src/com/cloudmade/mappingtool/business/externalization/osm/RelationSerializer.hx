package com.cloudmade.mappingtool.business.externalization.osm;

	import com.cloudmade.mappingtool.business.externalization.IXMLListSerializer;
	import com.cloudmade.mappingtool.business.externalization.IXMLSerializer;

	class RelationSerializer implements IXMLSerializer {
		
		var elementSerializer:IXMLSerializer;
		var membersSerializer:IXMLListSerializer;
		
		public function new(elementSerializer:IXMLSerializer, membersSerializer:IXMLListSerializer) {
			this.elementSerializer = elementSerializer;
			this.membersSerializer = membersSerializer;
		}

		public function serialize(relation:Dynamic):XML {
			var xml:XML = elementSerializer.serialize(relation);
			xml.setName("relation");
				
			var members:Array<Dynamic> = relation.members.source;
			xml.prependChild(membersSerializer.serialize(members));
			
			return xml;
		}
	}
