package com.cloudmade.mappingtool.business.externalization.osm;

	import com.cloudmade.mappingtool.business.externalization.IXMLDeserializer;
	import com.cloudmade.mappingtool.business.externalization.IXMLListDeserializer;
	import com.cloudmade.mappingtool.config.HashMap;
	import com.cloudmade.mappingtool.config.IHashMap;
	import com.cloudmade.mappingtool.config.IReadOnlyHashMap;
	
	class MapElementsDeserializerFactory
	 {
		
		public function new() {
		}
		
		public function create():IXMLDeserializer {
			var nodeMap:IHashMap = new HashMap();
			var waysDeserializer:IXMLListDeserializer = createWaysDeserializer(nodeMap);
			var nodesDeserializer:IXMLListDeserializer = createNodesDeserializer();
			var relationsDeserializer:IXMLListDeserializer = createRelationsDeserializer();
			return new MapElementsDeserializer(nodesDeserializer, waysDeserializer, relationsDeserializer, nodeMap);
		}
		
		function createWaysDeserializer(nodeMap:IReadOnlyHashMap):IXMLListDeserializer {
			var tagsDeserialzer:IXMLListDeserializer = createTagsDeserializer();
			var nodeReferencesDeserializer:IXMLListDeserializer = createNodeReferencesDeserializer(nodeMap);
			return createListDeserializer(new WayDeserializer(tagsDeserialzer, nodeReferencesDeserializer));
		}
		
		function createNodesDeserializer():IXMLListDeserializer {
			var tagsDeserializer:IXMLListDeserializer = createTagsDeserializer();
			return createListDeserializer(new NodeDeserializer(tagsDeserializer));
		}
		
		function createRelationsDeserializer():IXMLListDeserializer {
			var tagsDeserializer:IXMLListDeserializer = createTagsDeserializer();
			var membersDeserializer:IXMLListDeserializer = createMembersDeserializer();
			return createListDeserializer(new RelationDeserializer(tagsDeserializer, membersDeserializer));
		}
		
		function createTagsDeserializer():IXMLListDeserializer {
			return createListDeserializer(new TagExternalizer());
		}
		
		function createNodeReferencesDeserializer(nodeMap:IReadOnlyHashMap):IXMLListDeserializer {
			return createListDeserializer(new NodeReferenceDeserializer(nodeMap));
		}
		
		function createMembersDeserializer():IXMLListDeserializer {
			return createListDeserializer(new MemberExternalizer());
		}
		
		function createListDeserializer(deserializer:IXMLDeserializer):IXMLListDeserializer {
			return new ListExternalizer(null, deserializer);
		}
	}
