package com.cloudmade.mappingtool.business.externalization.osm;

	import com.cloudmade.mappingtool.business.externalization.IXMLListSerializer;
	import com.cloudmade.mappingtool.business.externalization.IXMLSerializer;
	import com.cloudmade.mappingtool.map.model.Tag;

	class ElementSerializer implements IXMLSerializer {
		
		var tagsSerializer:IXMLListSerializer;
		
		public function new(tagsSerializer:IXMLListSerializer) {
			this.tagsSerializer = tagsSerializer;
		}

		public function serialize(element:Dynamic):XML
		{
			var xml:XML = Xml.parse("<element></element>");
			xml.@id = element.id;
			xml.@version = element.version;
			
			var tags:Array<Dynamic> = element.tags.source.filter(tagNotEmpty);
			xml.appendChild(tagsSerializer.serialize(tags));
				
			return xml;
		}
		
		function tagNotEmpty(item:Tag, index:Int, array:Array<Dynamic>):Bool {
			return !item.isEmpty;
		}
	}
