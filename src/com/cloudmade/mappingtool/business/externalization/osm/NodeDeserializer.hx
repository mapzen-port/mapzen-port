package com.cloudmade.mappingtool.business.externalization.osm;

	import com.cloudmade.mappingtool.business.externalization.IXMLListDeserializer;
	import com.cloudmade.mappingtool.map.geo.LatLong;
	import com.cloudmade.mappingtool.map.model.Node;
	import com.cloudmade.mappingtool.business.externalization.IXMLDeserializer;

	class NodeDeserializer implements IXMLDeserializer {
		
		var tagsDeserializer:IXMLListDeserializer;
		
		public function new(tagsDeserializer:IXMLListDeserializer) {
			this.tagsDeserializer = tagsDeserializer;
		}

		public function deserialize(xml:XML):Dynamic {
			var tags:Array<Dynamic> = tagsDeserializer.deserialize(xml.tag);
			
			var position:LatLong = new LatLong(xml.@lat, xml.@lon);
			var node:Node = new Node(xml.@id, tags, position);
			node.version = xml.@version;
			
			return node;
		}
	}
