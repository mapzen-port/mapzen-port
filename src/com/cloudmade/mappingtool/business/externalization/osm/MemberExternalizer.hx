package com.cloudmade.mappingtool.business.externalization.osm;

	import com.cloudmade.mappingtool.business.externalization.IXMLExternalizer;
	import com.cloudmade.mappingtool.map.model.Member;

	class MemberExternalizer implements IXMLExternalizer {
		
		public function new()
		{
		}

		public function serialize(object:Dynamic):XML {
			var xml:XML = Xml.parse("<member/>");
			xml.@type = object.type;
			xml.@ref = object.elementId;
			xml.@role = object.role;
			return xml;
		}
		
		public function deserialize(xml:XML):Dynamic {
			return new Member(xml.@ref, xml.@type, xml.@role);
		}
		
	}
