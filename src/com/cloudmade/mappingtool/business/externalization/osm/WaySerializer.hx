package com.cloudmade.mappingtool.business.externalization.osm;

	import com.cloudmade.mappingtool.business.externalization.IXMLListSerializer;
	import com.cloudmade.mappingtool.business.externalization.IXMLSerializer;

	class WaySerializer implements IXMLSerializer {
		
		var elementSerializer:IXMLSerializer;
		var nodeReferencesSerializer:IXMLListSerializer;
		
		public function new(elementSerializer:IXMLSerializer, nodeReferencesSerializer:IXMLListSerializer) {
			this.elementSerializer = elementSerializer;
			this.nodeReferencesSerializer = nodeReferencesSerializer;
		}

		public function serialize(way:Dynamic):XML
		{
			var xml:XML = elementSerializer.serialize(way);
			xml.setName("way");
			
			var nodes:Array<Dynamic> = way.nodes.source;
			xml.prependChild(nodeReferencesSerializer.serialize(nodes));
			
			return xml;
		}
		
	}
