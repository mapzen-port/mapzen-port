package com.cloudmade.mappingtool.business.externalization.osm;

	import com.cloudmade.mappingtool.business.externalization.IXMLSerializer;

	class NodeSerializer implements IXMLSerializer {
		
		var elementSerializer:IXMLSerializer;
		
		public function new(elementSerializer:IXMLSerializer) {
			this.elementSerializer = elementSerializer;
		}

		public function serialize(node:Dynamic):XML
		{
			var xml:XML = elementSerializer.serialize(node);
			xml.setName("node");
			xml.@lat = node.position.lat;
			xml.@lon = node.position.long;
			
			return xml;
		}
	}
