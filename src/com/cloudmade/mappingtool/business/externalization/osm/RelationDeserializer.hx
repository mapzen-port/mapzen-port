package com.cloudmade.mappingtool.business.externalization.osm;

	import com.cloudmade.mappingtool.business.externalization.IXMLDeserializer;
	import com.cloudmade.mappingtool.business.externalization.IXMLListDeserializer;
	import com.cloudmade.mappingtool.map.model.Element;
	import com.cloudmade.mappingtool.map.model.Relation;

	class RelationDeserializer implements IXMLDeserializer {
		
		var tagsDeserializer:IXMLListDeserializer;
		var membersDeserializer:IXMLListDeserializer;
		
		public function new(tagsDeserializer:IXMLListDeserializer, membersDeserializer:IXMLListDeserializer) {
			this.tagsDeserializer = tagsDeserializer;
			this.membersDeserializer = membersDeserializer;
		}

		public function deserialize(xml:XML):Dynamic
		{
			var members:Array<Dynamic> = membersDeserializer.deserialize(xml.member);
	  		
	  		var tags:Array<Dynamic> = tagsDeserializer.deserialize(xml.tag);
	  		
	  		var relation:Relation = new Relation(xml.@id, tags, members);
	  		relation.version = xml.@version;
	  		
	  		return relation;
		}
	}
