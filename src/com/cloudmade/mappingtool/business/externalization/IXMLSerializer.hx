package com.cloudmade.mappingtool.business.externalization;

	interface IXMLSerializer
	{
		function serialize(object:Dynamic):XML;
	}
