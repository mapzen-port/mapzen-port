package com.cloudmade.mappingtool.business;

	import flash.utils.IDataOutput;
	import flash.utils.IDataInput;
	import flash.utils.IExternalizable;
	
	/*[RemoteClass(alias="com.cloudmade.mappingtool.business.ExternalizableDelegateGroup")]*/

	class ExternalizableDelegateGroup extends DelegateGroup, implements IExternalizable {
		
		public function new(?delegates:Array<Dynamic> = null) {
			super(delegates);
		}
		
		public function readExternal(input:IDataInput):Void {
			setDelegates(input.readObject());
		}
		
		public function writeExternal(output:IDataOutput):Void {
			output.writeObject(delegates);
		}
	}
