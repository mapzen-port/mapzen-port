package com.cloudmade.mappingtool.business;

	/*[RemoteClass(alias="com.cloudmade.mappingtool.business.GetNodeWaysNumberDelegate")]*/
	
	class GetNodeWaysNumberDelegate extends ExternalizableDelegate {
		
		public function new(?url:String = null, ?nodeId:String = null) {
			url += "node/" + nodeId + "/ways";
			super(url);
		}
		
		override function createResult(response:String):Dynamic {
			var xml:XML = new XML(response);
			var result:Int = xml.way.length();
			return result;
		}
	}
