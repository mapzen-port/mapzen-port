package com.cloudmade.mappingtool.business;

	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	
	/*[Event(name="complete", type="flash.events.Event")]*/
	/*[Event(name="ioError", type="flash.events.IOErrorEvent")]*/

	class DelegateGroup extends EventDispatcher, implements IDelegateGroup {
		
		public var delegates(getDelegates, null) : Array<Dynamic> ;
		public var result(getResult, null) : Dynamic ;
		var completeNumber:Int;
		
		var initialized:Bool ;
		
		public function new(delegates:Array<Dynamic>) {
			
			initialized = false;
			super();
			setDelegates(delegates);
		}
		
		var _result:Dynamic;
		public function getResult():Dynamic {
			return _result;
		}
		
		var _delegates:Array<Dynamic>;
		public function getDelegates():Array<Dynamic> {
			return _delegates.slice();
		}
		
		public function send():Void {
			initialize();
			setResult(null);
			completeNumber = 0;
		}
		
		function initialize():Void {
			if (initialized) return;
			for (delegate in delegates) {
				delegate.addEventListener(Event.COMPLETE, delegate_completeHandler);
				delegate.addEventListener(IOErrorEvent.IO_ERROR, delegate_ioErrorHandler);
			}
			initialized = true;
		}
		
		function setResult(value:Dynamic):Void {
			_result = value;
		}
		
		function setDelegates(delegates:Array<Dynamic>):Void {
			_delegates = delegates;
		}
		
		function delegate_completeHandler(event:Event):Void {
			completeNumber++;
			if (completeNumber == delegates.length) {
				setResult(true);
				dispatchEvent(event);
			}
		}
		
		function delegate_ioErrorHandler(event:IOErrorEvent):Void {
			setResult(false);
			dispatchEvent(event);
		}
	}
