package com.cloudmade.mappingtool.business;

	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.utils.IDataInput;
	import flash.utils.IDataOutput;
	import flash.utils.IExternalizable;
	
	/*[Event(name="complete", type="flash.events.Event")]*/
	/*[Event(name="ioError", type="flash.events.IOErrorEvent")]*/
	
	/*[RemoteClass(alias="com.cloudmade.mappingtool.business.GetNodeParentsNumberDelegate")]*/
	
	class GetNodeParentsNumberDelegate extends EventDispatcher, implements IDelegate, implements IExternalizable {
		
		public var result(getResult, null) : Dynamic ;
		var delegateGroup:IDelegateGroup;
		
		public function new(?delegateGroup:IDelegateGroup = null) {
			super();
			setDelegateGroup(delegateGroup);
		}
		
		var _result:Dynamic;
		public function getResult():Dynamic {
			return _result;
		}
		
		public function send():Void {
			delegateGroup.send();
			_result = null;
		}
		
		public function readExternal(input:IDataInput):Void {
			setDelegateGroup(input.readObject());
		}
		
		public function writeExternal(output:IDataOutput):Void {
			output.writeObject(delegateGroup);
		}
		
		function setDelegateGroup(delegateGroup:IDelegateGroup):Void {
			this.delegateGroup = delegateGroup;
			if (delegateGroup != null) {
				delegateGroup.addEventListener(Event.COMPLETE, delegateGroup_completeHandler);
				delegateGroup.addEventListener(IOErrorEvent.IO_ERROR, delegateGroup_ioErrorHandler);
			}
		}
		
		function delegateGroup_completeHandler(event:Event):Void {
			_result = 0;
			var delegates:Array<Dynamic> = delegateGroup.delegates;
			for (delegate in delegates) {
				_result += delegate.result;
			}
			dispatchEvent(event);
		}
		
		function delegateGroup_ioErrorHandler(event:IOErrorEvent):Void {
			dispatchEvent(event);
		}
	}
