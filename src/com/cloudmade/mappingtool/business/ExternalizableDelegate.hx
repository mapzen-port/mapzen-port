package com.cloudmade.mappingtool.business;

	import flash.utils.IDataInput;
	import flash.utils.IDataOutput;
	import flash.utils.IExternalizable;
	
	/*[RemoteClass(alias="com.cloudmade.mappingtool.business.ExternalizableDelegate")]*/
	
	class ExternalizableDelegate extends Delegate, implements IExternalizable {
		
		public function new(?url:String = null) {
			super(url);
		}
		
		public function readExternal(input:IDataInput):Void {
			url = input.readUTF();
		}
		
		public function writeExternal(output:IDataOutput):Void {
			output.writeUTF(url);
		}
	}
