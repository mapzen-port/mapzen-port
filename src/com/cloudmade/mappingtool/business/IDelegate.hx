package com.cloudmade.mappingtool.business;

	import flash.events.Event;
	import flash.events.IEventDispatcher;
	import flash.events.IOErrorEvent;
	
	/*[Event(name="complete", type="flash.events.Event")]*/
	/*[Event(name="ioError", type="flash.events.IOErrorEvent")]*/
	
	interface IDelegate implements IEventDispatcher{
		function result():Dynamic;
		
		function send():Void;
	}
