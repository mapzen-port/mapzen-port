package com.cloudmade.mappingtool.config;

	class RendererFindersPackage implements IRendererFindersPackage {
		
		public var areaFinder(getAreaFinder, null) : IElementTypeFinder ;
		public var nodeFinder(getNodeFinder, null) : IElementTypeFinder ;
		public var wayFinder(getWayFinder, null) : IElementTypeFinder ;
		public function new(nodeFinder:IElementTypeFinder, wayFinder:IElementTypeFinder, areaFinder:IElementTypeFinder) {
			_nodeFinder = nodeFinder;
			_wayFinder = wayFinder;
			_areaFinder = areaFinder;
		}
		
		var _nodeFinder:IElementTypeFinder;
		public function getNodeFinder():IElementTypeFinder {
			return _nodeFinder;
		}
		
		var _wayFinder:IElementTypeFinder;
		public function getWayFinder():IElementTypeFinder {
			return _wayFinder;
		}
		
		var _areaFinder:IElementTypeFinder;
		public function getAreaFinder():IElementTypeFinder {
			return _areaFinder;
		}
	}
