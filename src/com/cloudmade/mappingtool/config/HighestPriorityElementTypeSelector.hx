package com.cloudmade.mappingtool.config;

	class HighestPriorityElementTypeSelector implements IElementTypeSelector {
		
		public function new() {
		}
		
		public function select(types:Array<Dynamic>):IElementType {
			types.sortOn("priority", Array.NUMERIC | Array.DESCENDING);
			return types[0];
		}
	}
