package com.cloudmade.mappingtool.config.conditions;

	import com.cloudmade.mappingtool.config.parsers.IXMLListParser;
	import com.cloudmade.mappingtool.config.parsers.IXMLParser;
	import com.cloudmade.mappingtool.map.model.ITag;

	class ConditionsParser implements IXMLListParser {
		
		var conditionParser:IXMLParser;
		var conditionConstructor:IConditionConstructor;
		
		public function new(conditionParser:IXMLParser, conditionConstructor:IConditionConstructor) {
			this.conditionParser = conditionParser;
			this.conditionConstructor = conditionConstructor;
		}

		public function parse(xml:XMLList):Array<Dynamic>
		{
			var conditions:Array<Dynamic> = new Array();
			
			for (conditionXml in xml) {
				var parameters:Dynamic;
				
				if (conditionXml.children().length() > 0) {
					parameters = parse(conditionXml.children());
				} else {
					parameters = conditionParser.parse(conditionXml);
				}
				
				var condition:ICondition = conditionConstructor.create(conditionXml.name(), parameters);
				conditions.push(condition);
			}
			
			return conditions; 
		}
		
	}
