package com.cloudmade.mappingtool.config.conditions;

	interface IConditionConstructor
	{
		function create(type:String, parameters:Dynamic):ICondition;
	}
