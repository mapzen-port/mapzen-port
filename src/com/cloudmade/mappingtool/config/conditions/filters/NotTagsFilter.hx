package com.cloudmade.mappingtool.config.conditions.filters;

	class NotTagsFilter implements ITagsFilter {
		
		var filter:ITagsFilter;
		
		public function new(filter:ITagsFilter) {
			this.filter = filter;
		}

		public function filterTags(tags:Array<Dynamic>):Array<Dynamic>
		{
			var filteredTags:Array<Dynamic> = filter.filterTags(tags);
			return filteredTags == null ? new Array() : null;
		}
		
	}
