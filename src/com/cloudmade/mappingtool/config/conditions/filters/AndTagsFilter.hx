package com.cloudmade.mappingtool.config.conditions.filters;

	class AndTagsFilter implements ITagsFilter {
		
		var filters:Array<Dynamic>;
		
		public function new(filters:Array<Dynamic>) {
			this.filters = filters;
		}

		public function filterTags(tags:Array<Dynamic>):Array<Dynamic>
		{
			var filteredTags:Array<Dynamic> = new Array();
			var filtrationSuccessful:Bool = true;
			
			for (filter in filters) {
				var filteredTagsPart:Array<Dynamic> = filter.filterTags(tags);
				if (filteredTagsPart == null) {
					filtrationSuccessful = false;
					break;
				}
				
				filteredTags = filteredTags.concat(filteredTagsPart);
			}
			
			return filtrationSuccessful ? filteredTags : null;
		}
	}
