package com.cloudmade.mappingtool.config.conditions.filters;

	import com.cloudmade.mappingtool.map.model.ITag;
	
	class IsTagsFilter implements ITagsFilter {
		
		var filterTag:ITag;
		
		public function new(filterTag:ITag) {
			this.filterTag = filterTag;
		}

		public function filterTags(tags:Array<Dynamic>):Array<Dynamic>
		{
			var filteredTags:Array<Dynamic> = new Array();
			var filtrationSuccessful:Bool = false;
			
			for (tag in tags) {
				if (filterTag.equals(tag)) {
					filtrationSuccessful = true;
					filteredTags.push(tag);
				}
			}
			
			return filtrationSuccessful ? filteredTags : null;
		}  
		
	}
