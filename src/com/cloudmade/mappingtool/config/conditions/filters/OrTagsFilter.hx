package com.cloudmade.mappingtool.config.conditions.filters;

	class OrTagsFilter implements ITagsFilter {
		
		var filters:Array<Dynamic>;
		
		public function new(filters:Array<Dynamic>) {
			this.filters = filters;
		}

		public function filterTags(tags:Array<Dynamic>):Array<Dynamic>
		{
			var filteredTags:Array<Dynamic> = new Array();
			var filtrationSuccessful:Bool = (filters.length == 0);
			
			for (filter in filters) {
				var filteredTagsPart:Array<Dynamic> = filter.filterTags(tags);
				if (filteredTagsPart == null) continue;
				
				filtrationSuccessful = true;
				filteredTags = filteredTags.concat(filteredTagsPart);
			}
			
			return filtrationSuccessful ? filteredTags : null;
		}
		
	}
