package com.cloudmade.mappingtool.config.conditions.filters;

	import com.cloudmade.mappingtool.config.conditions.ICondition;
	
	interface ITagsFilter implements ICondition{
		function filterTags(tags:Array<Dynamic>):Array<Dynamic>;
	}
