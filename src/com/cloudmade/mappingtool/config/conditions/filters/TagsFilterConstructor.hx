package com.cloudmade.mappingtool.config.conditions.filters;

	import com.cloudmade.mappingtool.config.conditions.ConditionType;
	import com.cloudmade.mappingtool.config.conditions.ICondition;
	import com.cloudmade.mappingtool.config.conditions.IConditionConstructor;
	import com.cloudmade.mappingtool.map.model.ITag;

	class TagsFilterConstructor implements IConditionConstructor {
		
		public function new() {
		}

		public function create(type:String, parameters:Dynamic):ICondition
		{
			switch (type)
			{
				case ConditionType.AND: return new AndTagsFilter(cast( parameters, Array));
				case ConditionType.OR : return new OrTagsFilter(cast( parameters, Array));
				case ConditionType.NOT: return new NotTagsFilter(new AndTagsFilter(cast( parameters, Array)));
				case ConditionType.IS : return new IsTagsFilter(cast( parameters, ITag));
				
				default: throw new Error("Unsupported Tags Filter");
			}
			
			return null;
		}
		
	}
