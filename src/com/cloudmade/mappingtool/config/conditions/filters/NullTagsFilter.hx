package com.cloudmade.mappingtool.config.conditions.filters;

	class NullTagsFilter implements ITagsFilter {
		
		public function new() {
		}
		
		public function filterTags(tags:Array<Dynamic>):Array<Dynamic> {
			return new Array();
		}
	}
