package com.cloudmade.mappingtool.config.conditions;

	import com.cloudmade.mappingtool.config.parsers.IXMLListParser;
	import com.cloudmade.mappingtool.config.parsers.IXMLParser;
	
	class RootConditionParser implements IXMLParser {
		
		var conditionsParser:IXMLListParser;
		var conditionConstructor:IConditionConstructor;
		var type:String;
		
		public function new(conditionsParser:IXMLListParser, conditionConstructor:IConditionConstructor, type:String)
		{
			this.conditionsParser = conditionsParser;
			this.conditionConstructor = conditionConstructor;
			this.type = type;
		}
		
		public function parse(xml:XML):Dynamic
		{
			var conditionsXml:XMLList = xml.children().(name() != "default");
			var conditions:Array<Dynamic> = conditionsParser.parse(conditionsXml);
			
			return conditionConstructor.create(type, conditions);
		}
		
	}
