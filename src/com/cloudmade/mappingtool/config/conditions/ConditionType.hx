package com.cloudmade.mappingtool.config.conditions;

	class ConditionType
	 {
		public function new() { }
		
		public static var AND:String = "and";
		public static var OR:String = "or";
		public static var NOT:String = "not";
		public static var IS:String = "tag";
	}
