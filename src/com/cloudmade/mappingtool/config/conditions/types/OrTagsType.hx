package com.cloudmade.mappingtool.config.conditions.types;

	class OrTagsType implements ITagsType {
		
		var tagsTypes:Array<Dynamic>;
		
		public function new(tagsTypes:Array<Dynamic>) {
			this.tagsTypes = tagsTypes;
		}

		public function correspondsTo(tags:Array<Dynamic>):Bool
		{
			var corresponds:Bool = (tagsTypes.length == 0);
			
			for (tagsType in tagsTypes) {
				if (tagsType.correspondsTo(tags)) {
					corresponds = true;
					break;
				}
			}
			
			return corresponds;
		}
		
	}
