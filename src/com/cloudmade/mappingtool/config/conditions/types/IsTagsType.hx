package com.cloudmade.mappingtool.config.conditions.types;

	import com.cloudmade.mappingtool.map.model.ITag;
	
	class IsTagsType implements ITagsType {
		
		var typeTag:ITag;
		
		public function new(typeTag:ITag) {
			this.typeTag = typeTag;
		}

		public function correspondsTo(tags:Array<Dynamic>):Bool {
			var corresponds:Bool = false;
			
			for (tag in tags) {
				if (typeTag.equals(tag)) {
					corresponds = true;
					break;
				}
			}
			
			return corresponds;
		}
		
	}
