package com.cloudmade.mappingtool.config.conditions.types;

	import com.cloudmade.mappingtool.config.conditions.ConditionType;
	import com.cloudmade.mappingtool.config.conditions.ICondition;
	import com.cloudmade.mappingtool.config.conditions.IConditionConstructor;
	import com.cloudmade.mappingtool.map.model.ITag;

	class TagsTypeConstructor implements IConditionConstructor {
		
		public function new() {
		}

		public function create(type:String, parameters:Dynamic):ICondition
		{
			switch (type)
			{
				case ConditionType.AND: return new AndTagsType(cast( parameters, Array));
				case ConditionType.OR : return new OrTagsType(cast( parameters, Array));
				case ConditionType.NOT: return new NotTagsType(new AndTagsType(cast( parameters, Array)));
				case ConditionType.IS : return new IsTagsType(cast( parameters, ITag));
				
				default: throw new Error("Unsupported Tags Type");
			}
			
			return null;
		}
		
	}
