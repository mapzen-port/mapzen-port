package com.cloudmade.mappingtool.config.conditions.types;

	import com.cloudmade.mappingtool.config.conditions.ICondition;
	
	interface ITagsType implements ICondition{
		function correspondsTo(tags:Array<Dynamic>):Bool;
	}
