package com.cloudmade.mappingtool.config.conditions.types;

	import com.cloudmade.mappingtool.map.model.ITag;
	
	class AndTagsType implements ITagsType {
		
		var tagsTypes:Array<Dynamic>;
		
		public function new(tagsTypes:Array<Dynamic>) {
			this.tagsTypes = tagsTypes;
		}

		public function correspondsTo(tags:Array<Dynamic>):Bool {
			var corresponds:Bool = true;
			
			for (tagsType in tagsTypes) {
				if (!tagsType.correspondsTo(tags)) {
					corresponds = false;
					break;
				}
			}
			
			return corresponds;
		}
		
	}
