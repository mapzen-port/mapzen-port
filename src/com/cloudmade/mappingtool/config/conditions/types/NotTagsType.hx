package com.cloudmade.mappingtool.config.conditions.types;

	class NotTagsType implements ITagsType {
		
		var tagsType:ITagsType;
		
		public function new(tagsType:ITagsType) {
			this.tagsType = tagsType;
		}

		public function correspondsTo(tags:Array<Dynamic>):Bool {
			return !tagsType.correspondsTo(tags);
		}
	}
