package com.cloudmade.mappingtool.config;

	class PresetCategory implements IPresetCategory {
		
		public var name(getName, null) : String ;
		public var presets(getPresets, null) : Array<Dynamic> ;
		public function new(name:String, presets:Array<Dynamic>) {
			_name = name;
			_presets = presets;
		}
		
		var _presets:Array<Dynamic>;
		public function getPresets():Array<Dynamic> {
			return _presets;
		}
		
		var _name:String;
		public function getName():String {
			return _name;
		}
	}
