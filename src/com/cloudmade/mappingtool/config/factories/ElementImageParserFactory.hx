package com.cloudmade.mappingtool.config.factories;

	import com.cloudmade.mappingtool.config.HighestPriorityElementTypeSelector;
	import com.cloudmade.mappingtool.config.IElementTypeSelector;
	import com.cloudmade.mappingtool.config.IReadOnlyHashMap;
	import com.cloudmade.mappingtool.config.conditions.ConditionType;
	import com.cloudmade.mappingtool.config.conditions.IConditionConstructor;
	import com.cloudmade.mappingtool.config.conditions.types.TagsTypeConstructor;
	import com.cloudmade.mappingtool.config.parsers.ClassFactoryProvider;
	import com.cloudmade.mappingtool.config.parsers.ClassLocalizer;
	import com.cloudmade.mappingtool.config.parsers.ElementImageParser;
	import com.cloudmade.mappingtool.config.parsers.IClassLocalizer;
	import com.cloudmade.mappingtool.config.parsers.IFactoryProvider;
	import com.cloudmade.mappingtool.config.parsers.IStyleDeclarationProvider;
	import com.cloudmade.mappingtool.config.parsers.IStylesProvider;
	import com.cloudmade.mappingtool.config.parsers.IStylizedFactoryProvider;
	import com.cloudmade.mappingtool.config.parsers.IXMLParser;
	import com.cloudmade.mappingtool.config.parsers.StylesProvider;
	import com.cloudmade.mappingtool.config.parsers.StylizedRendererFactoryProvider;
	
	import mx.resources.IResourceManager;
	
	class ElementImageParserFactory implements IXMLParserFactory {
		
		var styleDeclarationProvider:IStyleDeclarationProvider;
		var resourceManager:IResourceManager;
		var rendererBundleName:String;
		var typeMap:IReadOnlyHashMap;
		var layerMap:IReadOnlyHashMap;
				
		public function new(typeMap:IReadOnlyHashMap, layerMap:IReadOnlyHashMap, resourceManager:IResourceManager, rendererBundleName:String, styleDeclarationProvider:IStyleDeclarationProvider) {
			this.typeMap = typeMap;
			this.layerMap = layerMap;
			this.resourceManager = resourceManager;
			this.rendererBundleName = rendererBundleName;
			this.styleDeclarationProvider = styleDeclarationProvider;
		}
		
		public function create():IXMLParser {
			var rendererLocalizer:IClassLocalizer = new ClassLocalizer(resourceManager, rendererBundleName);
			
			var rendererFactoryProvider:IFactoryProvider = new ClassFactoryProvider(rendererLocalizer);
			var stylesProvider:IStylesProvider = new StylesProvider(styleDeclarationProvider);
			var stylizedFactoryProvider:IStylizedFactoryProvider = new StylizedRendererFactoryProvider(rendererFactoryProvider, stylesProvider);
			
			var typesMap:IReadOnlyHashMap = new TypesMap(typeMap, ",");
			var tagsTypeConstructor:IConditionConstructor = new TagsTypeConstructor();
			var priorityTypeSelector:IElementTypeSelector = new HighestPriorityElementTypeSelector();
			var aggregatedTypeMap:IReadOnlyHashMap = new AggregatedTypeMap(typesMap, tagsTypeConstructor, ConditionType.OR, priorityTypeSelector);
			
			return new ElementImageParser(aggregatedTypeMap, layerMap, stylizedFactoryProvider);
		}
	}
