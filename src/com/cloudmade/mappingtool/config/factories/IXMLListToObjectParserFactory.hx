package com.cloudmade.mappingtool.config.factories;

	import com.cloudmade.mappingtool.config.parsers.IXMLListToObjectParser;
	
	interface IXMLListToObjectParserFactory
	{
		function create():IXMLListToObjectParser;
	}
