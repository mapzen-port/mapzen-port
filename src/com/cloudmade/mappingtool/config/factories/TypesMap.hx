package com.cloudmade.mappingtool.config.factories;

	import com.cloudmade.mappingtool.config.IReadOnlyHashMap;
	
	class TypesMap implements IReadOnlyHashMap {
		
		var typeMap:IReadOnlyHashMap;
		var delimiter:String;
		
		public function new(typeMap:IReadOnlyHashMap, delimiter:String) {
			this.typeMap = typeMap;
			this.delimiter = delimiter;
		}
		
		public function getData(key:String):Dynamic {
			var types:Array<Dynamic> = new Array();
			var typeNames:Array<Dynamic> = key.split(delimiter);
			for (typeName in typeNames) {
				types.push(typeMap.getData(typeName));
			}
			return types;
		}
		
	}
