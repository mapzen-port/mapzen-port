package com.cloudmade.mappingtool.config.factories;

	import com.cloudmade.mappingtool.config.IStringLocalizer;
	import com.cloudmade.mappingtool.config.StringLocalizer;
	import com.cloudmade.mappingtool.config.parsers.IXMLListParser;
	import com.cloudmade.mappingtool.config.parsers.IXMLParser;
	import com.cloudmade.mappingtool.config.parsers.LocalizedPresetCategoryParser;
	import com.cloudmade.mappingtool.config.parsers.PresetCategoryParser;
	import com.cloudmade.mappingtool.config.parsers.XMLListParser;
	
	import mx.resources.IResourceManager;
	
	class PresetCategoriesParserFactory implements IXMLListParserFactory {
		
		var presetsParser:IXMLListParser;
		var resourceManager:IResourceManager;
		var categoryBundleName:String;
		
		public function new(presetsParser:IXMLListParser, resourceManager:IResourceManager, categoryBundleName:String) {
			this.presetsParser = presetsParser;
			this.resourceManager = resourceManager;
			this.categoryBundleName = categoryBundleName;
		}
		
		public function create():IXMLListParser {
			var categoryParser:IXMLParser = new PresetCategoryParser(presetsParser);
			
			var categoryLocalizer:IStringLocalizer = new StringLocalizer(resourceManager, categoryBundleName);
			var localizedCategoryParser:IXMLParser = new LocalizedPresetCategoryParser(categoryParser, categoryLocalizer);
			
			return new XMLListParser(localizedCategoryParser);
		}
	}
