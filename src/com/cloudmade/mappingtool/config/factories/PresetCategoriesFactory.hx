package com.cloudmade.mappingtool.config.factories;

	import com.cloudmade.mappingtool.config.IReadOnlyHashMap;
	import com.cloudmade.mappingtool.config.factories.resources.IPresetsResources;
	import com.cloudmade.mappingtool.config.parsers.IXMLListParser;
	import com.cloudmade.mappingtool.config.parsers.IXMLListToObjectParser;
	import com.cloudmade.mappingtool.config.parsers.IXMLParser;
	
	import mx.resources.IResourceManager;
	
	class PresetCategoriesFactory implements IPresetsFactory {
		
		var resourceManager:IResourceManager;
		
		public function new(resourceManager:IResourceManager) {
			this.resourceManager = resourceManager;
		}
		
		public function createPresets(resources:IPresetsResources):Array<Dynamic>
		{
			var defaultTypesTagsMapParserFactory:IXMLListToObjectParserFactory = new DefaultTypesTagsMapParserFactory();
			var defaultTypesTagsMapParser:IXMLListToObjectParser = defaultTypesTagsMapParserFactory.create();
			var defaultTypesTagsMap:IReadOnlyHashMap = cast( defaultTypesTagsMapParser.parse(resources.typesXml), IReadOnlyHashMap);
			
			var presetParserFactory:IXMLParserFactory = new ElementPresetParserFactory(defaultTypesTagsMap, resourceManager, resources.presetBundleName, resources.presetIconBundleName); 
			var presetParser:IXMLParser = presetParserFactory.create();
			
			var typeMapParserFactory:IXMLListToObjectParserFactory = new ElementTypeMapParserFactory();
			var typeMapParser:IXMLListToObjectParser = typeMapParserFactory.create();
			var typeMap:IReadOnlyHashMap = cast( typeMapParser.parse(resources.typesXml), IReadOnlyHashMap);
			
			var tagsFilterMapParserFactory:IXMLListToObjectParserFactory = new ElementTypeTagsFilterMapParserFactory();
			var tagsFilterMapParser:IXMLListToObjectParser = tagsFilterMapParserFactory.create();
			var tagsFilterMap:IReadOnlyHashMap = cast( tagsFilterMapParser.parse(resources.typesXml), IReadOnlyHashMap);
			
			var presetsParserFactory:IXMLListParserFactory = new ElementTypePresetsParserFactory(presetParser, typeMap, tagsFilterMap);
			var presetsParser:IXMLListParser = presetsParserFactory.create();
			
			var categoriesParserFactory:IXMLListParserFactory = new PresetCategoriesParserFactory(presetsParser, resourceManager, resources.categoryBundleName);
			var categoriesParser:IXMLListParser = categoriesParserFactory.create();
			return categoriesParser.parse(resources.presetsXml);
		}
	}
