package com.cloudmade.mappingtool.config.factories;

	import com.cloudmade.mappingtool.config.IReadOnlyHashMap;
	import com.cloudmade.mappingtool.config.IStringLocalizer;
	import com.cloudmade.mappingtool.config.StringLocalizer;
	import com.cloudmade.mappingtool.config.parsers.ClassFactoryProvider;
	import com.cloudmade.mappingtool.config.parsers.ClassLocalizer;
	import com.cloudmade.mappingtool.config.parsers.ElementPresetParser;
	import com.cloudmade.mappingtool.config.parsers.IClassLocalizer;
	import com.cloudmade.mappingtool.config.parsers.IFactoryProvider;
	import com.cloudmade.mappingtool.config.parsers.IXMLParser;
	import com.cloudmade.mappingtool.config.parsers.LocalizedElementPresetParser;
	
	import mx.resources.IResourceManager;
	
	class ElementPresetParserFactory implements IXMLParserFactory {
		
		var defaultTypesTagsMap:IReadOnlyHashMap;
		var resourceManager:IResourceManager;
		var presetBundleName:String;
		var iconBundleName:String;
		
		public function new(defaultTypesTagsMap:IReadOnlyHashMap, resourceManager:IResourceManager, presetBundleName:String, iconBundleName:String)
		{
			this.defaultTypesTagsMap = defaultTypesTagsMap;
			
			this.resourceManager = resourceManager;
			this.presetBundleName = presetBundleName;
			this.iconBundleName = iconBundleName;
		}
		
		public function create():IXMLParser {
			var iconLocalizer:IClassLocalizer = new ClassLocalizer(resourceManager, iconBundleName);
			var iconProvider:IFactoryProvider = new ClassFactoryProvider(iconLocalizer);

			var presetParser:IXMLParser = new ElementPresetParser(defaultTypesTagsMap, iconProvider);
			
			var presetLocalizer:IStringLocalizer = new StringLocalizer(resourceManager, presetBundleName);
			return new LocalizedElementPresetParser(presetParser, presetLocalizer);
		}

	}
