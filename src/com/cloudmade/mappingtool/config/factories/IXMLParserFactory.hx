package com.cloudmade.mappingtool.config.factories;

	import com.cloudmade.mappingtool.config.parsers.IXMLParser;
	
	interface IXMLParserFactory
	{
		function create():IXMLParser;
	}
