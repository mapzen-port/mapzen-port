package com.cloudmade.mappingtool.config.factories;

	import com.cloudmade.mappingtool.config.parsers.IXMLListParser;
	
	interface IXMLListParserFactory
	{
		function create():IXMLListParser;
	}
