package com.cloudmade.mappingtool.config.factories;

	import com.cloudmade.mappingtool.config.parsers.DefaultTypeTagsParser;
	import com.cloudmade.mappingtool.config.parsers.IXMLListParser;
	import com.cloudmade.mappingtool.config.parsers.IXMLListToObjectParser;
	import com.cloudmade.mappingtool.config.parsers.IXMLParser;
	import com.cloudmade.mappingtool.config.parsers.HashMapParser;
	import com.cloudmade.mappingtool.config.parsers.TagParser;
	import com.cloudmade.mappingtool.config.parsers.XMLListParser;
	
	class DefaultTypesTagsMapParserFactory implements IXMLListToObjectParserFactory {
		
		public function new() {
		}

		public function create():IXMLListToObjectParser {
			var tagParser:IXMLParser = new TagParser();
			var tagsParser:IXMLListParser = new XMLListParser(tagParser);

			var defaultTypeTagsParser:IXMLParser = new DefaultTypeTagsParser(tagsParser);
			return new HashMapParser("name", defaultTypeTagsParser);
		}
	}
