package com.cloudmade.mappingtool.config.factories;

	import com.cloudmade.mappingtool.config.ElementType;
	import com.cloudmade.mappingtool.config.IElementType;
	import com.cloudmade.mappingtool.config.IElementTypeSelector;
	import com.cloudmade.mappingtool.config.IReadOnlyHashMap;
	import com.cloudmade.mappingtool.config.conditions.IConditionConstructor;
	import com.cloudmade.mappingtool.config.conditions.types.ITagsType;

	class AggregatedTypeMap implements IReadOnlyHashMap {
		
		var typesMap:IReadOnlyHashMap;
		var tagsTypeConstructor:IConditionConstructor;
		var conditionType:String;
		var priorityTypeSelector:IElementTypeSelector;
		
		public function new(typesMap:IReadOnlyHashMap, tagsTypeContstructor:IConditionConstructor, conditionType:String, priorityTypeSelector:IElementTypeSelector) {
			this.typesMap = typesMap;
			this.tagsTypeConstructor = tagsTypeContstructor;
			this.conditionType = conditionType;
			this.priorityTypeSelector = priorityTypeSelector;
		}
		
		public function getData(key:String):Dynamic {
			var types:Array<Dynamic> = cast( typesMap.getData(key), Array);
			var tagsType:ITagsType = cast( tagsTypeConstructor.create(conditionType, types), ITagsType);
			var priorityType:IElementType = cast( priorityTypeSelector.select(types), IElementType);
			return new ElementType(tagsType, priorityType.priority);
		}
	}
