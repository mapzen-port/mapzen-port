package com.cloudmade.mappingtool.config.factories;

	import com.cloudmade.mappingtool.config.ElementTypeFinder;
	import com.cloudmade.mappingtool.config.HighestPriorityElementTypeSelector;
	import com.cloudmade.mappingtool.config.IElementTypeFinder;
	import com.cloudmade.mappingtool.config.IElementTypeSelector;
	import com.cloudmade.mappingtool.config.IReadOnlyHashMap;
	import com.cloudmade.mappingtool.config.factories.resources.IRenderersResources;
	import com.cloudmade.mappingtool.config.parsers.HashMapParser;
	import com.cloudmade.mappingtool.config.parsers.IStyleDeclarationProvider;
	import com.cloudmade.mappingtool.config.parsers.IXMLListParser;
	import com.cloudmade.mappingtool.config.parsers.IXMLListToObjectParser;
	import com.cloudmade.mappingtool.config.parsers.IXMLParser;
	import com.cloudmade.mappingtool.config.parsers.LayerParser;
	import com.cloudmade.mappingtool.config.parsers.StyleDeclarationProvider;
	import com.cloudmade.mappingtool.config.parsers.XMLListParser;
	
	import mx.resources.IResourceManager;

	class RendererFinderFactory implements IRendererFinderFactory {
		
		var resourceManager:IResourceManager;
		
		public function new(resourceManager:IResourceManager) {
			this.resourceManager = resourceManager;
		}
		
		public function createRendererFinder(resources:IRenderersResources):IElementTypeFinder
		{
			var typeMapParserFactory:IXMLListToObjectParserFactory = new ElementTypeMapParserFactory();
			var typeMapParser:IXMLListToObjectParser = typeMapParserFactory.create();
			var typeMap:IReadOnlyHashMap = cast( typeMapParser.parse(resources.typesXml), IReadOnlyHashMap);
			
			var layerParser:IXMLParser = new LayerParser();
			var layersParser:IXMLListToObjectParser = new HashMapParser("name", layerParser);
			var layerMap:IReadOnlyHashMap = cast( layersParser.parse(resources.layersXml), IReadOnlyHashMap);
			
			var styleDeclarationProvider:IStyleDeclarationProvider = new StyleDeclarationProvider();
			var elementImageParserFactory:ElementImageParserFactory = new ElementImageParserFactory(typeMap, layerMap, resourceManager, resources.rendererBundleName, styleDeclarationProvider);
			var elementImageParser:IXMLParser = elementImageParserFactory.create();
			var elementImagesParser:IXMLListParser = new XMLListParser(elementImageParser);
			
			var elementImages:Array<Dynamic> = elementImagesParser.parse(resources.renderersXml);
			var typeSelector:IElementTypeSelector = new HighestPriorityElementTypeSelector();
			var rendererFinder:IElementTypeFinder = new ElementTypeFinder(elementImages, typeSelector);
			return rendererFinder;
		}
	}
