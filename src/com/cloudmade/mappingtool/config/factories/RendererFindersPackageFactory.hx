package com.cloudmade.mappingtool.config.factories;

	import com.cloudmade.mappingtool.config.IElementTypeFinder;
	import com.cloudmade.mappingtool.config.IRendererFindersPackage;
	import com.cloudmade.mappingtool.config.RendererFindersPackage;
	import com.cloudmade.mappingtool.config.factories.resources.AreasResources;
	import com.cloudmade.mappingtool.config.factories.resources.IRenderersResources;
	import com.cloudmade.mappingtool.config.factories.resources.NodesResources;
	import com.cloudmade.mappingtool.config.factories.resources.WaysResources;
	
	class RendererFindersPackageFactory
	 {
		
		var rendererFinderFactory:IRendererFinderFactory;
		
		public function new(rendererFinderFactory:IRendererFinderFactory)
		{
			this.rendererFinderFactory = rendererFinderFactory;
		}
		
		public function create():IRendererFindersPackage {
			var nodeFinder:IElementTypeFinder = createFinder(new NodesResources());
			var wayFinder:IElementTypeFinder = createFinder(new WaysResources());
			var areaFinder:IElementTypeFinder = createFinder(new AreasResources());
			return new RendererFindersPackage(nodeFinder, wayFinder, areaFinder);
		}
		
		function createFinder(resources:IRenderersResources):IElementTypeFinder {
			return rendererFinderFactory.createRendererFinder(resources);
		}
	}
