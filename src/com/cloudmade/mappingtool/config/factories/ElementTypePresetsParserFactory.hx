package com.cloudmade.mappingtool.config.factories;

	import com.cloudmade.mappingtool.config.IReadOnlyHashMap;
	import com.cloudmade.mappingtool.config.parsers.ElementTypePresetParser;
	import com.cloudmade.mappingtool.config.parsers.IXMLListParser;
	import com.cloudmade.mappingtool.config.parsers.IXMLParser;
	import com.cloudmade.mappingtool.config.parsers.XMLListParser;
	
	class ElementTypePresetsParserFactory implements IXMLListParserFactory {
		
		var presetParser:IXMLParser;
		var typeMap:IReadOnlyHashMap;
		var tagsFilterMap:IReadOnlyHashMap;
		
		public function new(presetParser:IXMLParser, typeMap:IReadOnlyHashMap, tagsFilterMap:IReadOnlyHashMap) {
			this.presetParser = presetParser;
			this.typeMap = typeMap;
			this.tagsFilterMap = tagsFilterMap;
		}
		
		public function create():IXMLListParser {
			var typePresetParser:IXMLParser = new ElementTypePresetParser(presetParser, typeMap, tagsFilterMap);
			return new XMLListParser(typePresetParser);
		}
	}
