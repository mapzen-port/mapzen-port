package com.cloudmade.mappingtool.config.factories;

	import com.cloudmade.mappingtool.config.factories.resources.IPresetsResources;
	
	interface IPresetsFactory
	{
		function createPresets(resources:IPresetsResources):Array<Dynamic>;
	}
