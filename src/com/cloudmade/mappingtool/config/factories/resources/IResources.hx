package com.cloudmade.mappingtool.config.factories.resources;

	interface IResources implements IPresetsResources, implements IRenderersResources{
		function type():String;
	}
