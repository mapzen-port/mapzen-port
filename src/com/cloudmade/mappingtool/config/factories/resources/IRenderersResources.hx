package com.cloudmade.mappingtool.config.factories.resources;

	interface IRenderersResources implements ITypesResource{
		function layersXml():XMLList;
		
		function renderersXml():XMLList;
		
		function rendererBundleName():String;
	}
