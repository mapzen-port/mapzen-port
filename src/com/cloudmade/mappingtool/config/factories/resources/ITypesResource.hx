package com.cloudmade.mappingtool.config.factories.resources;

	interface ITypesResource
	{
		function typesXml():XMLList;
	}
