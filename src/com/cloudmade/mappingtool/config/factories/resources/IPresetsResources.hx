package com.cloudmade.mappingtool.config.factories.resources;

	interface IPresetsResources implements ITypesResource{
		function presetsXml():XMLList;
		
		function presetBundleName():String;
		
		function presetIconBundleName():String;
		
		function categoryBundleName():String;
	}
