package com.cloudmade.mappingtool.config.factories;

	import com.cloudmade.mappingtool.config.IElementTypeFinder;
	import com.cloudmade.mappingtool.config.factories.resources.IRenderersResources;
	
	interface IRendererFinderFactory
	{
		function createRendererFinder(resources:IRenderersResources):IElementTypeFinder;
	}
