package com.cloudmade.mappingtool.config.factories;

	import com.cloudmade.mappingtool.config.parsers.HashMapParser;
	import com.cloudmade.mappingtool.config.parsers.IXMLListParser;
	import com.cloudmade.mappingtool.config.parsers.IXMLListToObjectParser;
	import com.cloudmade.mappingtool.config.parsers.IXMLParser;
	import com.cloudmade.mappingtool.config.parsers.NullTagsFilterParser;
	import com.cloudmade.mappingtool.config.parsers.TypeMapParser;
	import com.cloudmade.mappingtool.config.parsers.TypeTagParser;
	import com.cloudmade.mappingtool.config.conditions.ConditionType;
	import com.cloudmade.mappingtool.config.conditions.ConditionsParser;
	import com.cloudmade.mappingtool.config.conditions.IConditionConstructor;
	import com.cloudmade.mappingtool.config.conditions.RootConditionParser;
	import com.cloudmade.mappingtool.config.conditions.filters.TagsFilterConstructor;
	
	class ElementTypeTagsFilterMapParserFactory implements IXMLListToObjectParserFactory {
		
		public function new() {
		}

		public function create():IXMLListToObjectParser {
			var typeTagParser:IXMLParser = new TypeTagParser();
			var tagsFilterConstructor:IConditionConstructor = new TagsFilterConstructor();
			var tagsFiltersParser:IXMLListParser = new ConditionsParser(typeTagParser, tagsFilterConstructor);
			
			var rootTagsFilterParser:IXMLParser = new RootConditionParser(tagsFiltersParser, tagsFilterConstructor, ConditionType.AND);
			var concreteTypeTagsFilterMapParser:IXMLListToObjectParser = new HashMapParser("name", rootTagsFilterParser);
			
			var nullTagsFilterParser:IXMLParser = new NullTagsFilterParser();
			return new TypeMapParser(concreteTypeTagsFilterMapParser, nullTagsFilterParser);
		}
	}
