package com.cloudmade.mappingtool.config.factories;

	import com.cloudmade.mappingtool.config.IPresetsPackage;
	import com.cloudmade.mappingtool.config.PresetsPackage;
	import com.cloudmade.mappingtool.config.factories.resources.AreasResources;
	import com.cloudmade.mappingtool.config.factories.resources.IPresetsResources;
	import com.cloudmade.mappingtool.config.factories.resources.NodesResources;
	import com.cloudmade.mappingtool.config.factories.resources.WaysResources;
	
	class PresetsPackageFactory
	 {
		
		var presetsFactory:IPresetsFactory;
		
		public function new(presetsFactory:IPresetsFactory) {
			this.presetsFactory = presetsFactory;
		}
		
		public function create():IPresetsPackage {
			var nodePresets:Array<Dynamic> = createPresets(new NodesResources());
			var wayPresets:Array<Dynamic> = createPresets(new WaysResources());
			var areaPresets:Array<Dynamic> = createPresets(new AreasResources());
			return new PresetsPackage(nodePresets, wayPresets, areaPresets);
		}
		
		function createPresets(resources:IPresetsResources):Array<Dynamic> {
			return presetsFactory.createPresets(resources);
		}
	}
