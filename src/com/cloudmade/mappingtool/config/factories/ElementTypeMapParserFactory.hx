package com.cloudmade.mappingtool.config.factories;

	import com.cloudmade.mappingtool.config.parsers.AnyElementTypeParser;
	import com.cloudmade.mappingtool.config.parsers.ElementTypeParser;
	import com.cloudmade.mappingtool.config.parsers.HashMapParser;
	import com.cloudmade.mappingtool.config.parsers.IXMLListParser;
	import com.cloudmade.mappingtool.config.parsers.IXMLListToObjectParser;
	import com.cloudmade.mappingtool.config.parsers.IXMLParser;
	import com.cloudmade.mappingtool.config.parsers.TypeMapParser;
	import com.cloudmade.mappingtool.config.parsers.TypeTagParser;
	import com.cloudmade.mappingtool.config.conditions.ConditionType;
	import com.cloudmade.mappingtool.config.conditions.ConditionsParser;
	import com.cloudmade.mappingtool.config.conditions.IConditionConstructor;
	import com.cloudmade.mappingtool.config.conditions.RootConditionParser;
	import com.cloudmade.mappingtool.config.conditions.types.TagsTypeConstructor;
	
	class ElementTypeMapParserFactory implements IXMLListToObjectParserFactory {
		
		public function new() {
		}

		public function create():IXMLListToObjectParser {
			var typeTagParser:IXMLParser = new TypeTagParser();
			var tagsTypeConstructor:IConditionConstructor = new TagsTypeConstructor();
			var tagsTypesParser:IXMLListParser = new ConditionsParser(typeTagParser, tagsTypeConstructor);
			
			var rootTagsTypeParser:IXMLParser = new RootConditionParser(tagsTypesParser, tagsTypeConstructor, ConditionType.AND);
			var elementTypeParser:IXMLParser = new ElementTypeParser(rootTagsTypeParser);
			var concreteTypeMapParser:IXMLListToObjectParser = new HashMapParser("name", elementTypeParser);
			
			var anyTypeParser:IXMLParser = new AnyElementTypeParser();
			return new TypeMapParser(concreteTypeMapParser, anyTypeParser);
		}
	}
