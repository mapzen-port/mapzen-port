package com.cloudmade.mappingtool.config;

	class LocalizedElementPreset extends ElementPresetDecorator {
		
		public var name(getName, null) : String ;
		var localizer:IStringLocalizer;
		
		public function new(preset:IElementPreset, localizer:IStringLocalizer) {
			super(preset);
			this.localizer = localizer;
		}
		
		public override function getName():String {
			return localizer.getString(preset.name);
		}
	}
