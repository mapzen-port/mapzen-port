package com.cloudmade.mappingtool.config;

	interface IElementTypeSelector
	{
		function select(types:Array<Dynamic>):IElementType;
	}
