package com.cloudmade.mappingtool.config;

	interface IHashMap implements IReadOnlyHashMap{
		function setData(key:String, value:Dynamic):Void;
	}
