package com.cloudmade.mappingtool.config;

	class PresetsPackage implements IPresetsPackage {
		
		public var areas(getAreas, null) : Array<Dynamic> ;
		public var nodes(getNodes, null) : Array<Dynamic> ;
		public var ways(getWays, null) : Array<Dynamic> ;
		public function new(nodes:Array<Dynamic>, ways:Array<Dynamic>, areas:Array<Dynamic>) {
			_nodes = nodes;
			_ways = ways;
			_areas = areas;
		}
		
		var _nodes:Array<Dynamic>;
		public function getNodes():Array<Dynamic> {
			return _nodes;
		}
		
		var _ways:Array<Dynamic>;
		public function getWays():Array<Dynamic> {
			return _ways;
		}
		
		var _areas:Array<Dynamic>;
		public function getAreas():Array<Dynamic> {
			return _areas;
		}
	}
