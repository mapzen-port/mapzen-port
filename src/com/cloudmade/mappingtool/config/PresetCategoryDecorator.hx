package com.cloudmade.mappingtool.config;

	class PresetCategoryDecorator implements IPresetCategory {
		
		public var name(getName, null) : String ;
		public var presets(getPresets, null) : Array<Dynamic> ;
		var category:IPresetCategory;
		
		public function new(category:IPresetCategory) {
			this.category = category;
		}
		
		public function getPresets():Array<Dynamic> {
			return category.presets;
		}
		
		public function getName():String {
			return category.name;
		}
	}
