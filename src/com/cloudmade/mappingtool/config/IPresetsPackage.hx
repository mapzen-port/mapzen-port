package com.cloudmade.mappingtool.config;

	interface IPresetsPackage
	{
		function nodes():Array<Dynamic>;
		
		function ways():Array<Dynamic>;
		
		function areas():Array<Dynamic>;
	}
