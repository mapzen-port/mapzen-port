package com.cloudmade.mappingtool.config.parsers;

	interface IXMLListToObjectParser
	{
		function parse(xml:XMLList):Dynamic;
	}
