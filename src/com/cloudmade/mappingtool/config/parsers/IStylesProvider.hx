package com.cloudmade.mappingtool.config.parsers;

	interface IStylesProvider
	{
		function getStyles(name:String):Dynamic;
	}
