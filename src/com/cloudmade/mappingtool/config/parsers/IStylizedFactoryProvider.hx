package com.cloudmade.mappingtool.config.parsers;

	import mx.core.IFactory;
	
	interface IStylizedFactoryProvider
	{
		function createFactory(name:String, styleName:String):IFactory;		
	}
