package com.cloudmade.mappingtool.config.parsers;

	import com.cloudmade.mappingtool.config.conditions.filters.NullTagsFilter;
	
	class NullTagsFilterParser implements IXMLParser {
		
		public function new()
		{
		}

		public function parse(xml:XML):Dynamic
		{
			return new NullTagsFilter();
		}
	}
