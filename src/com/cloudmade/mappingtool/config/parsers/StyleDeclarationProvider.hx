package com.cloudmade.mappingtool.config.parsers;

	import mx.styles.CSSStyleDeclaration;
	import mx.styles.StyleManager;

	class StyleDeclarationProvider implements IStyleDeclarationProvider {
		
		public function new() {
		}
		
		public function getStyleDeclaration(selector:String):CSSStyleDeclaration {
			return StyleManager.getStyleDeclaration("." + selector);
		}
	}
