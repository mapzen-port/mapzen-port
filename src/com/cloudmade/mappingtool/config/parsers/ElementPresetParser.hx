package com.cloudmade.mappingtool.config.parsers;

	import com.cloudmade.mappingtool.config.ElementPreset;
	import com.cloudmade.mappingtool.config.IReadOnlyHashMap;
	
	import mx.core.IFactory;
	
	class ElementPresetParser implements IXMLParser {
		
		var defaultTagMap:IReadOnlyHashMap;
		var iconProvider:IFactoryProvider;
		
		public function new(defaultTagMap:IReadOnlyHashMap, iconProvider:IFactoryProvider) {
			this.defaultTagMap = defaultTagMap;
			this.iconProvider = iconProvider;
		}

		public function parse(xml:XML):Dynamic {
			var defaultTags:Array<Dynamic> = defaultTagMap.getData(xml.@type) as Array;
			var icon:IFactory = iconProvider.createFactory(xml.@icon);
			return new ElementPreset(xml.@name, defaultTags, icon);
		}
	}
