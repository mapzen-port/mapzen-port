package com.cloudmade.mappingtool.config.parsers;

	class DefaultTypeTagsParser implements IXMLParser {
		
		var tagsParser:IXMLListParser;
		
		public function new(tagsParser:IXMLListParser) {
			this.tagsParser = tagsParser;
		}
		
		public function parse(xml:XML):Dynamic {
			var tagsXml:XMLList = getTags(xml);
			return tagsParser.parse(tagsXml);
		}
		
		function getTags(xml:XML):XMLList {
			var defaultNodes:XMLList = xml.child("default");
			
			if (defaultNodes.length() > 0) {
				return defaultNodes.tag;
			}
			
			return xml.tag;
			
		}
	}
