package com.cloudmade.mappingtool.config.parsers;

	import mx.core.IFactory;
	
	interface IFactoryProvider
	{
		function createFactory(name:String):IFactory;
	}
