package com.cloudmade.mappingtool.config.parsers;

	import com.cloudmade.mappingtool.config.IElementPreset;
	import com.cloudmade.mappingtool.config.IStringLocalizer;
	import com.cloudmade.mappingtool.config.LocalizedElementPreset;
	
	class LocalizedElementPresetParser implements IXMLParser {
		
		var presetParser:IXMLParser;
		var localizer:IStringLocalizer;
		
		public function new(presetParser:IXMLParser, localizer:IStringLocalizer) {
			this.presetParser = presetParser;
			this.localizer = localizer;
		}

		public function parse(xml:XML):Dynamic {
			var preset:IElementPreset = cast( presetParser.parse(xml), IElementPreset);
			return new LocalizedElementPreset(preset, localizer);
		}
	}
