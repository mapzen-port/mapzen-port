package com.cloudmade.mappingtool.config.parsers;

	import com.cloudmade.mappingtool.config.AnyValueTag;
	
	class TypeTagParser extends TagParser {
		
		public function new()
		{
			super();
		}
		
		public override function parse(xml:XML):Dynamic {
			if (xml.@value == "*") {
				return new AnyValueTag(xml.@key);
			}
			return super.parse(xml);
		}
	}
