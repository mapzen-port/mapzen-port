package com.cloudmade.mappingtool.config.parsers;

	class XMLListParser implements IXMLListParser {
		
		var objectParser:IXMLParser;
		
		public function new(objectParser:IXMLParser)
		{
			this.objectParser = objectParser;
		}

		public function parse(xml:XMLList):Array<Dynamic>
		{
			var objects:Array<Dynamic> = new Array();
			for (node in xml) {
				objects.push(objectParser.parse(node));
			}
			return objects;
		}
		
	}
