package com.cloudmade.mappingtool.config.parsers;

	import mx.core.ClassFactory;
	import mx.core.IFactory;

	class ClassFactoryProvider implements IFactoryProvider {
		
		var localizer:IClassLocalizer;
		
		public function new(localizer:IClassLocalizer) {
			this.localizer = localizer;
		}

		public function createFactory(name:String):IFactory {
			var generator:Class<Dynamic> = localizer.getClass(name);
			return new ClassFactory(generator);
		}
	}
