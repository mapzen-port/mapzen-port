package com.cloudmade.mappingtool.config.parsers;

	interface IXMLListParser
	{
		function parse(xml:XMLList):Array<Dynamic>;
	}
