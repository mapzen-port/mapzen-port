package com.cloudmade.mappingtool.config.parsers;

	import com.cloudmade.mappingtool.config.AnyElementType;
	
	class AnyElementTypeParser implements IXMLParser {
		
		public function new() {
		}
		
		public function parse(xml:XML):Dynamic {
			return new AnyElementType(xml.@priority);
		}
		
	}
