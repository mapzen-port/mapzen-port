package com.cloudmade.mappingtool.config.parsers;

	import mx.core.IFactory;

	class StylizedRendererFactoryProvider implements IStylizedFactoryProvider {
		
		var rendererFactoryProvider:IFactoryProvider;
		var stylesProvider:IStylesProvider;
		
		public function new(rendererFactoryProvider:IFactoryProvider, stylesProvider:IStylesProvider) {
			this.rendererFactoryProvider = rendererFactoryProvider;
			this.stylesProvider = stylesProvider;
		}

		public function createFactory(name:String, styleName:String):IFactory
		{
			var factory:IFactory = rendererFactoryProvider.createFactory(name);
			var styles:Dynamic = stylesProvider.getStyles(styleName);
			return new StylizedFactory(factory, styles);
		}
	}
