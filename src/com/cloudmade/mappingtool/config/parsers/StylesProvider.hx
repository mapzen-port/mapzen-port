package com.cloudmade.mappingtool.config.parsers;

	import mx.styles.CSSStyleDeclaration;
	
	class StylesProvider implements IStylesProvider {
		
		var styleDeclarationProvider:IStyleDeclarationProvider;
		
		public function new(styleDeclarationProvider:IStyleDeclarationProvider)
		{
			this.styleDeclarationProvider = styleDeclarationProvider;
		}

		public function getStyles(styleName:String):Dynamic
		{
			var styles:Dynamic = new Object();
			var styleDeclaration:CSSStyleDeclaration = styleDeclarationProvider.getStyleDeclaration(styleName);
			if (styleDeclaration) {
				var stylesFactory:Dynamic = styleDeclaration.factory;
				styles = new stylesFactory();
			}
			return styles;
		}
	}
