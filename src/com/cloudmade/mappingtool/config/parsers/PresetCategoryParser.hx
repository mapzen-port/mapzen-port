package com.cloudmade.mappingtool.config.parsers;

	import com.cloudmade.mappingtool.config.PresetCategory;
	
	class PresetCategoryParser implements IXMLParser {
		
		var presetsParser:IXMLListParser;
		
		public function new(presetsParser:IXMLListParser) {
			this.presetsParser = presetsParser;
		}

		public function parse(xml:XML):Dynamic {
			var presets:Array<Dynamic> = presetsParser.parse(xml.preset);
			return new PresetCategory(xml.@name, presets);
		}
	}
