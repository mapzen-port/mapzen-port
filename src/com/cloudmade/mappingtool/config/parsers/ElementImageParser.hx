package com.cloudmade.mappingtool.config.parsers;

	import com.cloudmade.mappingtool.config.ElementImage;
	import com.cloudmade.mappingtool.config.IElementType;
	import com.cloudmade.mappingtool.config.IReadOnlyHashMap;
	
	import mx.core.IFactory;
	
	class ElementImageParser implements IXMLParser {
		
		var typeMap:IReadOnlyHashMap;
		var layerMap:IReadOnlyHashMap;
		var factoryProvider:IStylizedFactoryProvider;
		
		public function new(typeMap:IReadOnlyHashMap, layerMap:IReadOnlyHashMap, factoryProvider:IStylizedFactoryProvider) {
			this.typeMap = typeMap;
			this.layerMap = layerMap;
			this.factoryProvider = factoryProvider;
		}

		public function parse(xml:XML):Dynamic
		{
			var factory:IFactory = factoryProvider.createFactory(xml.@renderer, xml.@styleName);
			
			var type:IElementType = typeMap.getData(xml.@type) as IElementType; 
			var layerIndex:Int = layerMap.getData(xml.@layer) as int;
			return new ElementImage(type, factory, layerIndex);
		}
		
	}
