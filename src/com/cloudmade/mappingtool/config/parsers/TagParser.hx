package com.cloudmade.mappingtool.config.parsers;

	import com.cloudmade.mappingtool.map.model.Tag;
	
	class TagParser implements IXMLParser {
		
		public function new()
		{
		}
		
		public function parse(xml:XML):Dynamic {
			return new Tag(xml.@key, xml.@value);
		}
	}
