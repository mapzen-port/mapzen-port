package com.cloudmade.mappingtool.config.parsers;

	import com.cloudmade.mappingtool.config.ElementType;
	import com.cloudmade.mappingtool.config.conditions.types.ITagsType;
	
	class ElementTypeParser implements IXMLParser {
		
		var rootTagsTypeParser:IXMLParser;
		
		public function new(rootTagsTypeParser:IXMLParser) {
			this.rootTagsTypeParser = rootTagsTypeParser;
		}

		public function parse(xml:XML):Dynamic
		{
			var tagsType:ITagsType = cast( rootTagsTypeParser.parse(xml), ITagsType);
			
			return new ElementType(tagsType, xml.@priority);
		}
		
	}
