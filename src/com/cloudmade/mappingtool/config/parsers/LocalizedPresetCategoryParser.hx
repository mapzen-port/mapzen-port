package com.cloudmade.mappingtool.config.parsers;

	import com.cloudmade.mappingtool.config.IPresetCategory;
	import com.cloudmade.mappingtool.config.IStringLocalizer;
	import com.cloudmade.mappingtool.config.LocalizedPresetCategory;
	
	class LocalizedPresetCategoryParser implements IXMLParser {
		
		var categoryParser:IXMLParser;
		var localizer:IStringLocalizer;
		
		public function new(categoryParser:IXMLParser, localizer:IStringLocalizer) {
			this.categoryParser = categoryParser;
			this.localizer = localizer;
		}
		
		public function parse(xml:XML):Dynamic {
			var category:IPresetCategory = cast( categoryParser.parse(xml), IPresetCategory);
			return new LocalizedPresetCategory(category, localizer);
		}
	}
