package com.cloudmade.mappingtool.config.parsers;

	import com.cloudmade.mappingtool.config.ElementTypePreset;
	import com.cloudmade.mappingtool.config.IElementPreset;
	import com.cloudmade.mappingtool.config.IElementType;
	import com.cloudmade.mappingtool.config.IReadOnlyHashMap;
	import com.cloudmade.mappingtool.config.conditions.filters.ITagsFilter;
	
	class ElementTypePresetParser implements IXMLParser {
		
		var presetParser:IXMLParser;
		var typeMap:IReadOnlyHashMap;
		var tagsFilterMap:IReadOnlyHashMap;
		
		public function new(presetParser:IXMLParser, typeMap:IReadOnlyHashMap, tagsFilterMap:IReadOnlyHashMap) {
			this.presetParser = presetParser;
			this.typeMap = typeMap;
			this.tagsFilterMap = tagsFilterMap;
		}
		
		public function parse(xml:XML):Dynamic {
			var preset:IElementPreset = cast( presetParser.parse(xml), IElementPreset);
			var type:IElementType = typeMap.getData(xml.@type) as IElementType;
			var tagsFilter:ITagsFilter = tagsFilterMap.getData(xml.@type) as ITagsFilter;
			return new ElementTypePreset(preset, type, tagsFilter);
		}
	}
