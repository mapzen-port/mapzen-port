package com.cloudmade.mappingtool.config.parsers;

	import com.cloudmade.mappingtool.config.HashMap;
	
	class HashMapParser implements IXMLListToObjectParser {
		
		var objectParser:IXMLParser;
		var fieldName:String;
		
		public function new(fieldName:String, objectParser:IXMLParser) {
			this.fieldName = fieldName;
			this.objectParser = objectParser;
		}
		
		public function parse(xml:XMLList):Dynamic {
			var map:HashMap = new HashMap();
			for (xmlObject in xml) {
				var key:String = xmlObject.attribute(fieldName);
				var value:Dynamic = objectParser.parse(xmlObject);
				map.setData(key, value);
			}
			return map;
		}
	}
