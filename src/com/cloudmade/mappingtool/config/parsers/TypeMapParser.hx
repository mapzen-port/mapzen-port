package com.cloudmade.mappingtool.config.parsers;

	import com.cloudmade.mappingtool.config.AnyElementType;
	import com.cloudmade.mappingtool.config.IHashMap;
	
	class TypeMapParser implements IXMLListToObjectParser {
		
		var concreteTypeMapParser:IXMLListToObjectParser;
		var anyTypeParser:IXMLParser;
		
		public function new(concreteTypeMapParser:IXMLListToObjectParser, anyTypeParser:IXMLParser) {
			this.concreteTypeMapParser = concreteTypeMapParser;
			this.anyTypeParser = anyTypeParser;
		}
		
		public function parse(xml:XMLList):Dynamic
		{
			var concreteTypesXml:XMLList = xml.(name() == "type");
			var typeMap:IHashMap = cast( concreteTypeMapParser.parse(concreteTypesXml), IHashMap);
			
			var anyTypesXml:XMLList = xml.(name() == "anytype");
			if (anyTypesXml.length() > 0) {
				var anyTypeXml:XML = anyTypesXml[0];
				var anyType:Dynamic = anyTypeParser.parse(anyTypeXml);
				typeMap.setData(anyTypeXml.@name, anyType);
			}
			
			return typeMap;
		}
	}
