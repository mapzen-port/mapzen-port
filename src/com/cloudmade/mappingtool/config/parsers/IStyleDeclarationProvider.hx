package com.cloudmade.mappingtool.config.parsers;

	import mx.styles.CSSStyleDeclaration;
	
	interface IStyleDeclarationProvider
	{
		function getStyleDeclaration(selector:String):CSSStyleDeclaration;
	}
