package com.cloudmade.mappingtool.config.parsers;

	interface IClassLocalizer
	{
		function getClass(name:String):Class<Dynamic>;
	}
