package com.cloudmade.mappingtool.config.parsers;

	interface IXMLParser
	{
		function parse(xml:XML):Dynamic;
	}
