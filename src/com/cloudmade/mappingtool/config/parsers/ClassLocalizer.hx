package com.cloudmade.mappingtool.config.parsers;

	import mx.resources.IResourceManager;
	
	class ClassLocalizer implements IClassLocalizer {
		
		var resourceManager:IResourceManager;
		var bundleName:String;
		
		public function new(resourceManager:IResourceManager, bundleName:String) {
			this.resourceManager = resourceManager;
			this.bundleName = bundleName;
		}
		
		public function getClass(name:String):Class<Dynamic> {
			return resourceManager.getClass(bundleName, name);
		}
	}
