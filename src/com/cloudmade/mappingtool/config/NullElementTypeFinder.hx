package com.cloudmade.mappingtool.config;

	class NullElementTypeFinder implements IElementTypeFinder {
		
		public function new() {
		}
		
		public function findType(tags:Array<Dynamic>):IElementType {
			return null;
		}
	}
