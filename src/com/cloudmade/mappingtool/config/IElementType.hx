package com.cloudmade.mappingtool.config;

	import com.cloudmade.mappingtool.config.conditions.types.ITagsType;
	
	interface IElementType implements ITagsType{
		function priority():Int;
	}
