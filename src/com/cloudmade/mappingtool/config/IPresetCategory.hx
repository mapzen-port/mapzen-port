package com.cloudmade.mappingtool.config;

	interface IPresetCategory
	{
		function presets():Array<Dynamic>;
		
		function name():String;
	}
