package com.cloudmade.mappingtool.config;

	class AnyElementType implements IElementType {
		
		public var priority(getPriority, null) : Int ;
		public function new(?priority:Int = 0) {
			_priority = priority;
		}
		
		var _priority:Int;
		public function getPriority():Int {
			return _priority;
		}
		
		public function correspondsTo(tags:Array<Dynamic>):Bool {
			return true;
		}
	}
