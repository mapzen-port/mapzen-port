package com.cloudmade.mappingtool.config;

	import mx.core.IFactory;
	
	interface IElementPreset implements INamed{
		function icon():IFactory;
		
		function defaultTags():Array<Dynamic>;
	}
