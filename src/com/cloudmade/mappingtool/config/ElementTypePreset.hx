package com.cloudmade.mappingtool.config;

	import com.cloudmade.mappingtool.config.conditions.filters.ITagsFilter;
	
	class ElementTypePreset extends ElementPresetDecorator, implements IElementTypePreset {
		
		public var priority(getPriority, null) : Int ;
		var type:IElementType;
		var tagsFilter:ITagsFilter;
		
		public function new(preset:IElementPreset, type:IElementType, tagsFilter:ITagsFilter) {
			super(preset);
			this.type = type;
			this.tagsFilter = tagsFilter;
		}
		
		public function getPriority():Int {
			return type.priority;
		}
		
		public function correspondsTo(tags:Array<Dynamic>):Bool {
			return type.correspondsTo(tags);
		}
		
		public function filterTags(tags:Array<Dynamic>):Array<Dynamic> {
			return tagsFilter.filterTags(tags);
		}
	}
