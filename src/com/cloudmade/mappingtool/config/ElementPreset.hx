package com.cloudmade.mappingtool.config;

	import mx.core.IFactory;

	class ElementPreset implements IElementPreset {
		
		public var defaultTags(getDefaultTags, null) : Array<Dynamic> ;
		public var icon(getIcon, null) : IFactory ;
		public var name(getName, null) : String ;
		public function new(name:String, defaultTags:Array<Dynamic>, icon:IFactory) {
			_name = name;
			_defaultTags = defaultTags;
			_icon = icon;
		}
		
		var _icon:IFactory;
		public function getIcon():IFactory {
			return _icon;
		}
		
		var _defaultTags:Array<Dynamic>
		public function getDefaultTags():Array<Dynamic> {
			return _defaultTags;
		}
		
		var _name:String;
		public function getName():String {
			return _name;
		}
	}
