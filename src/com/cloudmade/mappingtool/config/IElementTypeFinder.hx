package com.cloudmade.mappingtool.config;

	interface IElementTypeFinder
	{
		function findType(tags:Array<Dynamic>):IElementType;
	}
