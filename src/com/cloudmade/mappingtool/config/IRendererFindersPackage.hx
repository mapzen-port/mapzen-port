package com.cloudmade.mappingtool.config;

	interface IRendererFindersPackage
	{
		function nodeFinder():IElementTypeFinder;
		
		function wayFinder():IElementTypeFinder;
		
		function areaFinder():IElementTypeFinder;
	}
