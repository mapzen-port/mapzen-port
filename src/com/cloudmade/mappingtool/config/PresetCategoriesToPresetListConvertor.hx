package com.cloudmade.mappingtool.config;

	class PresetCategoriesToPresetListConvertor
	 {
		
		public function new() {
		}
		
		public function convert(presetCategories:Array<Dynamic>):Array<Dynamic> {
			var presetList:Array<Dynamic> = new Array();
			for (category in presetCategories) {
				presetList = presetList.concat(category.presets);
			}
			return presetList;
		}
	}
