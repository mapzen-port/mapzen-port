package com.cloudmade.mappingtool.config;

	import mx.resources.IResourceManager;
	
	class StringLocalizer implements IStringLocalizer {
		
		var resourceManager:IResourceManager;
		var bundleName:String;
		
		public function new(resourceManager:IResourceManager, bundleName:String) {
			this.resourceManager = resourceManager;
			this.bundleName = bundleName;
		}
		
		public function getString(name:String):String {
			return resourceManager.getString(bundleName, name);
		}
	}
