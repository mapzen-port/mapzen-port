package com.cloudmade.mappingtool.config;

	class HashMap extends ReadOnlyHashMap, implements IHashMap {
		
		public function new()
		{
			super(new Object());
		}

		public function setData(key:String, value:Dynamic):Void {
			map[key] = value;
		}
	}
