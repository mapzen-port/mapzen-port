package com.cloudmade.mappingtool.config;

	import com.cloudmade.mappingtool.config.conditions.types.ITagsType;
	
	class ElementType implements IElementType {
		
		public var priority(getPriority, null) : Int ;
		var tagsType:ITagsType;
		
		public function new(tagsType:ITagsType, ?priority:Int = 0) {
			this.tagsType = tagsType;
			_priority = priority;
		}
		
		var _priority:Int;
		public function getPriority():Int {
			return _priority;
		}
		
		public function correspondsTo(tags:Array<Dynamic>):Bool {
			return tagsType.correspondsTo(tags);
		}
	}
