package com.cloudmade.mappingtool.config;

	class ElementTypeFinder implements IElementTypeFinder {
		
		var types:Array<Dynamic>;
		var priorityTypeSelector:IElementTypeSelector;
		
		public function new(types:Array<Dynamic>, priorityTypeSelector:IElementTypeSelector) {
			this.types = types;
			this.priorityTypeSelector = priorityTypeSelector;
		}
		
		public function findType(tags:Array<Dynamic>):IElementType {
			var foundTypes:Array<Dynamic> = new Array();
			for (type in types) {
				if (type.correspondsTo(tags)) {
					foundTypes.push(type);
				}
			}
			return priorityTypeSelector.select(foundTypes);
		}
	}
