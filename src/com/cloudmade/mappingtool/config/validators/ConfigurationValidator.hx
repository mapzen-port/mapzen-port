package com.cloudmade.mappingtool.config.validators;

	import com.cloudmade.mappingtool.config.IReadOnlyHashMap;
	import com.cloudmade.mappingtool.config.factories.resources.AreasResources;
	import com.cloudmade.mappingtool.config.factories.resources.IResources;
	import com.cloudmade.mappingtool.config.factories.resources.NodesResources;
	import com.cloudmade.mappingtool.config.factories.resources.RealWaysResources;
	import com.cloudmade.mappingtool.config.factories.resources.WaysResources;
	import com.cloudmade.mappingtool.config.parsers.HashMapParser;
	import com.cloudmade.mappingtool.config.parsers.IXMLListToObjectParser;
	import com.cloudmade.mappingtool.config.parsers.StyleDeclarationProvider;
	
	import mx.controls.Alert;
	
	class ConfigurationValidator
	 {
		
		public function new() {
		}
		
		public function validate():Void{
			var resourcesPackage:Array<Dynamic> = new Array();
			resourcesPackage.push(new NodesResources());
			resourcesPackage.push(new WaysResources());
			resourcesPackage.push(new AreasResources());
			resourcesPackage.push(new RealWaysResources());
			
			for (resources in resourcesPackage) {
				var typeName:String = resources.type;
				
				new UniqueNamesValidator(function(name:String):Void {
					showWarning("Duplicate layer name: " + name, typeName + " Layers");
				}).validate(resources.layersXml, "name");
				
				new UniqueNamesValidator(function(name:String):Void {
					showWarning("Duplicate type name: " + name, typeName + " Types");
				}).validate(resources.typesXml, "name");
				
				var typesMapParser:IXMLListToObjectParser = new HashMapParser("name", new BooleanXMLParser());
				var typesMap:IReadOnlyHashMap = cast( typesMapParser.parse(resources.typesXml), IReadOnlyHashMap);
				
				new ReferencesValidator(typesMap, function(name:String):Void {
					showWarning("Broken type reference: " + name, typeName + " Presets");
				}).validate(resources.presetsXml.preset, "type");
				
				new ReferencesValidator(typesMap, function(name:String):Void {
					showWarning("Broken type reference: " + name, typeName + " Renderers");
				}).validate(resources.renderersXml, "type");
				
				var layersMapParser:IXMLListToObjectParser = new HashMapParser("name", new BooleanXMLParser());
				var layersMap:IReadOnlyHashMap = cast( layersMapParser.parse(resources.layersXml), IReadOnlyHashMap);
				new ReferencesValidator(layersMap, function(name:String):Void {
					showWarning("Broken layer reference: " + name, typeName + " Renderers");
				}).validate(resources.renderersXml, "layer");
				
				var stylesMap:IReadOnlyHashMap = new StylesMap(new StyleDeclarationProvider());
				new ReferencesValidator(stylesMap, function(name:String):Void {
					showWarning("Broken style reference: " + name, typeName + " Styles");
				}).validate(resources.renderersXml, "styleName");
			}
		}
		
		function showWarning(message:String, title:String):Void {
			Alert.show(message, title, Alert.NONMODAL);
		}
	}
