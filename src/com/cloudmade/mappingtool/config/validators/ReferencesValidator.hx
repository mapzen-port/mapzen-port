package com.cloudmade.mappingtool.config.validators;

	import com.cloudmade.mappingtool.config.IReadOnlyHashMap;
	
	class ReferencesValidator implements IXMLValidator {
		
		var referenceMap:IReadOnlyHashMap;
		var errorCallback:Dynamic;
		
		public function new(referenceMap:IReadOnlyHashMap, errorCallback:Dynamic) {
			this.referenceMap = referenceMap;
			this.errorCallback = errorCallback;
		}
		
		public function validate(xml:XMLList, fieldName:String):Void {
			for (node in xml) {
				var names:Array<Dynamic> = node.attribute(fieldName).toString().split(",");
				for (name in names) {
					if (!referenceMap.getData(name)) {
						errorCallback(name);
					}
				}
			}
		}
	}
