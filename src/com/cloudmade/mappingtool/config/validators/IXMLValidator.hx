package com.cloudmade.mappingtool.config.validators;

	interface IXMLValidator
	{
		function validate(xml:XMLList, fieldName:String):Void;
	}
