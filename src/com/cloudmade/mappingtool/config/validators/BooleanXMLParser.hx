package com.cloudmade.mappingtool.config.validators;

	import com.cloudmade.mappingtool.config.parsers.IXMLParser;

	class BooleanXMLParser implements IXMLParser {
		
		public function new() {
		}

		public function parse(xml:XML):Dynamic {
			return true;
		}
	}
