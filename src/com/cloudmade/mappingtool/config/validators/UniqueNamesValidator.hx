package com.cloudmade.mappingtool.config.validators;

	class UniqueNamesValidator implements IXMLValidator {
		
		var errorCallback:Dynamic;
		
		public function new(errorCallback:Dynamic) {
			this.errorCallback = errorCallback;
		}
		
		public function validate(xml:XMLList, fieldName:String):Void{
			var names:Dynamic = new Object();
			
			for (node in xml) {
				var name:String = node.attribute(fieldName).toString();
				
				if (names[name]) {
					errorCallback(name);
				} else {
					names[name] = true;
				}
			}
		}
	}
