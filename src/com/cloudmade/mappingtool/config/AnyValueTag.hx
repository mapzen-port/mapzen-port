package com.cloudmade.mappingtool.config;

	import com.cloudmade.mappingtool.map.model.ITag;

	class AnyValueTag implements ITag {
		
		public var isEmpty(getIsEmpty, null) : Bool ;
		public var key(getKey, setKey) : String;
		public var value(getValue, setValue) : String;
		public function new(key:String) {
			_key = key;
		}
		
		var _key:String;
		public function getKey():String{
			return _key;
		}
		public function setKey(value:String):String{
			_key = value;
			return value;
		}
		
		public function getValue():String{
			return "*";
		}
		public function setValue(value:String):String{
			return value;
	}
		
		public function getIsEmpty():Bool {
			return (_key == "") || (_key == null);
		}
		
		public function equals(tag:ITag):Bool {
			return _key == tag.key;
		}
	}
