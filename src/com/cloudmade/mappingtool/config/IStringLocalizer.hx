package com.cloudmade.mappingtool.config;

	interface IStringLocalizer
	{
		function getString(name:String):String;
	}
