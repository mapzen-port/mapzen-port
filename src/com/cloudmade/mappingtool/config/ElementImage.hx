package com.cloudmade.mappingtool.config;

	import mx.core.IFactory;
	
	class ElementImage implements IElementImage {
		
		public var layerIndex(getLayerIndex, null) : Int ;
		public var priority(getPriority, null) : Int ;
		var type:IElementType;
		var factory:IFactory;
		
		public function new(type:IElementType, factory:IFactory, layerIndex:Int) {
			this.type = type;
			this.factory = factory;
			_layerIndex = layerIndex;
		}
		
		public function getPriority():Int {
			return type.priority;
		}
		
		var _layerIndex:Int;
		public function getLayerIndex():Int {
			return _layerIndex;
		}
		
		public function correspondsTo(tags:Array<Dynamic>):Bool {
			return type.correspondsTo(tags);
		}
		
		public function newInstance():Dynamic {
			return factory.newInstance();
		}
	}
