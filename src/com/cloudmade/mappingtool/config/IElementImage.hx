package com.cloudmade.mappingtool.config;

	import mx.core.IFactory;
	
	interface IElementImage implements IFactory, implements IElementType{
		function layerIndex():Int;
	}
