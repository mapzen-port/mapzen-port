package com.cloudmade.mappingtool.config;

	import com.cloudmade.mappingtool.config.conditions.filters.ITagsFilter;
	
	interface IElementTypePreset implements IElementPreset, implements IElementType, implements ITagsFilter{
		
	}
