package com.cloudmade.mappingtool.config;

	class LocalizedPresetCategory extends PresetCategoryDecorator {
		
		public var name(getName, null) : String ;
		var localizer:IStringLocalizer;
		
		public function new(category:IPresetCategory, localizer:IStringLocalizer) {
			super(category);
			this.localizer = localizer;
		}
		
		public override function getName():String {
			return localizer.getString(category.name);
		}
	}
