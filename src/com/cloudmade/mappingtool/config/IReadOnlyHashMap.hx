package com.cloudmade.mappingtool.config;

	interface IReadOnlyHashMap
	{
		function getData(key:String):Dynamic;
	}
