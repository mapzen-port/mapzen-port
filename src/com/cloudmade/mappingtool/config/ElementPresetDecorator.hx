package com.cloudmade.mappingtool.config;

	import mx.core.IFactory;

	class ElementPresetDecorator implements IElementPreset {
		
		public var defaultTags(getDefaultTags, null) : Array<Dynamic> ;
		public var icon(getIcon, null) : IFactory ;
		public var name(getName, null) : String ;
		var preset:IElementPreset;
		
		public function new(preset:IElementPreset) {
			this.preset = preset;
		}

		public function getIcon():IFactory {
			return preset.icon;
		}
		
		public function getDefaultTags():Array<Dynamic> {
			return preset.defaultTags;
		}
		
		public function getName():String {
			return preset.name;
		}
	}
