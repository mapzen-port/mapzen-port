package com.cloudmade.mappingtool.config;

	class ReadOnlyHashMap implements IReadOnlyHashMap {
		
		var map:Dynamic;
		
		public function new(map:Dynamic)
		{
			this.map = map;
		}
		
		public function getData(key:String):Dynamic {
			return map[key];
		}

	}
