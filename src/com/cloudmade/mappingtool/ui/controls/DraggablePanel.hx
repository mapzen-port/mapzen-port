package com.cloudmade.mappingtool.ui.controls;

	import com.cloudmade.mappingtool.utils.Bounds;
	
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	import mx.containers.Panel;

	class DraggablePanel extends Panel {
		
		public var dragArea(getDragArea, null) : Rectangle ;
		public function new() {
			super();
			isPopUp = true;
		}
		
		function getDragArea():Rectangle {
			var area:Rectangle = systemManager.screen.clone();
			area.width -= width;
			area.height -= height;
			return area;
		}
		
		public override function move(x:Int, y:Int):Void {
			var localPosition:Point = new Point(x - this.x, y - this.y);
			var globalPosition:Point = localToGlobal(localPosition);
			var bounds:Bounds = new Bounds(dragArea);
			globalPosition = bounds.limitPoint(globalPosition);
			localPosition = globalToLocal(globalPosition);
			x = this.x + localPosition.x;
			y = this.y + localPosition.y;
			super.move(x, y);
		}
	}
