package com.cloudmade.mappingtool.ui.controls;

	import flash.events.FocusEvent;
	
	import mx.controls.ComboBox;

	class ComboBoxPrompted extends ComboBox {
		
		public function new()
		{
			super();
			addEventListener(FocusEvent.FOCUS_IN, onFocusIn);
        	addEventListener(FocusEvent.FOCUS_OUT, onFocusOut);
		}
		
		function showPrompt():Void	{
			if (prompt != null && prompt != "" && text.length <= 0)	{
				
				text = prompt;
				textInput.setStyle("color", "#666666");
			}
		}
		
		function hidePrompt():Void	{
			text = "";
			textInput.setStyle("color", "#FFFFFF");
		}
		
		function onFocusIn(event:FocusEvent):Void {
			if (text == prompt) {
				hidePrompt();
			}
		}
		
		function onFocusOut(event:FocusEvent):Void {
			if (text == null || text == ""){
				showPrompt();
			}
		}
			
	}
