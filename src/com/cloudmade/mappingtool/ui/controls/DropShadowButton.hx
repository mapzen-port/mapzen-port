/**
 * A button with drop shadow on the text label
 */
package com.cloudmade.mappingtool.ui.controls;

	import flash.filters.DropShadowFilter;
	
	import mx.controls.Button;

	/*[Style(name="textDropShadowColor",type="Number",format="Color",inherit="no")]*/
	/*[Style(name="textDropShadowDistance",type="Number",format="Number",inherit="no")]*/
	/*[Style(name="textDropShadowAngle",type="Number",format="Number",inherit="no")]*/
	/*[Style(name="textDropShadowAlpha",type="Number",format="Number",inherit="no")]*/
	/*[Style(name="textDropShadowBlur",type="Number",format="Number",inherit="no")]*/

	class DropShadowButton extends Button {
		
		public var enabled(null, setEnabled) : Bool;
		public function new()
		{
			super();
		}
		
		var dropShadowChanged:Bool ;
		
		override function updateDisplayList(unscaledWidth:Int, unscaledHeight:Int):Void {
			super.updateDisplayList(unscaledWidth, unscaledHeight);
			if (enabledChanged) {
				if (enabled) {
					alpha = 1;
					dropShadowChanged = true;
				} else {
					alpha = 0.6;
					textField.filters = [];
					dropShadowChanged = false;
				}
				enabledChanged = false;
			}
			if (dropShadowChanged) {
				udpateDropShadow();
				dropShadowChanged = false;
			}
		}
		
		var enabledChanged:Bool ;
		public override function setEnabled(value:Bool):Bool{
			if (super.enabled != value) enabledChanged = true;
			super.enabled = value;
			return value;
		}
		
		public override function styleChanged(styleProp:String):Void {
			super.styleChanged(styleProp);
			
			if (!styleProp || 
					(styleProp == "styleName") || 
					(styleProp == "textDropShadowColor") || 
					(styleProp == "textDropShadowDistance") ||
					(styleProp == "textDropShadowAngle") ||
					(styleProp == "textDropShadowAlpha") ||
					(styleProp == "textDropShadowBlur")) {
				dropShadowChanged = true;
				invalidateDisplayList();
			}
		}
		
		function udpateDropShadow():Void {
			var shadow:DropShadowFilter = new DropShadowFilter();
			shadow.color = getStyle("textDropShadowColor");
			shadow.angle = getStyle("textDropShadowAngle");
			shadow.alpha = getStyle("textDropShadowAlpha");
			shadow.distance = getStyle("textDropShadowDistance");
			shadow.blurX = shadow.blurY = getStyle("textDropShadowBlur");
			textField.filters = [shadow];
		}
	}
