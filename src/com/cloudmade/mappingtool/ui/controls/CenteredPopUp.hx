package com.cloudmade.mappingtool.ui.controls;

	import com.cloudmade.mappingtool.managers.WindowManager;
	
	import flash.events.Event;
	
	import mx.core.IUIComponent;

	class CenteredPopUp extends ComponentDecorator, implements IPopUp {
		
		var windowManager:WindowManager;
		var modal:Bool;
		
		public function new(component:IUIComponent, windowManager:WindowManager, modal:Bool) {
			super(component);
			this.windowManager = windowManager;
			this.modal = modal;
		}
		
		public function open():Void {
			windowManager.openWindow(this, true);
			stage.addEventListener(Event.RESIZE, handleStageResize, false, 0, true);
		}
		
		public function close():Void {
			stage.removeEventListener(Event.RESIZE, handleStageResize);
			windowManager.closeWindow(this);
		}
		
		function handleStageResize(event:Event):Void {
			windowManager.centerWindow(this);
		}
	}
