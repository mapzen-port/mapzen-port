package com.cloudmade.mappingtool.ui.controls;

	import com.cloudmade.mappingtool.managers.WindowManager;
	
	import mx.containers.TitleWindow;
	import mx.events.CloseEvent;

	class Window extends TitleWindow {
		
		var windowManager:WindowManager ;
		
		public function new() {
			
			windowManager = WindowManager.getInstance();
			super();
			showCloseButton = true;
			addEventListener(CloseEvent.CLOSE, closeHandler);
		}
		
		public function open(?modal:Bool = false):Void {
			windowManager.openWindow(this, modal);
		}
		
		public function close():Void {
			windowManager.closeWindow(this);
		}
		
		function closeHandler(event:CloseEvent):Void {
			close();
		}
	}
