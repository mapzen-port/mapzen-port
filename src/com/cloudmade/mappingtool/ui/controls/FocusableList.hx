package com.cloudmade.mappingtool.ui.controls;

	import mx.controls.List;

	class FocusableList extends List {
		
		public function new() {
			super();
			tabEnabled = false;
			tabChildren = true;
		}
		
		override function createChildren():Void {
			super.createChildren();
			listContent.tabChildren = true;
		}
	}
