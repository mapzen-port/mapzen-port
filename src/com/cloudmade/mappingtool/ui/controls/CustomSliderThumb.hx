package com.cloudmade.mappingtool.ui.controls;

	import mx.controls.sliderClasses.SliderThumb;
    import mx.core.mx_internal;
    
    use namespace mx_internal;
	
	class CustomSliderThumb extends SliderThumb {
		
		public function new()
		{
			super();
		}
		
		override function measure():Void {
            super.measure();
            measuredWidth = currentSkin.measuredWidth;
            measuredHeight = currentSkin.measuredHeight;
        }
	}
