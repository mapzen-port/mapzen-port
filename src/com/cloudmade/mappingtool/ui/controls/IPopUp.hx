package com.cloudmade.mappingtool.ui.controls;

	interface IPopUp
	{
		function open():Void;
		
		function close():Void;
	}
