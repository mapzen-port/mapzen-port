package com.cloudmade.mappingtool.ui.controls;

	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.ui.Keyboard;
	
	import mx.controls.ComboBox;
	import mx.core.mx_internal;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;

	/*[Event(name="typedTextChange", type="flash.events.Event")]*/
	
	class SmartInput extends ComboBox {
		
		public var dataProvider(null, setDataProvider) : Dynamic;
		public var labelField(null, setLabelField) : String;
		public var lookAhead(getLookAhead, setLookAhead) : Bool ;
		public var prompt(null, setPrompt) : String;
		public var tempInputValue(getTempInputValue, setTempInputValue) : String ;
		public var typedText(getTypedText, setTypedText) : String;
		var cursorPosition:Int;
		var prevIndex:Int ;
		var removeHighlight:Bool ;
		var showDropdown:Bool ;
		var keyInput:Bool ;
		var dropdownClosed:Bool ;
		var dataProviderChanged:Bool ;
		var keyHandlerChanged:Bool ;
		var lookAheadChanged:Bool;
		var typedTextChanged:Bool;
		var promptChanged:Bool;
		var _dataProvider:Dynamic;
		var _lookAhead:Bool ;
		var _typedText:String ;
		var _tempInputValue:String ;
		
		public function new() {
			
			cursorPosition =0;
			prevIndex = -1;
			removeHighlight = false;
			showDropdown = false;
			keyInput = false;
			dropdownClosed = true;
			dataProviderChanged = false;
			keyHandlerChanged = false;
			_lookAhead = true;
			_typedText = "";
			_tempInputValue = "";
			super();
			editable = true;
		}
		
		public function setLookAhead(value:Bool):Bool {
			_lookAhead = value;
			return value;
		}

		public function getLookAhead():Bool {
			return _lookAhead;
		}
		
		public function setTempInputValue(value:String):String {
			_tempInputValue = value;
			return value;
		}

		public function getTempInputValue():String {
			return _tempInputValue;
		}
		
		public function getTypedText():String{
		    return _typedText;
		}
		
		public function setTypedText(input:String):String{
		    _typedText = input;
		    typedTextChanged = true;
			return input;
		   }
		
		public function setDefaultParams():Void {
			prompt = "";
			text = "";
		}
		
		public override function setFocus():Void {
			var selectionStart:Int = textInput.selectionBeginIndex;
			var selectionEnd:Int = textInput.selectionEndIndex;
			super.setFocus();
			textInput.setSelection(selectionEnd, selectionStart);
			textInput.validateProperties();
		}
		
		
		public override function setPrompt(value:String):String{
			if (value == prompt) return;
			super.prompt = value;
			promptChanged = true;
			return value;
		}
		
		public override function setDataProvider(value:Dynamic):Dynamic{
			setDefaultParams();
			if (value == _dataProvider) return;
			_dataProvider = value;
			super.dataProvider = value;
			dataProviderChanged = true;
			return value;
		}
		
		public override function setLabelField(value:String):String{
			super.labelField = value;
			return value;
		}
		
		public override function getStyle(styleProp:String):Dynamic {
			if ((styleProp != "openDuration") || !keyInput) {
		     	return super.getStyle(styleProp);
	  		};
	  		return 0;
		}
		
		public override function close(?event:Event = null):Void {
			if(selectedIndex == 0) {
		    	textInput.text = selectedLabel;
				textInput.setSelection(textInput.text.length, textInput.text.length);
	    	}
	    	prompt = text;
    		super.close(event);
    		if (tempInputValue != typedText){
    			dispatchEvent(new ListEvent(ListEvent.CHANGE));
    		};
    		dropdownClosed = true;
    	}
    	
    	public override function open():Void {
    		if(dataProvider && (dataProvider.length > 0) && dataProvider[0] != null){
    			super.open();
    			dropdownClosed = false;
    			tempInputValue = "";
    		}
    	}
		
		override function commitProperties():Void {
	    	super.commitProperties();
	    	if(promptChanged){
	    		_typedText = prompt;
	    	}
		    if(typedTextChanged) {
		    	updateDataProvider();
			    cursorPosition = textInput.selectionBeginIndex;
  			    if(typedText == "" || keyHandlerChanged || (collection && collection.length==0)) {
			    	showDropdown = false;
  			    } else {
					showDropdown = true;
					selectedIndex = 0;
 		    	}
 		    }
			if (dataProviderChanged) {
				dataProviderChanged = false;
				selectedIndex = -1;
			}
		}
		
		override function keyDownHandler(event:KeyboardEvent):Void {
			keyHandlerChanged = true;
	    	super.keyDownHandler(event);
		    if(!event.ctrlKey) {
			    if(event.keyCode == Keyboard.UP && prevIndex==0) {
		 		    textInput.text = _typedText;
				    textInput.setSelection(textInput.text.length, textInput.text.length);
				    selectedIndex = -1;
				} else if(event.keyCode==Keyboard.ESCAPE) {
				    textInput.setSelection(textInput.text.length, textInput.text.length);
				} else if(lookAhead && event.keyCode == Keyboard.BACKSPACE || event.keyCode == Keyboard.DELETE) {
				    removeHighlight = true;
				}
		 	}
	 	    prevIndex = selectedIndex;
	 	    keyHandlerChanged = false;
		}
		
		override function updateDisplayList(unscaledWidth:Int, unscaledHeight:Int):Void {
			mx_internal::selectionChanged = false;
		    super.updateDisplayList(unscaledWidth, unscaledHeight);
	  	    if(selectedIndex == -1) {
		    	textInput.text = typedText;
	  	    }
		    if(typedTextChanged) {
  			    if(lookAhead && showDropdown && typedText!="" && !removeHighlight) {
					var label:String = itemToLabel(collection[0]);
					var index:Int = label.toLowerCase().indexOf(_typedText.toLowerCase());
					if(index==0) {
					    textInput.text = _typedText+label.substr(_typedText.length);
					    textInput.setSelection(textInput.text.length, _typedText.length);
					} else {
					    textInput.text = _typedText;
					    textInput.setSelection(cursorPosition, cursorPosition);
					    removeHighlight = false;
					}
				} else {
				    textInput.text = _typedText;
				    textInput.setSelection(cursorPosition, cursorPosition);
				    removeHighlight = false;
				} 
				typedTextChanged = false;
			}
	 	    if(showDropdown && !dropdown.visible) {
	 	    	open();
		    	showDropdown = false;
	 	    } else {
	 	    	tempInputValue = text;
	 	    }
		}
		
		override function textInput_changeHandler(event:Event):Void {
			keyInput = true;
		    super.textInput_changeHandler(event);
		    typedText = text;
		    
		}
		
		override function downArrowButton_buttonDownHandler(event:FlexEvent):Void {
			collection.filterFunction = null;
			collection.refresh();
			keyInput = false;
			if (dropdownClosed) {
				open();
			} else {
				close();
			}
		}
		
		function filterFunction(element:Dynamic):Bool {
			var label:String = itemToLabel(element);
		    return (label.toLowerCase().substring(0,text.length) == text.toLowerCase());
		}
		
		function updateDataProvider():Void {
		 	collection.filterFunction = filterFunction;
			collection.refresh();
	  	}
	}
