package com.cloudmade.mappingtool.ui.controls;

    import flash.display.DisplayObject;
    import flash.events.Event;
    
    import mx.controls.HorizontalList;
    import mx.core.mx_internal;
    import mx.events.ScrollEvent;
    import mx.events.ScrollEventDetail;
	
	use namespace mx_internal;
	
    class SmoothScrollingHorizontalList extends HorizontalList {
        
        public var dataProvider(null, setDataProvider) : Dynamic;
        public var horizontalScrollPosition(getHorizontalScrollPosition, null) : Int
        ;
        var fudge:Int;
        
        public function new()
        {
            super();
            //offscreenExtraRowsOrColumns = 2;
        }

        override function configureScrollBars():Void
        {
            super.configureScrollBars();
            if (horizontalScrollBar)
                horizontalScrollBar.lineScrollSize = .125;  // should be inverse power of 2
        }
        
        public override function getHorizontalScrollPosition():Int
        {
            if (!isNaN(fudge))
            {
                var vsp:Int = super.horizontalScrollPosition + fudge;
                fudge = NaN;
                return vsp;
            }
            return Math.floor(super.horizontalScrollPosition);
        }
        
        public override function setDataProvider(value:Dynamic):Dynamic{
        	super.dataProvider = value;
        	commitSelectedIndex(selectedIndex);
        	return value;
        }

        override function scrollHandler(event:Event):Void
        {
            // going backward is trickier.  When you cross from, for instance 2.1 to 1.9, you need to convince
            // the superclass that it is going from 2 to 1 so the delta is -1 and not -.2.
            // we do this by adding a fudge factor to the first return from horizontalScrollPosition
            // which is used by the superclass logic.
            var last:Int = super.horizontalScrollPosition;
            var vsp:Int = this.horizontalScrollBar.scrollPosition;
            if (vsp <last) {
                if (last != Math.floor(last) || vsp != Math.floor(vsp)) {
                    if (Math.floor(vsp) <Math.floor(last)) {
                        fudge = Math.floor(last) - Math.floor(horizontalScrollBar.scrollPosition);
                    }
                }
            }
            super.scrollHandler(event);
            var pos:Int = super.horizontalScrollPosition;
            // if we get a THUMB_TRACK, then we need to calculate the position
            // because it gets rounded to an int by the ScrollThumb code, and 
            // we want fractional values.
            if (Std.is( event, ScrollEvent))
            {
                var se:ScrollEvent = ScrollEvent(event);
                if (se.detail == ScrollEventDetail.THUMB_TRACK)
                {
                    if (horizontalScrollBar.numChildren == 4)
                    {
                        var downArrow:DisplayObject = horizontalScrollBar.getChildAt(3);
                        var thumb:DisplayObject = horizontalScrollBar.getChildAt(2);
                        pos = (thumb.y - downArrow.height) / (downArrow.y - thumb.height - downArrow.height) * this.maxHorizontalScrollPosition;
                        // round to nearest lineScrollSize;
                        pos /= horizontalScrollBar.lineScrollSize;
                        pos = Math.round(pos);
                        pos *= horizontalScrollBar.lineScrollSize;
                    }
                }
            }
            var fraction:Int = pos - horizontalScrollPosition;
            fraction *= this.columnWidth;
            listContent.move(viewMetrics.left + listContent.leftOffset - fraction, listContent.y);
        }
    }
