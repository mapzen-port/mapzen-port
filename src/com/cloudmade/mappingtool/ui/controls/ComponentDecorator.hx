/**
 * A base class that enables decoration of visual components.
 * 
 * @see mx.core.IUIComponent
 */
package com.cloudmade.mappingtool.ui.controls;

	import flash.display.DisplayObject;
	
	import mx.core.IUIComponent;
	import mx.core.UIComponent;

	class ComponentDecorator extends UIComponent {
		
		var component:IUIComponent;
		
		public function new(component:IUIComponent) {
			super();
			this.component = component;
		}
		
		override function createChildren():Void {
			super.createChildren();
			addChild(cast( component, DisplayObject));
		}
		
		override function measure():Void {
			super.measure();
			measuredWidth = component.measuredWidth;
			measuredMinWidth = component.measuredMinWidth;
			measuredHeight = component.measuredHeight;
			measuredMinHeight = component.measuredMinHeight;
		}
		
		override function updateDisplayList(unscaledWidth:Int, unscaledHeight:Int):Void {
			super.updateDisplayList(unscaledWidth, unscaledHeight);
			component.setActualSize(unscaledWidth, unscaledHeight);
		}
	}
