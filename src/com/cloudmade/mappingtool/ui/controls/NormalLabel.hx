package com.cloudmade.mappingtool.ui.controls;

	import mx.controls.Label;
	import mx.core.UITextField;
	import mx.core.mx_internal;
	
	use namespace mx_internal;

	class NormalLabel extends Label {
		
		public function new() {
			super();
		}
		
		override function measure():Void {
			super.measure();
			measuredMinWidth = 0;
			measuredWidth -= UITextField.TEXT_WIDTH_PADDING;
			measuredHeight -= UITextField.TEXT_HEIGHT_PADDING;
			measuredMinHeight -= UITextField.TEXT_HEIGHT_PADDING;
		}
		
		override function updateDisplayList(unscaledWidth:Int, unscaledHeight:Int):Void {
			unscaledWidth += UITextField.TEXT_WIDTH_PADDING;
			unscaledHeight += UITextField.TEXT_HEIGHT_PADDING;
			super.updateDisplayList(unscaledWidth, unscaledHeight);
			
			var textFieldX:Int = textField.x - Math.floor(UITextField.TEXT_WIDTH_PADDING / 2);
			var textFieldY:Int = textField.y - Math.floor(UITextField.TEXT_HEIGHT_PADDING / 2);
			textField.move(textFieldX, textFieldY);
		}
	}
