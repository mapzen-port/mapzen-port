package com.cloudmade.mappingtool.ui;

	import com.cloudmade.mappingtool.actions.runners.IBufferingActionRunner;
	import com.cloudmade.mappingtool.config.IRendererFindersPackage;
	import com.cloudmade.mappingtool.map.geo.LatLong;
	import com.cloudmade.mappingtool.map.model.Map;
	import com.cloudmade.mappingtool.map.view.CloudMadeBackground;
	import com.cloudmade.mappingtool.map.view.IMapView;
	import com.cloudmade.mappingtool.map.view.MapView;
	import com.cloudmade.mappingtool.map.view.NullBackground;
	import com.cloudmade.mappingtool.map.view.YahooBackground;
	import com.cloudmade.mappingtool.ui.actions.navigation.MapNavigatorActionBuilder;
	import com.cloudmade.mappingtool.utils.minmax;
	
	import flash.display.Graphics;
	import flash.display.Shape;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	import mx.core.UIComponent;
	
	class MapNavigator extends UIComponent, implements IMapView {
		// TODO: Move these constants to a separate class that won't contain so many dependencies.
		
		public var backgroundType(getBackgroundType, setBackgroundType) : UInt;
		public var fadeBackground(getFadeBackground, setFadeBackground) : Bool;
		public var map(getMap, setMap) : Map;
		public var rendererFindersPackage(getRendererFindersPackage, setRendererFindersPackage) : IRendererFindersPackage;
		public var showElements(getShowElements, setShowElements) : Bool;
		// TODO: Move these constants to a separate class that won't contain so many dependencies.
		public static var EMPTY_BACKGROUND:UInt = 0;
		public static var YAHOO_BACKGROUND:UInt = 1;
		public static var CLOUDMADE_BACKGROUND:UInt = 2;
		
		static var FADE_ALPHA:Float = 0.6;
		static var BACKGROUND_COLOR:UInt = 0xF2EFE9;
		static var MAX_LATITUDE:Int = Math.atan(Math.exp(Math.PI)) * 360 / Math.PI - 90;
		
		public var maxZoomLevelDelta:Int ;
		public var mouseWheelEnabled:Bool ;
		
		var mapView:MapView;
		var mapBackground:UIComponent;
		var yahooBackground:YahooBackground;
		var cloudmadeBackground:CloudMadeBackground;
		var nullBackground:NullBackground;
		var actionBuilder:MapNavigatorActionBuilder;
		var moveMapRunner:IBufferingActionRunner;
		var mapChanged:Bool ;
		
		var lastMouseX:Int;
		var lastMouseY:Int;
		var zoomLevelDelta:Int;
		var centerXDelta:Int;
		var centerYDelta:Int;
		var mouseLatLong:LatLong;
		var backgroundTypeChanged:Bool ;
		var fadeBackgroundChanged:Bool ;
		var showElementsChanged:Bool ;
		var fadeOverlay:Shape;
		
		public function new() {
			
			maxZoomLevelDelta = 3;
			mouseWheelEnabled = true;
			mapChanged = false;
			backgroundTypeChanged = false;
			fadeBackgroundChanged = true;
			showElementsChanged = true;
			super();
			doubleClickEnabled = true;
			actionBuilder = new MapNavigatorActionBuilder(this);
		}
		
		var _map:Map;
		public function getMap():Map{
			return _map;
		}
		public function setMap(value:Map):Map{
			if (value == _map) return;
			if (_map != null) {
				removeMapListeners();
			}
			_map = value;
			if (_map != null) {
				addMapListeners();
			}
			mapChanged = true;
			invalidateProperties();
			invalidateDisplayList();
			return value;
		}
		
		var rendererFindersPackageChanged:Bool ;
		var _rendererFindersPackage:IRendererFindersPackage;
		public function getRendererFindersPackage():IRendererFindersPackage{
			return _rendererFindersPackage;
		}
		public function setRendererFindersPackage(value:IRendererFindersPackage):IRendererFindersPackage{
			if (value == _rendererFindersPackage) return;
			_rendererFindersPackage = value;
			rendererFindersPackageChanged = true;
			invalidateProperties();
			return value;
		}
		
		var _backgroundType:UInt ;
		public function getBackgroundType():UInt{
			return _backgroundType;
		}
		public function setBackgroundType(value:UInt):UInt{
			if (value == _backgroundType) return;
			_backgroundType = value;
			backgroundTypeChanged = true;
			invalidateProperties();
			return value;
		}
		
		var _fadeBackground:Bool ;
		public function getFadeBackground():Bool{
			return _fadeBackground;
		}
		public function setFadeBackground(value:Bool):Bool{
			if (value == _fadeBackground) return;
			_fadeBackground = value
			fadeBackgroundChanged = true;
			invalidateProperties();
			return value;
		}
		
		var _showElements:Bool ;
		public function getShowElements():Bool{
			return _showElements;
		}
		public function setShowElements(value:Bool):Bool{
			if (value == _showElements) return;
			_showElements = value
			showElementsChanged = true;
			invalidateProperties();
			return value;
		}
		
		override function createChildren():Void {
			super.createChildren();
			mapView = new MapView();
			fadeOverlay = new Shape();
			
			yahooBackground = new YahooBackground();
			cloudmadeBackground = new CloudMadeBackground();
			nullBackground = new NullBackground();
			mapBackground = nullBackground;
			addChildAt(mapBackground, 0);
			
			addChild(fadeOverlay);
			addChild(mapView);
		}
		
		override function commitProperties():Void {
			super.commitProperties();
			
			if (mapChanged) {
				mapChanged = false;
				zoomLevelDelta = 0;
				IMapView(mapBackground).map = map;
				mapView.map = map;
			}
			
			if (backgroundTypeChanged) {
				backgroundTypeChanged = false;

				removeChild(mapBackground);
				if (backgroundType == YAHOO_BACKGROUND) {
					mapBackground = yahooBackground;
				}
				else if (backgroundType == CLOUDMADE_BACKGROUND) {
					mapBackground = cloudmadeBackground;
				}
				else {
					mapBackground = nullBackground;
				}

				mapBackground.mouseEnabled = false;
				mapBackground.mouseChildren = false;
				IMapView(mapBackground).map = map;
				addChildAt(mapBackground, 0);
			}
			
			if (rendererFindersPackageChanged) {
				rendererFindersPackageChanged = false;
				mapView.nodeRendererFinder = rendererFindersPackage.nodeFinder;
				mapView.wayRendererFinder = rendererFindersPackage.wayFinder;
				mapView.areaRendererFinder = rendererFindersPackage.areaFinder;
			}
			
			if (zoomLevelDelta != 0) {
				zoomLevelDelta = minmax(zoomLevelDelta, -maxZoomLevelDelta, maxZoomLevelDelta);
				var oldZoomLevel:Int = map.zoomLevel;
				map.zoomLevel += zoomLevelDelta;
				zoomLevelDelta = 0;
				if (map.zoomLevel != oldZoomLevel) {
					var mousePoint:Point = map.project(mouseLatLong);
					mousePoint.offset(centerXDelta, centerYDelta);
					map.center = map.unproject(mousePoint);
				}
			}
			
			if (showElementsChanged) {
				showElementsChanged = false;
				mapView.visible = showElements;
			}
			
			if (fadeBackgroundChanged) {
				fadeBackgroundChanged = false;
				fadeOverlay.visible = fadeBackground;
			}
		}
		
		override function updateDisplayList(unscaledWidth:Int, unscaledHeight:Int):Void {
			super.updateDisplayList(unscaledWidth, unscaledHeight);
			if (map) {
				map.setSize(unscaledWidth, unscaledHeight);
			}
			graphics.clear();
			graphics.beginFill(BACKGROUND_COLOR);
			graphics.drawRect(0, 0, unscaledWidth, unscaledHeight);
			graphics.endFill();
			var graph:Graphics = fadeOverlay.graphics;
			graph.clear();
			graph.beginFill(BACKGROUND_COLOR, FADE_ALPHA);
			graph.drawRect(0, 0, unscaledWidth, unscaledHeight);
			graph.endFill();
		}
		
		function saveMousePosition():Void {
			var mousePoint:Point = new Point(map.x + mouseX, map.y + mouseY);
			mouseLatLong = map.unproject(mousePoint).normalize(MAX_LATITUDE);
			centerXDelta = map.centerX - mousePoint.x;
			centerYDelta = map.centerY - mousePoint.y;
		}
		
		function addMapListeners():Void {
			moveMapRunner = actionBuilder.buildMoveMapRunner(systemManager.stage, MAX_LATITUDE);
			moveMapRunner.activate();
			addEventListener(MouseEvent.MOUSE_WHEEL, mouseWheelHandler);
			addEventListener(MouseEvent.DOUBLE_CLICK, doubleClickHandler);
		}
		
		function removeMapListeners():Void {
			moveMapRunner.deactivate();
			moveMapRunner = null;
			removeEventListener(MouseEvent.MOUSE_WHEEL, mouseWheelHandler);
			removeEventListener(MouseEvent.DOUBLE_CLICK, doubleClickHandler);
		}
		
		function doubleClickHandler(event:MouseEvent):Void {
			zoomLevelDelta = 1;
			saveMousePosition();
			invalidateProperties();
		}
		
		function mouseWheelHandler(event:MouseEvent):Void {
			if (mouseWheelEnabled) {
				zoomLevelDelta += (event.delta > 0) ? 1 : -1;
				saveMousePosition();
				invalidateProperties();
			}
		}
	}
