package com.cloudmade.mappingtool.ui.editingHistory;

	import com.cloudmade.mappingtool.ui.controls.ComponentDecorator;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.geom.Point;
	import flash.utils.Timer;
	
	import mx.core.IUIComponent;
	import mx.events.FlexEvent;

	class AutoHideDecorator extends ComponentDecorator, implements ILockable {
		
		public var hideDelay(getHideDelay, setHideDelay) : Int;
		public var visible(null, setVisible) : Bool;
		var active:Bool ;
		var hideTimer:Timer ;
		
		public function new(component:IUIComponent) {
			
			active = false;
			hideTimer = new Timer(1000, 1);
			super(component);
			addEventListener(FlexEvent.SHOW, handleShow);
			addEventListener(FlexEvent.HIDE, handleHide);
			addEventListener(Event.ADDED_TO_STAGE, handleAddedToStage);
			addEventListener(Event.REMOVED_FROM_STAGE, handleRemovedFromStage);
			hideTimer.addEventListener(TimerEvent.TIMER_COMPLETE, handleHideTimerComplete);
		}
		
		public function getHideDelay():Int{
			return hideTimer.delay;
		}
		public function setHideDelay(value:Int):Int{
			hideTimer.delay = value;
			return value;
		}
		
		public override function setVisible(value:Bool):Bool{
			super.visible = value;
			restartHideTimer();
			return value;
		}
		
		public function lock():Void {
			handleHide(null);
		}
		
		public function unlock():Void {
			handleShow(null);
		}
		
		function startHideTimer():Void {
			hideTimer.start();
		}
		
		function stopHideTimer():Void {
			hideTimer.reset();
		}
		
		function restartHideTimer():Void {
			if (hideTimer.running) {
				stopHideTimer();
				startHideTimer();
			}
		}
		
		function isHovered():Bool {
			var localMousePoint:Point = new Point(mouseX, mouseY);
			var globalMousePoint:Point = localToGlobal(localMousePoint);
			return hitTestPoint(globalMousePoint.x, globalMousePoint.y, false);
		}
		
		function handleShow(event:FlexEvent):Void {
			active = true;
			if (!isHovered()) {
				startHideTimer();
			}
			addEventListener(MouseEvent.ROLL_OVER, handleRollOver);
			addEventListener(MouseEvent.ROLL_OUT, handleRollOut);
		}
		
		function handleHide(event:FlexEvent):Void {
			active = false;
			stopHideTimer();
			removeEventListener(MouseEvent.ROLL_OVER, handleRollOver);
			removeEventListener(MouseEvent.ROLL_OUT, handleRollOut);
		}
		
		function handleRollOver(event:MouseEvent):Void {
			stopHideTimer();
		}
		
		function handleRollOut(event:MouseEvent):Void {
			startHideTimer();
		}
		
		function handleHideTimerComplete(event:TimerEvent):Void {
			visible = false;
		}
		
		function handleAddedToStage(event:Event):Void {
			stage.addEventListener(MouseEvent.MOUSE_DOWN, handleStageMouseDown);
		}
		
		function handleRemovedFromStage(event:Event):Void {
			stage.removeEventListener(MouseEvent.MOUSE_DOWN, handleStageMouseDown);
		}
		
		function handleStageMouseDown(event:MouseEvent):Void {
			if (active && !isHovered()) {
				visible = false;
			}
		}
	}
