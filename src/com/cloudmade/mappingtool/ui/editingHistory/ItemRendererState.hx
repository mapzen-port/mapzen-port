/**
 * Enumeration for possible editing history item renderers states.
 * 
 * @see com.cloudmade.mappingtool.ui.editingHistory.IItemRenderer
 */
package com.cloudmade.mappingtool.ui.editingHistory;

	class ItemRendererState
	 {
		
		public static var EXECUTED:ItemRendererState = new ItemRendererState("executed");
		public static var EXECUTED_HOVERED:ItemRendererState = new ItemRendererState("executedHovered");
		public static var SELECTED_EXECUTED:ItemRendererState = new ItemRendererState("selectedExecuted");
		public static var SELECTED_NORMAL:ItemRendererState = new ItemRendererState("selectedNormal");
		public static var SELECTED_UNDONE:ItemRendererState = new ItemRendererState("selectedUndone");
		public static var UNDONE:ItemRendererState = new ItemRendererState("undone");
		
		var name:String;
		
		public function new(name:String) {
			this.name = name;
		}
		
		public function toString():String {
			return name;
		}
	}
