/**
 * Used for synchronization of editing history item renderers states when user hoveres some of them.
 * 
 * @see com.cloudmade.mappingtool.ui.editingHistory.ItemRendererState
 */
package com.cloudmade.mappingtool.ui.editingHistory;

	interface IItemRendererStateSynchronizer
	{
		function selectedIndex():Int;
		function selectedIndex(value:Int):Void;
		
		function hoveredIndex():Int;
		function hoveredIndex(value:Int):Void;
		
		function getItemRendererState(index:Int):ItemRendererState;
	}
