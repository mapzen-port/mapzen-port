package com.cloudmade.mappingtool.ui.editingHistory;

	import com.cloudmade.mappingtool.commands.editing.IDescribable;
	
	import flash.events.IEventDispatcher;
	
	/*[Event(name="change", type="flash.events.Event")]*/
	
	interface IFirstItemProvider implements IEventDispatcher{
		/*[Bindable("change")]*/
		function firstItem():IDescribable;
	}
