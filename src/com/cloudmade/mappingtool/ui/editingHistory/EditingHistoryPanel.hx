package com.cloudmade.mappingtool.ui.editingHistory;

	import com.cloudmade.mappingtool.editing.IEditingHistory;
	import com.cloudmade.mappingtool.model.OSMProxy;
	import com.cloudmade.mappingtool.ui.controls.NormalLabel;
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	
	import mx.collections.IList;
	import mx.containers.VBox;
	import mx.controls.Label;
	
	[ResourceBundle("EditingCommands")]

	class EditingHistoryPanel extends AutoHideDecorator {
		
		public var commandHistory(getCommandHistory, setCommandHistory) : IEditingHistory;
		public var commandList(getCommandList, setCommandList) : IList;
		public var currentIndex(getCurrentIndex, setCurrentIndex) : Int;
		var list:IVisualList;
		var firstItemProvider:IFirstItemProvider;
		
		public function new() {
			var container:VBox = new VBox();
			container.styleName = "editingHistoryContainer";
			
			super(container);
			
			var title:Label = new NormalLabel();
			title.styleName = "editingHistoryTitle";
			title.text += resourceManager.getString("EditingCommands", "undo_redo");
			container.addChild(title);
			
			list = new EditingHistoryListFactory().create();
			list.percentWidth = 100;
			list.percentHeight = 100;
			container.addChild(cast( list, DisplayObject));
			
			firstItemProvider = new FirstItemProviderFactory(
				OSMProxy.getInstance(), resourceManager).create();
			
			list.addEventListener(Event.CHANGE, handleListChange);
			firstItemProvider.addEventListener(Event.CHANGE, handleFirstItemProviderChange);
		}
		
		var _commandHistory:IEditingHistory;
		public function getCommandHistory():IEditingHistory{
			return _commandHistory;
		}
		public function setCommandHistory(value:IEditingHistory):IEditingHistory{
			_commandHistory = value;
			return value;
		}
		
		var _commandList:IList;
		public function getCommandList():IList{
			return _commandList;
		}
		public function setCommandList(value:IList):IList{
			if (value == _commandList) return;
			_commandList = value;
			var dataProvider:IList = (value != null) ?
				new EditingHistoryDataProvider(value, firstItemProvider.firstItem) : null;
			list.dataProvider = dataProvider;
			return value;
		}
		
		var _currentIndex:Int;
		public function getCurrentIndex():Int{
			return _currentIndex;
		}
		public function setCurrentIndex(value:Int):Int{
			if (value == _currentIndex) return;
			_currentIndex = value;
			list.selectedIndex = value;
			return value;
		}
		
		public override function lock():Void {
			visible = true;
			super.lock();
		}
		
		function handleListChange(event:Event):Void {
			var indexDelta:Int = list.selectedIndex - currentIndex;
			if (indexDelta < 0) {
				commandHistory.undo(Math.abs(indexDelta));
			} else {
				commandHistory.redo(indexDelta);
			}
		}
		
		function handleFirstItemProviderChange(event:Event):Void {
			if (list.dataProvider != null) {
				list.dataProvider.setItemAt(firstItemProvider.firstItem, 0);
			}
		}
	}
