package com.cloudmade.mappingtool.ui.editingHistory;

	import mx.collections.IList;
	import mx.core.IUIComponent;
	
	/*[Event(name="change", type="flash.events.Event")]*/
	
	interface IVisualList implements IUIComponent{
		function dataProvider():IList;
		function dataProvider(value:IList):Void;
		
		/*[Bindable(event="change")]*/
		function selectedIndex():Int;
		function selectedIndex(value:Int):Void;
	}
