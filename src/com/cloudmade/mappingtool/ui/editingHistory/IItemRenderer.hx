/**
 * An item renderer for displaying editing command state.
 * 
 * @see com.cloudmade.mappingtool.ui.editingHistory.ItemRendererState
 */
package com.cloudmade.mappingtool.ui.editingHistory;

	import mx.controls.listClasses.IListItemRenderer;
	
	interface IItemRenderer implements IListItemRenderer{
		function state():ItemRendererState;
		function state(value:ItemRendererState):Void;
		
		function label():String;
		function label(value:String):Void;
	}
