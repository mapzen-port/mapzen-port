/**
 * A list for displaying editing history with extended highlight functionality.
 * Requires implementation of IItemRenderer for its item renderers.
 * 
 * @see com.cloudmade.mappingtool.ui.editingHistory.IItemRenderer
 */
package com.cloudmade.mappingtool.ui.editingHistory;

	import flash.display.Graphics;
	import flash.display.Sprite;
	import flash.events.KeyboardEvent;
	
	import mx.collections.ListCollectionView;
	import mx.controls.List;
	import mx.controls.listClasses.IListItemRenderer;
	import mx.core.IFactory;
	import mx.core.ScrollPolicy;
	import mx.core.mx_internal;
	
	use namespace mx_internal;
	
	class EditingHistoryList extends List {
		
		var stateSynchronizer:IItemRendererStateSynchronizer;
		
		public function new(stateSynchronizer:IItemRendererStateSynchronizer, itemRenderer:IFactory) {
			super();
			this.stateSynchronizer = stateSynchronizer;
			this.itemRenderer = itemRenderer;
		}
		
		override function measure():Void {
			super.measure();
			if ((verticalScrollPolicy == ScrollPolicy.AUTO) &&
				verticalScrollBar && verticalScrollBar.visible) {
				measuredWidth += verticalScrollBar.minWidth;
				measuredMinWidth += verticalScrollBar.minWidth;
			}
		}
		
		override function adjustListContent(?unscaledWidth:Int = -1, ?unscaledHeight:Int = -1):Void {
			super.adjustListContent(unscaledWidth, unscaledHeight);
			var listContentHeight:Int = listContent.height - (listContent.height % rowHeight);
			listContent.setActualSize(listContent.width, listContentHeight);
		}
		
		
		function updateItemRendererState(renderer:IItemRenderer, data:Dynamic):Void {
			var index:Int = ListCollectionView(collection).getItemIndex(data);
			renderer.state = stateSynchronizer.getItemRendererState(index);
		}
		
		function updateItemRendererLabel(renderer:IItemRenderer, data:Dynamic):Void {
			renderer.label = itemToLabel(data);
		}
		
		
		override function drawHighlightIndicator(indicator:Sprite, x:Int, y:Int, width:Int, height:Int, color:UInt, itemRenderer:IListItemRenderer):Void {
			// BEGIN WORKAROUND: Forcing calling clearHighlightIndicator() when mouse intersects the bottom edge of the list.
			width = unscaledWidth - viewMetrics.left - viewMetrics.right;
			x = 0;
			
			var graph:Graphics = indicator.graphics;
			graph.clear();
			graph.beginFill(0x000000, 0);
			graph.drawRect(0, 0, width, height);
			graph.endFill();
			
			indicator.x = x;
			indicator.y = y;
			// END WORKAROUND
			
			stateSynchronizer.hoveredIndex = itemRendererToIndex(itemRenderer);
			updateItemRenderersState();
		}
		
		override function clearHighlightIndicator(indicator:Sprite, itemRenderer:IListItemRenderer):Void {
			// BEGIN WORKAROUND: see drawHighlightIndicator()
			super.clearHighlightIndicator(indicator, itemRenderer);
			// END WORKAROUND
			
			stateSynchronizer.hoveredIndex = -1;
			updateItemRenderersState();
		}
		
		mx_internal override function commitSelectedIndex(value:Int):Void {
			stateSynchronizer.selectedIndex = value;
			super.commitSelectedIndex(value);
		}
		
		override function drawSelectionIndicator(indicator:Sprite, x:Int, y:Int, width:Int, height:Int, color:UInt, itemRenderer:IListItemRenderer):Void {
			stateSynchronizer.selectedIndex = itemRendererToIndex(itemRenderer);
			updateItemRenderersState();
		}
		
		override function clearSelected(?transition:Bool = false):Void {
			super.clearSelected(false);
			updateItemRenderersState();
		}
		
		function updateItemRenderersState():Void {
			for (column in listItems) {
				if (column == null) continue;
				var renderer:IItemRenderer = cast( column[0], IItemRenderer);
				if (renderer) {
					updateItemRendererState(renderer, renderer.data);
				}
			}
		}
		
		
		public override function createItemRenderer(data:Dynamic):IListItemRenderer {
			var renderer:IItemRenderer = cast( super.createItemRenderer(data), IItemRenderer);
			updateItemRenderer(renderer, data);
			return renderer;
		}
		
		override function getReservedOrFreeItemRenderer(data:Dynamic):IListItemRenderer {
			var renderer:IItemRenderer = cast( super.getReservedOrFreeItemRenderer(data), IItemRenderer);
			if (renderer) {
				updateItemRenderer(renderer, data);
			}
			return renderer;
		}
		
		mx_internal override function setupRendererFromData(item:IListItemRenderer, wrappedData:Dynamic):Void {
			super.setupRendererFromData(item, wrappedData);
			updateItemRenderer(cast( item, IItemRenderer), wrappedData);
		}
		
		function updateItemRenderer(renderer:IItemRenderer, data:Dynamic):Void {
			updateItemRendererState(renderer, data);
			updateItemRendererLabel(renderer, data);
		}
		
		override function keyDownHandler(event:KeyboardEvent):Void {
		}
	}
