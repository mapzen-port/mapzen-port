/**
 * A factory that encapsulates EditingHistoryList creation.
 * 
 * @see com.cloudmade.mappingtool.ui.editingHistory.EditingHistoryList
 */
package com.cloudmade.mappingtool.ui.editingHistory;

	import mx.controls.List;
	import mx.core.ClassFactory;
	import mx.core.IFactory;
	
	class EditingHistoryListFactory
	 {
		
		public function new() {
		}
		
		public function create():IVisualList {
			var stateSynchronizer:IItemRendererStateSynchronizer = new ItemRendererStateSynchronizer();
			var itemRenderer:IFactory = new ClassFactory(ItemRenderer);
			var list:List = new EditingHistoryList(stateSynchronizer, itemRenderer);
			list.labelField = "description";
			list.styleName = "editingHistoryList";
			list.rowHeight = 19;
			
			var listAdapter:EditingHistoryListAdapter = new EditingHistoryListAdapter(list);
			listAdapter.percentWidth = 100;
			listAdapter.percentHeight = 100;
			return listAdapter;
		}
	}
