/**
 * Used for synchronization of editing history item renderers states when user hoveres some of them.
 * 
 * @see com.cloudmade.mappingtool.ui.editingHistory.ItemRendererState
 */
package com.cloudmade.mappingtool.ui.editingHistory;

	class ItemRendererStateSynchronizer implements IItemRendererStateSynchronizer {
		
		public var hoveredIndex(getHoveredIndex, setHoveredIndex) : Int;
		public var selectedIndex(getSelectedIndex, setSelectedIndex) : Int;
		public function new(?selectedIndex:Int = -1, ?hoveredIndex:Int = -1) {
			this.selectedIndex = selectedIndex;
			this.hoveredIndex = hoveredIndex;
		}
		
		var _selectedIndex:Int;
		public function getSelectedIndex():Int{
			return _selectedIndex;
		}
		public function setSelectedIndex(value:Int):Int{
			_selectedIndex = value;
			return value;
		}
		
		var _hoveredIndex:Int;
		public function getHoveredIndex():Int{
			return _hoveredIndex;
		}
		public function setHoveredIndex(value:Int):Int{
			_hoveredIndex = value;
			return value;
		}
		
		public function getItemRendererState(index:Int):ItemRendererState {
			if (index == selectedIndex) {
				return getSelectedRendererState();
			}
			if (index == hoveredIndex) {
				return ItemRendererState.EXECUTED_HOVERED;
			}
			var edgeIndex:Int = (hoveredIndex != -1) ? hoveredIndex : selectedIndex;
			if (index < edgeIndex) {
				return ItemRendererState.EXECUTED;
			}
			if (index > edgeIndex) {
				return ItemRendererState.UNDONE;
			}
			return null;
		}
		
		function getSelectedRendererState():ItemRendererState {
			if ((hoveredIndex == -1) || (hoveredIndex == selectedIndex)) {
				return ItemRendererState.SELECTED_NORMAL;
			}
			if (hoveredIndex < selectedIndex) {
				return ItemRendererState.SELECTED_UNDONE;
			}
			if (hoveredIndex > selectedIndex) {
				return ItemRendererState.SELECTED_EXECUTED;
			}
			return null;
		}
	}
