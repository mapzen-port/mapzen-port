package com.cloudmade.mappingtool.ui.editingHistory;

	interface ILockable
	{
		function lock():Void;
		function unlock():Void;
	}
