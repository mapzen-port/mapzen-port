package com.cloudmade.mappingtool.ui.editingHistory;

	import com.cloudmade.mappingtool.ui.controls.ComponentDecorator;
	
	import flash.events.Event;
	
	import mx.collections.IList;
	import mx.controls.List;
	import mx.events.ListEvent;
	
	/*[Event(name="change", type="flash.events.Event")]*/
	
	class EditingHistoryListAdapter extends ComponentDecorator, implements IVisualList {
		
		public var dataProvider(getDataProvider, setDataProvider) : IList;
		public var selectedIndex(getSelectedIndex, setSelectedIndex) : Int;
		var list:List;
		
		public function new(list:List) {
			super(list);
			this.list = list;
			list.addEventListener(ListEvent.CHANGE, handleListChange);
		}
		
		var _dataProvider:IList;
		public function getDataProvider():IList{
			return _dataProvider;
		}
		public function setDataProvider(value:IList):IList{
			if (value == _dataProvider) return;
			_dataProvider = value;
			list.dataProvider = value;
			return value;
		}
		
		/*[Bindable(event="change")]*/
		public function getSelectedIndex():Int{
			return list.selectedIndex;
		}
		public function setSelectedIndex(value:Int):Int{
			list.selectedIndex = value;
			scrollList(value);
			return value;
		}
		
		function scrollList(index:Int):Void {
			list.validateNow();
			list.scrollToIndex(index);
		}
		
		function handleListChange(event:ListEvent):Void {
			dispatchEvent(new Event(Event.CHANGE));
		}
	}
