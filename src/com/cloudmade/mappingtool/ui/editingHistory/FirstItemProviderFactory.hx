package com.cloudmade.mappingtool.ui.editingHistory;

	import com.cloudmade.mappingtool.commands.editing.IDescribable;
	import com.cloudmade.mappingtool.model.OSMProxy;
	
	import mx.resources.IResourceManager;
	
	[ResourceBundle("EditingCommands")]
	
	class FirstItemProviderFactory
	 {
		
		var osmProxy:OSMProxy;
		var resourceManager:IResourceManager;
		
		public function new(osmProxy:OSMProxy, resourceManager:IResourceManager) {
			this.osmProxy = osmProxy;
			this.resourceManager = resourceManager;
		}
		
		public function create():IFirstItemProvider {
			var openDescription:String = resourceManager.getString("EditingCommands", "open");
			var openItem:IDescribable = new FirstItem(openDescription);
			
			var saveDescription:String = resourceManager.getString("EditingCommands", "save");
			var saveItem:IDescribable = new FirstItem(saveDescription);
			
			return new FirstItemProvider(osmProxy, openItem, saveItem);
		}
	}
