package com.cloudmade.mappingtool.ui.editingHistory;

	import flash.events.EventDispatcher;
	
	import mx.collections.IList;
	import mx.core.EventPriority;
	import mx.events.CollectionEvent;

	class ListDecorator extends EventDispatcher, implements IList {
		
		public var length(getLength, null) : Int ;
		var list:IList;
		
		public function new(list:IList) {
			this.list = list;
			list.addEventListener(CollectionEvent.COLLECTION_CHANGE, handleListCollectionChange, false, EventPriority.BINDING + 1, true);
		}
		
		public function getLength():Int {
			return list.length;
		}
		
		public function addItem(item:Dynamic):Void {
			list.addItem(item);
		}
		
		public function addItemAt(item:Dynamic, index:Int):Void {
			list.addItemAt(item, index);
		}
		
		public function getItemAt(index:Int, ?prefetch:Int = 0):Dynamic {
			return list.getItemAt(index, prefetch);
		}
		
		public function getItemIndex(item:Dynamic):Int {
			return list.getItemIndex(item);
		}
		
		public function itemUpdated(item:Dynamic, ?property:Dynamic = null, ?oldValue:Dynamic = null, ?newValue:Dynamic = null):Void {
			list.itemUpdated(item, property, oldValue, newValue);
		}
		
		public function removeAll():Void {
			list.removeAll();
		}
		
		public function removeItemAt(index:Int):Dynamic {
			return list.removeItemAt(index);
		}
		
		public function setItemAt(item:Dynamic, index:Int):Dynamic {
			return list.setItemAt(item, index);
		}
		
		public function toArray():Array<Dynamic> {
			return list.toArray();
		}
		
		function handleListCollectionChange(event:CollectionEvent):Void {
			dispatchEvent(event);
		}
	}
