package com.cloudmade.mappingtool.ui.editingHistory;

	import com.cloudmade.mappingtool.commands.editing.IDescribable;
	import com.cloudmade.mappingtool.events.OSMProxyResultEvent;
	import com.cloudmade.mappingtool.model.OSMProxy;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	/*[Event(name="change", type="flash.events.Event")]*/
	
	class FirstItemProvider extends EventDispatcher, implements IFirstItemProvider {
		
		public var firstItem(getFirstItem, null) : IDescribable ;
		var osmProxy:OSMProxy;
		var openItem:IDescribable;
		var saveItem:IDescribable;
		
		public function new(osmProxy:OSMProxy, openItem:IDescribable, saveItem:IDescribable) {
			super();
			this.osmProxy = osmProxy;
			this.openItem = openItem;
			this.saveItem = saveItem;
			_firstItem = openItem;
			osmProxy.addEventListener(OSMProxyResultEvent.SAVE, handleOsmProxySave, false, 0, true);
		}
		
		/*[Bindable("change")]*/
		var _firstItem:IDescribable;
		public function getFirstItem():IDescribable {
			return _firstItem;
		}
		
		function handleOsmProxySave(event:OSMProxyResultEvent):Void {
			_firstItem = saveItem;
			dispatchEvent(new Event(Event.CHANGE));
		}
	}
