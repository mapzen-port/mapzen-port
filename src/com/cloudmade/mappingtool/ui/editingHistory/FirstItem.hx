package com.cloudmade.mappingtool.ui.editingHistory;

	import com.cloudmade.mappingtool.commands.editing.IDescribable;

	class FirstItem implements IDescribable {
		
		public var description(getDescription, null) : String ;
		public function new(description:String) {
			_description = description;
		}
		
		var _description:String;
		public function getDescription():String {
			return _description;
		}
	}
