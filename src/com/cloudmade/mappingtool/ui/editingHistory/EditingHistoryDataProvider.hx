package com.cloudmade.mappingtool.ui.editingHistory;

	import mx.collections.IList;
	import mx.events.CollectionEvent;
	import mx.events.CollectionEventKind;
	import mx.events.PropertyChangeEvent;

	class EditingHistoryDataProvider extends ListDecorator {
		
		public var length(getLength, null) : Int ;
		var firstItem:Dynamic;
		
		public function new(list:IList, firstItem:Dynamic) {
			super(list);
			this.firstItem = firstItem;
		}
		
		public override function getLength():Int {
			return super.length + 1;
		}
		
		public override function addItemAt(item:Dynamic, index:Int):Void {
			super.addItemAt(item, index - 1);
		}
		
		public override function getItemAt(index:Int, ?prefetch:Int = 0):Dynamic {
			if (index == 0) {
				return firstItem;
			}
			return super.getItemAt(index - 1, prefetch);
		}
		
		public override function getItemIndex(item:Dynamic):Int {
			if (item == firstItem) {
				return 0;
			}
			return super.getItemIndex(item) + 1;
		}
		
		public override function itemUpdated(item:Dynamic, ?property:Dynamic = null, ?oldValue:Dynamic = null, ?newValue:Dynamic = null):Void {
			if (item == firstItem) {
				firstItemUpdated(property, oldValue, newValue);
			}
			else {
				super.itemUpdated(item, property, oldValue, newValue);
			}
		}
		
		function firstItemUpdated(property:Dynamic, oldValue:Dynamic, newValue:Dynamic):Void {
			var propertyEvent:PropertyChangeEvent = PropertyChangeEvent.createUpdateEvent(firstItem, property, oldValue, newValue);
			dispatchFirstItemUpdateCollectionEvent(propertyEvent);
			dispatchFirstItemUpdatePropertyChangeEvent(propertyEvent);
		}
		
		function dispatchFirstItemUpdateCollectionEvent(propEvent:PropertyChangeEvent):Void {
			dispatchEvent(createCollectionEvent(CollectionEventKind.UPDATE, propEvent));
		}
		
		function createCollectionEvent(kind:String, propEvent:PropertyChangeEvent):CollectionEvent {
			var event:CollectionEvent = new CollectionEvent(CollectionEvent.COLLECTION_CHANGE);
			event.kind = kind;
			event.items.push(propEvent);
			return event;
		}
		
		function dispatchFirstItemUpdatePropertyChangeEvent(propEvent:PropertyChangeEvent):Void {
			var event:PropertyChangeEvent = cast( propEvent.clone(), PropertyChangeEvent);
			var index:UInt = 0;
			event.property = index.toString() + "." + event.property;
			dispatchEvent(event);
		}
		
		public override function removeItemAt(index:Int):Dynamic {
			return super.removeItemAt(index - 1);
		}
		
		public override function setItemAt(item:Dynamic, index:Int):Dynamic {
			if (index == 0) {
				return setFirstItem(item);
			}
			return super.setItemAt(item, index - 1);
		}
		
		function setFirstItem(item:Dynamic):Dynamic {
			var oldItem:Dynamic = firstItem;
			firstItem = item;
			
			var index:Int = 0;
			var propertyEvent:PropertyChangeEvent = PropertyChangeEvent.createUpdateEvent(null, index, oldItem, item);
			
			var collectionEvent:CollectionEvent = createCollectionEvent(CollectionEventKind.REPLACE, propertyEvent);
			collectionEvent.location = index;
			
			dispatchEvent(collectionEvent);
			dispatchEvent(propertyEvent);
			
			return oldItem;
		}
		
		public override function toArray():Array<Dynamic> {
			var array:Array<Dynamic> = super.toArray();
			array.unshift(firstItem);
			return array;
		}
		
		override function handleListCollectionChange(event:CollectionEvent):Void {
			if (event.kind == CollectionEventKind.RESET) {
				var newEvent:CollectionEvent = cast( event.clone(), CollectionEvent);
				newEvent.kind = CollectionEventKind.REFRESH;
				dispatchEvent(newEvent);
			}
			else {
				super.handleListCollectionChange(event);
			}
		}
	}
