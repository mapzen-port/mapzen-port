package com.cloudmade.mappingtool.ui.context;

	import com.cloudmade.mappingtool.commands.editing.events.ChangeElementTagEvent;
	import com.cloudmade.mappingtool.commands.editing.events.ElementTagEditingEvent;
	import com.cloudmade.mappingtool.map.model.Tag;
	import com.cloudmade.mappingtool.ui.context.fields.ITagField;
	import com.cloudmade.mappingtool.ui.context.fields.TagFieldChangeEvent;
	import com.cloudmade.mappingtool.ui.context.fields.TagFieldEvent;
	
	/*[Event(name="addElementTag", type="com.cloudmade.mappingtool.commands.editing.events.ElementTagEditingEvent")]*/
	/*[Event(name="removeElementTag", type="com.cloudmade.mappingtool.commands.editing.events.ElementTagEditingEvent")]*/
	/*[Event(name="changeElementTagValue", type="com.cloudmade.mappingtool.commands.editing.events.ChangeElementTagEvent")]*/
	
	class TagsContextPanel extends ContextPanel {
		
		public function new() {
			super();
			addEventListener(TagFieldEvent.TAG_ADD, tagField_tagAddHandler);
			addEventListener(TagFieldChangeEvent.TAG_VALUE_CHANGE, tagField_tagValueChangeHandler);
		}
		
		function createAddElementTagEvent(tag:Tag):ElementTagEditingEvent {
			return new ElementTagEditingEvent(ElementTagEditingEvent.ADD_ELEMENT_TAG, selection, tag, true);
		}
		
		function createRemoveElementTagEvent(tag:Tag):ElementTagEditingEvent {
			return new ElementTagEditingEvent(ElementTagEditingEvent.REMOVE_ELEMENT_TAG, selection, tag, true);
		}
		
		function createChangeElementTagValueEvent(tag:Tag, newValue:String):ElementTagEditingEvent {
			return new ChangeElementTagEvent(ChangeElementTagEvent.CHANGE_ELEMENT_TAG_VALUE, selection, tag, newValue, true);
		}
		
		function isValidTagValue(tagValue:String):Bool {
			return (tagValue != null) && (tagValue != "");
		}
		
		function dispatchTagEditingEvent(event:ElementTagEditingEvent, source:TagFieldEvent):Void {
			var tagField:ITagField = cast( source.target, ITagField);
			event.fieldName = tagField.fieldName;
			dispatchEvent(event);
		}
		
		function tagField_tagAddHandler(event:TagFieldEvent):Void {
			var tag:Tag = cast( event.tag, Tag);
			if (isValidTagValue(tag.value)) {
				var editingEvent:ElementTagEditingEvent = createAddElementTagEvent(tag);
				dispatchTagEditingEvent(editingEvent, event);
			}
			event.stopPropagation();
		}
		
		function tagField_tagValueChangeHandler(event:TagFieldChangeEvent):Void {
			var tag:Tag = cast( event.tag, Tag);
			var editingEvent:ElementTagEditingEvent;
			if (isValidTagValue(event.newValue)) {
				editingEvent = createChangeElementTagValueEvent(tag, event.newValue);
			}
			else {
				editingEvent = createRemoveElementTagEvent(tag);
			}
			dispatchTagEditingEvent(editingEvent, event);
			event.stopPropagation();
		}
	}
