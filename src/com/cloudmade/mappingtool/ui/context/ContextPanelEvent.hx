package com.cloudmade.mappingtool.ui.context;

	import flash.events.Event;
	
	class ContextPanelEvent extends Event {
		
		public static var METADATA_VISIBLE_CHANGE:String = "metadataVisibleChange";
		
		public function new(type:String) {
			super(type);
		}
		
		public override function clone():Event {
			return new ContextPanelEvent(type);
		}
	}
