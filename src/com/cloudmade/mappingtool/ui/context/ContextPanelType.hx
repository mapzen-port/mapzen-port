package com.cloudmade.mappingtool.ui.context;

	class ContextPanelType
	 {
		public function new() { }
		
		public static var NULL:Int = 0;
		public static var WAY:Int = 1;
		public static var AREA:Int = 2;
		public static var NODE:Int = 3;
		public static var WAY_NODE:Int = 4;
	}
