package com.cloudmade.mappingtool.ui.context;

	import com.cloudmade.mappingtool.map.model.Element;
	
	import flash.events.Event;
	
	import mx.core.LayoutContainer;
	
	/*[Event(name="metadataVisibleChange", type="com.cloudmade.mappingtool.ui.context.ContextPanelEvent")]*/

	class ContextPanel extends LayoutContainer, implements IContextPanel {
		
		public var metadataVisible(getMetadataVisible, setMetadataVisible) : Bool;
		public var selection(getSelection, setSelection) : Element;
		public function new() {
			super();
		}
		
		var _metadataVisible:Bool;
		/*[Bindable(event="metadataVisibleChange")]*/
		public function getMetadataVisible():Bool{
			return _metadataVisible;
		}
		public function setMetadataVisible(value:Bool):Bool{
			if (value == _metadataVisible) return;
			_metadataVisible = value;
			dispatchEvent(new ContextPanelEvent(ContextPanelEvent.METADATA_VISIBLE_CHANGE));
			return value;
		}
		
		var _selection:Element;
		/*[Bindable(event="selectionChanged")]*/
		public function getSelection():Element{
			return _selection;
		}
		public function setSelection(value:Element):Element{
			if (value == _selection) return;
			_selection = value;
			dispatchEvent(new Event("selectionChanged"));
			return value;
		}
	}
