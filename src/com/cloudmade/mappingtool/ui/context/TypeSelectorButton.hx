package com.cloudmade.mappingtool.ui.context;

	import com.cloudmade.mappingtool.config.IElementPreset;
	
	import flash.display.DisplayObject;
	
	import mx.controls.Button;
	import mx.controls.ButtonLabelPlacement;
	import mx.core.IDataRenderer;
	import mx.core.IFlexDisplayObject;
	import mx.core.mx_internal;
	
	use namespace mx_internal;

	class TypeSelectorButton extends Button {
		
		public var horizontalPaddings(getHorizontalPaddings, null) : Int ;
		public var selectedPreset(getSelectedPreset, setSelectedPreset) : IElementPreset;
		public var verticalPaddings(getVerticalPaddings, null) : Int ;
		var nullPreset:IElementPreset;
		var presetRenderer:IDataRenderer;
		
		public function new(nullPreset:IElementPreset, presetRenderer:IDataRenderer) {
			super();
			width = 90;
			height = 64;
			toggle = true;
			styleName = "typeSelectorButton";
			labelPlacement = ButtonLabelPlacement.LEFT;
			this.nullPreset = nullPreset;
			this.presetRenderer = presetRenderer;
		}
		
		var selectedPresetChanged:Bool ;
		var _selectedPreset:IElementPreset;
		public function getSelectedPreset():IElementPreset{
			return _selectedPreset;
		}
		public function setSelectedPreset(value:IElementPreset):IElementPreset{
			if (value == _selectedPreset) return;
			_selectedPreset = value;
			selectedPresetChanged = true;
			invalidateProperties();
			invalidateSize();
			return value;
		}
		
		function getHorizontalPaddings():Int {
			return getStyle("paddingLeft") + getStyle("paddingRight");
		}
		
		function getVerticalPaddings():Int {
			return getStyle("paddingTop") + getStyle("paddingBottom");
		}
		
		override function createChildren():Void {
			super.createChildren();
			addChild(cast( presetRenderer, DisplayObject));
		}
		
		override function commitProperties():Void {
			super.commitProperties();
			if (selectedPresetChanged) {
				selectedPresetChanged = false;
				commitSelectedPreset();
			}
		}
		
		function commitSelectedPreset():Void {
			presetRenderer.data = selectedPreset ? selectedPreset : nullPreset;
		}
		
		override function measure():Void {
			var renderer:IFlexDisplayObject = cast( presetRenderer, IFlexDisplayObject);
			measuredWidth = renderer.measuredWidth + horizontalPaddings;
			measuredMinWidth = measuredWidth;
			measuredHeight = renderer.measuredHeight + verticalPaddings;
			measuredMinHeight = measuredHeight;
		}
		
		override function updateDisplayList(unscaledWidth:Int, unscaledHeight:Int):Void {
			super.updateDisplayList(unscaledWidth, unscaledHeight);
			var renderer:IFlexDisplayObject = cast( presetRenderer, IFlexDisplayObject);
			renderer.setActualSize(unscaledWidth - horizontalPaddings, unscaledHeight - verticalPaddings);
			renderer.move(getStyle("paddingLeft"), getStyle("paddingTop"));
			
		}
		
		mx_internal override function layoutContents(unscaledWidth:Int, unscaledHeight:Int, offset:Bool):Void {
			super.layoutContents(unscaledWidth, unscaledHeight, offset);
			setChildIndex(cast( presetRenderer, DisplayObject), numChildren - 1);
		}
	}
