package com.cloudmade.mappingtool.ui.context;

	import com.cloudmade.mappingtool.map.model.Element;
	
	import flash.events.IEventDispatcher;
	
	/*[Event(name="metadataVisibleChange", type="com.cloudmade.mappingtool.ui.context.ContextPanelEvent")]*/
	
	interface IContextPanel implements IEventDispatcher{
		/*[Bindable(event="metadataVisibleChange")]*/
		function metadataVisible():Bool;
		function metadataVisible(value:Bool):Void;
		
		function selection(value:Element):Void;
	}
