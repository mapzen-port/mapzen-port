package com.cloudmade.mappingtool.ui.context;

	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.controls.Button;
	import mx.core.UIComponent;

	class TypeSelectorMenuAnchor extends UIComponent {
		
		public var selectorButton(getSelectorButton, setSelectorButton) : Button;
		var selectorMenu:UIComponent;
		
		public function new(selectorMenu:UIComponent) {
			super();
			this.selectorMenu = selectorMenu;
		}
		
		var _selectorButton:Button;
		public function getSelectorButton():Button{
			return _selectorButton;
		}
		public function setSelectorButton(value:Button):Button{
			if (value == _selectorButton) return;
			if (_selectorButton) {
				deactivate();
			}
			_selectorButton = value;
			if (_selectorButton) {
				activate();
			}
			return value;
		}
		
		override function createChildren():Void {
			super.createChildren();
			addChild(selectorMenu);
		}
		
		// It is working because selectorMenu calls parent.invalidateDisplayList() when its size changes.
		override function updateDisplayList(unscaledWidth:Int, unscaledHeight:Int):Void {
			super.updateDisplayList(unscaledWidth, unscaledHeight);
			var menuWidth:Int = selectorMenu.getExplicitOrMeasuredWidth();
			var menuHeight:Int = selectorMenu.getExplicitOrMeasuredHeight();
			selectorMenu.setActualSize(menuWidth, menuHeight);
			selectorMenu.move(0, -menuHeight);
		}
		
		function activate():Void {
			if (stage) {
				addedToStageHandler(null);
			}
			addEventListener(Event.ADDED_TO_STAGE, addedToStageHandler);
			addEventListener(Event.REMOVED_FROM_STAGE, removedFromStageHandler);
		}
		
		function deactivate():Void {
			removedFromStageHandler(null);
			removeEventListener(Event.ADDED_TO_STAGE, addedToStageHandler);
			removeEventListener(Event.REMOVED_FROM_STAGE, removedFromStageHandler);
		}
		
		function showSelectorMenu():Void {
			selectorMenu.visible = true;
			stage.addEventListener(MouseEvent.MOUSE_DOWN, stage_mouseDownHandler, false, 0, true);
		}
		
		function hideSelectorMenu():Void {
			selectorMenu.visible = false;
			stage.removeEventListener(MouseEvent.MOUSE_DOWN, stage_mouseDownHandler);
		}
		
		function selectorButton_changeHandler(event:Event):Void {
			if (selectorButton.selected) {
				showSelectorMenu();
			} else {
				hideSelectorMenu();
			}
		}
		
		function stage_mouseDownHandler(event:MouseEvent):Void {
			var clickObject:DisplayObject = cast( event.target, DisplayObject);
			var isOutsideOfMenuClick:Bool = !selectorMenu.contains(clickObject);
			var isOutsideOfButtonClick:Bool = !selectorButton.contains(clickObject);
			if (isOutsideOfMenuClick && isOutsideOfButtonClick) {
				selectorButton.selected = false;
				hideSelectorMenu();
			}
		}
		
		function addedToStageHandler(event:Event):Void {
			selectorButton.addEventListener(Event.CHANGE, selectorButton_changeHandler, false, 0, true);
		}
		
		function removedFromStageHandler(event:Event):Void {
			stage.removeEventListener(MouseEvent.MOUSE_DOWN, stage_mouseDownHandler);
			selectorButton.removeEventListener(Event.CHANGE, selectorButton_changeHandler);
		}
	}
