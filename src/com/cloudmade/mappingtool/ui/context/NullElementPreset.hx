package com.cloudmade.mappingtool.ui.context;

	import mx.core.IFactory;
	import com.cloudmade.mappingtool.config.IElementPreset;

	class NullElementPreset implements IElementPreset {
		
		public var defaultTags(getDefaultTags, null) : Array<Dynamic>
		;
		public var icon(getIcon, null) : IFactory
		;
		public var name(getName, null) : String
		;
		public function new()
		{
		}

		public function getIcon():IFactory
		{
			return null;
		}
		
		public function getDefaultTags():Array<Dynamic>
		{
			return null;
		}
		
		public function getName():String
		{
			return null;
		}
		
	}
