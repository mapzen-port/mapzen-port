package com.cloudmade.mappingtool.ui.context;

	import com.cloudmade.mappingtool.config.IElementTypeFinder;
	import com.cloudmade.mappingtool.map.model.Element;
	import com.cloudmade.mappingtool.map.model.Map;
	import com.cloudmade.mappingtool.map.model.Node;
	import com.cloudmade.mappingtool.map.model.Way;
	
	class ContextPanelTypeRecognizer
	 {
		
		var map:Map;
		var areaTypeFinder:IElementTypeFinder;
		
		public function new(map:Map, areaTypeFinder:IElementTypeFinder) {
			this.map = map;
			this.areaTypeFinder = areaTypeFinder;
		}
		
		public function getContextPanelType(selection:Element):Int {
			if (Std.is( selection, Way)) {
				return getWayContextPanel(cast( selection, Way));
			} else if (Std.is( selection, Node)) {
				return getNodeContextPanel(cast( selection, Node));
			}
			return ContextPanelType.NULL;
		}
		
		function getWayContextPanel(selection:Way):Int {
			if (isArea(selection)) {
				return ContextPanelType.AREA;
			}
			return ContextPanelType.WAY;
		}
		
		function getNodeContextPanel(selection:Node):Int {
			var nodeWays:Array<Dynamic> = map.getNodeWays(cast( selection, Node));
			if (nodeWays.length == 0) {
				return ContextPanelType.NODE;
			}
			var hasArea:Bool = false;
			for (way in nodeWays) {
				if (isArea(way)) {
					hasArea = true;
					break;
				}
			}
			if (!hasArea) {
				return ContextPanelType.WAY_NODE;
			}
			return ContextPanelType.NULL;
		}
		
		function isArea(way:Way):Bool {
			var tags:Array<Dynamic> = way.tags.source;
			return areaTypeFinder.findType(tags) != null;
		}
	}
