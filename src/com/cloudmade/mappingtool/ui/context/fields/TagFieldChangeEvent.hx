package com.cloudmade.mappingtool.ui.context.fields;

	import com.cloudmade.mappingtool.map.model.ITag;
	
	import flash.events.Event;

	class TagFieldChangeEvent extends TagFieldEvent {
		
		public static var TAG_VALUE_CHANGE:String = "tagValueChange";
		
		public var newValue:String;
		
		public function new(type:String, tag:ITag, newValue:String, ?bubbles:Bool = false) {
			super(type, tag, bubbles);
			this.newValue = newValue;
		}
		
		public override function clone():Event {
			return new TagFieldChangeEvent(type, tag, newValue, bubbles);
		}
	}
