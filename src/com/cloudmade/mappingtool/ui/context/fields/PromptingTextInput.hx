/*
Copyright (c) 2007 FlexLib Contributors.  See:
    http://code.google.com/p/flexlib/wiki/ProjectContributors

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

package com.cloudmade.mappingtool.ui.context.fields;


   import flash.events.Event;
   import flash.events.FocusEvent;
   import flash.text.TextField;

   import mx.controls.TextInput;
   import mx.events.FlexEvent;

   /**
    * The <code>PromptingTextInput</code> component is a small enhancement to
    * standard <code>TextInput</code>.  It adds the ability to specify a prompt
    * value that displays when the text is empty, similar to how the prompt
    * property of the <code>ComboBox</code> behaves when there is no selected value.
    */
   class PromptingTextInput extends TextInput {
   	/** Flag to indicate if the text is empty or not */
   	
   	public var displayAsPassword(getDisplayAsPassword, setDisplayAsPassword) : Bool ;
   	public var prompt(getPrompt, setPrompt) : String ;
   	public var promptFormat(getPromptFormat, setPromptFormat) : String ;
   	public var text(getText, setText) : String
   	;
   	/** Flag to indicate if the text is empty or not */
   	var _textEmpty:Bool;

   	/**
   	 * Flag to prevent us from re-inserting the prompt if the text is cleared
   	 * while the component still has focus.
   	 */
   	var _currentlyFocused:Bool ;


   	/**
   	 * Constructor
   	 */
   	public function new()
   	{
   		
   		_currentlyFocused = false;
   		_textEmpty = true;

   		addEventListener( Event.CHANGE, handleChange );
   		addEventListener( FocusEvent.FOCUS_IN, handleFocusIn );
   		addEventListener( FocusEvent.FOCUS_OUT, handleFocusOut );
   	}

   	// ==============================================================
   	//	prompt
   	// ==============================================================

   	/** Storage for the prompt property */
   	var _prompt:String ;

   	/**
   	 * The string to use as the prompt value
   	 */
   	public function getPrompt():String {
   		return _prompt;
   	}

   	/*[Bindable]*/
   	public function setPrompt( value:String ):String {
   		_prompt = value;

   		invalidateProperties();
   		return value;
   	}

   	// ==============================================================
   	//	promptFormat
   	// ==============================================================

   	/** Storage for the promptFormat property */
   	var _promptFormat:String ;

   	/**
   	 * A format string to specify how the prompt is displayed.  This is typically
   	 * an HTML string that can set the font color and style.  Use <code>[prompt]</code>
   	 * within the string as a replacement token that will be replaced with the actual
   	 * prompt text.
   	 *
   	 * The default value is "&lt;font color="#999999"&gt;&lt;i&gt;[prompt]&lt;/i&gt;&lt;/font&gt;"
   	 */
   	public function getPromptFormat():String {
   		return _promptFormat;
   	}

   	public function setPromptFormat( value:String ):String {
   		_promptFormat = value;
   		// Check to see if the replacement code is found in the new format string
   		if ( _promptFormat.indexOf( "[prompt]" ) < 0 )
   		{
   			// TODO: Log error with the logging framework, or just use trace?
   			//trace( "PromptingTextInput warning: prompt format does not contain [prompt] replacement code." );
   		}

   		invalidateDisplayList();
   		return value;
   	}

   	// ==============================================================
   	//	text
   	// ==============================================================


   	/**
   	 * Override the behavior of text so that it doesn't take into account
   	 * the prompt.  If the prompt is displaying, the text is just an empty
   	 * string.
   	 */
   	/*[Bindable("textChanged")]*/
       [CollapseWhiteSpace]
       /*[NonCommittingChangeEvent("change")]*/
   	public override function setText( value:String ):String
   	{
   		// changed the test to also test for null values, not just 0 length
   		// if we were passed undefined or null then the zero length test would
   		// still return false. - Doug McCune
   		_textEmpty = (!value) || value.length == 0;
   		super.text = value;
   		invalidateDisplayList();
   		return value;
   		// changed the test to also test for null values, not just 0 length
   		// if we were passed undefined or null then the zero length test would
   		// still return false. - Doug McCune
   	}

   	public override function getText():String
   	{
   		// If the text has changed
   		if ( _textEmpty )
   		{
   			// Skip the prompt text value
   			return "";
   		}
   		else
   		{
   			return super.text;
   		}
   	}



   	/**
   	 * We store a local copy of displayAsPassword. We need to keep this so that we can
   	 * change it to false if we're showing the prompt. Then we change it back (if it was
   	 * set to true) once we're no longer showing the prompt.
   	 */
   	var _displayAsPassword:Bool ;

   	public override function setDisplayAsPassword(value:Bool):Bool {
   		_displayAsPassword = value;
   		super.displayAsPassword = value;
   		return value;
   	}
   	public override function getDisplayAsPassword():Bool {
   		return _displayAsPassword;
   	}

   	// ==============================================================
   	//	overriden methods
   	// ==============================================================

   	/**
   	 * @private
   	 *
   	 * Determines if the prompt needs to be displayed.
   	 */
   	override function updateDisplayList( unscaledWidth:Int, unscaledHeight:Int ):Void
   	{
   		// If the text is empty and a prompt value is set and the
   		// component does not currently have focus, then the component
   		// needs to display the prompt
   		if ( _textEmpty && _prompt != "" && !_currentlyFocused )
   		{
   			if ( _promptFormat == "" )
   			{
   				super.text = _prompt;
   			}
   			else
   			{
   				super.htmlText = _promptFormat.replace( ~/\[prompt\]/g, _prompt );
   			}

   			if ( super.displayAsPassword )
   			{
   				//If we're showing the prompt and we have displayAsPassword set then
   				//we need to set it to false while the prompt is showing.
   				var oldVal:Bool = _displayAsPassword;
   				super.displayAsPassword = false;
   				_displayAsPassword = oldVal;
   			}
   		}
   		else
   		{
   			if ( super.displayAsPassword != _displayAsPassword )
   			{
   				super.displayAsPassword = _displayAsPassword;
   			}
   		}

   		super.updateDisplayList( unscaledWidth, unscaledHeight );
   	}

   	// ==============================================================
   	//	event handlers
   	// ==============================================================

   	/**
   	 * @private
   	 */
   	function handleChange( event:Event ):Void
   	{
   		_textEmpty = super.text.length == 0;
   	}

   	/**
   	 * @private
   	 *
   	 * When the component recevies focus, check to see if the prompt
   	 * needs to be cleared or not.
   	 */
   	function handleFocusIn( event:FocusEvent ):Void
   	{
   		_currentlyFocused = true;

   		// If the text is empty, clear the prompt
   		if ( _textEmpty )
   		{
   			super.htmlText = "";
   			// KLUDGE: Have to validate now to avoid a bug where the format
   			// gets "stuck" even though the text gets cleared.
   			validateNow();
   		}
   	}

   	/**
   	 * @private
   	 *
   	 * When the component loses focus, check to see if the prompt needs
   	 * to be displayed or not.
   	 */
   	function handleFocusOut( event:FocusEvent ):Void
   	{
   		_currentlyFocused = false;

   		// If the text is empty, put the prompt back
   		invalidateDisplayList();
   	}
   } // end class
