package com.cloudmade.mappingtool.ui.context.fields;

	import com.cloudmade.mappingtool.map.model.ITag;
	
	import flash.events.Event;

	class TagFieldEvent extends Event {
		
		public static var TAG_ADD:String = "tagAdd";
		
		public var tag:ITag;
		
		public function new(type:String, tag:ITag, ?bubbles:Bool = false) {
			super(type, bubbles);
			this.tag = tag;
		}
		
		public override function clone():Event {
			return new TagFieldEvent(type, tag, bubbles);
		}
	}
