package com.cloudmade.mappingtool.ui.context.fields;

	import mx.controls.ComboBox;
	
	class ContextComboBoxTagField extends ComboBoxTagField {
		
		public function new() {
			super();
		}
		
		override function createComboBox():ComboBox {
			var comboBox:ComboBox = super.createComboBox();
			comboBox.styleName = "contextComboBox";
			return comboBox;
		}
	}
