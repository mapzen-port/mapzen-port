package com.cloudmade.mappingtool.ui.context.fields;

	import com.cloudmade.mappingtool.map.model.Tag;
	
	import mx.controls.TextInput;
	
	class ContextTextTagField extends TextTagField {
		
		public var prompt(getPrompt, setPrompt) : String;
		var textInput:PromptingTextInput;
		
		public function new() {
			super();
			tabEnabled = true;
		}
		
		var promptChanged:Bool;
		var _prompt:String;
		public function getPrompt():String{
			return _prompt;
		}
		public function setPrompt(value:String):String{
			if (value == _prompt) return;
			_prompt = value;
			promptChanged = true;
			invalidateProperties();
			return value;
		}
		
		override function createTextInput():TextInput {
			textInput = new PromptingTextInput();
			textInput.maxChars = Tag.MAX_CHARS;
			textInput.styleName = "contextTextInput";
			textInput.promptFormat = "<font color='#666666'>[prompt]</font>";
			return textInput;
		}
		
		override function commitProperties():Void {
			super.commitProperties();
			if (promptChanged) {
				promptChanged = false;
				commitPrompt();
			}
		}
		
		function commitPrompt():Void {
			textInput.prompt = prompt;
		}
	}
