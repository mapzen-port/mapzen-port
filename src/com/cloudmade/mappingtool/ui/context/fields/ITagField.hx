package com.cloudmade.mappingtool.ui.context.fields;

	import com.cloudmade.mappingtool.commands.editing.events.TagFieldName;
	
	import flash.events.IEventDispatcher;
	
	import mx.collections.ArrayList;
	
	/*[Event(name="tagAdd", type="com.cloudmade.mappingtool.ui.context.fields.TagFieldEvent")]*/
	/*[Event(name="tagValueChange", type="com.cloudmade.mappingtool.ui.context.fields.TagFieldChangeEvent")]*/
	
	interface ITagField implements IEventDispatcher{
		function fieldName():TagFieldName;
		
		function tagValue():String;
		
		function tagKey():String;
		function tagKey(value:String):Void;
		
		function tags():ArrayList;
		function tags(value:ArrayList):Void;
		
		function useBubbling():Bool;
		function useBubbling(value:Bool):Void;
	}
