package com.cloudmade.mappingtool.ui.context.fields;

	import com.cloudmade.mappingtool.config.AnyElementType;
	import com.cloudmade.mappingtool.config.AnyValueTag;
	import com.cloudmade.mappingtool.config.ElementPreset;
	import com.cloudmade.mappingtool.config.ElementType;
	import com.cloudmade.mappingtool.config.ElementTypePreset;
	import com.cloudmade.mappingtool.config.IElementPreset;
	import com.cloudmade.mappingtool.config.IElementType;
	import com.cloudmade.mappingtool.config.IElementTypePreset;
	import com.cloudmade.mappingtool.config.IStringLocalizer;
	import com.cloudmade.mappingtool.config.LocalizedElementPreset;
	import com.cloudmade.mappingtool.config.conditions.filters.ITagsFilter;
	import com.cloudmade.mappingtool.config.conditions.filters.IsTagsFilter;
	import com.cloudmade.mappingtool.config.conditions.filters.NullTagsFilter;
	import com.cloudmade.mappingtool.config.conditions.types.ITagsType;
	import com.cloudmade.mappingtool.config.conditions.types.IsTagsType;
	import com.cloudmade.mappingtool.map.model.ITag;
	import com.cloudmade.mappingtool.map.model.Tag;
	
	class WayTypesFactory
	 {
		
		var localizer:IStringLocalizer;
		
		public function new(localizer:IStringLocalizer) {
			this.localizer = localizer;
		}
		
		public function create():Array<Dynamic> {
			var wayTypes:Array<Dynamic> = new Array();
			
			wayTypes.push(createDefaultWayType("default", 0));
			
			var tunnelTag:ITag = new Tag("tunnel", "yes");
			wayTypes.push(createWayType("tunnel", tunnelTag, tunnelTag, 1));
			
			var bridgeTypeTag:ITag = new AnyValueTag("bridge");
			var bridgeDefaultTag:ITag = new Tag("bridge", "yes");
			wayTypes.push(createWayType("bridge", bridgeTypeTag, bridgeDefaultTag, 1));
			
			return wayTypes;
		}
		
		function createDefaultWayType(name:String, priority:Int):IElementTypePreset {
			var preset:IElementPreset = createElementPreset(name, new Array());
			var type:IElementType = new AnyElementType(priority);
			var nullTagsFilter:ITagsFilter = new NullTagsFilter();
			return new ElementTypePreset(preset, type, nullTagsFilter);
		}
		
		function createWayType(name:String, typeTag:ITag, defaultTag:ITag, priority:Int):IElementTypePreset {
			var preset:IElementPreset = createElementPreset(name, [defaultTag]);
			
			var tagsType:ITagsType = new IsTagsType(typeTag);
			var tagsFilter:ITagsFilter = new IsTagsFilter(typeTag);
			
			return createElementTypePreset(preset, tagsType, tagsFilter, priority);
		}
		
		function createElementPreset(name:String, defaultTags:Array<Dynamic>):IElementPreset {
			var preset:IElementPreset = new ElementPreset(name, defaultTags, null);
			return new LocalizedElementPreset(preset, localizer);
		}
		
		function createElementTypePreset(preset:IElementPreset, tagsType:ITagsType, tagsFilter:ITagsFilter, priority:Int):IElementTypePreset {
			var type:IElementType = new ElementType(tagsType, priority);
			return new ElementTypePreset(preset, type, tagsFilter);
		}
	}
