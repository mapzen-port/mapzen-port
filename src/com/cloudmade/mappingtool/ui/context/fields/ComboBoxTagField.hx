package com.cloudmade.mappingtool.ui.context.fields;

	import mx.collections.ICollectionView;
	import mx.collections.IViewCursor;
	import mx.controls.ComboBox;
	import mx.core.UIComponent;
	import mx.events.ListEvent;
	
	class ComboBoxTagField extends TagField {
		
		public var dataField(getDataField, setDataField) : String;
		public var dataProvider(getDataProvider, setDataProvider) : Dynamic;
		public var labelFunction(getLabelFunction, setLabelFunction) : Dynamic;
		public var prompt(getPrompt, setPrompt) : String;
		var comboBox:ComboBox;
		
		public function new() {
			super();
		}
		
		var _dataField:String;
		public function getDataField():String{
			return _dataField;
		}
		public function setDataField(value:String):String{
			_dataField = value;
			return value;
		}
		
		var dataProviderCollection:ICollectionView;
		var dataProviderChanged:Bool ;
		var _dataProvider:Dynamic;
		public function getDataProvider():Dynamic{
			return _dataProvider;
		}
		public function setDataProvider(value:Dynamic):Dynamic{
			if (value == _dataProvider) return;
			_dataProvider = value;
			dataProviderChanged = true;
			invalidateProperties();
			return value;
		}
		
		var promptChanged:Bool ;
		var _prompt:String;
		public function getPrompt():String{
			return _prompt;
		}
		public function setPrompt(value:String):String{
			if (value == _prompt) return;
			_prompt = value;
			promptChanged = true;
			invalidateProperties();
			return value;
		}
		
		var labelFunctionChanged:Bool ;
		var _labelFunction:Dynamic;
		public function getLabelFunction():Dynamic{
			return _labelFunction;
		}
		public function setLabelFunction(value:Dynamic):Dynamic{
			if (value == _labelFunction) return;
			_labelFunction = value;
			labelFunctionChanged = true;
			invalidateProperties();
			return value;
		}
		
		override function createField():UIComponent {
			comboBox = createComboBox();
			comboBox.addEventListener(ListEvent.CHANGE, comboBox_changeHandler);
			return comboBox;
		}
		
		function createComboBox():ComboBox {
			return new ComboBox();
		}
		
		override function commitProperties():Void {
			if (dataProviderChanged) {
				dataProviderChanged = false;
				commitDataProvider();
			}
			super.commitProperties();
			if (promptChanged) {
				promptChanged = true;
				commitPrompt();
			}
			if (labelFunctionChanged) {
				labelFunctionChanged = false;
				commitLabelFunction();
			}
		}
		
		function commitDataProvider():Void {
			comboBox.dataProvider = dataProvider;
			dataProviderCollection = cast( comboBox.dataProvider, ICollectionView);
		}
		
		function commitPrompt():Void {
			comboBox.prompt = prompt;
		}
		
		function commitLabelFunction():Void {
			comboBox.labelFunction = labelFunction;
		}
		
		override function commitTagValue():Void {
			var foundItem:Dynamic = null;
			var dataCursor:IViewCursor = dataProviderCollection.createCursor();
			while (!dataCursor.afterLast) {
				if (dataCursor.current.data == tagValue) {
					foundItem = dataCursor.current;
					break;
				}
				dataCursor.moveNext();
			}
			comboBox.selectedItem = foundItem;
		}
		
		function comboBox_changeHandler(event:ListEvent):Void {
			setTagValue(comboBox.selectedItem.data);
		}
	}
