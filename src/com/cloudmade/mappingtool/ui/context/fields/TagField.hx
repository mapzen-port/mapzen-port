/**
 * A base class for all tag fields.
 */
package com.cloudmade.mappingtool.ui.context.fields;

	import com.cloudmade.mappingtool.commands.editing.events.TagFieldName;
	import com.cloudmade.mappingtool.map.model.ITag;
	import com.cloudmade.mappingtool.map.model.Tag;
	import com.cloudmade.mappingtool.ui.controls.ComponentDecorator;
	
	import mx.collections.ArrayList;
	import mx.core.UIComponent;
	import mx.events.CollectionEvent;
	
	/*[Event(name="tagAdd", type="com.cloudmade.mappingtool.ui.context.fields.TagFieldEvent")]*/
	/*[Event(name="tagValueChange", type="com.cloudmade.mappingtool.ui.context.fields.TagFieldChangeEvent")]*/
	
	class TagField extends ComponentDecorator, implements ITagField {
		
		public var fieldName(getFieldName, setFieldName) : TagFieldName;
		public var fieldTag(getFieldTag, setFieldTag) : ITag;
		public var tagKey(getTagKey, setTagKey) : String;
		public var tagValue(getTagValue, null) : String ;
		public var tags(getTags, setTags) : ArrayList;
		public var useBubbling(getUseBubbling, setUseBubbling) : Bool;
		public function new() {
			super(createField());
			tabEnabled = false;
			tabChildren = true;
			fieldName = TagFieldName.METADATA
		}
		
		var _fieldName:TagFieldName;
		public function getFieldName():TagFieldName{
			return _fieldName;
		}
		public function setFieldName(value:TagFieldName):TagFieldName{
			_fieldName = value;
			return value;
		}
		
		var _tagValue:String;
		/*[Bindable(event="tagValueChange")]*/
		public function getTagValue():String {
			return _tagValue;
		}
		function setTagValue(value:String):Void {
			if (value == _tagValue) return;
			_tagValue = value;
			if (fieldTag.value != null) {
				dispatchEvent(new TagFieldChangeEvent(TagFieldChangeEvent.TAG_VALUE_CHANGE, fieldTag, value, useBubbling));
			} else {
				var newTag:ITag = new Tag(fieldTag.key, value);
				dispatchEvent(new TagFieldEvent(TagFieldEvent.TAG_ADD, newTag, useBubbling));
			}
		}
		
		var tagKeys:Array<Dynamic> ;
		public function getTagKey():String{
			return tagKeys.join("|");
		}
		public function setTagKey(value:String):String{
			tagKeys = (value != null) ? value.split("|") : new Array();
			invalidateFieldTag();
			return value;
		}
		
		var _tags:ArrayList;
		public function getTags():ArrayList{
			return _tags;
		}
		public function setTags(value:ArrayList):ArrayList{
			if (value == _tags) return;
			if (_tags) {
				_tags.removeEventListener(CollectionEvent.COLLECTION_CHANGE, tags_collectionChangeHandler);
			}
			_tags = value;
			if (_tags) {
				_tags.addEventListener(CollectionEvent.COLLECTION_CHANGE, tags_collectionChangeHandler, false, 0, true);
			}
			invalidateFieldTag();
			return value;
		}
		
		function tags_collectionChangeHandler(evnet:CollectionEvent):Void {
			invalidateFieldTag();
		}
		
		var _useBubbling:Bool ;
		public function getUseBubbling():Bool{
			return _useBubbling;
		}
		public function setUseBubbling(value:Bool):Bool{
			_useBubbling = value;
			return value;
		}
		
		var fieldTagInvalid:Bool ;
		var _fieldTag:ITag;
		function getFieldTag():ITag{
			return _fieldTag;
		}
		function setFieldTag(value:ITag):ITag{
			_fieldTag = value;
			_tagValue = _fieldTag ? _fieldTag.value : null;
			commitTagValue();
			return value;
		}
		
		function invalidateFieldTag():Void {
			fieldTagInvalid = true;
			invalidateProperties();
		}
		
		function createField():UIComponent {
			return null;
		}
		
		override function commitProperties():Void {
			super.commitProperties();
			if (fieldTagInvalid) {
				fieldTagInvalid = false;
				updateFieldTag();
			}
		}
		
		function commitTagValue():Void {
		}
		
		function updateFieldTag():Void {
			var foundTag:ITag = new Tag(tagKeys[0], null);
			var tagsSource:Array<Dynamic> = tags ? tags.source : null;
			for (tag in tagsSource) {
				if (tagKeys.indexOf(tag.key) != -1) {
					foundTag = tag;
					break;
				}
			}
			fieldTag = foundTag;
		}
	}
