package com.cloudmade.mappingtool.ui.context.fields;

	import flash.events.FocusEvent;
	
	import mx.controls.TextInput;
	import mx.core.UIComponent;
	import mx.events.FlexEvent;
	import mx.managers.IFocusManagerComponent;
	
	class TextTagField extends TagField {
		
		var textInput:TextInput;
		
		public function new() {
			super();
		}
		
		function createTextInput():TextInput {
			return new TextInput();
		}
		
		override function createField():UIComponent {
			textInput = createTextInput();
			textInput.addEventListener(FocusEvent.FOCUS_OUT, textInput_focusOutHandler);
			textInput.addEventListener(FlexEvent.ENTER, textInput_enterHandler);
			return textInput;
		}
		
		override function commitTagValue():Void {
			textInput.text = tagValue;
		}
		
		override function measure():Void {
			super.measure();
			measuredWidth = 0;
			measuredMinWidth = 0;
		}
		
		function changeTagValue():Void {
			setTagValue(textInput.text);
		}
		
		function textInput_focusOutHandler(event:FocusEvent):Void {
			changeTagValue();
		}
		
		function textInput_enterHandler(event:FlexEvent):Void {
			changeTagValue();
		}
	}
