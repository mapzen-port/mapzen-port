package com.cloudmade.mappingtool.ui.context;

	import com.cloudmade.mappingtool.config.IElementTypePreset;
	
	import flash.events.Event;
	
	class ElementTypePresetSelectEvent extends Event {
		
		public static var PRESET_SELECT:String = "presetSelect";
		
		public var oldPreset:IElementTypePreset;
		public var newPreset:IElementTypePreset;
		
		public function new(type:String, oldPreset:IElementTypePreset, newPreset:IElementTypePreset) {
			super(type);
			this.oldPreset = oldPreset;
			this.newPreset = newPreset;
		}
		
		public override function clone():Event {
			return new ElementTypePresetSelectEvent(type, oldPreset, newPreset);
		}
	}
