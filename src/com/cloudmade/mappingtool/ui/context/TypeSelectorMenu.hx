package com.cloudmade.mappingtool.ui.context;

	import com.cloudmade.mappingtool.config.IElementTypePreset;
	import com.cloudmade.mappingtool.config.IPresetCategory;
	
	import mx.controls.List;
	import mx.core.UIComponent;
	import mx.events.ListEvent;

	class TypeSelectorMenu extends UIComponent {
		
		public var presetCategories(getPresetCategories, setPresetCategories) : Array<Dynamic>;
		public var selectedPreset(getSelectedPreset, setSelectedPreset) : IElementTypePreset;
		var categoryList:List;
		var presetList:List;
		var categoryListWidth:Int;
		var presetListWidth:Int;
		
		public function new(?categoryListWidth:Int = 120, ?presetListWidth:Int = 150) {
			super();
			this.categoryListWidth = categoryListWidth;
			this.presetListWidth = presetListWidth;
			width = categoryListWidth + presetListWidth;
		}
		
		var presetCategoriesChanged:Bool ;
		var _presetCategories:Array<Dynamic>;
		public function getPresetCategories():Array<Dynamic>{
			return _presetCategories;
		}
		public function setPresetCategories(value:Array<Dynamic>):Array<Dynamic>{
			if (value == _presetCategories) return;
			_presetCategories = value;
			presetCategoriesChanged = true;
			invalidateProperties();
			invalidateSize();
			return value;
		}
		
		var selectedPresetChanged:Bool ;
		var _selectedPreset:IElementTypePreset;
		public function getSelectedPreset():IElementTypePreset{
			return _selectedPreset;
		}
		public function setSelectedPreset(value:IElementTypePreset):IElementTypePreset{
			if (value == _selectedPreset) return;
			setSelectedPreset(value);
			selectedPresetChanged = true;
			invalidateProperties();
			return value;
		}
		
		function setSelectedPreset(preset:IElementTypePreset):Void {
			_selectedPreset = preset;
		}
		
		override function createChildren():Void {
			super.createChildren();
			categoryList = createCategoryList();
			categoryList.addEventListener(ListEvent.CHANGE, categoryList_itemClickHandler);
			addChild(categoryList);
			presetList = createPresetList();
			presetList.addEventListener(ListEvent.CHANGE, presetList_itemClickHandler);
			addChild(presetList);
		}
		
		function createCategoryList():List {
			var categoryList:List = new List();
			categoryList.styleName ="categoryDropDownList";
			categoryList.labelField = "name";
			categoryList.width = categoryListWidth;
			return categoryList;
		}
		
		function createPresetList():List {
			var presetList:List = new List();
			presetList.visible = false;
			presetList.styleName="presetsDropDownList";
			presetList.labelField = "name";
			presetList.width = presetListWidth;
			return presetList;
		}
		
		override function commitProperties():Void {
			super.commitProperties();
			if (presetCategoriesChanged) {
				presetCategoriesChanged = false;
				commitPresetCategories();
			}
			if (selectedPresetChanged) {
				selectedPresetChanged = false;
				commitSelectedPreset();
			}
		}
		
		function commitPresetCategories():Void {
			categoryList.rowCount = presetCategories ? presetCategories.length : 0;
			categoryList.dataProvider = presetCategories;
			clearSelection();
			invalidateSize();
		}
		
		function commitSelectedPreset():Void {
			var category:IPresetCategory = findPresetCategory(selectedPreset);
			if (category) {
				selectCategory(category);
			} else {
				clearSelection();
			}
		}
		
		function selectCategory(category:IPresetCategory):Void {
			categoryList.selectedItem = category;
			
			var presets:Array<Dynamic> = category.presets;
			presetList.dataProvider = presets;
			if (presets.indexOf(selectedPreset) != -1) {
				presetList.selectedItem = selectedPreset;
			}
			presetList.visible = true;
			invalidateSize();
		}
		
		function selectPreset(preset:IElementTypePreset):Void {
			var oldPreset:IElementTypePreset = selectedPreset;
			setSelectedPreset(preset);
			dispatchEvent(new ElementTypePresetSelectEvent(ElementTypePresetSelectEvent.PRESET_SELECT, oldPreset, preset));
		}
		
		function clearSelection():Void {
			categoryList.selectedItem = null;
			presetList.dataProvider = null;
			presetList.visible = false;
			invalidateSize();
		}
		
		function findPresetCategory(preset:IElementTypePreset):IPresetCategory {
			var foundCategory:IPresetCategory = null;
			for (category in presetCategories) {
				if (category.presets.indexOf(selectedPreset) != -1) {
					foundCategory = category;
					break;
				}
			}
			return foundCategory;
		}
		
		override function measure():Void {
			super.measure();
			if (presetList.visible) {
				measureWidthWithPresetList();
			} else {
				measureWidthWithoutPresetList();
			}
			measuredHeight = categoryList.measuredHeight;
			measuredMinHeight = categoryList.measuredMinHeight;
		}
		
		function measureWidthWithPresetList():Void {
			measuredWidth = categoryList.measuredWidth + presetList.measuredWidth;
			measuredMinWidth = categoryList.measuredMinWidth + presetList.measuredMinWidth;
		}
		
		function measureWidthWithoutPresetList():Void {
			measuredWidth = categoryList.measuredWidth;
			measuredMinWidth = categoryList.measuredMinWidth;
		}
		
		override function updateDisplayList(unscaledWidth:Int, unscaledHeight:Int):Void {
			super.updateDisplayList(unscaledWidth, unscaledHeight);
			var categoryListWidth:Int = categoryList.getExplicitOrMeasuredWidth();
			categoryList.setActualSize(categoryListWidth, unscaledHeight);
			presetList.setActualSize(unscaledWidth - categoryListWidth, unscaledHeight);
			presetList.move(categoryListWidth, 0);
		}
		
		function categoryList_itemClickHandler(event:ListEvent):Void {
			selectCategory(cast( categoryList.selectedItem, IPresetCategory));
		}
		
		function presetList_itemClickHandler(event:ListEvent):Void {
			selectPreset(cast( presetList.selectedItem, IElementTypePreset));
		}
	}
