package com.cloudmade.mappingtool.ui.context;

	import com.cloudmade.mappingtool.commands.editing.events.ChangeElementTagsEvent;
	import com.cloudmade.mappingtool.config.ElementPreset;
	import com.cloudmade.mappingtool.config.ElementTypeFinder;
	import com.cloudmade.mappingtool.config.HighestPriorityElementTypeSelector;
	import com.cloudmade.mappingtool.config.IElementPreset;
	import com.cloudmade.mappingtool.config.IElementTypeFinder;
	import com.cloudmade.mappingtool.config.IElementTypePreset;
	import com.cloudmade.mappingtool.config.IElementTypeSelector;
	import com.cloudmade.mappingtool.config.IStringLocalizer;
	import com.cloudmade.mappingtool.config.LocalizedElementPreset;
	import com.cloudmade.mappingtool.config.PresetCategoriesToPresetListConvertor;
	import com.cloudmade.mappingtool.config.StringLocalizer;
	import com.cloudmade.mappingtool.map.model.Element;
	import com.cloudmade.mappingtool.ui.presets.PresetRenderer;
	
	import mx.core.ClassFactory;
	import mx.core.ContainerLayout;
	import mx.core.IFactory;
	import mx.events.CollectionEvent;
	
	/*[Event(name="changeElementTags", type="com.cloudmade.mappingtool.commands.editing.events.ChangeElementTagsEvent")]*/
	
	[ResourceBundle("NullPreset")]
	
	class TypeContextPanel extends TagsContextPanel, implements ITypeContextPanel {
		
		public var presetCategories(getPresetCategories, setPresetCategories) : Array<Dynamic>;
		public var selection(null, setSelection) : Element;
		var typeSelectorAnchor:TypeSelectorMenuAnchor;
		
		var elementPresets:Array<Dynamic>;
		var typeSelectorMenu:TypeSelectorMenu;
		var typeSelectorButton:TypeSelectorButton;
		
		public function new() {
			super();
			layout = ContainerLayout.HORIZONTAL;
		}
		
		var presetCategoriesChanged:Bool ;
		var _presetCategories:Array<Dynamic>;
		public function getPresetCategories():Array<Dynamic>{
			return _presetCategories;
		}
		public function setPresetCategories(value:Array<Dynamic>):Array<Dynamic>{
			if (value == _presetCategories) return;
			_presetCategories = value;
			presetCategoriesChanged = true;
			invalidateProperties();
			return value;
		}
		
		var selectionChanged:Bool ;
		public override function setSelection(value:Element):Element{
			if (value == selection) return;
			if (selection) {
				selection.tags.removeEventListener(CollectionEvent.COLLECTION_CHANGE, selectionTags_collectionChangeHandler);
			}
			super.selection = value;
			if (selection) {
				selection.tags.addEventListener(CollectionEvent.COLLECTION_CHANGE, selectionTags_collectionChangeHandler, false, 0, true);
			}
			selectionChanged = true;
			invalidateProperties();
			return value;
		}
		
		override function createChildren():Void {
			super.createChildren();
			
			typeSelectorMenu = new TypeSelectorMenu();
			typeSelectorMenu.visible = false;
			
			var nullPreset:IElementPreset = createNullPreset();
			var presetRenderer:PresetRenderer = new PresetRenderer();
			presetRenderer.styleName = "typeSelectorLabel";
			typeSelectorButton = new TypeSelectorButton(nullPreset, presetRenderer);
			addChildAt(typeSelectorButton, 0);
			
			typeSelectorAnchor = new TypeSelectorMenuAnchor(typeSelectorMenu);
			typeSelectorAnchor.selectorButton = typeSelectorButton;
			typeSelectorAnchor.includeInLayout = false;
			addChildAt(typeSelectorAnchor, 0);
			
			typeSelectorMenu.addEventListener(ElementTypePresetSelectEvent.PRESET_SELECT, typeSelectorMenu_presetSelectHandler);
		}
		
		function createNullPreset():IElementPreset {
			var iconClass:Class<Dynamic> = resourceManager.getClass("NullPreset", "icon");
			var iconFactory:IFactory = new ClassFactory(iconClass);
			var preset:IElementPreset = new ElementPreset("name", null, iconFactory);
			var localizer:IStringLocalizer = new StringLocalizer(resourceManager, "NullPreset");
			var localizedPreset:IElementPreset = new LocalizedElementPreset(preset, localizer);
			return localizedPreset;
		}
		
		override function commitProperties():Void {
			super.commitProperties();
			if (presetCategoriesChanged) {
				presetCategoriesChanged = false;
				selectionChanged = true;
				commitPresetCategories();
			}
			if (selectionChanged) {
				selectionChanged = false;
				commitSelection();
			}
		}
		
		function commitPresetCategories():Void {
			var presetsConvertor:PresetCategoriesToPresetListConvertor = new PresetCategoriesToPresetListConvertor();
			elementPresets = presetsConvertor.convert(presetCategories);
			typeSelectorMenu.presetCategories = presetCategories;
		}
		
		function commitSelection():Void {
			if (selection) {
				updateTypePreset();
			} else {
				selectTypePreset(null);
			}
		}
		
		function findTypePreset(typePresets:Array<Dynamic>):IElementTypePreset {
			var selectionTags:Array<Dynamic> = selection.tags.toArray();
			var typeSelector:IElementTypeSelector = new HighestPriorityElementTypeSelector();
			var typeFinder:IElementTypeFinder = new ElementTypeFinder(typePresets, typeSelector);
			return cast( typeFinder.findType(selectionTags), IElementTypePreset);
		}
		
		function updateTypePreset():Void {
			var typePreset:IElementTypePreset = findTypePreset(elementPresets);
			selectTypePreset(typePreset);
		}
		
		function selectTypePreset(preset:IElementTypePreset):Void {
			typeSelectorMenu.selectedPreset = preset;
			typeSelectorButton.selectedPreset = preset;
		}
		
		function updateTypeSelectorButton():Void {
			typeSelectorButton.selectedPreset = typeSelectorMenu.selectedPreset;
		}
		
		function changeElementType(oldPreset:IElementTypePreset, newPreset:IElementTypePreset):Void {
			var selectionTags:Array<Dynamic> = selection.tags.toArray();
			var oldTags:Array<Dynamic> = oldPreset ? oldPreset.filterTags(selectionTags) : new Array();
			var newTags:Array<Dynamic> = newPreset ? newPreset.defaultTags : new Array();
			var event:ChangeElementTagsEvent = new ChangeElementTagsEvent(ChangeElementTagsEvent.CHANGE_ELEMENT_TAGS, selection, oldTags, newTags, true);
			event.oldType = oldPreset.name;
			event.newType = newPreset.name;
			dispatchEvent(event);
		}
		
		function selectionTags_collectionChangeHandler(event:CollectionEvent):Void {
			selectionChanged = true;
			invalidateProperties();
		}
		
		function typeSelectorMenu_presetSelectHandler(event:ElementTypePresetSelectEvent):Void {
			changeElementType(event.oldPreset, event.newPreset);
			updateTypeSelectorButton();
		}
	}
