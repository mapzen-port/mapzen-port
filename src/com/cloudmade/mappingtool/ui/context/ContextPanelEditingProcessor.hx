package com.cloudmade.mappingtool.ui.context;

	import com.cloudmade.mappingtool.actions.ITriggerHandler;
	import com.cloudmade.mappingtool.commands.editing.events.ChangeElementTagEvent;
	import com.cloudmade.mappingtool.commands.editing.events.ChangeElementTagsEvent;
	import com.cloudmade.mappingtool.commands.editing.events.EditingEvent;
	import com.cloudmade.mappingtool.commands.editing.events.ElementTagEditingEvent;
	import com.cloudmade.mappingtool.commands.editing.events.WayNodeEditingEvent;
	import com.cloudmade.mappingtool.services.MapEditService;
	
	class ContextPanelEditingProcessor implements ITriggerHandler {
		
		var panelStack:ContextPanelStack;
		var editService:MapEditService;
		
		public function new(panelStack:ContextPanelStack, editService:MapEditService) {
			this.panelStack = panelStack;
			this.editService = editService;
		}
		
		public function activate():Void {
			panelStack.addEventListener(ElementTagEditingEvent.ADD_ELEMENT_TAG, panelStack_editingHandler);
			panelStack.addEventListener(ElementTagEditingEvent.REMOVE_ELEMENT_TAG, panelStack_editingHandler);
			panelStack.addEventListener(ChangeElementTagEvent.CHANGE_ELEMENT_TAG_VALUE, panelStack_editingHandler);
			panelStack.addEventListener(ChangeElementTagsEvent.CHANGE_ELEMENT_TAGS, panelStack_editingHandler);
			panelStack.addEventListener(WayNodeEditingEvent.SPLIT_NODE_WAYS, panelStack_editingHandler);
			panelStack.addEventListener(WayNodeEditingEvent.BREAK_NODE_WAYS, panelStack_editingHandler);
		}
		
		public function deactivate():Void {
			panelStack.removeEventListener(ElementTagEditingEvent.ADD_ELEMENT_TAG, panelStack_editingHandler);
			panelStack.removeEventListener(ElementTagEditingEvent.REMOVE_ELEMENT_TAG, panelStack_editingHandler);
			panelStack.removeEventListener(ChangeElementTagEvent.CHANGE_ELEMENT_TAG_VALUE, panelStack_editingHandler);
			panelStack.removeEventListener(ChangeElementTagsEvent.CHANGE_ELEMENT_TAGS, panelStack_editingHandler);
			panelStack.removeEventListener(WayNodeEditingEvent.SPLIT_NODE_WAYS, panelStack_editingHandler);
			panelStack.removeEventListener(WayNodeEditingEvent.BREAK_NODE_WAYS, panelStack_editingHandler);
		}
		
		function panelStack_editingHandler(event:EditingEvent):Void {
			editService.executeEditingCommand(event);
			event.stopPropagation();
		}
	}
