package com.cloudmade.mappingtool.ui.context;

	import com.cloudmade.mappingtool.actions.ITriggerHandler;
	import com.cloudmade.mappingtool.config.IElementTypeFinder;
	import com.cloudmade.mappingtool.map.model.Map;
	import com.cloudmade.mappingtool.services.MapEditService;
	
	class ContextPanelFactory
	 {
		
		var panelStack:ContextPanelStack;
		var map:Map;
		var editService:MapEditService;
		var areaTypeFinder:IElementTypeFinder;
		
		public function new(panelStack:ContextPanelStack, map:Map, editService:MapEditService, areaTypeFinder:IElementTypeFinder) {
			this.panelStack = panelStack;
			this.map = map;
			this.editService = editService;
			this.areaTypeFinder = areaTypeFinder;
		}
		
		public function create():IContextPanel {
			var editingProcessor:ITriggerHandler = new ContextPanelEditingProcessor(panelStack, editService);
			editingProcessor.activate();
			var typeRecognizer:ContextPanelTypeRecognizer = new ContextPanelTypeRecognizer(map, areaTypeFinder);
			var contextPanel:IContextPanel = new ContextPanelProcessor(panelStack, typeRecognizer);
			return contextPanel;
		}
	}
