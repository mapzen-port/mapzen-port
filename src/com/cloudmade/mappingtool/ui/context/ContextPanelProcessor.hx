package com.cloudmade.mappingtool.ui.context;

	import com.cloudmade.mappingtool.map.model.Element;
	
	import flash.events.EventDispatcher;
	
	import mx.core.Container;
	
	/*[Event(name="metadataVisibleChange", type="com.cloudmade.mappingtool.ui.context.ContextPanelEvent")]*/

	class ContextPanelProcessor extends EventDispatcher, implements IContextPanel {
		
		public var metadataVisible(getMetadataVisible, setMetadataVisible) : Bool;
		public var selection(null, setSelection) : Element;
		var selectedPanel:IContextPanel;
		var panelStack:ContextPanelStack;
		var typeRecognizer:ContextPanelTypeRecognizer;
		
		public function new(panelStack:ContextPanelStack, typeRecognizer:ContextPanelTypeRecognizer) {
			super();
			this.panelStack = panelStack;
			this.typeRecognizer = typeRecognizer;
		}
		
		var _metadataVisible:Bool;
		/*[Bindable(event="metadataVisibleChange")]*/
		public function getMetadataVisible():Bool{
			return _metadataVisible;
		}
		public function setMetadataVisible(value:Bool):Bool{
			if (value == _metadataVisible) return;
			_metadataVisible = value;
			if (selectedPanel) {
				selectedPanel.metadataVisible = value;
			}
			dispatchEvent(new ContextPanelEvent(ContextPanelEvent.METADATA_VISIBLE_CHANGE));
			return value;
		}
		
		public function setSelection(value:Element):Element{
			purgeSelectedPanel();
			selectNewPanel(value);
			setupSelectedPanel(value);
			return value;
		}
		
		function selectNewPanel(selection:Element):Void {
			var newPanel:IContextPanel = getPanel(selection);
			panelStack.selectedChild = cast( newPanel, Container);
			selectedPanel = newPanel;
		}
		
		function getPanel(selection:Element):IContextPanel {
			var panelType:Int = typeRecognizer.getContextPanelType(selection);
			var panel:IContextPanel = panelStack.getContextPanel(panelType);
			return panel;
		}
		
		function purgeSelectedPanel():Void {
			if (!selectedPanel) return;
			selectedPanel.selection = null;
			selectedPanel.removeEventListener(ContextPanelEvent.METADATA_VISIBLE_CHANGE, selectedPanel_metadataVisibleChangeHandler);
			selectedPanel = null;
		}
		
		function setupSelectedPanel(selection:Element):Void {
			if (!selectedPanel) return;
			selectedPanel.selection = selection;
			selectedPanel.metadataVisible = metadataVisible;
			selectedPanel.addEventListener(ContextPanelEvent.METADATA_VISIBLE_CHANGE, selectedPanel_metadataVisibleChangeHandler, false, 0, true);
		}
		
		function selectedPanel_metadataVisibleChangeHandler(event:ContextPanelEvent):Void {
			metadataVisible = selectedPanel.metadataVisible;
		}
	}
