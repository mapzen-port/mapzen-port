package com.cloudmade.mappingtool.ui.context;

	import com.cloudmade.mappingtool.map.model.Element;
	
	import mx.core.Container;

	class NullContextPanel extends Container, implements IContextPanel {
		
		public var metadataVisible(getMetadataVisible, setMetadataVisible) : Bool;
		public var selection(null, setSelection) : Element;
		public function new(){
			super();
		}
		
		public function getMetadataVisible():Bool{
			return false;
		}
		public function setMetadataVisible(value:Bool):Bool{
			return value;
	}
		
		public function setSelection(value:Element):Element{
			return value;
	}
	}
