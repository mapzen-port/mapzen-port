package com.cloudmade.mappingtool.ui.actions;

	import flash.display.InteractiveObject;
	import flash.events.Event;
	import flash.geom.Point;
	
	class BaseMoveAction
	 {
		
		var lastMouseGlobalPosition:Point;
		
		public function new() {
		}
		
		function getMouseGlobalPosition(event:Event):Point {
			var target:InteractiveObject = cast( event.target, InteractiveObject);
			var mouseLocalPosition:Point = new Point(target.mouseX, target.mouseY);
			return target.localToGlobal(mouseLocalPosition);
		}
	}
