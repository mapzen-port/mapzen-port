package com.cloudmade.mappingtool.ui.actions.editing.triggers;

	import com.cloudmade.mappingtool.actions.triggers.ConditionalTrigger;
	import com.cloudmade.mappingtool.map.view.IAreaRenderer;
	
	import flash.events.Event;
	import flash.events.IEventDispatcher;

	class AreaRendererTrigger extends ConditionalTrigger {
		
		public function new(dispatcher:IEventDispatcher, eventType:String) {
			super(dispatcher, eventType);
		}
		
		override function triggerFilter(event:Event):Bool {
			return Std.is( event.target, IAreaRenderer);
		}
	}
