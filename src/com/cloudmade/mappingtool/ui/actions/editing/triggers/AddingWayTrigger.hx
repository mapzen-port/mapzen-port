package com.cloudmade.mappingtool.ui.actions.editing.triggers;

	import com.cloudmade.mappingtool.actions.triggers.ConditionalTrigger;
	import com.cloudmade.mappingtool.events.MapEditServiceEvent;
	import com.cloudmade.mappingtool.services.MapEditService;
	
	import flash.events.Event;
	
	class AddingWayTrigger extends ConditionalTrigger {
		
		var editService:MapEditService;
		var triggerAddingWayValue:Bool;
		
		public function new(editService:MapEditService, triggerAddingWayValue:Bool) {
			super(editService, MapEditServiceEvent.ADDING_WAY_CHANGE);
			this.editService = editService;
			this.triggerAddingWayValue = triggerAddingWayValue;
		}
		
		override function triggerFilter(event:Event):Bool {
			return editService.addingWay == triggerAddingWayValue;
		}
	}
