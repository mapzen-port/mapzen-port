package com.cloudmade.mappingtool.ui.actions.editing.triggers;

	import com.cloudmade.mappingtool.actions.triggers.ConditionalTrigger;
	import com.cloudmade.mappingtool.events.MapEditServiceSelectionChangeEvent;
	import com.cloudmade.mappingtool.map.model.Way;
	import com.cloudmade.mappingtool.services.MapEditService;
	
	import flash.events.Event;

	class SelectWayTrigger extends ConditionalTrigger {
		
		var editService:MapEditService;
		
		public function new(editService:MapEditService) {
			super(editService, MapEditServiceSelectionChangeEvent.SELECTION_CHANGE);
			this.editService = editService;
		}
		
		override function triggerFilter(event:Event):Bool {
			return Std.is( editService.selection, Way);
		}
	}
