package com.cloudmade.mappingtool.ui.actions.editing.triggers;

	import com.cloudmade.mappingtool.map.view.INodeRenderer;
	import com.cloudmade.mappingtool.services.MapEditService;
	
	import flash.events.Event;
	import flash.events.IEventDispatcher;
	import flash.events.MouseEvent;

	class AddWayFromNodeTrigger extends NodeRendererTrigger {
		
		var editService:MapEditService;
		
		public function new(dispatcher:IEventDispatcher, editService:MapEditService) {
			super(dispatcher, MouseEvent.DOUBLE_CLICK);
			this.editService = editService;
		}
		
		override function triggerFilter(event:Event):Bool {
			var isNodeRenderer:Bool = super.triggerFilter(event);
			var isWaysCategory:Bool = editService.selectedCategory == MapEditService.WAYS;
			var isAreasCategory:Bool = editService.selectedCategory == MapEditService.AREAS;
			return !editService.addingWay && isNodeRenderer && (isWaysCategory || isAreasCategory);
		}
	}
