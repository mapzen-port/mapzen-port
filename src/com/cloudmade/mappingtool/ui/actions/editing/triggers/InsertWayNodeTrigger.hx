package com.cloudmade.mappingtool.ui.actions.editing.triggers;

	import com.cloudmade.mappingtool.services.MapEditService;
	
	import flash.events.Event;
	import flash.events.IEventDispatcher;
	import flash.events.MouseEvent;

	class InsertWayNodeTrigger extends WayRendererTrigger {
		
		var editService:MapEditService;
		
		public function new(dispatcher:IEventDispatcher, editService:MapEditService) {
			super(dispatcher, MouseEvent.DOUBLE_CLICK);
			this.editService = editService;
		}
		
		/*override protected function triggerFilter(event:Event):Boolean {
			var isWayRenderer:Boolean = super.triggerFilter(event);
			var isNodesCategory:Boolean = editService.selectedCategory == MapEditService.NODES;
			return isWayRenderer && isNodesCategory;
		}*/
	}
