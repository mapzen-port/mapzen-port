package com.cloudmade.mappingtool.ui.actions.editing;

	import com.cloudmade.mappingtool.map.geo.LatLong;
	import com.cloudmade.mappingtool.map.model.Map;
	import com.cloudmade.mappingtool.utils.PolylinePointProjection;
	
	import flash.display.InteractiveObject;
	import flash.geom.Point;
	
	class InsertWayNodePosition
	 {
		
		public var nodeIndex(getNodeIndex, null) : Int ;
		public var nodePosition(getNodePosition, null) : LatLong ;
		var map:Map;
		var editor:InteractiveObject;
		
		public function new(editor:InteractiveObject, map:Map) {
			this.editor = editor;
			this.map = map;
		}
		
		var _nodeIndex:Int;
		public function getNodeIndex():Int {
			return _nodeIndex;
		}
		
		var _nodePosition:LatLong;
		public function getNodePosition():LatLong {
			return _nodePosition;
		}
		
		public function calculatePosition(wayId:String):Void {
			var wayPoints:Array<Dynamic> = map.getWay(wayId).getNodePoints();
			var projection:PolylinePointProjection = new PolylinePointProjection(wayPoints);
			projection.projectPoint(new Point(editor.mouseX, editor.mouseY));
			var positionPoint:Point = projection.projectionPoint.clone();
			positionPoint.offset(map.x, map.y);
			_nodeIndex = projection.projectionIndex;
			_nodePosition = map.unproject(positionPoint);
		}
	}
