package com.cloudmade.mappingtool.ui.actions.editing;

	import com.cloudmade.mappingtool.actions.IBufferingAction;
	import com.cloudmade.mappingtool.map.model.Element;
	import com.cloudmade.mappingtool.services.MapEditService;
	
	import flash.events.Event;
	
	import mx.core.IDataRenderer;
	
	class MoveActionStateMachine implements IBufferingAction {
		
		var editService:MapEditService;
		var currentAction:IBufferingAction;
		var moveMapAction:IBufferingAction;
		var moveElementAction:IBufferingAction;
		
		public function new(editService:MapEditService, moveMapAction:IBufferingAction, moveElementAction:IBufferingAction) {
			this.editService = editService;
			this.moveMapAction = moveMapAction;
			this.moveElementAction = moveElementAction;
		}
		
		public function begin(event:Event):Void {
			// TODO: Move adding way state out of here in some central place.
			if (editService.addingWay) {
				currentAction = moveMapAction;
			} else if (isSelectedElement(event)) {
				currentAction = moveElementAction;
			} else {
				currentAction = moveMapAction;
			}
			currentAction.begin(event);
		}
		
		public function run(event:Event):Void {
			currentAction.run(event);
		}
		
		public function end(event:Event):Void {
			currentAction.end(event);
		}
		
		function isSelectedElement(event:Event):Bool {
			var target:IDataRenderer = cast( event.target, IDataRenderer);
			var selection:Element = editService.selection;
			var result:Bool = target && selection && (target.data.id == selection.id);
			return result;
		}
	}
