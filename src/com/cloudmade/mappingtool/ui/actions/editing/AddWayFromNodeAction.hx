package com.cloudmade.mappingtool.ui.actions.editing;

	import com.cloudmade.mappingtool.actions.IAction;
	import com.cloudmade.mappingtool.commands.editing.events.AddWayFromNodeEvent;
	import com.cloudmade.mappingtool.commands.editing.events.EditingEvent;
	import com.cloudmade.mappingtool.map.model.Node;
	import com.cloudmade.mappingtool.map.model.Way;
	import com.cloudmade.mappingtool.map.view.INodeRenderer;
	import com.cloudmade.mappingtool.services.MapEditService;
	
	import flash.events.Event;
	
	class AddWayFromNodeAction implements IAction {
		
		var editService:MapEditService;
		
		public function new(editService:MapEditService) {
			this.editService = editService;
		}
		
		public function run(event:Event):Void {
			var way:Way = createNewWay();
			var editingEvent:EditingEvent = createEditingEvent(way, event);
			editService.executeEditingCommand(editingEvent);
			editService.selection = way;
			editService.addingWay = true;
		}
		
		function createNewWay():Way {
			var way:Way = new Way(String(--editService.lastId), editService.presetTags, null);
			way.isArea = editService.selectedCategory == MapEditService.AREAS;
			return way;
		}
		
		function createEditingEvent(way:Way, event:Event):EditingEvent {
			var renderer:INodeRenderer = cast( event.target, INodeRenderer);
			var node:Node = cast( renderer.data, Node);
			return new AddWayFromNodeEvent(AddWayFromNodeEvent.ADD_WAY_FROM_NODE, way, node);
		}
	}
