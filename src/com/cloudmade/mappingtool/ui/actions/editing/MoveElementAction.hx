package com.cloudmade.mappingtool.ui.actions.editing;

	import com.cloudmade.mappingtool.actions.IBufferingAction;
	import com.cloudmade.mappingtool.services.MapEditService;
	import com.cloudmade.mappingtool.ui.actions.BaseMoveAction;
	
	import flash.events.Event;
	import flash.geom.Point;
	
	class MoveElementAction extends BaseMoveAction, implements IBufferingAction {
		
		var editService:MapEditService;
		
		public function new(editService:MapEditService) {
			super();
			this.editService = editService;
		}
		
		public function begin(event:Event):Void {
			lastMouseGlobalPosition = getMouseGlobalPosition(event);
			editService.startMovingSelectedElement();
		}

		public function run(event:Event):Void {
			var mouseGlobalPosition:Point = getMouseGlobalPosition(event);
			var offset:Point = mouseGlobalPosition.subtract(lastMouseGlobalPosition);
			editService.moveSelectedElement(offset.x, offset.y);
			lastMouseGlobalPosition = mouseGlobalPosition;
		}
		
		public function end(event:Event):Void {
			editService.stopMovingSelectedElement();
		}
	}
