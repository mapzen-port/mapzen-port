package com.cloudmade.mappingtool.ui.actions.editing;

	import com.cloudmade.mappingtool.actions.IAction;
	import com.cloudmade.mappingtool.map.view.IWayRenderer;
	import com.cloudmade.mappingtool.services.MapEditService;
	
	import flash.events.Event;

	class InsertWayNodeAction implements IAction {
		
		var editService:MapEditService;
		var insertPosition:InsertWayNodePosition;
		
		public function new(editService:MapEditService, insertPosition:InsertWayNodePosition) {
			this.editService = editService;
			this.insertPosition = insertPosition;
		}
		
		public function run(event:Event):Void {
			var renderer:IWayRenderer = cast( event.target, IWayRenderer);
			insertPosition.calculatePosition(renderer.data.id);
			editService.insertWayNode(insertPosition.nodeIndex, insertPosition.nodePosition);
		}
	}
