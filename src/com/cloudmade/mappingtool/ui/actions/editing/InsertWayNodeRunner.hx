package com.cloudmade.mappingtool.ui.actions.editing;

	import com.cloudmade.mappingtool.actions.IAction;
	import com.cloudmade.mappingtool.actions.runners.ActionRunner;
	import com.cloudmade.mappingtool.actions.triggers.ITrigger;
	
	import flash.events.Event;
	
	// Workaround for preventing adding node to not selected way.

	class InsertWayNodeRunner extends ActionRunner {
		
		var activateTrigger:ITrigger;
		
		public function new(action:IAction, runTrigger:ITrigger, activateTrigger:ITrigger) {
			super(action, runTrigger);
			this.activateTrigger = activateTrigger;
		}
		
		public override function activate():Void {
			activateTrigger.addListener(firstActivateListener);
		}
		
		public override function deactivate():Void {
			super.deactivate();
			activateTrigger.removeListener(firstActivateListener);
			activateTrigger.removeListener(secondActivateListener);
		}
		
		function firstActivateListener(event:Event):Void {
			activateTrigger.removeListener(firstActivateListener);
			activateTrigger.addListener(secondActivateListener);
		}
		
		function secondActivateListener(event:Event):Void {
			activateTrigger.removeListener(secondActivateListener);
			super.activate();
		}
	}
