package com.cloudmade.mappingtool.ui.actions.editing;

	import com.cloudmade.mappingtool.actions.IAction;
	import com.cloudmade.mappingtool.actions.IBufferingAction;
	import com.cloudmade.mappingtool.actions.ITriggerHandler;
	import com.cloudmade.mappingtool.actions.TriggerHandlerActivator;
	import com.cloudmade.mappingtool.actions.TriggerHandlerGroup;
	import com.cloudmade.mappingtool.actions.runners.ActionRunner;
	import com.cloudmade.mappingtool.actions.runners.ActionRunnerEvent;
	import com.cloudmade.mappingtool.actions.runners.IActionRunner;
	import com.cloudmade.mappingtool.actions.runners.IBufferingActionRunner;
	import com.cloudmade.mappingtool.actions.triggers.ITrigger;
	import com.cloudmade.mappingtool.actions.triggers.PriorityTrigger;
	import com.cloudmade.mappingtool.actions.triggers.Trigger;
	import com.cloudmade.mappingtool.actions.triggers.TriggerGroup;
	import com.cloudmade.mappingtool.events.MapEditServiceSelectionChangeEvent;
	import com.cloudmade.mappingtool.map.view.IMapView;
	import com.cloudmade.mappingtool.services.MapEditService;
	import com.cloudmade.mappingtool.ui.actions.editing.triggers.AddWayFromNodeTrigger;
	import com.cloudmade.mappingtool.ui.actions.editing.triggers.AddingWayTrigger;
	import com.cloudmade.mappingtool.ui.actions.editing.triggers.InsertWayNodeTrigger;
	import com.cloudmade.mappingtool.ui.actions.editing.triggers.SelectWayTrigger;
	import com.cloudmade.mappingtool.ui.actions.navigation.MapNavigatorActionBuilder;
	
	import flash.display.InteractiveObject;
	import flash.events.MouseEvent;
	
	// TODO: Refactor to convert it to Abstract Factory with MapNavigatorActionBuilder or divide them completely.
	
	class MapEditorActionBuilder extends MapNavigatorActionBuilder {
		
		var editService:MapEditService;
		
		public function new(target:IMapView, editService:MapEditService) {
			super(target);
			this.editService = editService;
		}
		
		public function buildEditingActions(moveMapRunner:IBufferingActionRunner, addWayListener:Dynamic):ITriggerHandler {
			var addWayAction:ITriggerHandler = buildAddWayAction(moveMapRunner, addWayListener);
			var addWayFromNodeAction:ITriggerHandler = buildAddWayFromNodeAction();
			//var addWayFromWayAction:ITriggerHandler = buildAddWayFromWayAction();
			var insertWayNodeAction:ITriggerHandler = buildInsertWayNodeAction();
			var editingActions:ITriggerHandler = new TriggerHandlerGroup([addWayAction, addWayFromNodeAction, /*addWayFromWayAction,*/ insertWayNodeAction]);
			return editingActions;
		}
		
		override function buildMoveAction(maxLat:Int):IBufferingAction {
			var mapAction:IBufferingAction = super.buildMoveAction(maxLat);
			var elementAction:IBufferingAction = new MoveElementAction(editService);
			var stateMachine:IBufferingAction = new MoveActionStateMachine(editService, mapAction, elementAction);
			return stateMachine;
		}
		
		function buildAddWayAction(moveMapRunner:IBufferingActionRunner, runListener:Dynamic):ITriggerHandler {
			var addAction:IAction = new AddWayAction(runListener);
			var runTrigger:ITrigger = new Trigger(mapView, MouseEvent.CLICK);
			var addRunner:IActionRunner = new ActionRunner(addAction, runTrigger);
			var moveActivator:ITriggerHandler = createMoveMapActivator(addRunner, moveMapRunner);
			var addingActivator:ITriggerHandler = createAddingWayActivator(moveActivator);
			return addingActivator;
		}
		
		function buildAddWayFromNodeAction():ITriggerHandler {
			var action:IAction = new AddWayFromNodeAction(editService);
			var runTrigger:ITrigger = new AddWayFromNodeTrigger(mapView, editService);
			var runner:IActionRunner = new ActionRunner(action, runTrigger);
			return runner;
		}
		
		/*private function buildAddWayFromWayAction():ITriggerHandler {
			var insertPosition:InsertWayNodePosition = new InsertWayNodePosition(mapView as InteractiveObject, mapView.map);
			var action:IAction = new AddWayFromWayAction(editService, mapView.map, insertPosition);
			var runTrigger:ITrigger = new AddWayFromWayTrigger(mapView, editService);
			var runner:IActionRunner = new ActionRunner(action, runTrigger);
			return runner;
		}*/
		
		function buildInsertWayNodeAction():ITriggerHandler {
			var insertPosition:InsertWayNodePosition = new InsertWayNodePosition(cast( mapView, InteractiveObject), mapView.map);
			var insertAction:IAction = new InsertWayNodeAction(editService, insertPosition);
			var runTrigger:ITrigger = new InsertWayNodeTrigger(mapView, editService);
			//var activateTrigger:ITrigger = new Trigger(mapView, MouseEvent.MOUSE_DOWN);
			//var insertRunner:IActionRunner = new InsertWayNodeRunner(insertAction, runTrigger, activateTrigger);
			var insertRunner:IActionRunner = new ActionRunner(insertAction, runTrigger);
			var activator:ITriggerHandler = createSelectWayActivator(insertRunner);
			return activator;
		}
		
		function createAddingWayActivator(handler:ITriggerHandler):ITriggerHandler {
			var activateTrigger:ITrigger = new AddingWayTrigger(editService, true);
			var deactivateTrigger:ITrigger = new AddingWayTrigger(editService, false);
			var activator:ITriggerHandler = new TriggerHandlerActivator(handler, activateTrigger, deactivateTrigger);
			return activator;
		}
		
		function createMoveMapActivator(handler:ITriggerHandler, moveMapRunner:IBufferingActionRunner):ITriggerHandler {
			var activateTrigger:ITrigger = new Trigger(mapView, MouseEvent.MOUSE_DOWN);
			var moveDeactivateTrigger:ITrigger = new Trigger(moveMapRunner, ActionRunnerEvent.RUN);
			var addingDeactivateTrigger:ITrigger = new AddingWayTrigger(editService, false);
			var deactivateTrigger:ITrigger = new TriggerGroup([moveDeactivateTrigger, addingDeactivateTrigger]);
			var activator:ITriggerHandler = new TriggerHandlerActivator(handler, activateTrigger, deactivateTrigger);
			return activator;
		}
		
		function createSelectWayActivator(handler:ITriggerHandler):ITriggerHandler {
			var activateTrigger:ITrigger = new SelectWayTrigger(editService);
			// Don't deactivate if selection changes from one way to another:
			var deactivateTrigger:ITrigger = new PriorityTrigger(editService, MapEditServiceSelectionChangeEvent.SELECTION_CHANGE, 1);
			var activator:ITriggerHandler = new TriggerHandlerActivator(handler, activateTrigger, deactivateTrigger);
			return activator;
		}
	}
