package com.cloudmade.mappingtool.ui.actions.navigation;

	import com.cloudmade.mappingtool.actions.IBufferingAction;
	import com.cloudmade.mappingtool.actions.runners.BufferingActionRunner;
	import com.cloudmade.mappingtool.actions.runners.IBufferingActionRunner;
	import com.cloudmade.mappingtool.actions.triggers.ITrigger;
	import com.cloudmade.mappingtool.actions.triggers.Trigger;
	import com.cloudmade.mappingtool.actions.triggers.TriggerGroup;
	import com.cloudmade.mappingtool.map.view.IMapView;
	
	import flash.display.Stage;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	class MapNavigatorActionBuilder
	 {
		
		var mapView:IMapView;
		
		public function new(mapView:IMapView) {
			this.mapView = mapView;
		}
		
		public function buildMoveMapRunner(stage:Stage, maxLat:Int):IBufferingActionRunner {
			var action:IBufferingAction = buildMoveAction(maxLat);
			var beginTrigger:ITrigger = new Trigger(mapView, MouseEvent.MOUSE_DOWN);
			var runTrigger:ITrigger = new Trigger(stage, MouseEvent.MOUSE_MOVE);
			var endTrigger1:ITrigger = new Trigger(stage, MouseEvent.MOUSE_UP);
			var endTrigger2:ITrigger = new Trigger(stage, Event.DEACTIVATE);
			var endTrigger:ITrigger = new TriggerGroup([endTrigger1, endTrigger2]);
			var runner:IBufferingActionRunner = new BufferingActionRunner(action, beginTrigger, runTrigger, endTrigger);
			return runner;
		}
		
		function buildMoveAction(maxLat:Int):IBufferingAction {
			var action:IBufferingAction = new MoveMapAction(mapView.map, maxLat);
			return action;
		}
	}
