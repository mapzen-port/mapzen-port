package com.cloudmade.mappingtool.ui.actions.navigation;

	import com.cloudmade.mappingtool.actions.IBufferingAction;
	import com.cloudmade.mappingtool.map.model.Map;
	import com.cloudmade.mappingtool.ui.actions.BaseMoveAction;
	
	import flash.events.Event;
	import flash.geom.Point;
	
	class MoveMapAction extends BaseMoveAction, implements IBufferingAction {
		
		var map:Map;
		var maxLat:Int;
		
		public function new(map:Map, maxLat:Int) {
			super();
			this.map = map;
			this.maxLat = maxLat;
		}

		public function begin(event:Event):Void {
			lastMouseGlobalPosition = getMouseGlobalPosition(event);
			map.disableBoundsUpdate();
		}
		
		public function run(event:Event):Void {
			var mouseGlobalPosition:Point = getMouseGlobalPosition(event);
			var newCenter:Point = lastMouseGlobalPosition.subtract(mouseGlobalPosition);
			newCenter.offset(map.centerX, map.centerY);
			map.center = map.unproject(newCenter).normalize(maxLat);
			lastMouseGlobalPosition = mouseGlobalPosition;
		}
		
		public function end(event:Event):Void {
			map.enableBoundsUpdate();
		}
	}
