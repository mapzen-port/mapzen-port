package com.cloudmade.mappingtool.ui.windows;

	import com.cloudmade.mappingtool.events.OSMProxySaveEvent;
	import com.cloudmade.mappingtool.managers.WindowManager;
	import com.cloudmade.mappingtool.model.OSMProxy;
	import com.cloudmade.mappingtool.services.MapEditService;
	
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	
	import mx.containers.Canvas;
	
	class SaveWindowBase extends Canvas {
		
		static var COMMENTING:Int = 0;
		static var SAVING:Int = 1;
		
		/*[Bindable]*/
		var selectedView:Int;
		
		var osmProxy:OSMProxy;
		var editService:MapEditService;
		
		public function new() {
			super();
			osmProxy = OSMProxy.getInstance();
			editService = MapEditService.getInstance();
		}
		
		public function open():Void {
			selectedView = COMMENTING;
			WindowManager.getInstance().openWindow(this, true);
		}
		
		public function close():Void {
			WindowManager.getInstance().closeWindow(this);
		}
		
		function save(comment:String):Void {
			selectedView = SAVING;
			addListeners();
			editService.save(osmProxy, comment);
		}
		
		function finishSaving():Void {
			removeListeners();
			close();
		}
		
		function addListeners():Void {
			osmProxy.addEventListener(OSMProxySaveEvent.SAVE_COMPLETE, osmProxy_saveCompleteHandler, false, 0, true);
			osmProxy.addEventListener(OSMProxySaveEvent.SAVE_FAILED, saveFailedHandler, false, 0, true);
			editService.addEventListener(IOErrorEvent.IO_ERROR, saveFailedHandler, false, 0, true);
		}
		
		function removeListeners():Void {
			osmProxy.removeEventListener(OSMProxySaveEvent.SAVE_COMPLETE, osmProxy_saveCompleteHandler);
			osmProxy.removeEventListener(OSMProxySaveEvent.SAVE_FAILED, saveFailedHandler);
			editService.removeEventListener(IOErrorEvent.IO_ERROR, saveFailedHandler);
		}
		
		function osmProxy_saveCompleteHandler(event:OSMProxySaveEvent):Void {
			finishSaving();
		}
		
		function saveFailedHandler(event:Event):Void {
			finishSaving();
		}
	}
