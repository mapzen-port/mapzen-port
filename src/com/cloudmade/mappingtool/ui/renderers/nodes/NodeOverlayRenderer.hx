package com.cloudmade.mappingtool.ui.renderers.nodes;

	import com.cloudmade.mappingtool.map.model.Node;
	import com.cloudmade.mappingtool.map.view.INodeRenderer;
	
	import flash.display.Shape;
	
	class NodeOverlayRenderer extends Shape, implements INodeRenderer {
		
		public var data(getData, setData) : Dynamic;
		public function new() {
			super();
		}
		
		var node:Node;
		public function getData():Dynamic{
			return node;
		}
		public function setData(value:Dynamic):Dynamic{
			node = cast( value, Node);
			return value;
		}
		
		function drawOverlay(color:UInt, alpha:Int):Void {
			graphics.beginFill(color, alpha);
			graphics.drawCircle(0, 0, 10);
			graphics.drawCircle(0, 0, 8);
			graphics.drawCircle(0, 0, 3);
			graphics.endFill();
		}
	}
