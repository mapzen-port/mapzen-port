package com.cloudmade.mappingtool.ui.renderers.nodes;

	class NodeHighlightRenderer extends NodeOverlayRenderer {
		
		public function new() {
			super();
			drawOverlay(0xFF00FF, 0.6);
		}
	}
