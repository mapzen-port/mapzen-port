package com.cloudmade.mappingtool.ui.renderers.nodes;

	import com.cloudmade.mappingtool.map.model.Node;
	import com.cloudmade.mappingtool.map.view.INodeRenderer;
	import com.cloudmade.mappingtool.ui.renderers.IStylized;
	
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	
	class IconNodeRenderer extends Sprite, implements INodeRenderer, implements IStylized {
		
		public var data(getData, setData) : Dynamic;
		public var styles(getStyles, setStyles) : Dynamic;
		var icon:DisplayObject;
		
		public function new() {
			super();
		}
		
		var _styles:Dynamic;
		public function getStyles():Dynamic{
			return _styles;
		}
		public function setStyles(value:Dynamic):Dynamic{
			_styles = value;
			return value;
		}
		
		var node:Node
		public function getData():Dynamic{
			return node;
		}
		public function setData(value:Dynamic):Dynamic{
			node = cast( value, Node);
			if (!icon) {
				icon = new styles.icon();
				addChild(icon);
			}
			return value;
		}
	}
