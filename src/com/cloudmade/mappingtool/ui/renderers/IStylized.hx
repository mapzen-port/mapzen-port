package com.cloudmade.mappingtool.ui.renderers;

	interface IStylized
	{
		function styles():Dynamic;
		function styles(value:Dynamic):Void;
	}
