package com.cloudmade.mappingtool.ui.renderers.ways;

	import flash.display.JointStyle;
	import flash.display.LineScaleMode;
	
	import mx.core.IFactory;
	
	class SolidWayRenderer extends StylizedWayRenderer {
		
		public function new() {
			super();
		}
		
		override function draw():Void {
			graphics.lineStyle(wayWidth, wayColor, wayAlpha);
			drawPolyline();
			drawStylizedArrows();
		}
	}
