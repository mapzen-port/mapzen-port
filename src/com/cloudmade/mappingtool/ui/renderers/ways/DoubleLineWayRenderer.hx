package com.cloudmade.mappingtool.ui.renderers.ways;

	import mx.core.IFactory;
	import flash.display.JointStyle;
	import flash.display.LineScaleMode;
	
	class DoubleLineWayRenderer extends StylizedWayRenderer {
		
		public function new() {
			super();
		}

		override function draw():Void {
			graphics.lineStyle(wayBorderWidth, wayBorderColor, wayBorderAlpha, false, LineScaleMode.NORMAL, null);
			var offset:Int = (wayWidth + wayBorderWidth) / 2;
			drawOffsetPolyline(offset);
			drawOffsetPolyline(-offset);
			graphics.lineStyle(wayWidth, wayColor, wayAlpha, false, LineScaleMode.NORMAL, null, JointStyle.ROUND, 0);
			drawPolyline();
			drawStylizedArrows();
		}
	}
