package com.cloudmade.mappingtool.ui.renderers.ways;

	class WayHighlightRenderer extends BaseWayRenderer {
		
		public function new() {
			super();
		}
		
		override function draw():Void {
			graphics.lineStyle(4, 0xFF00FF, 0.6);
			drawPolyline();
		}
	}
