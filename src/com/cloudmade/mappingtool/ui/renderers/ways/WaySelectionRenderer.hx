package com.cloudmade.mappingtool.ui.renderers.ways;

	class WaySelectionRenderer extends BaseWayRenderer {
		
		public function new() {
			super();
		}
		
		override function draw():Void {
			graphics.lineStyle(4, 0x0000FF, 0.6);
			drawPolyline();
		}
	}
