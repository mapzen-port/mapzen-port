package com.cloudmade.mappingtool.ui.renderers.ways;

	import flash.display.Sprite;
	
	import mx.core.BitmapAsset;
	import mx.core.IFactory;

	class BitmapArrowFactory implements IFactory {
		
		/*[Embed(source="../../../../../../assets/way_arrow.png")]*/
		static var ARROW:Class<Dynamic>;
		
		public function new() {
		}
		
		public function newInstance():Dynamic {
			var arrow:BitmapAsset = new ARROW();
			arrow.smoothing = true;
			arrow.x = -Math.round(arrow.measuredWidth / 2);
			arrow.y = -Math.round(arrow.measuredHeight / 2);
			
			var container:Sprite = new Sprite();
			container.addChild(arrow);
			return container;
		}
	}
