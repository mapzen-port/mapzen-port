package com.cloudmade.mappingtool.ui.renderers.ways;

	import flash.display.CapsStyle;
	import flash.display.LineScaleMode;
	import flash.display.JointStyle;

	class DashStyleWayRenderer extends StylizedWayRenderer {
		
		public function new() {
			super();
		}
		
		override function draw():Void {
			graphics.lineStyle(wayWidth, wayBackgroundColor, wayBackgroundAlpha, false, LineScaleMode.NORMAL, null, JointStyle.ROUND, 0);
			drawPolyline();
			graphics.lineStyle(wayWidth, wayColor, wayAlpha, false, LineScaleMode.NORMAL, null, JointStyle.ROUND, 0);
			drawDashedPolyline(dashWidth, gapWidth);
		}
	}
