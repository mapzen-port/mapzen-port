package com.cloudmade.mappingtool.ui.renderers.ways;

	import flash.display.CapsStyle;
	import flash.display.LineScaleMode;
	import flash.display.JointStyle;
	
	import mx.core.IFactory;
	
	class DoubleLineDashedWayRenderer extends StylizedWayRenderer {
		
		public function new() {
			super();
		}
		
		override function draw():Void {
			graphics.lineStyle(wayBackgroundWidth, wayBackgroundColor, wayBackgroundAlpha, false, LineScaleMode.NORMAL, null, JointStyle.ROUND, 0);
			drawPolyline();
			graphics.lineStyle(wayWidth, wayColor, wayAlpha, false, LineScaleMode.NORMAL, null, JointStyle.ROUND, 0);
			drawPolyline();
			graphics.lineStyle(wayBorderWidth, wayBorderColor, wayBorderAlpha, false, LineScaleMode.NORMAL, null, JointStyle.MITER, 0);
			var offset:Int = (wayWidth + wayBorderWidth) / 2;
			drawOffsetDashedPolyline(offset, dashWidth, gapWidth);
			drawOffsetDashedPolyline(-offset, dashWidth, gapWidth);
			drawStylizedArrows();
		}
	}
