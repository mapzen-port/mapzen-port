package com.cloudmade.mappingtool.ui.renderers.ways;

	import flash.display.CapsStyle;
	import flash.display.LineScaleMode;
	import flash.display.JointStyle;
	
	import mx.core.IFactory;
	
	class DashStyleFilledWayRenderer extends StylizedWayRenderer {
		
		public function new() {
			super();
		}

		override function draw():Void {
			graphics.lineStyle(wayWidth, wayColor, wayAlpha, false, LineScaleMode.NORMAL, null, JointStyle.ROUND, 0);
			drawPolyline();
			graphics.lineStyle(wayLineWidth, wayLineColor, wayLineAlpha, false, LineScaleMode.NORMAL, null, JointStyle.MITER, 255);
			drawDashedPolyline(wayLineDashWidth, wayLineGapWidth);
			if (way.isOneWay) {
				var arrowFactory:IFactory = new ArrowFactory(arrowColor, arrowAlpha);
				drawArrows(arrowFactory, 50);
			}
		}
	}
