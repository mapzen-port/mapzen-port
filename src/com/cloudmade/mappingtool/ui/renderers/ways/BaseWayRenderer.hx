package com.cloudmade.mappingtool.ui.renderers.ways;

	import com.cloudmade.mappingtool.map.model.Node;
	import com.cloudmade.mappingtool.map.model.Way;
	import com.cloudmade.mappingtool.map.view.IWayRenderer;
	
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.geom.Point;
	
	import mx.core.IFactory;

	class BaseWayRenderer extends Sprite, implements IWayRenderer {
		
		public var data(getData, setData) : Dynamic;
		static var RIGHT_ANGLE:Int = Math.PI / 2;
		
		public function new() {
			super();
		}
		
		var way:Way;
		public function getData():Dynamic{
			return way;
		}
		public function setData(value:Dynamic):Dynamic{
			way = cast( value, Way);
			graphics.clear();
			// Remove all arrows.
			while (numChildren > 0) {
				removeChildAt(0);
			}
			if (way) draw();
			return value;
		}
		
		function draw():Void {
		}
		
		function drawPolyline():Void {
			internalDrawPolyline(way.getNodePoints());
		}
		
		function drawDashedPolyline(length:Int, gap:Int):Void {
			internalDrawDashedPolyline(way.getNodePoints(), length, gap);
		}

		function drawOffsetPolyline(offset:Int):Void {
			internalDrawPolyline(shiftRoute(way.getNodePoints(), offset));
			drawCircles(way.getNodePoints(), offset);
		}
		
		function drawOffsetDashedPolyline(offset:Int, length:Int, gap:Int):Void {
			internalDrawDashedPolyline(shiftRoute(way.getNodePoints(), offset), length, gap);
			drawCircles(way.getNodePoints(), offset);
		}
		
		function drawArrows(arrowFactory:IFactory, minLength:Int):Void {
			var rotationFactor:Int = way.isReverse ? -1 : 1;
			var nodes:Array<Dynamic> = way.nodes.source;
			var node:Node = nodes[0];
			var endPoint:Point = new Point(node.x, node.y);
			var n:Int = nodes.length;
			for (i in 1...n) {
				var startPoint:Point = endPoint;
				node = nodes[i];
				endPoint = new Point(node.x, node.y);
				if (Point.distance(startPoint, endPoint) > minLength) {
					var arrow:DisplayObject = arrowFactory.newInstance();
					var arrowPosition:Point = Point.interpolate(startPoint, endPoint, 0.5);
					arrow.x = arrowPosition.x;
					arrow.y = arrowPosition.y;
					var xDelta:Int = (endPoint.x - startPoint.x) * rotationFactor;
					var yDelta:Int = (endPoint.y - startPoint.y) * rotationFactor;
					var angle:Int = Math.atan2(yDelta, xDelta);
					arrow.rotation = angle * 180 / Math.PI;
					addChild(arrow);
				}
			}
		}
		
		function shiftRoute(points:Array<Dynamic>, offset:Int):Array<Dynamic> {
			var n:Int = points.length;
			var point2:Point = points[0];
			var offsetPoints:Array<Dynamic> = new Array();
			for (i in 1...n) {
				var point1:Point = point2;
				point2 = points[i];
				var offsetPoint:Point = createPerpendicularPoint(point1, point2, offset);
				offsetPoints.push(point1.add(offsetPoint));
				offsetPoints.push(point2.add(offsetPoint));
			}
			return offsetPoints;
		}
		
		/* Algorithm for intersection points defining.
		
		private function createOffsetRoute(nodes:Array, offset:Number):Array {
			var points:Array = createRoute(nodes);
			var currentPoint:Point = points[0];
			var nextPoint:Point = points[1];
			var offsetPoint2:Point =  createPerpendicularPoint(currentPoint, nextPoint, offset);
			var startPoint2:Point = offsetPoint2.add(currentPoint);
			var endPoint2:Point = offsetPoint2.add(nextPoint);
			var offsetPoints:Array = [startPoint2];
			var n:int = nodes.length - 1;
			for (var i:int = 1; i < n; i++) {
				currentPoint = nextPoint;
				nextPoint = points[i + 1];
				var offsetPoint1:Point = offsetPoint2;
				var startPoint1:Point = startPoint2;
				var endPoint1:Point = endPoint2;
				offsetPoint2 = createPerpendicularPoint(currentPoint, nextPoint, offset);
				startPoint2 = offsetPoint2.add(currentPoint);
				endPoint2 = offsetPoint2.add(nextPoint);
				var intersectionPoint:Point = GeometryUtils.calculateIntersection(startPoint1, endPoint1, startPoint2, endPoint2);
				if (intersectionPoint){
					offsetPoints.push(new Point(intersectionPoint.x, intersectionPoint.y));
				}
			}
			offsetPoints.push(createPerpendicularPoint(currentPoint, nextPoint, offset).add(nextPoint));
			return offsetPoints;
		}
		*/
		
		function createPerpendicularPoint(startPoint:Point, endPoint:Point, offset:Int):Point {
			var angle:Int = Math.atan2(endPoint.y - startPoint.y, endPoint.x - startPoint.x);
			return Point.polar(offset, angle - RIGHT_ANGLE);
		}
		
		function internalDrawPolyline(points:Array<Dynamic>):Void {
			var n:Int = points.length;
			var point:Point = points[0];
			graphics.moveTo(point.x, point.y);
			for (i in 1...n) {
				point = points[i];
				graphics.lineTo(point.x, point.y);
			}
		}
		
		function internalDrawDashedPolyline(points:Array<Dynamic>, length:Int, gap:Int):Void {
			var n:Int = points.length;
			var currentPoint:Point = points[0];
			graphics.moveTo(currentPoint.x, currentPoint.y);
			for (i in 1...n){
				var taleEnd:Int = 0;
				var previousPoint:Point = currentPoint;
				currentPoint = points[i];
				var segmentAngle:Int = Math.atan2(currentPoint.y - previousPoint.y, currentPoint.x - previousPoint.x);
				var fullSegmentLength:Int = Point.distance(previousPoint, currentPoint);
				var segmentLength:Int = 0;
				var currentDashPoint:Point = previousPoint;
				var dashPolarEnd:Point =  Point.polar(length, segmentAngle);
				var gapPolarEnd:Point = Point.polar(gap, segmentAngle);
				var fullDashSegmentLength:Int = fullSegmentLength - length;
				while (segmentLength < fullDashSegmentLength) {
					segmentLength += length;
					currentDashPoint.offset(dashPolarEnd.x, dashPolarEnd.y);
					graphics.lineTo(currentDashPoint.x, currentDashPoint.y);
					segmentLength += gap;
					currentDashPoint.offset(gapPolarEnd.x, gapPolarEnd.y);
					graphics.moveTo(currentDashPoint.x, currentDashPoint.y);
				}
				if (segmentLength < fullSegmentLength) {
					graphics.lineTo(currentPoint.x, currentPoint.y);
				}
			}
		}
		
		function drawCircles(points:Array<Dynamic>, radius:Int):Void {
			points.pop();
			points.shift();
			for (point in points) {
				graphics.drawCircle(point.x, point.y, radius);
			}
		}
	}
