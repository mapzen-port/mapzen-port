package com.cloudmade.mappingtool.ui.renderers.ways;

	import mx.core.IFactory;
	import flash.display.JointStyle;
	import flash.display.LineScaleMode;
	
	class TripleDashStyleLineWayRenderer extends StylizedWayRenderer {
		
		public function new() {
			super();
		}
		
		override function draw():Void {
			var offset:Int;
			graphics.lineStyle(wayBorderWidth, wayBorderColor, wayBorderAlpha, false, LineScaleMode.NORMAL, null, JointStyle.MITER, 255);
			offset = (wayWidth + wayBorderWidth) / 2;
			drawOffsetPolyline(offset);
			drawOffsetPolyline(-offset);
			graphics.lineStyle(wayWidth, wayColor, wayAlpha, false, LineScaleMode.NORMAL, null, JointStyle.ROUND, 0);
			drawPolyline();
			graphics.lineStyle(wayLineWidth, wayLineColor, wayLineAlpha, false, LineScaleMode.NORMAL, null, JointStyle.ROUND, 0);
			drawDashedPolyline(wayLineDashWidth, wayLineGapWidth);
			drawStylizedArrows();
		}
	}
