package com.cloudmade.mappingtool.ui.renderers.ways;

	import mx.core.IFactory;
	import flash.display.JointStyle;
	import flash.display.LineScaleMode;
	
	class TripleLineWayRenderer extends StylizedWayRenderer {
		
		public function new() {
			super();
		}
		
		override function draw():Void {
			var offset:Int;
			graphics.lineStyle(wayBorderWidth, wayBorderColor, wayBorderAlpha, false, LineScaleMode.NORMAL, null, JointStyle.ROUND, 0);
			offset = wayWidth + wayBorderWidth;
			drawOffsetPolyline(offset);
			drawOffsetPolyline(-offset);
			graphics.lineStyle(wayWidth, wayColor, wayAlpha, false, LineScaleMode.NORMAL, null, JointStyle.ROUND, 0);
			offset = (wayLineWidth + wayWidth) / 2;
			drawOffsetPolyline(offset);
			drawOffsetPolyline(-offset);
			graphics.lineStyle(wayLineWidth, wayLineColor, wayLineAlpha, false, LineScaleMode.NORMAL, null, JointStyle.MITER, 255);
			drawPolyline();
			drawStylizedArrows();
		}
	}
