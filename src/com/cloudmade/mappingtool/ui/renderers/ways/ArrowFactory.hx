package com.cloudmade.mappingtool.ui.renderers.ways;

	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Graphics;
	import flash.display.PixelSnapping;
	import flash.display.Shape;
	import flash.display.Sprite;
	
	import mx.core.IFactory;
	
	class ArrowFactory implements IFactory {
		
		var arrowX:Int;
		var arrowY:Int;
		var arrowImage:BitmapData;
		
		public function new(arrowColor:UInt, arrowAlpha:Int, ?stemWidth:Int = 6, ?stemHeight:Int = 2, ?headWidth:Int = 4, ?headHeight:Int = 4) {
			var shape:Shape = new Shape();
			var graph:Graphics = shape.graphics;
			graph.beginFill(arrowColor, isNaN(arrowAlpha) ? 1 : arrowAlpha);
			graph.drawRect(0, (headHeight - stemHeight) / 2, stemWidth, stemHeight);
			graph.moveTo(stemWidth, 0);
			graph.lineTo(stemWidth + headWidth, headHeight / 2);
			graph.lineTo(stemWidth, headHeight);
			graph.lineTo(stemWidth, 0);
			graph.endFill();
			arrowImage = new BitmapData(stemWidth + headWidth, headHeight, true, 0x00000000);
			arrowImage.draw(shape);
			arrowX = -headHeight / 2;
			arrowY = -(stemWidth + headWidth) / 2;
		}
		
		public function newInstance():Dynamic {
			var arrow:Bitmap = new Bitmap(arrowImage, PixelSnapping.AUTO, true);
			var container:Sprite = new Sprite();
			arrow.y = arrowX;
			arrow.x = arrowY;
			container.addChild(arrow);
			return container;
		}
	}
