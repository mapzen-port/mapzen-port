package com.cloudmade.mappingtool.ui.renderers.ways;

	import com.cloudmade.mappingtool.ui.renderers.IStylized;
	
	import mx.core.IFactory;

	class StylizedWayRenderer extends BaseWayRenderer, implements IStylized {
		
		public var arrowAlpha(getArrowAlpha, null) : Int ;
		public var arrowColor(getArrowColor, null) : Int ;
		public var dashWidth(getDashWidth, null) : Int ;
		public var gapWidth(getGapWidth, null) : Int ;
		public var styles(getStyles, setStyles) : Dynamic;
		public var wayAlpha(getWayAlpha, null) : Int ;
		public var wayBackgroundAlpha(getWayBackgroundAlpha, null) : Int ;
		public var wayBackgroundColor(getWayBackgroundColor, null) : Int ;
		public var wayBackgroundWidth(getWayBackgroundWidth, null) : Int ;
		public var wayBorderAlpha(getWayBorderAlpha, null) : Int ;
		public var wayBorderColor(getWayBorderColor, null) : Int ;
		public var wayBorderWidth(getWayBorderWidth, null) : Int ;
		public var wayColor(getWayColor, null) : Int ;
		public var wayLineAlpha(getWayLineAlpha, null) : Int ;
		public var wayLineColor(getWayLineColor, null) : Int ;
		public var wayLineDashWidth(getWayLineDashWidth, null) : Int ;
		public var wayLineGapWidth(getWayLineGapWidth, null) : Int ;
		public var wayLineWidth(getWayLineWidth, null) : Int ;
		public var wayWidth(getWayWidth, null) : Int ;
		public function new(){
			super();
		}
		
		var _styles:Dynamic ;
		public function getStyles():Dynamic{
			return _styles;
		}
		public function setStyles(value:Dynamic):Dynamic{
			_styles = value;
			return value;
		}
		
		function getWayWidth():Int {
			return isNaN(styles.wayWidth) ? 1 : styles.wayWidth;
		}
		
		function getWayColor():Int {
			return isNaN(styles.wayColor) ? 0x000000 : styles.wayColor;
		}
		
		function getWayBackgroundWidth():Int {
			return isNaN(styles.wayBackgroundWidth) ? 1 : styles.wayBackgroundWidth;
		}
		
		function getWayBackgroundColor():Int {
			return isNaN(styles.wayBackgroundColor) ? 0x000000 : styles.wayBackgroundColor;
		}
		
		function getWayBackgroundAlpha():Int {
			return isNaN(styles.wayBackgroundAlpha) ? 0 : styles.wayBackgroundAlpha;
		}
		
		function getWayAlpha():Int {
			return isNaN(styles.wayAlpha) ? 1 : styles.wayAlpha;
		}
		
		function getDashWidth():Int {
			return isNaN(styles.dashWidth) ? 1 : styles.dashWidth;
		} 
		
		function getGapWidth():Int {
			return isNaN(styles.gapWidth) ? 1 : styles.gapWidth;
		}
		
		function getWayLineWidth():Int {
			return isNaN(styles.wayLineWidth) ? 1 : styles.wayLineWidth;
		}
		
		function getWayLineColor():Int {
			return isNaN(styles.wayLineColor) ? 0x000000 : styles.wayLineColor;
		}
		
		function getWayLineAlpha():Int {
			return isNaN(styles.wayLineAlpha) ? 1 : styles.wayLineAlpha;
		}
		
		function getWayLineDashWidth():Int {
			return isNaN(styles.wayLineAlpha) ? 1 : styles.wayLineAlpha;
		}
		
		function getWayLineGapWidth():Int {
			return isNaN(styles.wayLineGapWidth) ? 15 : styles.wayLineGapWidth;
		}
		
		function getArrowColor():Int {
			return isNaN(styles.arrowColor) ? 0xFFFFFF : styles.arrowColor;
		}
		
		function getArrowAlpha():Int {
			return isNaN(styles.arrowAlpha) ? 1 : styles.arrowAlpha;
		}
		
		function getWayBorderWidth():Int {
			return isNaN(styles.wayBorderWidth) ? 1 : styles.wayBorderWidth;
		}
		
		function getWayBorderColor():Int {
			return isNaN(styles.wayBorderColor) ? 0x000000 : styles.wayBorderColor;
		}
		
		function getWayBorderAlpha():Int {
			return isNaN(styles.wayBorderAlpha) ? 1 : styles.wayBorderAlpha;
		}
		
		function drawStylizedArrows():Void {
			if (way.isOneWay) {
				//var arrowFactory:IFactory = new ArrowFactory(arrowColor, arrowAlpha);
				var arrowFactory:IFactory = new BitmapArrowFactory();
				drawArrows(arrowFactory, 30);
			}
		}
	}
