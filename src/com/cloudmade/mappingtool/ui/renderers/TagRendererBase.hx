package com.cloudmade.mappingtool.ui.renderers;

	import com.cloudmade.mappingtool.commands.editing.events.ChangeElementTagEvent;
	import com.cloudmade.mappingtool.commands.editing.events.ElementTagEditingEvent;
	import com.cloudmade.mappingtool.map.model.Tag;
	import com.cloudmade.mappingtool.model.ApplicationData;
	import com.cloudmade.mappingtool.model.config.TagGroup;
	
	import mx.containers.HBox;
	import mx.controls.List;
	import mx.controls.TextInput;
	import mx.controls.listClasses.IListItemRenderer;
	
	/*[Event(name="removeElementTag", type="com.cloudmade.mappingtool.commands.editing.events.ElementTagEditingEvent")]*/
	/*[Event(name="changeElementTagKey", type="com.cloudmade.mappingtool.commands.editing.events.ChangeElementTagEvent")]*/
	/*[Event(name="changeElementTagValue", type="com.cloudmade.mappingtool.commands.editing.events.ChangeElementTagEvent")]*/

	class TagRendererBase extends HBox {
		
		/*[Bindable]*/
		public var keyInput:TextInput;
		/*[Bindable]*/
		public var valueInput:TextInput;
		
		/*[Bindable(event="unused")]*/
		var appData:ApplicationData ;
		/*[Bindable]*/
		var baseTabIndex:Int;
		
		public function new() {
			
			appData = ApplicationData.getInstance();
			super();
		}
		
		function removeTag():Void {
			dispatchEvent(new ElementTagEditingEvent(ElementTagEditingEvent.REMOVE_ELEMENT_TAG, null, cast( data, Tag), true));
		}
		
		function changeTagKey():Void {
			var key:String = removeControlCharacters(keyInput.text);
			if (key != data.key) {
				dispatchEvent(new ChangeElementTagEvent(ChangeElementTagEvent.CHANGE_ELEMENT_TAG_KEY, null, cast( data, Tag), key, true));
			}
			/*var tagGroup:TagGroup = findTagGroup(data.key);
			var newValue:String = "";
			if (tagGroup && tagGroup.values && (tagGroup.values.length > 0)) {
				newValue = tagGroup.values[0];
			}*/
		}
		
		function changeTagValue():Void {
			var value:String = removeControlCharacters(valueInput.text);
			if (value != data.value) {
				dispatchEvent(new ChangeElementTagEvent(ChangeElementTagEvent.CHANGE_ELEMENT_TAG_VALUE, null, cast( data, Tag), value, true));
			}
		}
		
		function findTagGroup(key:String):TagGroup {
			var tagGroups:Array<Dynamic> = appData.selectedTagGroups;
			for (tagGroup in tagGroups) {
				if (tagGroup.key == key) {
					return tagGroup;
				}
			}
			return null;
		}
		
		function dataChangeHandler():Void {
			baseTabIndex = Math.max(List(owner).itemRendererToIndex(cast( this, IListItemRenderer)), 0) * 3;
		}
		
		function removeControlCharacters(value:String):String {
			return value.replace(~/[\x00-\x1F]/g, "");
		}
	}
