package com.cloudmade.mappingtool.ui.renderers.areas;

	class AreaHighlightRenderer extends BaseAreaRenderer {
		
		public function new() {
			super();
		}
		
		override function draw():Void {
			graphics.beginFill(0xFF00FF, 0.25);
			drawArea();
		}
	}
