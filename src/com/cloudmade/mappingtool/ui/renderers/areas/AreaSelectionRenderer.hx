package com.cloudmade.mappingtool.ui.renderers.areas;

	class AreaSelectionRenderer extends BaseAreaRenderer {
		
		public function new() {
			super();
		}
		
		override function draw():Void {
			graphics.beginFill(0x0000FF, 0.25);
			drawArea();
		}
	}
