package com.cloudmade.mappingtool.ui.renderers.areas;

	import com.cloudmade.mappingtool.map.model.Node;
	import com.cloudmade.mappingtool.map.model.Way;
	import com.cloudmade.mappingtool.map.view.IAreaRenderer;
	
	import flash.display.Sprite;

	class BaseAreaRenderer extends Sprite, implements IAreaRenderer {
		
		public var data(getData, setData) : Dynamic;
		public function new() {
			super();
		}
		
		var area:Way;
		public function getData():Dynamic{
			return area;
		}
		public function setData(value:Dynamic):Dynamic{
			area = cast( value, Way);
			graphics.clear();
			if (area) draw();
			return value;
		}
		
		function draw():Void {
		}
		
		function drawArea():Void {
			var nodes:Array<Dynamic> = area.nodes.source;
			var n:Int = nodes.length;
			var node:Node = nodes[0];
			graphics.moveTo(node.x, node.y);
			for (i in 1...n) {
				node = nodes[i];
				graphics.lineTo(node.x, node.y);
			}
			graphics.endFill();
		}
	}
