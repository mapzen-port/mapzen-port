package com.cloudmade.mappingtool.ui.renderers.areas;

	import flash.filters.BevelFilter;
	
	class BevelAreaRenderer extends SolidAreaRenderer {
		
		static var bevelFilter:BevelFilter = createBevelFilter();
		
		static function createBevelFilter():BevelFilter {
			var filter:BevelFilter = new BevelFilter(1.5);
			filter.strength = 4;
			filter.blurX = 4;
			filter.blurY = 4;
			filter.shadowAlpha = 0.7;
			filter.highlightAlpha = 0.4;
			return filter;
		}
		
		public function new() {
			super();
			filters = [bevelFilter];
		}
	}
