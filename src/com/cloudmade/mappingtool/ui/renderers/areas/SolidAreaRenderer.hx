package com.cloudmade.mappingtool.ui.renderers.areas;

	class SolidAreaRenderer extends StylizedAreaRenderer {
		
		public function new() {
			super();
		}
		
		override function draw():Void {
			graphics.beginFill(areaColor, areaAlpha);
			drawArea();
		}
	}
