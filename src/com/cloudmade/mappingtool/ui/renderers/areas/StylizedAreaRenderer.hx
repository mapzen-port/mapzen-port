package com.cloudmade.mappingtool.ui.renderers.areas;

	import com.cloudmade.mappingtool.ui.renderers.IStylized;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;

	class StylizedAreaRenderer extends BaseAreaRenderer, implements IStylized {
		
		public var areaAlpha(getAreaAlpha, null) : Int ;
		public var areaBitmap(getAreaBitmap, null) : BitmapData ;
		public var areaColor(getAreaColor, null) : Int ;
		public var styles(getStyles, setStyles) : Dynamic;
		public function new() {
			super();
		}
		
		var _styles:Dynamic ;
		public function getStyles():Dynamic{
			return _styles;
		}
		public function setStyles(value:Dynamic):Dynamic{
			_styles = value;
			return value;
		}
		
		function getAreaColor():Int {
			return isNaN(styles.areaColor) ? 1 : styles.areaColor;
		}
		
		function getAreaAlpha():Int {
			return isNaN(styles.areaAlpha) ? 0.8 : styles.areaAlpha;
		}
		
		function getAreaBitmap():BitmapData {
			if (styles.areaBitmap) {
				return Bitmap(new styles.areaBitmap()).bitmapData;
			}
			return new BitmapData(100, 100);
		}
		
	}
