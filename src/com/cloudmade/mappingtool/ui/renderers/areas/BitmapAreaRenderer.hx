package com.cloudmade.mappingtool.ui.renderers.areas;

	class BitmapAreaRenderer extends StylizedAreaRenderer {
		
		public function new() {
			super();
		}
		
		override function draw():Void {
			graphics.beginBitmapFill(areaBitmap, null, true);
			alpha = areaAlpha;
			drawArea();
		}
	}
