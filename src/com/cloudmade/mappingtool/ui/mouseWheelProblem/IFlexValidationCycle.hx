package com.cloudmade.mappingtool.ui.mouseWheelProblem;

	import flash.events.IEventDispatcher;
	
	/*[Event(name="validate", type="com.cloudmade.mappingtool.ui.mouseWheelProblem.FlexValidationCycleEvent")]*/
	
	interface IFlexValidationCycle implements IEventDispatcher{
		function invalidate():Void;
	}
