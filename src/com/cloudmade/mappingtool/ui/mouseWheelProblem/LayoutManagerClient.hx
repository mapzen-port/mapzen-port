package com.cloudmade.mappingtool.ui.mouseWheelProblem;

	import flash.events.EventDispatcher;
	
	import mx.managers.ILayoutManager;
	import mx.managers.ILayoutManagerClient;

	class LayoutManagerClient extends EventDispatcher, implements ILayoutManagerClient {
		
		public var initialized(getInitialized, setInitialized) : Bool;
		public var nestLevel(getNestLevel, setNestLevel) : Int;
		public var processedDescriptors(getProcessedDescriptors, setProcessedDescriptors) : Bool;
		public var updateCompletePendingFlag(getUpdateCompletePendingFlag, setUpdateCompletePendingFlag) : Bool;
		var layoutManager:ILayoutManager;
		var validateFunction:Dynamic;
		var invalidatePropertiesFlag:Bool ;
		
		public function new(layoutManager:ILayoutManager, validateFunction:Dynamic) {
			
			invalidatePropertiesFlag = false;
			super();
			this.layoutManager = layoutManager;
			this.validateFunction = validateFunction;
		}
		
		public function getInitialized():Bool{
			return true;
		}
		public function setInitialized(value:Bool):Bool{
			return value;
	}
		
		var _nestLevel:Int;
		public function getNestLevel():Int{
			return _nestLevel;
		}
		public function setNestLevel(value:Int):Int{
			_nestLevel = value;
			return value;
		}
		
		public function getProcessedDescriptors():Bool{
			return true;
		}
		public function setProcessedDescriptors(value:Bool):Bool{
			return value;
	}
		
		var _updateCompletePendingFlag:Bool ;
		public function getUpdateCompletePendingFlag():Bool{
			return _updateCompletePendingFlag;
		}
		public function setUpdateCompletePendingFlag(value:Bool):Bool{
			_updateCompletePendingFlag = value;
			return value;
		}
		
		public function validateProperties():Void {
			if (invalidatePropertiesFlag) {
				invalidatePropertiesFlag = false;
				validateFunction();
			}
		}
		
		public function validateSize(?recursive:Bool = false):Void {
		}
		
		public function validateDisplayList():Void {
		}
		
		public function invalidateProperties():Void {
			if (!invalidatePropertiesFlag) {
				invalidatePropertiesFlag = true;
				layoutManager.invalidateProperties(this);
			}
		}
	}
