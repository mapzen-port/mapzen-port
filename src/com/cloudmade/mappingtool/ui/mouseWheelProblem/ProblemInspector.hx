package com.cloudmade.mappingtool.ui.mouseWheelProblem;

	import flash.events.MouseEvent;
	
	class ProblemInspector implements IProblemInspector {
		
		var maxDelta:Int;
		var totalDelta:Int ;
		var eventCount:Int ;
		
		public function new(maxDelta:Int) {
			
			totalDelta = 0;
			eventCount = 0;
			this.maxDelta = maxDelta;
		}
		
		public function hasProblem():Bool {
			return calculateDeltaPerEvent() >= maxDelta;
		}
		
		public function addEvent(event:MouseEvent):Void {
			totalDelta += Math.abs(event.delta);
			eventCount++;
		}
		
		public function reset():Void {
			totalDelta = 0;
			eventCount = 0;
		}
		
		function calculateDeltaPerEvent():Int {
			return totalDelta / eventCount;
		}
	}
