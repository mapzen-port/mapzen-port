package com.cloudmade.mappingtool.ui.mouseWheelProblem;

	import flash.events.MouseEvent;
	
	interface IProblemInspector
	{
		function hasProblem():Bool;
		
		function addEvent(event:MouseEvent):Void;
		
		function reset():Void;
	}
