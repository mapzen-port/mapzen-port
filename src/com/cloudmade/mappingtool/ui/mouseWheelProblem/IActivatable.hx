package com.cloudmade.mappingtool.ui.mouseWheelProblem;

	interface IActivatable
	{
		function activate():Void;
		
		function deactivate():Void;
	}
