package com.cloudmade.mappingtool.ui.mouseWheelProblem;

	import com.cloudmade.mappingtool.model.settings.UserSettings;
	import com.cloudmade.mappingtool.ui.controls.IPopUp;
	
	import flash.display.InteractiveObject;
	import flash.events.MouseEvent;
	
	class ProblemNotifier implements IActivatable {
		
		var mouseTarget:InteractiveObject;
		var validationCycle:IFlexValidationCycle;
		var problemInspector:IProblemInspector;
		var notificationPopUp:IPopUp;
		var userSettings:UserSettings;
		
		public function new(
				mouseTarget:InteractiveObject,
				validationCycle:IFlexValidationCycle,
				problemInspector:IProblemInspector,
				notificationPopUp:IPopUp,
				userSettings:UserSettings
			) {
			this.mouseTarget = mouseTarget;
			this.validationCycle = validationCycle;
			this.problemInspector = problemInspector;
			this.notificationPopUp = notificationPopUp;
			this.userSettings = userSettings;
		}
		
		public function activate():Void {
			mouseTarget.addEventListener(MouseEvent.MOUSE_WHEEL, handleMouseTargetMouseWheel);
			validationCycle.addEventListener(FlexValidationCycleEvent.VALIDATE, handleValidationCycleValidate);
		}
		
		public function deactivate():Void {
			mouseTarget.removeEventListener(MouseEvent.MOUSE_WHEEL, handleMouseTargetMouseWheel);
			validationCycle.removeEventListener(FlexValidationCycleEvent.VALIDATE, handleValidationCycleValidate);
		}
		
		function handleMouseTargetMouseWheel(event:MouseEvent):Void {
			problemInspector.addEvent(event);
			validationCycle.invalidate();
		}
		
		function handleValidationCycleValidate(event:FlexValidationCycleEvent):Void {
			if (problemInspector.hasProblem()) {
				notificationPopUp.open();
				userSettings.mouseWheelProblemNotified = true;
				deactivate();
			}
			problemInspector.reset();
		}
	}
