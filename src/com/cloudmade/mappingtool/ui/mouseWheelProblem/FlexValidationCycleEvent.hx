package com.cloudmade.mappingtool.ui.mouseWheelProblem;

	import flash.events.Event;

	class FlexValidationCycleEvent extends Event {
		
		public static var VALIDATE:String = "validate";
		
		public function new(type:String) {
			super(type);
		}
		
		public override function clone():Event {
			return new FlexValidationCycleEvent(type);
		}
	}
