package com.cloudmade.mappingtool.ui.mouseWheelProblem;

	import flash.events.EventDispatcher;
	
	import mx.managers.ILayoutManager;
	
	/*[Event(name="validate", type="com.cloudmade.mappingtool.ui.mouseWheelProblem.FlexValidationCycleEvent")]*/

	class FlexValidationCylcle extends EventDispatcher, implements IFlexValidationCycle {
		
		var layoutManagerClient:LayoutManagerClient
		
		public function new(layoutManager:ILayoutManager, ?nestLevel:Int = 0) {
			super();
			layoutManagerClient = new LayoutManagerClient(layoutManager, validate);
			layoutManagerClient.nestLevel = nestLevel;
		}
		
		public function invalidate():Void {
			layoutManagerClient.invalidateProperties();
		}
		
		function validate():Void {
			dispatchEvent(new FlexValidationCycleEvent(FlexValidationCycleEvent.VALIDATE));
		}
	}
