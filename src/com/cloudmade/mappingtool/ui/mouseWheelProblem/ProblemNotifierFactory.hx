package com.cloudmade.mappingtool.ui.mouseWheelProblem;

	import com.cloudmade.mappingtool.managers.WindowManager;
	import com.cloudmade.mappingtool.model.settings.UserSettings;
	import com.cloudmade.mappingtool.ui.controls.CenteredPopUp;
	
	import flash.display.InteractiveObject;
	
	import mx.core.UIComponentGlobals;
	import mx.core.mx_internal;
	import mx.managers.ILayoutManager;
	
	class ProblemNotifierFactory
	 {
		
		var mouseTarget:InteractiveObject;
		var layoutManager:ILayoutManager;
		var userSettings:UserSettings;
		
		public function new(mouseTarget:InteractiveObject, userSettings:UserSettings) {
			this.mouseTarget = mouseTarget;
			this.userSettings = userSettings;
		}
		
		public function create():IActivatable {
			if (userSettings.mouseWheelProblemNotified) {
				return new NullActivatable();
			}
			
			var notificationPopUp:NotificationPopUp = new NotificationPopUp();
			notificationPopUp.construct(userSettings);
			
			var notifier:IActivatable = new ProblemNotifier(
				mouseTarget,
				new FlexValidationCylcle(
					UIComponentGlobals.mx_internal::layoutManager
				),
				new ProblemInspector(4),
				new CenteredPopUp(
					notificationPopUp,
					WindowManager.getInstance(),
					true
				),
				userSettings
			);
			return notifier;
		}
	}
