package com.cloudmade.mappingtool.ui;

	import com.cloudmade.mappingtool.actions.DummyTriggerHandler;
	import com.cloudmade.mappingtool.actions.ITriggerHandler;
	import com.cloudmade.mappingtool.events.MapEditServiceEvent;
	import com.cloudmade.mappingtool.events.MapEditServiceSelectionChangeEvent;
	import com.cloudmade.mappingtool.map.events.MapEvent;
	import com.cloudmade.mappingtool.map.geo.LatLong;
	import com.cloudmade.mappingtool.map.model.Element;
	import com.cloudmade.mappingtool.map.model.Node;
	import com.cloudmade.mappingtool.map.model.Way;
	import com.cloudmade.mappingtool.map.view.IAreaRenderer;
	import com.cloudmade.mappingtool.map.view.INodeRenderer;
	import com.cloudmade.mappingtool.map.view.IWayRenderer;
	import com.cloudmade.mappingtool.services.MapEditService;
	import com.cloudmade.mappingtool.ui.actions.editing.MapEditorActionBuilder;
	import com.cloudmade.mappingtool.ui.renderers.areas.AreaHighlightRenderer;
	import com.cloudmade.mappingtool.ui.renderers.areas.AreaSelectionRenderer;
	import com.cloudmade.mappingtool.ui.renderers.nodes.NodeHighlightRenderer;
	import com.cloudmade.mappingtool.ui.renderers.nodes.NodeSelectionRenderer;
	import com.cloudmade.mappingtool.ui.renderers.ways.WayHighlightRenderer;
	import com.cloudmade.mappingtool.ui.renderers.ways.WaySelectionRenderer;
	import com.cloudmade.mappingtool.utils.PolylinePointNormalization;
	import com.cloudmade.mappingtool.utils.PolylinePointProjection;
	
	import flash.display.DisplayObject;
	import flash.display.InteractiveObject;
	import flash.display.Sprite;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.geom.Point;
	import flash.ui.Keyboard;
	import flash.utils.Timer;
	
	import mx.core.ClassFactory;
	import mx.core.IDataRenderer;
	import mx.core.IFactory;
	import mx.core.mx_internal;
	import mx.events.FlexEvent;
	import mx.managers.IFocusManagerComponent;
	
	// TODO: Use actions.
	// TODO: Use polymorphism.
	
	class MapEditor extends MapNavigator, implements IFocusManagerComponent {
		
		public var editingEnabled(getEditingEnabled, setEditingEnabled) : Bool;
		static var WAY_PREVIEW_SEGMENT_ALPHA:Float = 0.6;
		
		public var wayHighlightRenderer:IFactory ;
		public var waySelectionRenderer:IFactory ;
		public var areaHighlightRenderer:IFactory ;
		public var areaSelectionRenderer:IFactory ;
		public var nodeHighlightRenderer:IFactory ;
		public var nodeSelectionRenderer:IFactory ;
		
		var editService:MapEditService;
		var areaHighlight:DisplayObject;
		var areaSelection:DisplayObject;
		var nodeHighlight:DisplayObject;
		var nodeSelection:DisplayObject;
		var wayOverlaysContainer:Sprite;
		var areaOverlaysContainer:Sprite;
		var nodeOverlaysContainer:Sprite;
		var wayNodesOverlaysContainer:Sprite;
		var wayPreviewSegment:DisplayObject;
		var wayPartHighlights:Array<Dynamic> ;
		var wayPartSelections:Array<Dynamic> ;
		var wayNodesHighlights:Array<Dynamic> ;
		var wayNodesSelections:Array<Dynamic> ;
		var areaTimer:Timer ;
		var selectedArea:Way;
		var editingActions:ITriggerHandler ;
		var editingEnabledChanged:Bool ;
		
		public function new() {
			
			wayHighlightRenderer = new ClassFactory(WayHighlightRenderer);
			waySelectionRenderer = new ClassFactory(WaySelectionRenderer);
			areaHighlightRenderer = new ClassFactory(AreaHighlightRenderer);
			areaSelectionRenderer = new ClassFactory(AreaSelectionRenderer);
			nodeHighlightRenderer = new ClassFactory(NodeHighlightRenderer);
			nodeSelectionRenderer = new ClassFactory(NodeSelectionRenderer);
			wayPartHighlights = new Array();
			wayPartSelections = new Array();
			wayNodesHighlights = new Array();
			wayNodesSelections = new Array();
			areaTimer = new Timer(250, 1);
			editingActions = new DummyTriggerHandler();
			editingEnabledChanged = false;
			super();
			editService = MapEditService.getInstance();
			actionBuilder = new MapEditorActionBuilder(this, editService);
			editService.addEventListener(MapEditServiceEvent.ADDING_WAY_CHANGE, editService_addingWayChangeHandler);
			editService.addEventListener(MapEditServiceSelectionChangeEvent.SELECTION_CHANGE, editService_selectionChangeHandler, false, 0, true);
		}
		
		var _editingEnabled:Bool ;
		public function getEditingEnabled():Bool{
			return _editingEnabled;
		}
		public function setEditingEnabled(value:Bool):Bool{
			if (value == _editingEnabled) return;
			_editingEnabled = value;
			editingEnabledChanged = true;
			invalidateProperties();
			return value;
		}
		
		override function createChildren():Void {
			super.createChildren();
			areaOverlaysContainer = createOverlaysContainer();
			wayOverlaysContainer = createOverlaysContainer();
			wayNodesOverlaysContainer = createOverlaysContainer();
			nodeOverlaysContainer = createOverlaysContainer();
			areaHighlight = createAreaOverlay(areaHighlightRenderer);
			areaSelection = createAreaOverlay(areaSelectionRenderer);
			nodeHighlight = createNodeOverlay(nodeHighlightRenderer);
			nodeSelection = createNodeOverlay(nodeSelectionRenderer);			
		}
		
		override function commitProperties():Void {
			if (mapChanged) {
				editingActions.deactivate();
				if (map) {
					editingActions = MapEditorActionBuilder(actionBuilder).buildEditingActions(moveMapRunner, mouseClickHandler);
				} else {
					editingActions = new DummyTriggerHandler();
				}
				editingEnabledChanged = true;
			}
			super.commitProperties();
			if (editingEnabledChanged) {
				if (editingEnabled) {
					addEditingListeners();
					editingActions.activate();
				} else {
					removeEditingListeners();
					editingActions.deactivate();
				}
				editingEnabledChanged = false;
			}
		}
		
		
		
		function createOverlaysContainer():Sprite {
			var container:Sprite = new Sprite();
			container.mouseChildren = false;
			container.mouseEnabled = false;
			addChild(container);
			return container;
		}
		
		function createNodeOverlay(renderer:IFactory):DisplayObject {
			var overlay:DisplayObject = renderer.newInstance();
			overlay.visible = false;
			nodeOverlaysContainer.addChild(overlay);
			return overlay;
		}
		
		function setupNodeOverlay(overlay:DisplayObject, node:Node):Void {
			INodeRenderer(overlay).data = node;
			moveNodeOverlay(overlay, node);
			overlay.visible = true;
		}
		
		function purgeNodeOverlay(overlay:DisplayObject):Void {
			INodeRenderer(overlay).data = null;
			overlay.visible = false;
		}
		
		function moveNodeOverlay(overlay:DisplayObject, node:Node):Void {
			// Consider using overlay shift when moving rather than project.
			var point:Point = map.project(node.position);
			point.offset(-map.x, -map.y);
			overlay.x = Math.round(point.x);
			overlay.y = Math.round(point.y);
		}
		
		function setupWayOverlays(wayOverlays:Array<Dynamic>, nodeOverlays:Array<Dynamic>, wayRenderer:IFactory, nodeRenderer:IFactory, wayId:String):Void {
			var wayParts:Array<Dynamic> = mapView.getWayParts(wayId);
			for (wayPart in wayParts) {
				var overlay:DisplayObject = wayRenderer.newInstance();
				IWayRenderer(overlay).data = wayPart;
				wayOverlays.push(overlay);
				wayOverlaysContainer.addChild(overlay);
				// Don't draw node overlays for clipped endings. But don't modify original nodes.
				var nodes:Array<Dynamic> = wayPart.nodes.toArray();
				var firstNode:Node = nodes[0];
				if (!firstNode.position.equals(map.getNode(firstNode.id).position)) {
					nodes.shift();
				}
				var lastNode:Node = nodes[nodes.length - 1];
				if (!lastNode.position.equals(map.getNode(lastNode.id).position)) {
					nodes.pop();
				}
				moveNodeOverlay(overlay, firstNode);
				for (node in nodes) {
					var nodeOverlay:DisplayObject = nodeRenderer.newInstance();
					setupNodeOverlay(nodeOverlay, node);
					nodeOverlays.push(nodeOverlay);
					wayNodesOverlaysContainer.addChild(nodeOverlay);
				}
			}
		}
		
		function purgeWayOverlays(wayOverlays:Array<Dynamic>, nodeOverlays:Array<Dynamic>):Void {
			while (wayOverlays.length > 0) {
				wayOverlaysContainer.removeChild(wayOverlays.pop());
			}
			while (nodeOverlays.length > 0) {
				wayNodesOverlaysContainer.removeChild(nodeOverlays.pop());
			}
		}
		
		function moveWayOverlays(wayOverlays:Array<Dynamic>, nodeOverlays:Array<Dynamic>):Void {
			var overlay:DisplayObject;
			for (overlay in wayOverlays) {
				var firstNode:Node = IWayRenderer(overlay).data.nodes.getItemAt(0);
				moveNodeOverlay(overlay, firstNode);
			}
			for (overlay in nodeOverlays) {
				moveNodeOverlay(overlay, cast( INodeRenderer(overlay).data, Node));
			}
		}
		
		function createAreaOverlay(renderer:IFactory):DisplayObject {
			var overlay:DisplayObject = renderer.newInstance();
			overlay.visible = false;
			areaOverlaysContainer.addChild(overlay);
			return overlay;
		}
		
		function setupAreaOverlay(overlay:DisplayObject, areaId:String):Void {
			var area:Way = mapView.getArea(areaId);
			if (area) {
				IAreaRenderer(overlay).data = area;
				var firstNode:Node = cast( area.nodes.getItemAt(0), Node);
				var areaPosition:Point = map.project(firstNode.position);
				overlay.x = areaPosition.x - map.x;
				overlay.y = areaPosition.y - map.y;
				overlay.visible = true;
			}
		}
		
		function purgeAreaOverlay(overlay:DisplayObject):Void {
			IAreaRenderer(overlay).data = null;
			overlay.visible = false;
		}
		
		function setupWaySelection(id:String):Void {
			setupWayOverlays(wayPartSelections, wayNodesSelections, waySelectionRenderer, nodeSelectionRenderer, id);
			setupAreaOverlay(areaSelection, id);
		}
		function setupWayHighlight(id:String):Void {
			setupWayOverlays(wayPartHighlights, wayNodesHighlights, wayHighlightRenderer, nodeHighlightRenderer, id);
			setupAreaOverlay(areaHighlight, id);
		}
		function purgeWaySelection():Void {
			purgeWayOverlays(wayPartSelections, wayNodesSelections);
			purgeAreaOverlay(areaSelection);
		}
		function purgeWayHighlight():Void {
			purgeWayOverlays(wayPartHighlights, wayNodesHighlights);
			purgeAreaOverlay(areaHighlight);
		}
		
		function addNode():Void {
			editService.addNode(map.unproject(new Point(map.x + mouseX, map.y + mouseY)));
		}
		
		function showWayPreviewSegment():Void {
			var way:Way = cast( editService.selection, Way);
			var renderer:IFactory = mapView.findWayRenderer(way);
			wayPreviewSegment = cast( renderer.newInstance(), DisplayObject);
			wayPreviewSegment.alpha = WAY_PREVIEW_SEGMENT_ALPHA;
			if (Std.is( wayPreviewSegment, InteractiveObject)) {
				InteractiveObject(wayPreviewSegment).mouseEnabled = false;
			}
			updateWayPreviewSegment();
			wayOverlaysContainer.addChildAt(wayPreviewSegment, 0);
		}
		
		function hideWayPreviewSegment():Void {
			wayOverlaysContainer.removeChild(wayPreviewSegment);
			wayPreviewSegment = null;
		}
		
		function updateWayPreviewSegment():Void {
			if (!wayPreviewSegment) return;
			var way:Way = cast( editService.selection, Way);
			var nodes:Array<Dynamic> = way.nodes.source;
			var node1:Node = Node(nodes[nodes.length - 1]).clone();
			var node2:Node = new Node(null, node1.tags.source.slice(), null);
			var position:Point = map.project(node1.position);
			position.offset(-map.x, -map.y);
			node1.move(0, 0);
			node2.move(mouseX - position.x, mouseY - position.y);
			var previewWay:Way = new Way(null, way.tags.source, [node1, node2]);
			wayPreviewSegment.x = position.x;
			wayPreviewSegment.y = position.y;
			IWayRenderer(wayPreviewSegment).data = previewWay;
		}
		
		function getInsertWayNodePosition(way:Way):Dynamic {
			var wayPoints:Array<Dynamic> = map.getWay(way.id).getNodePoints();
			var shiftX:Int = mapView.initialX - map.x;
			var shiftY:Int = mapView.initialY - map.y;
			for (point in wayPoints) {
				point.offset(shiftX, shiftY);
			}
			
			var mousePoint:Point = new Point(mouseX, mouseY);
			var projection:PolylinePointProjection = new PolylinePointProjection(wayPoints);
			projection.projectPoint(mousePoint);
			
			var index:Int;
			var position:LatLong = null;
			if (projection.projectionPoint != null) {
				var positionPoint:Point = projection.projectionPoint.clone();
				positionPoint.offset(map.x, map.y);
				index = projection.projectionIndex;
				position = map.unproject(positionPoint);
			}
			else {
				var normalization:PolylinePointNormalization = new PolylinePointNormalization(wayPoints);
				index = normalization.normalizePoint(mousePoint);
			}
			
			return {index: index, position: position};
		}
		
		override function addMapListeners():Void {
			super.addMapListeners();
			map.addEventListener(MapEvent.COORDINATES_CHANGE, map_coordinatesChangeHandler, false, 0, true);
		}
		
		override function removeMapListeners():Void {
			super.removeMapListeners();
			map.removeEventListener(MapEvent.COORDINATES_CHANGE, map_coordinatesChangeHandler);
		}
		
		function addEditingListeners():Void {
			addEventListener(MouseEvent.MOUSE_OUT, mouseOutHandler);
			addEventListener(MouseEvent.MOUSE_OVER, mouseOverHandler);
			addEventListener(MouseEvent.MOUSE_DOWN, mouseDownHandler);
			addEventListener(KeyboardEvent.KEY_UP, handleKeyUp);
			areaTimer.addEventListener(TimerEvent.TIMER_COMPLETE, areaTimer_timerCompleteHandler);
		}
		
		function removeEditingListeners():Void {
			removeEventListener(MouseEvent.MOUSE_OUT, mouseOutHandler);
			removeEventListener(MouseEvent.MOUSE_OVER, mouseOverHandler);
			removeEventListener(MouseEvent.MOUSE_DOWN, mouseDownHandler);
			removeEventListener(KeyboardEvent.KEY_UP, handleKeyUp);
			areaTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, areaTimer_timerCompleteHandler);
		}
		
		function mouseDownHandler(event:MouseEvent):Void {
			if (!editService.addingWay) {
				var target:IDataRenderer = cast( event.target, IDataRenderer);
				if (!target || !editService.selection || (target.data.id != editService.selection.id)) {
					addEventListener(MouseEvent.MOUSE_UP, mouseUpHandler);
				}
			}
		}
		
		override function doubleClickHandler(event:MouseEvent):Void {
			if (editingEnabled) {
				var target:IDataRenderer = cast( event.target, IDataRenderer);
				if (!target || (Std.is( target, IAreaRenderer))) {
					if (Std.is( target, IAreaRenderer)) {
						removeEventListener(MouseEvent.MOUSE_MOVE, mouseMoveHandler);
						purgeWayHighlight();
						areaTimer.reset();
					}
					addNode();
				}
			} else {
				super.doubleClickHandler(event);
			}
		}
		
		function mouseClickHandler(event:MouseEvent):Void {
			var selection:Way = cast( editService.selection, Way);
			var lastNode:Node = cast( selection.nodes.getItemAt(selection.nodes.length - 1), Node);
			var target:IDataRenderer = cast( event.target, IDataRenderer)
			if (target) {
				if (target.data == lastNode) {
					finishAddingWay();
				}
				else if (Std.is( target, INodeRenderer)) {
					// Join way to existing node.
					editService.joinWayNode(cast( target.data, Node));
				}
				else if (Std.is( target, IWayRenderer)) {
					var way:Way = cast( target.data, Way);
					var result:Dynamic = getInsertWayNodePosition(way);
					
					// Join to way.
					if (result.position != null) {
						editService.insertAndAppendWayNode(way, result.index, result.position);
					}
					// Join to way node.
					else {
						var joinNode:Node = cast( way.nodes.getItemAt(result.index), Node);
						editService.joinWayNode(joinNode);
					}
				}
				else if (Std.is( target, IAreaRenderer)) {
					addNode();
				}
			}
			else {
				// Add new node to way.
				addNode();
			}
			updateWayPreviewSegment();
		}
		
		function addingWay_doubleClickHandler(event:MouseEvent):Void {
			var target:INodeRenderer = cast( event.target, INodeRenderer);
			var way:Way = cast( editService.selection, Way);
			var lastNode:Node = cast( way.nodes.getItemAt(way.nodes.length - 1), Node);
			if (target && (target.data == lastNode)) {
				finishAddingWay();
			}
		}
		
		function finishAddingWay():Void {
			var way:Way = cast( editService.selection, Way);
			// Finish adding way by clicking on last node if way is valid.
			if (way.isArea) {
				if (way.nodes.length >= 3) {
					editService.finishAddingWay();
				}
			} else if (way.nodes.length >= 2) {
				editService.finishAddingWay();
			}
		}
		
		function mouseUpHandler(event:MouseEvent):Void {
			removeEventListener(MouseEvent.MOUSE_UP, mouseUpHandler);
			var target:IDataRenderer = cast( event.target, IDataRenderer);
			if (Std.is( target, INodeRenderer)) {
				purgeNodeOverlay(nodeHighlight);
				editService.selection = cast( target.data, Node);
			} else if (Std.is( target, IWayRenderer)) {
				purgeWayHighlight();
				var way:Way = map.getWay(target.data.id);
				editService.selection = way;
			} else if (Std.is( target, IAreaRenderer)) {
				// Force selection because double click will be impossible and mouseOut can be dispatched.
				addEventListener(MouseEvent.MOUSE_MOVE, mouseMoveHandler);
				selectedArea = map.getWay(target.data.id);
				areaTimer.reset();
				areaTimer.start();
			} else {
				editService.selection = null;
			}
		}
		
		function mouseOverHandler(event:MouseEvent):Void {
			if (event.buttonDown) return;
			var target:IDataRenderer = cast( event.target, IDataRenderer);
			if (!target) return;
			var element:Element = cast( target.data, Element);
			if (Std.is( target, INodeRenderer)) {
				if (element != editService.selection) {
					setupNodeOverlay(nodeHighlight, cast( element, Node));
				}
			} else if ((Std.is( target, IWayRenderer)) || (Std.is( target, IAreaRenderer))) {
				if (!editService.selection || (element.id != editService.selection.id)) {
					setupWayHighlight(element.id);
				}
			}
		}
		
		function mouseOutHandler(event:MouseEvent):Void {
			var target:IDataRenderer = cast( event.target, IDataRenderer);
			if (Std.is( target, INodeRenderer)) {
				purgeNodeOverlay(nodeHighlight);
			} else if ((Std.is( target, IWayRenderer)) || (Std.is( target, IAreaRenderer))) {
				purgeWayHighlight();
			}
		}
		
		function mouseMoveHandler(event:MouseEvent):Void {
			removeEventListener(MouseEvent.MOUSE_MOVE, mouseMoveHandler);
			areaTimer.reset();
			areaTimer_timerCompleteHandler(null);
		}
		
		function handleKeyUp(event:KeyboardEvent):Void {
			if ((event.keyCode == Keyboard.DELETE) || (event.keyCode == Keyboard.BACKSPACE)) {
				editService.removeSelectedElement();
			}
			else if (event.keyCode == Keyboard.ENTER) {
				var way:Way = cast( editService.selection, Way)
				if (way && !way.isArea) {
					finishAddingWay();
				}
			}
			else if (event.keyCode == Keyboard.ESCAPE) {
				if (editService.addingWay) {
					editService.revertAddingWay();
				}
			}
		}
		
		function map_coordinatesChangeHandler(event:MapEvent):Void {
			purgeNodeOverlay(nodeHighlight);
			purgeAreaOverlay(areaHighlight);
			purgeWayOverlays(wayPartHighlights, wayNodesHighlights);
			if (Std.is( editService.selection, Node)) {
				moveNodeOverlay(nodeSelection, cast( editService.selection, Node));
			} else if (Std.is( editService.selection, Way)) {
				updateWayPreviewSegment();
				moveWayOverlays(wayPartSelections, wayNodesSelections);
				setupAreaOverlay(areaSelection, editService.selection.id);
			}
			removeEventListener(MouseEvent.MOUSE_UP, mouseUpHandler);
		}
		
		function previewSegment_mouseMoveHandler(event:MouseEvent):Void {
			updateWayPreviewSegment();
		}
		
		function editService_addingWayChangeHandler(event:MapEditServiceEvent):Void {
			if (editService.addingWay) {
				showWayPreviewSegment();
				addEventListener(MouseEvent.DOUBLE_CLICK, addingWay_doubleClickHandler);
				stage.addEventListener(MouseEvent.MOUSE_MOVE, previewSegment_mouseMoveHandler);
			} else {
				hideWayPreviewSegment();
				removeEventListener(MouseEvent.DOUBLE_CLICK, addingWay_doubleClickHandler);
				stage.removeEventListener(MouseEvent.MOUSE_MOVE, previewSegment_mouseMoveHandler);
			}
		}
		
		function editService_selectionChangeHandler(event:MapEditServiceSelectionChangeEvent):Void {
			// Workaround for adding way:
			if (editService.selection == null) {
				editService.finishAddingWay();
			}
			selectedArea = null;
			// Remove just in case.
			mapView.removeEventListener(FlexEvent.UPDATE_COMPLETE, mapView_updateCompleteHandler);
			if (Std.is( event.oldSelection, Node)) {
				purgeNodeOverlay(nodeSelection);
			} else if (Std.is( event.oldSelection, Way)) {
				purgeWaySelection();
			}
			if (Std.is( editService.selection, Node)) {
				setupNodeOverlay(nodeSelection, cast( editService.selection, Node));
			} else if (Std.is( editService.selection, Way)) {
				if (mapView.mx_internal::invalidateDisplayListFlag) {
					// Wait until ways will be divided into parts so they will be accessible through getWayParts.
					mapView.addEventListener(FlexEvent.UPDATE_COMPLETE, mapView_updateCompleteHandler);
				} else {
					mapView_updateCompleteHandler(null);
				}
			}
		}
		
		function mapView_updateCompleteHandler(event:FlexEvent):Void {
			mapView.removeEventListener(FlexEvent.UPDATE_COMPLETE, mapView_updateCompleteHandler);
			setupWaySelection(editService.selection.id);
		}
		
		function areaTimer_timerCompleteHandler(event:TimerEvent):Void {
			removeEventListener(MouseEvent.MOUSE_MOVE, mouseMoveHandler);
			purgeWayHighlight();
			editService.selection = selectedArea;
		}
	}
