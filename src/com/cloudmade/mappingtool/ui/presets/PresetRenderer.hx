/**
 * Renders element preset representation (icon and label), used in presets and hint panels
 * 
 * @see com.cloudmade.mappingtool.config.IElementPreset
 */
package com.cloudmade.mappingtool.ui.presets;

	import com.cloudmade.mappingtool.config.IElementPreset;
	
	import mx.controls.Image;
	import mx.controls.Label;
	import mx.controls.listClasses.IListItemRenderer;
	import mx.core.UIComponent;

	class PresetRenderer extends UIComponent, implements IListItemRenderer {
		
		public var data(getData, setData) : Dynamic;
		var label:Label;
		var icon:Image;
		
		public function new() {
			super();
		}
		
		var dataChanged:Bool ;
		var _data:Dynamic;
		public function getData():Dynamic{
			return _data;
		}
		public function setData(value:Dynamic):Dynamic{
			if (value == _data) return;
			_data = value;
			dataChanged = true;
			invalidateProperties();
			invalidateSize();
			invalidateDisplayList();
			return value;
		}
		
		override function createChildren():Void {
			super.createChildren();
			
			label = new Label();
			addChild(label);
			
			icon = new Image();
			icon.setStyle("horizontalAlign", "center");
			icon.setStyle("verticalAlign", "middle");
			addChild(icon);
		}
		
		override function commitProperties():Void {
			super.commitProperties();
			if (dataChanged) {
				dataChanged = false;
				commitData();
			}
		}
		
		function commitData():Void {
			var preset:IElementPreset = cast( data, IElementPreset);
			label.text = preset.name;
			icon.source = preset.icon.newInstance();
			toolTip = preset.name;
		}
		
		override function measure():Void {
			super.measure();
			measuredWidth = Math.max(label.measuredWidth, icon.measuredWidth);
			measuredMinWidth = Math.max(label.measuredMinWidth, icon.measuredMinWidth);
			measuredHeight = label.measuredHeight + icon.measuredHeight;
			measuredMinHeight = label.measuredMinHeight + icon.measuredMinHeight;
		}
		
		override function updateDisplayList(unscaledWidth:Int, unscaledHeight:Int):Void {
			super.updateDisplayList(unscaledWidth, unscaledHeight);
			
			var labelHeight:Int = label.getExplicitOrMeasuredHeight();
			label.setActualSize(unscaledWidth, labelHeight);
			
			var labelY:Int = unscaledHeight - labelHeight;
			label.move(0, labelY);
			
			var iconWidth:Int = Math.min(unscaledWidth, icon.measuredWidth);
			var iconHeight:Int = Math.min(unscaledHeight - labelHeight, icon.measuredHeight);
			icon.setActualSize(iconWidth, iconHeight);
			
			var iconX:Int = Math.round((unscaledWidth - iconWidth) / 2);
			var iconY:Int = Math.round((labelY - iconHeight) / 2);
			icon.move(iconX, iconY);
		}
	}
