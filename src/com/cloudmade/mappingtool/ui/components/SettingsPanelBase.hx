package com.cloudmade.mappingtool.ui.components;

	import com.cloudmade.mappingtool.model.settings.UserSettings;
	
	import flash.display.DisplayObject;
	import flash.display.InteractiveObject;
	import flash.events.MouseEvent;
	
	import mx.containers.Panel;
	import mx.controls.Button;

	class SettingsPanelBase extends Panel {
		
		/*[Bindable]*/
		public var settings:UserSettings;
		public var openButton:Button;

		public function new() {
			super();
			visible = false;
			addEventListener(Event.ADDED_TO_STAGE, addedToStageHandler);
		}

		function stage_clickHandler(event:MouseEvent):Void {
			var target:InteractiveObject = cast( event.target, InteractiveObject);
			if (target == openButton) {
				visible = !visible;
			} else if (isOutsideObject(target)) {
				visible = false;
			}
		}
		
		function isOutsideObject(object:DisplayObject):Bool {
			var isThis:Bool = object == this;
			var isChild:Bool = contains(object);
			return !isThis && !isChild;
		}
		
		function addedToStageHandler(event:Event):Void {
			stage.addEventListener(MouseEvent.CLICK, stage_clickHandler);
		}
	}
