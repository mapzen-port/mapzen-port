package com.cloudmade.mappingtool.ui.components;

	import com.cloudmade.mappingtool.model.ApplicationData;
	import com.cloudmade.mappingtool.services.MapEditService;
	import com.cloudmade.mappingtool.ui.windows.SaveWindow;
	
	import flash.events.Event;
	
	import mx.containers.ApplicationControlBar;
	import mx.controls.Button;
	import mx.events.FlexEvent;
	
	/*[Event(name="exit", type="flash.events.Event")]*/

	class ControlsBarBase extends ApplicationControlBar {
		
		/*[Bindable]*/
		public var showExitButton:Bool;
		public var settingsButton:Button;
		public var settingsPanel:SettingsPanel;
		
		/*[Bindable]*/
		var appData:ApplicationData ;
		/*[Bindable]*/
		var editService:MapEditService;
		
		public function new() {
			
			appData = ApplicationData.getInstance();
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteHandler);
		}
		
		function undo():Void {
			editService.undo();
		}
		
		function redo():Void {
			editService.redo();
		}
		
		function exit():Void {
			dispatchEvent(new Event("exit"));
		}

		function save():Void {
			var saveWindow:SaveWindow = new SaveWindow();
			saveWindow.open();
		}
		
		function creationCompleteHandler(event:FlexEvent):Void {
			editService = MapEditService.getInstance();
			settingsPanel.openButton = settingsButton;
		}
	}
