package com.cloudmade.mappingtool.ui.components;

	import com.cloudmade.mappingtool.commands.editing.events.ChangeElementTagEvent;
	import com.cloudmade.mappingtool.commands.editing.events.ElementTagEditingEvent;
	import com.cloudmade.mappingtool.map.model.ITag;
	import com.cloudmade.mappingtool.map.model.Tag;
	import com.cloudmade.mappingtool.model.ApplicationData;
	import com.cloudmade.mappingtool.services.MapEditService;
	import com.cloudmade.mappingtool.ui.controls.FocusableList;
	import com.cloudmade.mappingtool.ui.renderers.TagRenderer;
	
	import flash.events.Event;
	
	import mx.collections.ArrayList;
	import mx.containers.Canvas;
	import mx.effects.Move;
	
	class MetadataPanelBase extends Canvas {
		
		public var hidden(getHidden, setHidden) : Bool;
		/*[Bindable]*/
		public var editService:MapEditService;
		
		public var moveIn:Move;
		public var moveOut:Move;
		public var list:FocusableList;
		
		/*[Bindable(event="unused")]*/
		var appData:ApplicationData ;
		
		public function new() {
			
			appData = ApplicationData.getInstance();
			super();
			addEventListener("hiddenChange", hiddenChangeHandler);
			addEventListener(ElementTagEditingEvent.REMOVE_ELEMENT_TAG, tagEditingHandler);
			addEventListener(ChangeElementTagEvent.CHANGE_ELEMENT_TAG_KEY, tagEditingHandler);
			addEventListener(ChangeElementTagEvent.CHANGE_ELEMENT_TAG_VALUE, tagEditingHandler);
		}

		var _hidden:Bool ;
		/*[Bindable(event="hiddenChange")]*/
		public function getHidden():Bool{
			return _hidden;
		}
		public function setHidden(value:Bool):Bool{
			if (value == _hidden) return;
			_hidden = value;
			dispatchEvent(new Event("hiddenChange"));
			return value;
		}
		
		function addTag():Void {
			if (!previousTagIsEmpty()) {
				var newTag:Tag = new Tag("", "");
				tagEditingHandler(new ElementTagEditingEvent(ElementTagEditingEvent.ADD_ELEMENT_TAG, null, newTag));
			}
			list.validateNow();
			setFocusOnLastRenderer();
		}
		
		function previousTagIsEmpty():Bool {
			var isEmpty:Bool = false;
			var selectionTags:ArrayList = editService.selection.tags;
			if (selectionTags.length > 0) {
				var lastTag:ITag = cast( selectionTags.getItemAt(selectionTags.length - 1), ITag);
				isEmpty = lastTag.isEmpty;
			}
			return isEmpty;
		}
		
		function setFocusOnLastRenderer():Void {
			list.verticalScrollPosition = list.maxVerticalScrollPosition;
			var lastRendererIndex:Int = editService.selection.tags.length - 1;
			var renderer:TagRenderer = cast( list.indexToItemRenderer(lastRendererIndex), TagRenderer);
			renderer.keyInput.setFocus();
		}
		
		function hiddenChangeHandler(event:Event):Void {
			if (hidden) {
				moveIn.stop();
				moveOut.play();
			} else {
				moveOut.stop();
				moveIn.play();
			}
		}
		
		function tagEditingHandler(event:ElementTagEditingEvent):Void {
			event.element = editService.selection;
			editService.executeEditingCommand(event);
			event.stopPropagation();
		}
	}
