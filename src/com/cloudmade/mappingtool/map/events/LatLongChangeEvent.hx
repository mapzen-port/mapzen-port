package com.cloudmade.mappingtool.map.events;

	import com.cloudmade.mappingtool.map.geo.LatLong;
	
	import flash.events.Event;
	
	class LatLongChangeEvent extends Event {
		
		public static var CENTER_CHANGE:String = "centerChange";
		public static var POSITION_CHANGE:String = "positionChange";
		
		public var oldValue:LatLong;
		
		public function new(type:String, oldValue:LatLong) {
			super(type);
			this.oldValue = oldValue;
		}
		
		public override function clone():Event {
			return new LatLongChangeEvent(type, oldValue);
		}
	}
