package com.cloudmade.mappingtool.map.events;

	import com.cloudmade.mappingtool.map.geo.LatLongBounds;
	
	import flash.events.Event;

	class BoundsChangeEvent extends Event {
		
		public static var BOUNDS_CHANGE:String = "boundsChange";
		
		public var oldBounds:LatLongBounds;
		
		public function new(type:String, oldBounds:LatLongBounds) {
			super(type);
		}
		
		public override function clone():Event {
			return new BoundsChangeEvent(type, oldBounds);
		}
	}
