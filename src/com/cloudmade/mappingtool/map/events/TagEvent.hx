package com.cloudmade.mappingtool.map.events;

	import flash.events.Event;

	class TagEvent extends Event {
		
		public static var KEY_CHANGE:String = "keyChange";
		public static var VALUE_CHANGE:String = "valueChange";
		
		public function new(type:String) {
			super(type);
		}
		
		public override function clone():Event {
			return new TagEvent(type);
		}
	}
