package com.cloudmade.mappingtool.map.events;

	import flash.events.Event;
	
	class ResizeEvent extends Event {
		
		public static var RESIZE:String = "resize";
		
		public var oldWidth:Int;
		public var oldHeight:Int;
		
		public function new(type:String, oldWidth:Int, oldHeight:Int) {
			super(type);
			this.oldWidth = oldWidth;
			this.oldHeight = oldHeight;
		}
		
		public override function clone():Event {
			return new ResizeEvent(type, oldWidth, oldHeight);
		}
	}
