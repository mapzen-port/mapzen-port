package com.cloudmade.mappingtool.map.events;

	import flash.events.Event;

	class MapEvent extends Event {
		
		public static var ELEMENTS_CHANGE:String = "elementsChange";
		public static var COORDINATES_CHANGE:String = "coordinatesChange";
		
		public function new(type:String) {
			super(type);
		}
		
		public override function clone():Event {
			return new MapEvent(type);
		}
	}
