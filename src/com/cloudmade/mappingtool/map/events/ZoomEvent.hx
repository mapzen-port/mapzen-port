package com.cloudmade.mappingtool.map.events;

	import flash.events.Event;

	class ZoomEvent extends Event {
		
		public static var ZOOM:String = "zoom";
		
		public var oldZoomLevel:Int;
		
		public function new(type:String, oldZoomLevel:Int) {
			super(type);
			this.oldZoomLevel = oldZoomLevel;
		}
		
		public override function clone():Event {
			return new ZoomEvent(type, oldZoomLevel);
		}
	}
