package com.cloudmade.mappingtool.map.model;

	import com.cloudmade.mappingtool.map.events.LatLongChangeEvent;
	import com.cloudmade.mappingtool.map.geo.LatLong;
	
	import flash.utils.IDataInput;
	import flash.utils.IDataOutput;
	
	/*[RemoteClass(alias="com.cloudmade.mappingtool.map.model.Node")]*/
	
	/*[Event(name="positionChange", type="com.cloudmade.mappingtool.map.events.LatLongChangeEvent")]*/
	
	class Node extends Element {
		
		public var position(getPosition, setPosition) : LatLong;
		public function new(?id:String = null, ?tags:Array<Dynamic> = null, ?position:LatLong = null) {
			super(id, tags);
			_position = position;
		}
		
		var _position:LatLong;
		/*[Bindable(event="positionChange")]*/
		public function getPosition():LatLong{
			return _position;
		}
		public function setPosition(value:LatLong):LatLong{
			if (value.equals(_position)) return;
			var changeEvent:LatLongChangeEvent = new LatLongChangeEvent(LatLongChangeEvent.POSITION_CHANGE, _position);
			_position = value;
			dispatchEvent(changeEvent);
			return value;
		}
		
		public function clone():Node {
			var node:Node = new Node(id, tags.toArray(), _position.clone());
			node.version = version;
			return node;
		}
		
		public override function readExternal(input:IDataInput):Void {
			super.readExternal(input);
			_position = input.readObject();
		}
		
		public override function writeExternal(output:IDataOutput):Void {
			super.writeExternal(output);
			output.writeObject(_position);
		}
	}
