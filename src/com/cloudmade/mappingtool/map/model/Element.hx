package com.cloudmade.mappingtool.map.model;

	import flash.events.EventDispatcher;
	import flash.utils.IDataInput;
	import flash.utils.IDataOutput;
	import flash.utils.IExternalizable;
	
	import mx.collections.ArrayList;
	
	/*[RemoteClass(alias="com.cloudmade.mappingtool.map.model.Element")]*/

	class Element extends EventDispatcher, implements IExternalizable {
		
		public var id(getId, setId) : String;
		public var isNew(getIsNew, null) : Bool ;
		public var tags(getTags, null) : ArrayList ;
		public var version(getVersion, setVersion) : String;
		public var x(getX, setX) : Int;
		public var y(getY, setY) : Int;
		public function new(?id:String = null, ?tags:Array<Dynamic> = null) {
			super();
			_id = id;
			_tags = new ArrayList(tags);
		}
		
		public function getIsNew():Bool {
			return _id.charAt(0) == "-";
		}
		
		var _tags:ArrayList;
		public function getTags():ArrayList {
			return _tags;
		}
		
		var _id:String;
		public function getId():String{
			return _id;
		}
		public function setId(value:String):String{
			_id = value;
			return value;
		}
		
		var _version:String ;
		public function getVersion():String{
			return _version;
		}
		public function setVersion(value:String):String{
			_version = value;
			return value;
		}
		
		var _x:Int;
		public function getX():Int{
			return _x;
		}
		public function setX(value:Int):Int{
			_x = value;
			return value;
		}
		
		var _y:Int;
		public function getY():Int{
			return _y;
		}
		public function setY(value:Int):Int{
			_y = value;
			return value;
		}
		
		public function getTag(key:String, value:String):Tag {
			var copy:Tag = new Tag(key, value);
			var tags:Array<Dynamic> = _tags.source;
			for (tag in tags) {
				if (tag.equals(copy)) {
					return tag;
				}
			}
			return null;
		}
		
		public function findTag(key:String):Tag {
			var tags:Array<Dynamic> = _tags.source;
			for (tag in tags) {
				if (tag.key == key) {
					return tag;
				}
			}
			return null;
		}
		
		public function cloneTags():Array<Dynamic> {
			var clonedTags:Array<Dynamic> = new Array();
			var tagsSource:Array<Dynamic> = tags.source;
			for (tag in tagsSource) {
				clonedTags.push(tag.clone());
			}
			return clonedTags;
		}
		
		public function move(x:Int, y:Int):Void {
			_x = x;
			_y = y;
		}
		
		public function readExternal(input:IDataInput):Void {
			_id = input.readUTF();
			_tags.source = input.readObject();
			_version = input.readUTF();
		}
		
		public function writeExternal(output:IDataOutput):Void {
			output.writeUTF(_id);
			output.writeObject(_tags.source);
			output.writeUTF(_version);
		}
	}
