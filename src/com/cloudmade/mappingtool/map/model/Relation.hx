package com.cloudmade.mappingtool.map.model;

	import flash.utils.IDataInput;
	import flash.utils.IDataOutput;
	
	import mx.collections.ArrayList;
	
	/*[RemoteClass(alias="com.cloudmade.mappingtool.map.model.Relation")]*/
	
	class Relation extends Element {
		
		public var members(getMembers, null) : ArrayList ;
		public function new(?id:String = null, ?tags:Array<Dynamic> = null, ?members:Array<Dynamic> = null) {
			super(id, tags);
			_members = new ArrayList(members);
		}
		
		var _members:ArrayList;
		public function getMembers():ArrayList {
			return _members;
		}
		
		public override function readExternal(input:IDataInput):Void {
			super.readExternal(input);
			_members.source = input.readObject();
		}
		
		public override function writeExternal(output:IDataOutput):Void {
			super.writeExternal(output);
			output.writeObject(_members.source);
		}
	}
