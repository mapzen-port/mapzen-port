package com.cloudmade.mappingtool.map.model;

	interface IMapElements
	{
		function nodes():Array<Dynamic>;
		
		function ways():Array<Dynamic>;
		
		function relations():Array<Dynamic>;
	}
