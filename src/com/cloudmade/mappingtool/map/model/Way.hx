package com.cloudmade.mappingtool.map.model;

	import flash.events.Event;
	import flash.geom.Point;
	import flash.utils.IDataInput;
	import flash.utils.IDataOutput;
	
	import mx.collections.ArrayList;
	
	/*[RemoteClass(alias="com.cloudmade.mappingtool.map.model.Way")]*/
	
	class Way extends Element {
		
		public var isOneWay(getIsOneWay, setIsOneWay) : Bool;
		public var isReverse(getIsReverse, null) : Bool ;
		public var nodes(getNodes, null) : ArrayList ;
		public var isArea:Bool ;
		
		public function new(?id:String = null, ?tags:Array<Dynamic> = null, ?nodes:Array<Dynamic> = null) {
			
			isArea = false;
			super(id, tags);
			_nodes = new ArrayList(nodes);
		}
		
		var _nodes:ArrayList;
		public function getNodes():ArrayList {
			return _nodes;
		}
		
		public function getIsReverse():Bool {
			var tag:Tag = findTag("oneway");
			if (tag) {
				var result:Bool = tag.value == "-1";
				return result;
			}
			return false;
		}
		
		/*[Bindable(event="isOneWayChanged")]*/
		public function getIsOneWay():Bool{
			var tag:Tag = findTag("oneway");
			if (tag) {
				var result:Bool = (tag.value == "yes") || (tag.value == "-1") || (tag.value == "true");
				return result;
			}
			return false;
		}
		public function setIsOneWay(value:Bool):Bool{
			if (value == isOneWay) return;
			var tag:Tag = findTag("oneway");
			if (!tag) {
				tag = new Tag("oneway", null);
				tags.addItem(tag);
			}
			tag.value = value ? "yes" : "no";
			dispatchEvent(new Event("isOneWayChanged"));
			return value;
		}
		
		public function getNodePoints():Array<Dynamic> {
			var points:Array<Dynamic> = new Array();
			var nodes:Array<Dynamic> = _nodes.source;
			for (node in nodes) {
				points.push(new Point(node.x, node.y));
			}
			return points;
		}
		
		public function clone():Way {
			var way:Way = new Way(id, tags.toArray(), nodes.toArray());
			way.version = version;
			return way;
		}
		
		public override function readExternal(input:IDataInput):Void {
			super.readExternal(input);
			_nodes.source = input.readObject();
		}
		
		public override function writeExternal(output:IDataOutput):Void {
			super.writeExternal(output);
			output.writeObject(_nodes.source);
		}
	}
