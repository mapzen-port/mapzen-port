package com.cloudmade.mappingtool.map.model;

	import flash.utils.IDataInput;
	import flash.utils.IDataOutput;
	import flash.utils.IExternalizable;
	
	/*[RemoteClass(alias="com.cloudmade.mappingtool.map.model.Member")]*/
	
	class Member implements IExternalizable {
		
		public var elementId(getElementId, null) : String ;
		public var role(getRole, null) : String ;
		public var type(getType, null) : String ;
		public function new(?elementId:String = null, ?type:String = null, ?role:String = null) {
			_elementId = elementId;
			_type = type;
			_role = role;
		}
		
		var _elementId:String;
		public function getElementId():String {
			return _elementId;
		}
		
		var _type:String;
		public function getType():String {
			return _type;
		}
		
		var _role:String;
		public function getRole():String {
			return _role;
		}
		
		public function equals(other:Member):Bool {
			return (_type == other._type) && (_role == other._role) && (_elementId == other._elementId);
		}
		
		public function readExternal(input:IDataInput):Void {
			_elementId = input.readUTF();
			_type = input.readUTF();
			_role = input.readUTF();
		}
		
		public function writeExternal(output:IDataOutput):Void {
			output.writeUTF(_elementId);
			output.writeUTF(_type);
			output.writeUTF(_role);
		}
	}
