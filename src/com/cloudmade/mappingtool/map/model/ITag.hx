package com.cloudmade.mappingtool.map.model;

	interface ITag
	{
		function key():String;
		function key(value:String):Void;
		
		function value():String;
		function value(value:String):Void;
		
		function isEmpty():Bool;
		
		function equals(tag:ITag):Bool;
	}
