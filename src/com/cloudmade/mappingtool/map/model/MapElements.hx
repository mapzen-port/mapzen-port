package com.cloudmade.mappingtool.map.model;

	class MapElements implements IMapElements {
		
		public var nodes(getNodes, null) : Array<Dynamic>
		;
		public var relations(getRelations, null) : Array<Dynamic>
		;
		public var ways(getWays, null) : Array<Dynamic>
		;
		public function new(nodes:Array<Dynamic>, ways:Array<Dynamic>, relations:Array<Dynamic>) {
			_nodes = nodes;
			_ways = ways;
			_relations = relations;
		}

		var _nodes:Array<Dynamic>;
		public function getNodes():Array<Dynamic>
		{
			return _nodes;
		}
		
		var _ways:Array<Dynamic>;
		public function getWays():Array<Dynamic>
		{
			return _ways;
		}
		
		var _relations:Array<Dynamic>;
		public function getRelations():Array<Dynamic>
		{
			return _relations;
		}
		
	}
