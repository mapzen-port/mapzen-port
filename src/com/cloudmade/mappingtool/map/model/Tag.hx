package com.cloudmade.mappingtool.map.model;

	import com.cloudmade.mappingtool.map.events.TagEvent;
	
	import flash.events.EventDispatcher;
	import flash.utils.IDataInput;
	import flash.utils.IDataOutput;
	import flash.utils.IExternalizable;
	
	/*[RemoteClass(alias="com.cloudmade.mappingtool.map.model.Tag")]*/
	
	/*[Event(name="keyChange", type="com.cloudmade.mappingtool.map.events.TagEvent")]*/
	/*[Event(name="valueChange", type="com.cloudmade.mappingtool.map.events.TagEvent")]*/

	class Tag extends EventDispatcher, implements ITag, implements IExternalizable {
		
		public var isEmpty(getIsEmpty, null) : Bool ;
		public var key(getKey, setKey) : String;
		public var value(getValue, setValue) : String;
		public static var MAX_CHARS:Int = 255;
		
		public function new(?key:String = null, ?value:String = null) {
			super();
			_key = key;
			_value = value;
		}
		
		/*[Bindable(event="keyChange")]*/
		/*[Bindable(event="valueChange")]*/
		public function getIsEmpty():Bool {
			return ((_key == "") || (_key == null)) && ((_value == "") || (_value == null));
		}
		
		var _key:String;
		/*[Bindable(event="keyChange")]*/
		public function getKey():String{
			return _key;
		}
		public function setKey(value:String):String{
			if (value == _key) return;
			_key = value;
			dispatchEvent(new TagEvent(TagEvent.KEY_CHANGE));
			return value;
		}
		
		var _value:String;
		/*[Bindable(event="valueChange")]*/
		public function getValue():String{
			return _value;
		}
		public function setValue(value:String):String{
			if (value == _value) return;
			_value = value;
			dispatchEvent(new TagEvent(TagEvent.VALUE_CHANGE));
			return value;
		}
		
		public function equals(tag:ITag):Bool {
			return (_key == tag.key) && (_value == tag.value);
		}
		
		public function clone():Tag {
			return new Tag(_key, _value);
		}
		
		public function readExternal(input:IDataInput):Void {
			_key = input.readUTF();
			_value = input.readUTF();
		}
		
		public function writeExternal(output:IDataOutput):Void {
			output.writeUTF(_key);
			output.writeUTF(_value);
		}
		
		public override function toString():String {
			return key + "=" + value;
		}
	}
