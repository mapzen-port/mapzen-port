package com.cloudmade.mappingtool.map.model;

	import com.cloudmade.mappingtool.map.events.BoundsChangeEvent;
	import com.cloudmade.mappingtool.map.events.LatLongChangeEvent;
	import com.cloudmade.mappingtool.map.events.MapEvent;
	import com.cloudmade.mappingtool.map.events.ResizeEvent;
	import com.cloudmade.mappingtool.map.events.ZoomEvent;
	import com.cloudmade.mappingtool.map.geo.IProjection;
	import com.cloudmade.mappingtool.map.geo.ITransformation;
	import com.cloudmade.mappingtool.map.geo.LatLong;
	import com.cloudmade.mappingtool.map.geo.LatLongBounds;
	import com.cloudmade.mappingtool.map.geo.MercatorProjection;
	import com.cloudmade.mappingtool.map.geo.MercatorTransformation;
	
	import flash.events.EventDispatcher;
	import flash.geom.Point;
	import flash.utils.IDataInput;
	import flash.utils.IDataOutput;
	import flash.utils.IExternalizable;
	
	import mx.collections.ArrayList;
	
	/*[RemoteClass(alias="com.cloudmade.mappingtool.map.model.Map")]*/
	
	/*[Event(name="zoom", type="com.cloudmade.mappingtool.map.events.ZoomEvent")]*/
	/*[Event(name="resize", type="com.cloudmade.mappingtool.map.events.ResizeEvent")]*/
	/*[Event(name="elementsChange", type="com.cloudmade.mappingtool.map.events.MapEvent")]*/
	/*[Event(name="coordinatesChange", type="com.cloudmade.mappingtool.map.events.MapEvent")]*/
	/*[Event(name="boundsChange", type="com.cloudmade.mappingtool.map.events.BoundsChangeEvent")]*/
	/*[Event(name="centerChange", type="com.cloudmade.mappingtool.map.events.LatLongChangeEvent")]*/
	
	class Map extends EventDispatcher, implements IProjection, implements IExternalizable {
		
		public var bounds(getBounds, null) : LatLongBounds ;
		public var center(getCenter, setCenter) : LatLong;
		public var centerX(getCenterX, null) : Int ;
		public var centerY(getCenterY, null) : Int ;
		public var height(getHeight, setHeight) : Int;
		public var maxZoomLevel(getMaxZoomLevel, null) : Int ;
		public var minZoomLevel(getMinZoomLevel, null) : Int ;
		public var nodes(getNodes, null) : ArrayList ;
		public var relations(getRelations, null) : ArrayList ;
		public var tileSize(getTileSize, null) : Int ;
		public var ways(getWays, null) : ArrayList ;
		public var width(getWidth, setWidth) : Int;
		public var x(getX, null) : Int ;
		public var y(getY, null) : Int ;
		public var zoomLevel(getZoomLevel, setZoomLevel) : Int;
		var wayMap:ElementMap;
		var nodeMap:ElementMap;
		var relationMap:ElementMap;
		var projection:IProjection;
		var transformation:ITransformation;
		var coordinatesChanged:Bool ;
		var boundsUpdateEnabled:Bool ;
		
		public function new(?tileSize:Int = 256, ?minZoomLevel:Int = 0, ?maxZoomLevel:Int = 18) {
			
			coordinatesChanged = false;
			boundsUpdateEnabled = true;
			super();
			_tileSize = tileSize;
			_minZoomLevel = minZoomLevel;
			_maxZoomLevel = maxZoomLevel;
			projection = new MercatorProjection();
			transformation = new MercatorTransformation(tileSize, 0);
			nodeMap = new ElementMap(nodes);
			wayMap = new ElementMap(ways);
			relationMap = new ElementMap(relations);
		}
		
		var _tileSize:Int;
		public function getTileSize():Int {
			return _tileSize;
		}
		
		var _minZoomLevel:Int;
		public function getMinZoomLevel():Int {
			return _minZoomLevel;
		}
		
		var _maxZoomLevel:Int;
		public function getMaxZoomLevel():Int {
			return _maxZoomLevel;
		}
		
		var _nodes:ArrayList ;
		public function getNodes():ArrayList {
			return _nodes;
		}
		
		var _ways:ArrayList ;
		public function getWays():ArrayList {
			return _ways;
		}
		
		var _relations:ArrayList ;
		public function getRelations():ArrayList {
			return _relations;
		}
		
		var _bounds:LatLongBounds;
		/*[Bindable(event="boundsChange")]*/
		public function getBounds():LatLongBounds {
			return _bounds;
		}
		
		var _x:Int;
		/*[Bindable(event="coordinatesChange")]*/
		public function getX():Int {
			return _x;
		}
		
		var _y:Int;
		/*[Bindable(event="coordinatesChange")]*/
		public function getY():Int {
			return _y;
		}
		
		var _centerX:Int;
		/*[Bindable(event="zoom")]*/
		/*[Bindable(event="centerChange")]*/
		public function getCenterX():Int {
			return _centerX;
		}
		
		var _centerY:Int;
		/*[Bindable(event="zoom")]*/
		/*[Bindable(event="centerChange")]*/
		public function getCenterY():Int {
			return _centerY;
		}
		
		var _center:LatLong ;
		/*[Bindable(event="centerChange")]*/
		public function getCenter():LatLong{
			return _center;
		}
		public function setCenter(value:LatLong):LatLong{
			if (value.equals(_center)) return;
			var changeEvent:LatLongChangeEvent = new LatLongChangeEvent(LatLongChangeEvent.CENTER_CHANGE, _center);
			_center = value;
			dispatchEvent(changeEvent);
			updateCoordinates();
			return value;
		}
		
		var _width:Int ;
		/*[Bindable(event="resize")]*/
		public function getWidth():Int{
			return _width;
		}
		public function setWidth(value:Int):Int{
			if (value == _width) return;
			var resizeEvent:ResizeEvent = new ResizeEvent(ResizeEvent.RESIZE, _width, _height);
			_width = value;
			dispatchEvent(resizeEvent);
			updateCoordinates();
			return value;
		}
		
		var _height:Int ;
		/*[Bindable(event="resize")]*/
		public function getHeight():Int{
			return _height;
		}
		public function setHeight(value:Int):Int{
			if (value == _height) return;
			var resizeEvent:ResizeEvent = new ResizeEvent(ResizeEvent.RESIZE, _width, _height);
			_height = value;
			dispatchEvent(resizeEvent);
			updateCoordinates();
			return value;
		}
		
		var _zoomLevel:Int ;
		/*[Bindable(event="zoom")]*/
		public function getZoomLevel():Int{
			return _zoomLevel;
		}
		public function setZoomLevel(value:Int):Int{
			value = Math.min(Math.max(value, _minZoomLevel), _maxZoomLevel);
			if (value == _zoomLevel) return;
			var zoomEvent:ZoomEvent = new ZoomEvent(ZoomEvent.ZOOM, _zoomLevel);
			_zoomLevel = value;
			transformation = new MercatorTransformation(_tileSize, value);
			dispatchEvent(zoomEvent);
			updateCoordinates();
			return value;
		}
		
		public function getNode(id:String):Node {
			return cast( nodeMap.getElement(id), Node);
		}
		
		public function getWay(id:String):Way {
			return cast( wayMap.getElement(id), Way);
		}
		
		public function getRelation(id:String):Relation {
			return cast( relationMap.getElement(id), Relation);
		}
		
		public function getNodeWays(node:Node):Array<Dynamic> {
			var result:Array<Dynamic> = new Array();
			var ways:Array<Dynamic> = this.ways.source;
			for (way in ways) {
				var nodes:Array<Dynamic> = way.nodes.source;
				for (wayNode in nodes) {
					if (wayNode == node) {
						result.push(way);
						break;
					}
				}
			}
			return result;
		}
		
		public function getElementRelations(element:Element):Array<Dynamic> {
			var elementId:String = element.id;
			var result:Array<Dynamic> = new Array();
			var relations:Array<Dynamic> = this.relations.source;
			for (relation in relations) {
				var members:Array<Dynamic> = relation.members.source;
				for (member in members) {
					if (member.elementId == elementId) {
						result.push(relation);
						break;
					}
				}
			}
			return result;
		}
		
		public function setElements(nodes:Array<Dynamic>, ways:Array<Dynamic>, relations:Array<Dynamic>):Void {
			this.nodes.source = nodes;
			this.ways.source = ways;
			this.relations.source = relations;
			dispatchEvent(new MapEvent(MapEvent.ELEMENTS_CHANGE));
		}
		
		public function setSize(width:Int, height:Int):Void {
			if ((width == _width) && (height == _height)) return;
			var resizeEvent:ResizeEvent = new ResizeEvent(ResizeEvent.RESIZE, _width, _height);
			_width = width;
			_height = height;
			dispatchEvent(resizeEvent);
			updateCoordinates();
		}
		
		public function project(latLong:LatLong):Point {
			return transformation.transform(projection.project(latLong));
		}
		
		public function unproject(point:Point):LatLong {
			return projection.unproject(transformation.untransform(point));
		}
		
		public function enableBoundsUpdate():Void {
			boundsUpdateEnabled = true;
			if (coordinatesChanged) {
				coordinatesChanged = false;
				updateBounds();
			}
		}
		public function disableBoundsUpdate():Void {
			boundsUpdateEnabled = false;
		}
		
		public function readExternal(input:IDataInput):Void {
			_tileSize = input.readDouble();
			_minZoomLevel = input.readInt();
			_maxZoomLevel = input.readInt();
			_zoomLevel = input.readInt();
			_center = input.readObject();
			_width = input.readDouble();
			_height = input.readDouble();
			_nodes.source = input.readObject();
			_ways.source = input.readObject();
			_relations.source = input.readObject();
			transformation = new MercatorTransformation(_tileSize, _zoomLevel);
			updateCoordinates();
		}
		
		public function writeExternal(output:IDataOutput):Void {
			output.writeDouble(_tileSize);
			output.writeInt(_minZoomLevel);
			output.writeInt(_maxZoomLevel);
			output.writeInt(_zoomLevel);
			output.writeObject(_center);
			output.writeDouble(_width);
			output.writeDouble(_height);
			output.writeObject(_nodes.source);
			output.writeObject(_ways.source);
			output.writeObject(_relations.source);
		}
		
		function updateCoordinates():Void {
			var centerPoint:Point = project(_center);
			_centerX = Math.round(centerPoint.x);
			_centerY = Math.round(centerPoint.y);
			_x = Math.round(_centerX - _width / 2);
			_y = Math.round(_centerY - _height / 2);
			dispatchEvent(new MapEvent(MapEvent.COORDINATES_CHANGE));
			if (boundsUpdateEnabled) {
				updateBounds();
			} else {
				coordinatesChanged = true;
			}
		}
		
		function updateBounds():Void {
			var halfWidth:Int = _width / 2;
			var halfHeight:Int = _height / 2;
			var tlLatLong:LatLong = unproject(new Point(_x, _y));
			var trLatLong:LatLong = unproject(new Point(_centerX + halfWidth, _centerY - halfHeight));
			var brLatLong:LatLong = unproject(new Point(_centerX + halfWidth, _centerY + halfHeight));
			var blLatLong:LatLong = unproject(new Point(_centerX - halfWidth, _centerY + halfHeight));
			var westLong:Int = Math.min(tlLatLong.long, blLatLong.long);
			var southLat:Int = Math.min(blLatLong.lat, brLatLong.lat);
			var eastLong:Int = Math.max(trLatLong.long, brLatLong.long);
			var northLat:Int = Math.max(tlLatLong.lat, trLatLong.lat);
			var changeEvent:BoundsChangeEvent = new BoundsChangeEvent(BoundsChangeEvent.BOUNDS_CHANGE, _bounds);
			_bounds = new LatLongBounds(westLong, southLat, eastLong, northLat);
			dispatchEvent(changeEvent);
		}
	}
