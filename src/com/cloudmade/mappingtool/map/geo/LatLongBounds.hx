package com.cloudmade.mappingtool.map.geo;

	class LatLongBounds
	 {
		
		public var eastLong(getEastLong, null) : Int ;
		public var northLat(getNorthLat, null) : Int ;
		public var southLat(getSouthLat, null) : Int ;
		public var westLong(getWestLong, null) : Int ;
		public function new(westLong:Int, southLat:Int, eastLong:Int, northLat:Int) {
			_westLong = westLong;
			_southLat = southLat;
			_eastLong = eastLong;
			_northLat = northLat;
		}
		
		var _westLong:Int;
		public function getWestLong():Int {
			return _westLong;
		}
		
		var _southLat:Int;
		public function getSouthLat():Int {
			return _southLat;
		}
		
		var _eastLong:Int;
		public function getEastLong():Int {
			return _eastLong;
		}
		
		var _northLat:Int;
		public function getNorthLat():Int {
			return _northLat;
		}
		
		public function equals(other:LatLongBounds):Bool {
			return (_westLong == other._westLong) && (_southLat == other._southLat) && (_eastLong == other._eastLong) && (_northLat == other._northLat);
		}
		
		public function clone():LatLongBounds {
			return new LatLongBounds(_westLong, _southLat, _eastLong, _northLat);
		}
	}
