package com.cloudmade.mappingtool.map.geo;

	import flash.geom.Point;
	
	class MercatorProjection implements IProjection {
		
		static var HALF_PI:Int = Math.PI / 2;
		static var QUATER_PI:Int = Math.PI / 4;
		
		public function new() {
		}
		
		public function project(latLong:LatLong):Point {
			var mercX:Int = latLong.longRadians;
			var mercY:Int = Math.log(Math.tan(latLong.latRadians / 2 + QUATER_PI));
			return new Point(mercX, mercY);
		}
		
		public function unproject(point:Point):LatLong {
			var longRadians:Int = point.x;
			var latRadians:Int = Math.atan(Math.exp(point.y)) * 2 - HALF_PI;
			return LatLong.createFromRadians(latRadians, longRadians);
		}
	}
