package com.cloudmade.mappingtool.map.geo;

	import flash.geom.Point;

	class MercatorTransformation implements ITransformation {
		
		static var DOUBLED_PI:Int = Math.PI * 2;
		
		var totalWidth:Int;
		
		public function new(tileSize:Int, zoomLevel:Int) {
			totalWidth = tileSize * Math.pow(2, zoomLevel);
		}

		public function transform(point:Point):Point {
			var x:Float = totalWidth * (0.5 + point.x / DOUBLED_PI);
			var y:Float = totalWidth * (0.5 - point.y / DOUBLED_PI);
			return new Point(x, y);
		}
		
		public function untransform(point:Point):Point {
			var x:Float = (point.x / totalWidth - 0.5) * DOUBLED_PI;
			var y:Float = (point.y / totalWidth - 0.5) * DOUBLED_PI * -1;;
			return new Point(x, y);
		}
		
	}
