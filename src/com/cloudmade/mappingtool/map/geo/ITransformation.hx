package com.cloudmade.mappingtool.map.geo;

	import flash.geom.Point;
	
	interface ITransformation
	{
		function transform(point:Point):Point;
		
		function untransform(point:Point):Point;
	}
