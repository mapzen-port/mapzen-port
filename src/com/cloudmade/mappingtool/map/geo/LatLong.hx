package com.cloudmade.mappingtool.map.geo;

	import flash.utils.IDataInput;
	import flash.utils.IDataOutput;
	import flash.utils.IExternalizable;
	
	/*[RemoteClass(alias="com.cloudmade.mappingtool.map.geo.LatLong")]*/
	
	class LatLong implements IExternalizable {
		
		public var lat(getLat, null) : Int ;
		public var latRadians(getLatRadians, null) : Int ;
		public var long(getLong, null) : Int ;
		public var longRadians(getLongRadians, null) : Int ;
		static var RADIANS_FACTOR:Int = Math.PI / 180;
		static var DEGREES_FACTOR:Int = 180 / Math.PI;
		
		public static function createFromRadians(latRadians:Int, longRadians:Int):LatLong {
			return new LatLong(latRadians * DEGREES_FACTOR, longRadians * DEGREES_FACTOR);
		}
		
		public function new(?lat:Int = 0, ?long:Int = 0) {
			_lat = lat;
			_long = long;
		}
		
		var _lat:Int;
		public function getLat():Int {
			return _lat;
		}
		
		var _long:Int;
		public function getLong():Int {
			return _long;
		}
		
		public function getLatRadians():Int {
			return _lat * RADIANS_FACTOR;
		}
		
		public function getLongRadians():Int {
			return _long * RADIANS_FACTOR;
		}
		
		public function equals(other:LatLong):Bool {
			return (_lat == other._lat) && (_long == other._long);
		}
		
		public function clone():LatLong {
			return new LatLong(_lat, _long);
		}
		
		public function offset(dLat:Int, dLong:Int):LatLong {
			return new LatLong(_lat + dLat, _long + dLong);
		}
		
		public function normalize(maxLat:Int):LatLong {
			var newLat:Int = Math.min(Math.max(_lat, -maxLat), maxLat);
			//var newLong:Number = (_long + 180) % 360 + (_long < -180 ? 180 : -180);
			return new LatLong(newLat, _long);
		}
		
		public function readExternal(input:IDataInput):Void {
			_lat = input.readDouble();
			_long = input.readDouble();
		}
		
		public function writeExternal(output:IDataOutput):Void {
			output.writeDouble(_lat);
			output.writeDouble(_long);
		}
	}
