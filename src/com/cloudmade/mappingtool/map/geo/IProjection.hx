package com.cloudmade.mappingtool.map.geo;

	import flash.geom.Point;
	
	interface IProjection
	{
		function project(latLong:LatLong):Point;
		
		function unproject(point:Point):LatLong;
	}
