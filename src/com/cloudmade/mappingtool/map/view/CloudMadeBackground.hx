package com.cloudmade.mappingtool.map.view;

	import com.cloudmade.mappingtool.map.events.LatLongChangeEvent;
	import com.cloudmade.mappingtool.map.events.ResizeEvent;
	import com.cloudmade.mappingtool.map.events.ZoomEvent;
	import com.cloudmade.mappingtool.map.model.Map;
	
	import flash.geom.Rectangle;
	
	import mx.controls.SWFLoader;
	import mx.core.UIComponent;
	import mx.effects.Fade;
	import mx.events.EffectEvent;

	class CloudMadeBackground extends UIComponent, implements IMapView {
		
		public var map(getMap, setMap) : Map;
		static var TILE_SIZE:Int = 256;
		
		var tileMap:Dynamic;
		var oldMapX:Int;
		var oldMapY:Int;
		var initialX:Int;
		var initialY:Int;
		var contentPane:UIComponent;
		var needReset:Bool ;
		
		public function new() {
			
			needReset = false;
			super();
		}
		
		var _map:Map;
		public function getMap():Map{
			return _map;
		}
		public function setMap(value:Map):Map{
			if (value == _map) return;
			if (_map != null) {
				_map.removeEventListener(ZoomEvent.ZOOM, map_zoomHandler);
				_map.removeEventListener(ResizeEvent.RESIZE, map_resizeHandler);
				_map.removeEventListener(LatLongChangeEvent.CENTER_CHANGE, map_centerChangeHandler);
			}
			_map = value;
			needReset = true;
			if (_map != null) {
				_map.addEventListener(ZoomEvent.ZOOM, map_zoomHandler, false, 0, true);
				_map.addEventListener(ResizeEvent.RESIZE, map_resizeHandler, false, 0, true);
				_map.addEventListener(LatLongChangeEvent.CENTER_CHANGE, map_centerChangeHandler, false, 0, true);
				setActualSize(_map.width, _map.height);
			} else {
				invalidateDisplayList();
			}
			return value;
		}
		
		override function createChildren():Void {
			super.createChildren();
			contentPane = new UIComponent();
			addChild(contentPane);
		}
		
		override function updateDisplayList(unscaledWidth:Int, unscaledHeight:Int):Void {
			super.updateDisplayList(unscaledWidth, unscaledHeight);
			if (needReset) {
				needReset = false;
				for (var tileId:String in tileMap) {
					var tile:SWFLoader = cast( contentPane.removeChild(tileMap[tileId]), SWFLoader);
					var localX:Int = tile.x - oldMapX + initialX;
					var localY:Int = tile.y - oldMapY + initialY;
					if ((localX > -TILE_SIZE) && (localX < unscaledWidth) && (localY > -TILE_SIZE) && (localY < unscaledHeight)) {
						var fade:Fade = new Fade();
						fade.alphaFrom = 1;
						fade.alphaTo = 0;
						fade.addEventListener(EffectEvent.EFFECT_END, tileFade_effectEndHandler);
						tile.setStyle("addedEffect", fade);
						tile.move(localX, localY);
						addChild(tile);
					}
				}
				tileMap = new Object();
				if (map) {
					initialX = map.x;
					initialY = map.y;
				}
			}
			if (map) {
				var startColIndex:Int = Math.floor(map.x / TILE_SIZE);
				var startRowIndex:Int = Math.floor(map.y / TILE_SIZE);
				var colNum:Int = Math.ceil((map.x + map.width) / TILE_SIZE);
				var rowNum:Int = Math.ceil((map.y + map.height) / TILE_SIZE);
				var i:Int = startColIndex;
				while (i < colNum) {
					var j:Int = startRowIndex;
					while (j < rowNum) {
						createTileRenderer(i, j);
						j++;
					}
					i++;
				}
				contentPane.scrollRect = new Rectangle(map.x - initialX, map.y - initialY, map.width, map.height);
				scrollRect = new Rectangle(0, 0, unscaledWidth, unscaledHeight);
				oldMapX = map.x;
				oldMapY = map.y;
			}
		}
		
		function createTileRenderer(colIndex:Int, rowIndex:Int):Void {
			var tileId:String = colIndex + " " + rowIndex;
			if (!(tileId in tileMap)) {
				var loader:SWFLoader = new SWFLoader();
				loader.setStyle("brokenImageSkin", null);
				loader.setStyle("brokenImageBorderSkin", null);
				loader.setStyle("completeEffect", new Fade());
				loader.setActualSize(TILE_SIZE, TILE_SIZE);
				loader.move(colIndex * TILE_SIZE - initialX, rowIndex * TILE_SIZE - initialY);
				loader.source = formTileUrl(colIndex, rowIndex);
				tileMap[tileId] = loader;
				contentPane.addChild(loader);
			}
		}
		
		function formTileUrl(colIndex:Int, rowIndex:Int):String {
			var limit:Int = Math.pow(2, map.zoomLevel);
			if ((rowIndex >= 0) && (rowIndex < limit)) {
				colIndex = (colIndex % limit + limit) % limit;
				var subdomains:Array<Dynamic> = ["a", "b", "c"];
				var subdomain:String = subdomains[(colIndex + rowIndex) % subdomains.length];
				return "http://" + subdomain + ".tile.alpha.cloudmade.com/d4cdaedf0e1b5753ae3cdec58e77f571/6618/256/" + map.zoomLevel + "/" + colIndex + "/" + rowIndex + ".png";
			}
			return "http://cloudmade.com/js-api/images/empty-tile.png";
		}
		
		function map_zoomHandler(event:ZoomEvent):Void {
			needReset = true;
			invalidateDisplayList();
		}
		
		function map_resizeHandler(event:ResizeEvent):Void {
			setActualSize(map.width, map.height);
		}
		
		function map_centerChangeHandler(event:LatLongChangeEvent):Void {
			invalidateDisplayList();
		}
		
		function tileFade_effectEndHandler(event:EffectEvent):Void {
			var fade:Fade = cast( event.target, Fade);
			fade.removeEventListener(EffectEvent.EFFECT_END, tileFade_effectEndHandler);
			var tile:SWFLoader = cast( fade.target, SWFLoader);
			removeChild(tile);
		}
	}
