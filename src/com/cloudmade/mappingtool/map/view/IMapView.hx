package com.cloudmade.mappingtool.map.view;

	import com.cloudmade.mappingtool.map.model.Map;
	
	import flash.events.IEventDispatcher;
	
	interface IMapView implements IEventDispatcher{
		function map():Map;
		
		function map(value:Map):Void;
	}
