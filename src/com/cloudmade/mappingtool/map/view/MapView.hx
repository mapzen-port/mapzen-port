package com.cloudmade.mappingtool.map.view;

	import com.cloudmade.mappingtool.config.IElementImage;
	import com.cloudmade.mappingtool.config.IElementTypeFinder;
	import com.cloudmade.mappingtool.map.events.LatLongChangeEvent;
	import com.cloudmade.mappingtool.map.events.ResizeEvent;
	import com.cloudmade.mappingtool.map.events.ZoomEvent;
	import com.cloudmade.mappingtool.map.geo.LatLong;
	import com.cloudmade.mappingtool.map.model.Element;
	import com.cloudmade.mappingtool.map.model.Map;
	import com.cloudmade.mappingtool.map.model.Node;
	import com.cloudmade.mappingtool.map.model.Way;
	import com.cloudmade.mappingtool.utils.GeometryUtils;
	
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.display.InteractiveObject;
	import flash.display.Sprite;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	import mx.controls.Alert;
	import mx.core.IDataRenderer;
	import mx.core.IFactory;
	import mx.core.UIComponent;
	import mx.events.CollectionEvent;
	import mx.events.CollectionEventKind;
	import mx.events.FlexEvent;
	import mx.events.PropertyChangeEvent;
	
	// TODO: Use polymorphism.

	class MapView extends UIComponent, implements IMapView {
		
		public var areaRendererFinder(getAreaRendererFinder, setAreaRendererFinder) : IElementTypeFinder;
		public var map(getMap, setMap) : Map;
		public var nodeRendererFinder(getNodeRendererFinder, setNodeRendererFinder) : IElementTypeFinder;
		public var wayRendererFinder(getWayRendererFinder, setWayRendererFinder) : IElementTypeFinder;
		static var TAG_SEPARATOR:String = String.fromCharCode(0);
		
		public var initialX:Int;
		public var initialY:Int;
		
		var contentPane:Sprite;
		var hasMap:Bool ;
		var mapChanged:Bool ;
		var waysStorage:ElementsStorage ;
		var areasStorage:ElementsStorage ;
		var nodesStorage:ElementsStorage ;
		
		public function new() {
			
			hasMap = false;
			mapChanged = false;
			waysStorage = new ElementsStorage();
			areasStorage = new ElementsStorage();
			nodesStorage = new ElementsStorage();
			super();
			addEventListener(FlexEvent.SHOW, showHandler);
		}
		
		var _map:Map;
		public function getMap():Map{
			return _map;
		}
		public function setMap(value:Map):Map{
			if (value == _map) return;
			if (_map != null) {
				removeMapListeners();
			}
			_map = value;
			if (_map != null) {
				addMapListeners();
			}
			mapChanged = true;
			invalidateProperties();
			invalidateDisplayList();
			return value;
		}
		
		function findElementRenderer(finder:IElementTypeFinder, element:Element):IFactory {
			return cast( finder.findType(element.tags.source), IElementImage);
		}
		
		public function findNodeRenderer(node:Node):IFactory {
			return findElementRenderer(nodeRendererFinder, node);
		}
		
		var _nodeRendererFinder:IElementTypeFinder;
		public function getNodeRendererFinder():IElementTypeFinder{
			return _nodeRendererFinder;
		}
		public function setNodeRendererFinder(value:IElementTypeFinder):IElementTypeFinder{
			if (value == _nodeRendererFinder) return;
			_nodeRendererFinder = value;
			nodesStorage.needElementsReset = true;
			invalidateDisplayList();
			return value;
		}
		
		public function findWayRenderer(way:Way):IFactory {
			return findElementRenderer(wayRendererFinder, way);
		}
		
		var _wayRendererFinder:IElementTypeFinder;
		public function getWayRendererFinder():IElementTypeFinder{
			return _wayRendererFinder;
		}
		public function setWayRendererFinder(value:IElementTypeFinder):IElementTypeFinder{
			if (value == _wayRendererFinder) return;
			_wayRendererFinder = value;
			waysStorage.needElementsReset = true;
			invalidateDisplayList();
			return value;
		}
		
		public function findAreaRenderer(area:Way):IFactory {
			return findElementRenderer(areaRendererFinder, area);
		}
		
		var _areaRendererFinder:IElementTypeFinder;
		public function getAreaRendererFinder():IElementTypeFinder{
			return _areaRendererFinder;
		}
		public function setAreaRendererFinder(value:IElementTypeFinder):IElementTypeFinder{
			if (value == _areaRendererFinder) return;
			_areaRendererFinder = value;
			areasStorage.needElementsReset = true;
			invalidateDisplayList();
			return value;
		}
		
		public function getWayParts(wayId:String):Array<Dynamic> {
			var wayParts:Array<Dynamic> = new Array();
			var wayRenderers:Array<Dynamic> = waysStorage.elementMap[wayId];
			for (rendererInfo in wayRenderers) {
				wayParts.push(rendererInfo.renderer.data);
			}
			return wayParts;
		}
		
		public function getArea(areaId:String):Way {
			var rendererInfo:RendererInfo = areasStorage.elementMap[areaId];
			if (rendererInfo) {
				return cast( rendererInfo.renderer.data, Way);
			}
			return null;
		}
		
		override function createChildren():Void {
			super.createChildren();
			contentPane = new Sprite();
			addChild(contentPane);
		}
		
		override function commitProperties():Void {
			super.commitProperties();
			if (mapChanged) {
				mapChanged = false;
				hasMap = map != null;
				waysStorage.needElementsReset = true;
				nodesStorage.needElementsReset = true;
				if (hasMap) {
					setActualSize(map.width, map.height);
				}
			}
		}
		
		override function updateDisplayList(unscaledWidth:Int, unscaledHeight:Int):Void {
			super.updateDisplayList(unscaledWidth, unscaledHeight);
			
			if (nodesStorage.elementsChanged || waysStorage.elementsChanged || areasStorage.elementsChanged) {
				contentPane.cacheAsBitmap = false;
			}
			
			var bounds:Rectangle;
			if (hasMap) {
				bounds = new Rectangle(0, 0, unscaledWidth, unscaledHeight);
				if (nodesStorage.needElementsReset && waysStorage.needElementsReset) {
					initialX = map.x;
					initialY = map.y;
				}
			}
			
			var node:Node;
			var rendererInfo:RendererInfo;
			if (nodesStorage.needElementsReset) {
				for (rendererInfo in nodesStorage.elementMap) {
					removeElementRenderer(rendererInfo);
				}
				nodesStorage.reset();
				if (hasMap) {
					var nodes:Array<Dynamic> = map.nodes.source;
					for (node in nodes) {
						if (setupNodePosition(node, bounds)) {
							addNodeRenderer(node);
						}
					}
				}
			}
			
			if (nodesStorage.elementsRemoved) {
				nodesStorage.elementsRemoved = false;
				for (var nodeId:Dynamic in nodesStorage.removedElementMap) {
					rendererInfo = nodesStorage.elementMap[nodeId];
					if (rendererInfo) {
						removeElementRenderer(rendererInfo);
						delete nodesStorage.elementMap[nodeId];
					}
				}
				nodesStorage.removedElementMap = new Object();
			}
			
			if (nodesStorage.elementsAdded) {
				nodesStorage.elementsAdded = false;
				for (node in nodesStorage.addedElementMap) {
					if (setupNodePosition(node, bounds)) {
						addNodeRenderer(node);
					}
				}
				nodesStorage.addedElementMap = new Object();
			}
			
			var way:Way;
			var wayParts:Array<Dynamic>;
			if (waysStorage.needElementsReset) {
				for (wayParts in waysStorage.elementMap) {
					for (rendererInfo in wayParts) {
						removeElementRenderer(rendererInfo);
					}
				}
				waysStorage.reset();
				if (hasMap) {
					var ways:Array<Dynamic> = map.ways.source;
					for (way in ways) {
						divideWayIntoParts(way, bounds);
					}
				}
			}
			
			if (waysStorage.elementsRemoved) {
				waysStorage.elementsRemoved = false;
				for (var wayId:Dynamic in waysStorage.removedElementMap) {
					wayParts = waysStorage.elementMap[wayId];
					for (rendererInfo in wayParts) {
						removeElementRenderer(rendererInfo);
						delete waysStorage.elementMap[wayId];
					}
				}
				waysStorage.removedElementMap = new Object();
			}
			
			if (waysStorage.elementsAdded) {
				waysStorage.elementsAdded = false;
				for (way in waysStorage.addedElementMap) {
					divideWayIntoParts(way, bounds);
				}
				waysStorage.addedElementMap = new Object();
			}
			
			var area:Way;
			if (areasStorage.needElementsReset) {
				for (rendererInfo in areasStorage.elementMap) {
					removeElementRenderer(rendererInfo);
				}
				areasStorage.reset();
				if (hasMap) {
					var areas:Array<Dynamic> = map.ways.source;
					for (area in areas) {
						addArea(area, bounds);
					}
				}
			}
			
			if (areasStorage.elementsRemoved) {
				areasStorage.elementsRemoved = false;
				for (var areaId:Dynamic in areasStorage.removedElementMap) {
					rendererInfo = areasStorage.elementMap[areaId];
					if (rendererInfo) {
						removeElementRenderer(rendererInfo);
						delete areasStorage.elementMap[areaId];
					}
				}
				areasStorage.removedElementMap = new Object();
			}
			
			if (areasStorage.elementsAdded) {
				areasStorage.elementsAdded = false;
				for (area in areasStorage.addedElementMap) {
					addArea(area, bounds);
				}
				areasStorage.addedElementMap = new Object();
			}
			
			if (hasMap) {
				contentPane.scrollRect = new Rectangle(map.x - initialX, map.y - initialY, unscaledWidth, unscaledHeight);
				// Maybe it is better to cache elements as bitmap only when dragging map by watching Map boundsUpdateEnabled property?
				if (!contentPane.cacheAsBitmap) {
					contentPane.cacheAsBitmap = true;
				}
			}
		}
		
		function divideWayIntoParts(way:Way, bounds:Rectangle):Void {
			var nodes:Array<Dynamic> = way.nodes.source;
			var wayParts:Array<Dynamic> = new Array();
			var n:Int = nodes.length - 1;
			var wayPart:Way;
			var i:Int = 0;
			while (i < n) {
				var node1:Node = nodes[i];
				var node2:Node = nodes[++i];
				var point1:Point = new Point(node1.x, node1.y);
				var point2:Point = new Point(node2.x, node2.y);
				var originalPoint:Point = point2.clone();
				var clipResult:UInt = GeometryUtils.clipLineSegment(point1, point2, bounds);
				if ((clipResult != GeometryUtils.OUTLYING) && (clipResult != GeometryUtils.CONTACTING)) {
					var wayPosition:Point = point1;
					var wayNodes:Array<Dynamic> = [copyWayNode(node1, point1, wayPosition), copyWayNode(node2, point2, wayPosition)];
					if (point2.equals(originalPoint)) {
						while(i < n) {
							node1 = node2;
							node2 = nodes[++i];
							point1 = point2;
							point2 = new Point(node2.x, node2.y);
							clipResult = GeometryUtils.clipLineSegment(point1, point2, bounds);
							if (clipResult == GeometryUtils.INLYING) {
								wayNodes.push(copyWayNode(node2, point2, wayPosition));
							} else if (clipResult == GeometryUtils.INTERSECTING) {
								wayNodes.push(copyWayNode(node2, point2, wayPosition));
								break;
							} else {
								break;
							}
						}
					}
					// Update endings positions if clipped.
					normalizeNodePosition(wayNodes[0], wayPosition);
					normalizeNodePosition(wayNodes[wayNodes.length - 1], wayPosition);
					wayPart = new Way(way.id, way.tags.source, wayNodes);
					wayPart.move(wayPosition.x, wayPosition.y);
					wayParts.push(wayPart);
				}
			}
			if (wayParts.length > 0) {
				waysStorage.elementMap[way.id] = new Array();
				for (wayPart in wayParts) {
					addWayRenderer(wayPart);
				}
			}
		}
		
		function addArea(area:Way, bounds:Rectangle):Void {
			var nodes:Array<Dynamic> = area.nodes.source;
			if (nodes[0] != nodes[nodes.length - 1]) return;
			var points:Array<Dynamic> = area.getNodePoints();
			var clipResult:UInt = GeometryUtils.clipPolygon(points, bounds);
			if ((clipResult == GeometryUtils.INLYING) ||
				(clipResult == GeometryUtils.INTERSECTING)) {
				addAreaRenderer(copyArea(area, points));
			}
		}
		
		function addNodeRenderer(node:Node):Void {
			var elementImage:IElementImage = findElementImage(nodeRendererFinder, node);
			if (elementImage) {
				var renderer:DisplayObject = cast( addElementRenderer(elementImage, node), DisplayObject);
				renderer.x = Math.round(node.x - renderer.width / 2);
				renderer.y = Math.round(node.y - renderer.width / 2);
				nodesStorage.elementMap[node.id] = new RendererInfo(cast( renderer, IDataRenderer), elementImage);
			}
		}
		
		function addWayRenderer(way:Way):Void {
			var elementImage:IElementImage = findElementImage(wayRendererFinder, way);
			if (elementImage) {
				var renderer:IDataRenderer = addBaseWayRenderer(elementImage, way);
				waysStorage.elementMap[way.id].push(new RendererInfo(renderer, elementImage));
			}
		}
		
		function addAreaRenderer(area:Way):Void {
			var elementImage:IElementImage = findElementImage(areaRendererFinder, area);
			if (elementImage) {
				var renderer:IDataRenderer = addBaseWayRenderer(elementImage, area);
				areasStorage.elementMap[area.id] = new RendererInfo(renderer, elementImage);
			}
		}
		
		function addBaseWayRenderer(elementImage:IElementImage, way:Way):IDataRenderer {
			var renderer:DisplayObject = cast( addElementRenderer(elementImage, way), DisplayObject);
			renderer.x = Math.round(way.x);
			renderer.y = Math.round(way.y);
			return cast( renderer, IDataRenderer);
		}
		
		function removeElementRenderer(rendererInfo:RendererInfo):Void {
			var renderer:DisplayObject = cast( rendererInfo.renderer, DisplayObject);
			renderer.parent.removeChild(renderer);
		}
		
		function findElementImage(rendererFinder:IElementTypeFinder, element:Element):IElementImage {
			var tags:Array<Dynamic> = element.tags.source;
			return cast( rendererFinder.findType(tags), IElementImage);
		}
		
		function addElementRenderer(elementImage:IElementImage, element:Element):IDataRenderer {
			var renderer:InteractiveObject = elementImage.newInstance();
			if (Std.is( renderer, DisplayObjectContainer)) {
				DisplayObjectContainer(renderer).mouseChildren = false;
			}
			InteractiveObject(renderer).doubleClickEnabled = true;
			IDataRenderer(renderer).data = element;
			
			while (elementImage.layerIndex > (contentPane.numChildren - 1)) {
				contentPane.addChild(new Sprite());
			}
			var container:Sprite = cast( contentPane.getChildAt(elementImage.layerIndex), Sprite);
			container.addChild(renderer);
			
			return cast( renderer, IDataRenderer);
		}
		
		function setupNodePosition(node:Node, bounds:Rectangle):Bool {
			var point:Point = map.project(node.position);
			point.offset(-initialX, -initialY);
			node.move(point.x, point.y);
			return bounds.containsPoint(point);
		}
		
		function copyWayNode(originalNode:Node, nodePosition:Point, wayPosition:Point):Node {
			var node:Node = originalNode.clone();
			node.move(nodePosition.x - wayPosition.x, nodePosition.y - wayPosition.y);
			return node;
		}
		
		function copyArea(area:Way, points:Array<Dynamic>):Way {
			var areaPoint:Point = Point(points[0]).clone();
			var point:Point;
			for (point in points) {
				point.offset(-areaPoint.x, -areaPoint.y);
			}
			var nodes:Array<Dynamic> = new Array();
			for (point in points) {
				var node:Node = new Node(null, null, null);
				node.move(point.x, point.y);
				nodes.push(node);
			}
			point = areaPoint.add(new Point(initialX, initialY));
			var firstNodePosition:LatLong = map.unproject(point);
			nodes[0] = new Node(null, null, firstNodePosition);
			var newArea:Way = new Way(area.id, area.tags.source, nodes);
			newArea.move(areaPoint.x, areaPoint.y);
			return newArea;
		}
		
		function normalizeNodePosition(node:Node, wayPosition:Point):Void {
			var point:Point = new Point(node.x, node.y);
			point.offset(wayPosition.x, wayPosition.y);
			var originalNode:Node = map.getNode(node.id);
			if (originalNode) {
				var originalPoint:Point = new Point(originalNode.x, originalNode.y);
				// Check with original node for avoiding rounding errors.
				if (!point.equals(originalPoint)) {
					node.position = map.unproject(new Point(point.x + initialX, point.y + initialY));
				}
			} else {
				// TODO: Handle this situation explicitly.
				CONFIG::debug {
					Alert.show("Seems that way nodes are not synchronized in the map.");
				}
			}
		}
		
		function addMapListeners():Void {
			_map.addEventListener(ZoomEvent.ZOOM, map_zoomHandler, false, 0, true);
			_map.addEventListener(ResizeEvent.RESIZE, map_resizeHandler, false, 0, true);
			_map.addEventListener(LatLongChangeEvent.CENTER_CHANGE, map_centerChangeHandler, false, 0, true);
			_map.ways.addEventListener(CollectionEvent.COLLECTION_CHANGE, mapWays_collectionChangeHandler, false, 0, true);
			_map.nodes.addEventListener(CollectionEvent.COLLECTION_CHANGE, mapNodes_collectionChangeHandler, false, 0, true);
		}
		
		function removeMapListeners():Void {
			_map.removeEventListener(ZoomEvent.ZOOM, map_zoomHandler);
			_map.removeEventListener(ResizeEvent.RESIZE, map_resizeHandler);
			_map.removeEventListener(LatLongChangeEvent.CENTER_CHANGE, map_centerChangeHandler);
			_map.ways.removeEventListener(CollectionEvent.COLLECTION_CHANGE, mapWays_collectionChangeHandler);
			_map.nodes.removeEventListener(CollectionEvent.COLLECTION_CHANGE, mapNodes_collectionChangeHandler);
		}
		
		// TODO: Check if it is applicable in normal mode.
		function map_zoomHandler(event:ZoomEvent):Void {
			CONFIG::restore {
				nodesStorage.needElementsReset = true;
				waysStorage.needElementsReset = true;
				areasStorage.needElementsReset = true;
				invalidateDisplayList();
			}
		}
		
		function map_resizeHandler(event:ResizeEvent):Void {
			setActualSize(map.width, map.height);
		}
		
		function map_centerChangeHandler(event:LatLongChangeEvent):Void {
			invalidateDisplayList();
		}
		
		function mapNodes_collectionChangeHandler(event:CollectionEvent):Void {
			elements_changeHandler(event, nodesStorage);
		}
		
		function mapWays_collectionChangeHandler(event:CollectionEvent):Void {
			elements_changeHandler(event, waysStorage);
			elements_changeHandler(event, areasStorage);
		}
		
		function elements_changeHandler(event:CollectionEvent, storage:ElementsStorage):Void {
			var element:Element;
			if (event.kind == CollectionEventKind.ADD) {
				for (element in event.items) {
					storage.addedElementMap[element.id] = element;
				}
				storage.elementsAdded = true;
			} else if (event.kind == CollectionEventKind.REMOVE) {
				for (element in event.items) {
					storage.removedElementMap[element.id] = element;
					delete storage.addedElementMap[element.id];
				}
				storage.elementsRemoved = true;
			} else if (event.kind == CollectionEventKind.REPLACE) {
				for (updateInfo in event.items) {
					storage.removedElementMap[updateInfo.oldValue.id] = updateInfo.oldValue;
					storage.addedElementMap[updateInfo.newValue.id] = updateInfo.newValue;
				}
				storage.elementsAdded = true;
				storage.elementsRemoved = true;
			} else if (event.kind == CollectionEventKind.RESET) {
				storage.needElementsReset = true;
			}
			invalidateDisplayList();
		}
		
		function showHandler(event:FlexEvent):Void {
			// Force update.
			contentPane.scrollRect = contentPane.scrollRect;
		}
	}
