package com.cloudmade.mappingtool.map.view;

	import com.cloudmade.mappingtool.map.events.LatLongChangeEvent;
	import com.cloudmade.mappingtool.map.events.ResizeEvent;
	import com.cloudmade.mappingtool.map.events.ZoomEvent;
	import com.cloudmade.mappingtool.map.model.Map;
	import com.yahoo.maps.api.MapTypes;
	import com.yahoo.maps.api.YahooMap;
	import com.yahoo.maps.api.YahooMapEvent;
	import com.yahoo.maps.api.core.location.LatLon;
	
	import flash.geom.Rectangle;
	
	import mx.core.UIComponent;
	
	class YahooBackground extends UIComponent, implements IMapView {
		
		public var map(getMap, setMap) : Map;
		static var INIT_WIDTH:Int = 800;
		static var INIT_HEIGHT:Int = 600;
		
		var yahooMap:YahooMap;
		var mapChanged:Bool ;
		var sizeChanged:Bool ;
		var zoomChanged:Bool ;
		var lastZoomLevel:Bool ;
		var centerChanged:Bool ;
		
		public function new() {
			
			mapChanged = false;
			sizeChanged = false;
			zoomChanged = false;
			lastZoomLevel = false;
			centerChanged = false;
			super();
		}
		
		var _map:Map;
		public function getMap():Map{
			return _map;
		}
		public function setMap(value:Map):Map{
			if (value == _map) return;
			if (_map != null) {
				removeMapListeners();
			}
			_map = value;
			mapChanged = true;
			if (_map != null) {
				addMapListeners();
			}
			invalidateProperties();
			return value;
		}
		
		override function createChildren():Void {
			super.createChildren();
			yahooMap = new YahooMap();
			yahooMap.addEventListener(YahooMapEvent.MAP_INITIALIZE, yahooMap_mapInitializeHandler);
			yahooMap.init("9kPdThDV34ElG7TS98j_8NBxWc6I7HUF.lQ9dI7zgpwGAgWMXw_L5EDVpG4cGUpjveuf2A--", INIT_WIDTH, INIT_HEIGHT);
			addChild(yahooMap);
		}
		
		override function commitProperties():Void {
			super.commitProperties();
			if (!yahooMap.initialized) return;
			if (mapChanged) {
				mapChanged = false;
				var hasMap:Bool = map != null;
				zoomChanged = hasMap;
				sizeChanged = hasMap;
				centerChanged = hasMap;
			}
			if (sizeChanged) {
				sizeChanged = false;
				if (lastZoomLevel) {
					// Recalculate map position.
					zoomChanged = true;
				}
				yahooMap.setSize(map.width, map.height);
				scrollRect = new Rectangle(0, 0, map.width, map.height);
			}
			if (zoomChanged) {
				zoomChanged = false;
				var zoomLevel:Int = map.maxZoomLevel - map.zoomLevel;
				var deltaZoomLevel:Int = yahooMap.config.minZoom - zoomLevel;
				if (deltaZoomLevel <= 0) {
					if (lastZoomLevel) {
						lastZoomLevel = false;
						yahooMap.scaleX = 1;
						yahooMap.scaleY = 1;
						yahooMap.x = 0;
						yahooMap.y = 0;
					}
					yahooMap.zoomLevel = zoomLevel;
				} else {
					if (!lastZoomLevel) {
						lastZoomLevel = true;
						yahooMap.zoomLevel = yahooMap.config.minZoom;
					}
					var scale:Int = Math.pow(2, deltaZoomLevel)
					yahooMap.scaleX = scale;
					yahooMap.scaleY = scale;
					yahooMap.x = Math.round(-map.width / scale);
					yahooMap.y = Math.round(-map.height / scale);
				}
			}
			if (centerChanged) {
				centerChanged = false;
				yahooMap.centerLatLon = new LatLon(map.center.lat, map.center.long);
			}
		}
		
		function addMapListeners():Void {
			map.addEventListener(ZoomEvent.ZOOM, map_zoomHandler, false, 0, true);
			map.addEventListener(ResizeEvent.RESIZE, map_resizeHandler, false, 0, true);
			map.addEventListener(LatLongChangeEvent.CENTER_CHANGE, map_centerChangeHandler, false, 0, true);
		}
		
		function removeMapListeners():Void {
			map.removeEventListener(ZoomEvent.ZOOM, map_zoomHandler);
			map.removeEventListener(ResizeEvent.RESIZE, map_resizeHandler);
			map.removeEventListener(LatLongChangeEvent.CENTER_CHANGE, map_centerChangeHandler);
		}
		
		function yahooMap_mapInitializeHandler(event:YahooMapEvent):Void {
			yahooMap.mapType = MapTypes.SATELLITE;
			invalidateProperties();
		}
		
		function map_zoomHandler(event:ZoomEvent):Void {
			zoomChanged = true;
			invalidateProperties();
		}
		
		function map_resizeHandler(event:ResizeEvent):Void {
			sizeChanged = true;
			invalidateProperties();
		}
		
		function map_centerChangeHandler(event:LatLongChangeEvent):Void {
			centerChanged = true;
			invalidateProperties();
		}
	}
