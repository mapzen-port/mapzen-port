package com.cloudmade.mappingtool.map.view;

	import com.cloudmade.mappingtool.map.model.Map;
	
	import mx.core.UIComponent;

	class NullBackground extends UIComponent, implements IMapView {
		
		public var map(getMap, setMap) : Map;
		public function new()
		{
			super();
		}
		
		var _map:Map;
		public function getMap():Map{
			return _map;
		}
		
		public function setMap(value:Map):Map{
			_map = value;
			return value;
		}
	}
