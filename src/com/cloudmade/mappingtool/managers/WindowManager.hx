package com.cloudmade.mappingtool.managers;

	import flash.events.EventDispatcher;
	
	import mx.core.Application;
	import mx.core.IFlexDisplayObject;
	import mx.managers.PopUpManager;

	class WindowManager extends EventDispatcher {
		
		public var numWindows(getNumWindows, null) : Int ;
		static var instance:WindowManager = null;
		
		public static function getInstance():WindowManager {
			if (instance == null) {
				instance = new WindowManager(new SingletonEnforcer());
			}
			return instance;
		}
		
		public function new(enforcer:SingletonEnforcer) {
			super();
		}
		
		var _numWindows:Int ;
		public function getNumWindows():Int {
			return _numWindows;
		}
		
		public function openWindow(window:IFlexDisplayObject, ?modal:Bool = false):Void {
			if (window.parent == null) {
				var app:Application = cast( Application.application, Application);
				PopUpManager.addPopUp(window, app, modal);
				centerWindow(window);
				_numWindows++;
			}
		}
		
		public function closeWindow(window:IFlexDisplayObject):Void {
			PopUpManager.removePopUp(window);
			_numWindows--;
		}
		
		public function centerWindow(window:IFlexDisplayObject):Void {
			PopUpManager.centerPopUp(window);
		}
	}
