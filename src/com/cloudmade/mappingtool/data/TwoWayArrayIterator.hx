/**
 * A two-way iterator for array.
 */
package com.cloudmade.mappingtool.data;

	class TwoWayArrayIterator extends ArrayIterator, implements ITwoWayIterator {
		
		public function new(source:Array<Dynamic>, ?index:Int = 0) {
			super(source, index);
		}
		
		public function getPrevious():Dynamic {
			return source[index - 1];
		}
		
		public function hasPrevious():Bool {
			return index > 0;
		}
		
		public function moveBackward():Void {
			index--;
		}
	}
