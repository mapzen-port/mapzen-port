/**
 * A two directional stack.
 */	
package com.cloudmade.mappingtool.data;

	interface IStack implements ITwoWayIterator, implements IIteratable{
		function insertAfter(item:Dynamic):Void;
		
		function insertBefore(item:Dynamic):Void;
		
		function removeNext():Void;
		
		function removePrevious():Void;
		
		function clear():Void;
		
		function getNextIterator():IIterator;
		
		function getPreviousIterator():IIterator;
	}
