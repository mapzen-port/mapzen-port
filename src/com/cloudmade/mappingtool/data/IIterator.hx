/**
 * Iterator to go over a list of items in one direction.
 * 
 * @see com.cloudmade.mappingtool.data.IIteratable
 */
package com.cloudmade.mappingtool.data;
	
	interface IIterator
	{
		function getNext():Dynamic;
		
		function hasNext():Bool;
		
		function moveForward():Void;
	}
