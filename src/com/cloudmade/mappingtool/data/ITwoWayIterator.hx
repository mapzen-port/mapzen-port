/**
 * Iterator to go over a list of items in two directions.
 * 
 * @see com.cloudmade.mappingtool.data.ITwoWayIteratable
 */
package com.cloudmade.mappingtool.data;

	interface ITwoWayIterator implements IIterator{
		function getPrevious():Dynamic;
		
		function hasPrevious():Bool;
		
		function moveBackward():Void;
	}
