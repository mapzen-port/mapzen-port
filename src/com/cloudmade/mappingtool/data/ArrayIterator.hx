/**
 * Iterator for array.
 */
package com.cloudmade.mappingtool.data;

	class ArrayIterator implements IIterator {
		
		var source:Array<Dynamic>;
		var index:Int;
		
		public function new(source:Array<Dynamic>, ?index:Int = 0) {
			this.source = source;
			this.index = index;
		}
		
		public function getNext():Dynamic {
			return source[index];
		}
		
		public function hasNext():Bool {
			return index < source.length;
		}
		
		public function moveForward():Void {
			index++;
		}
	}
