/**
 * Structure that can be exposed in two directions.
 * 
 * @see com.cloudmade.mappingtool.data.ITwoWayIterator
 */
package com.cloudmade.mappingtool.data;

	interface ITwoWayIteratable
	{
		function getTwoWayIterator():ITwoWayIterator;
	}
