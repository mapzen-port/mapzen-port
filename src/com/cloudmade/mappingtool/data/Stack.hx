/**
 * A two directional stack.
 */
package com.cloudmade.mappingtool.data;

	class Stack extends TwoWayArrayIterator, implements IStack {
		
		public function new(source:Array<Dynamic>, ?index:Int = 0) {
			super(source, index);
		}
		
		public function insertAfter(item:Dynamic):Void {
			source.splice(index, 0, item);
		}
		
		public function insertBefore(item:Dynamic):Void {
			insertAfter(item);
			moveForward();
		}
		
		public function removeNext():Void {
			source.splice(index, 1);
		}
		
		public function removePrevious():Void {
			moveBackward();
			removeNext();
		}
		
		public function clear():Void {
			source = new Array();
			index = 0;
		}
		
		public function getIterator():IIterator {
			return new ArrayIterator(source);
		}
		
		public function getNextIterator():IIterator {
			return new ArrayIterator(source, index);
		}
		
		public function getPreviousIterator():IIterator {
			return new ArrayIterator(source.slice(0, index));
		}
	}
