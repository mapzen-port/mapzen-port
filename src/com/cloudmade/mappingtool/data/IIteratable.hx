/**
 * Structure that can be exposed in one direction.
 * 
 * @see com.cloudmade.mappingtool.data.IIterator
 */
package com.cloudmade.mappingtool.data;

	interface IIteratable
	{
		function getIterator():IIterator;
	}
